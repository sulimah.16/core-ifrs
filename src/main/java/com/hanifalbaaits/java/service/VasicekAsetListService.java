package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.VasicekAsetList;
import com.hanifalbaaits.java.repo.VasicekAsetListRepo;

@Service
public class VasicekAsetListService {
	
	@Autowired
	VasicekAsetListRepo repo;
	
	public List<VasicekAsetList> findAll() {
		return repo.findAll();
	}
	
	public VasicekAsetList findById(int id) {
		return repo.findById(id);
	}
	
	public List<VasicekAsetList> findByIdDraft(int id) {
		return repo.findByIdDraft(id);
	}
	
	public void save(VasicekAsetList role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
	
	public void deleteDataInputByDraft(int id) {
		this.repo.deleteDataInputByDraft(id);
	}
}
