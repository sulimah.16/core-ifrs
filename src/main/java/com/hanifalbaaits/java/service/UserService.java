package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.repo.UserRepo;

@Service
public class UserService {
	
	@Autowired
	UserRepo repo;
	
	public List<User> findAll() {
		return repo.findAll();
	}
	
	public User findById(int id) {
		return repo.findById(id);
	}
	
	public User findByUsername(String username) {
		return repo.findByUsername(username);
	}
	
	public void save(User user) {
		this.repo.save(user);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
}
