package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.UserRole;
import com.hanifalbaaits.java.repo.UserRoleRepo;

@Service
public class UserRoleService {
	
	@Autowired
	UserRoleRepo repo;
	
	public List<UserRole> findAll() {
		return repo.findAll();
	}
	
	public UserRole findById(int id) {
		return repo.findById(id);
	}

	public void save(UserRole role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
}
