package com.hanifalbaaits.java.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.VasicekGDP;
import com.hanifalbaaits.java.repo.VasicekGdpRepo;

@Service
public class VasicekGdpService {
	
	@Autowired
	VasicekGdpRepo repo;
	
	public List<VasicekGDP> findAll() {
		return repo.findAll();
	}
	
	public List<VasicekGDP> findStartEnd(Date start, Date end) {
		return repo.findStartEnd(start, end);
	}
	
	public VasicekGDP findById(int id) {
		return repo.findById(id);
	}
	
	public VasicekGDP findByBulanTahun(int bulan, int tahun) {
		return repo.findByBulanTahun(bulan, tahun);
	}
	
	public void save(VasicekGDP role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
	
	public void truncate() {
		this.repo.truncate();
	}
}
