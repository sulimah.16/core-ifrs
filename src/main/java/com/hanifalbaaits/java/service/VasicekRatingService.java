package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.VasicekRating;
import com.hanifalbaaits.java.repo.VasicekRatingRepo;

@Service
public class VasicekRatingService {
	
	@Autowired
	VasicekRatingRepo repo;
	
	public List<VasicekRating> findAll() {
		return repo.findAll();
	}
	
	public VasicekRating findById(int id) {
		return repo.findById(id);
	}
	
	public VasicekRating findByKode(String kode) {
		return repo.findByKode(kode);
	}
	
	public List<VasicekRating> findByStatus(int status) {
		return repo.findByStatus(status);
	}

	public void save(VasicekRating role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
}
