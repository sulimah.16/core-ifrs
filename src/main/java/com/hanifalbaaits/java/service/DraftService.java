package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.repo.DraftRepo;

@Service
public class DraftService {
	
	@Autowired
	DraftRepo repo;
	
	public List<Draft> findAll() {
		return repo.findAll();
	}
	
	public Draft findById(int id) {
		return repo.findById(id);
	}
	
	public List<Draft> findDraftByIdFlag(int id) {
		return repo.findDraftByIdFlag(id);
	}
	
	public Draft findLast() {
		return repo.findLast();
	}
	
	public void save(Draft role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
	
	public void truncate() {
		this.repo.truncate();
	}
}
