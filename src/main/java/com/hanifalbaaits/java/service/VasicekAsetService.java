package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.VasicekAset;
import com.hanifalbaaits.java.repo.VasicekAsetRepo;

@Service
public class VasicekAsetService {
	
	@Autowired
	VasicekAsetRepo repo;
	
	public List<VasicekAset> findAll() {
		return repo.findAll();
	}
	
	public VasicekAset findById(int id) {
		return repo.findById(id);
	}
	
	public void save(VasicekAset role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
}
