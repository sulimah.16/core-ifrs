package com.hanifalbaaits.java.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hanifalbaaits.java.utils.FileStorage;
import com.hanifalbaaits.java.utils.MyFileNotFoundException;
import com.hanifalbaaits.java.properties.FileProperties;

@Service
public class UploadService {
	
	private Path fileStorageLocation;

    @Autowired
    FileProperties fileStorageProperties;
    
    
    public String storeFile(MultipartFile file,String fileName, String type) {

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorage("Sorry! Filename contains invalid path sequence " + fileName);
            }

            //tentukan path penyimpanan terlebih dahulu.
            if (type.contentEquals("profile")) {
            	this.fileStorageLocation = Paths.get(fileStorageProperties.getPath_profile()).toAbsolutePath().normalize();
            } else if (type.contentEquals("input")) {
            	this.fileStorageLocation = Paths.get(fileStorageProperties.getPath_input()).toAbsolutePath().normalize();
			} else if (type.contentEquals("support")) {
            	this.fileStorageLocation = Paths.get(fileStorageProperties.getPath_support()).toAbsolutePath().normalize();
			} else {
				throw new FileStorage("Sorry! Type contains invalid path sequence " + type);
			}
            
            try {
                Files.createDirectories(this.fileStorageLocation);
            } catch (Exception ex) {
                throw new FileStorage("Could not create the directory where the uploaded files will be stored.", ex);
            }
           
            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorage("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String fileName, String type) {
        try {
        	//tentukan path penyimpanan terlebih dahulu.
        	 if (type.contentEquals("profile")) {
             	this.fileStorageLocation = Paths.get(fileStorageProperties.getPath_profile()).toAbsolutePath().normalize();
             } else if (type.contentEquals("input")) {
             	this.fileStorageLocation = Paths.get(fileStorageProperties.getPath_input()).toAbsolutePath().normalize();
 			} else if (type.contentEquals("support")) {
             	this.fileStorageLocation = Paths.get(fileStorageProperties.getPath_support()).toAbsolutePath().normalize();
 			} else {
 				throw new FileStorage("Sorry! Type contains invalid path sequence " + type);
 			}
            
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }
}


