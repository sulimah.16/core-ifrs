package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.DraftBucket;
import com.hanifalbaaits.java.modelDB.NilaiBucket;
import com.hanifalbaaits.java.repo.DraftBucketRepo;
import com.hanifalbaaits.java.repo.NilaiBucketRepo;

@Service
public class DraftBucketService {
	
	@Autowired
	DraftBucketRepo repo;
	
	@Autowired
	NilaiBucketRepo repoNilai;
	
	public List<DraftBucket> findAll() {
		return repo.findAll();
	}
	
	public DraftBucket findById(int id) {
		return repo.findById(id);
	}
	
	public DraftBucket findDraftBucketByKode(String kode) {
		return repo.findDraftBucketByKode(kode);
	}
	
	public DraftBucket findLast() {
		return repo.findLast();
	}
	
	public void save(DraftBucket role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
	
	public void truncate() {
		this.repo.truncate();
	}
	
	//SERVICE MODEL NILAI BUCKET
	
	public List<NilaiBucket> findNilaiBucketByIdDraftBucket(int id) {
		return repoNilai.findNilaiBucketByIdDraftBucket(id);
	}
	
	public List<NilaiBucket> findNilaiBucketByIdDraftBucketSTEP(int id, int step) {
		return repoNilai.findNilaiBucketByIdDraftBucketSTEP(id, step);
	}
	
	public NilaiBucket findNilaiBucketByIdDraftBucketSTEPdate(int id, int step, int bulan, int tahun) {
		return repoNilai.findNilaiBucketByIdDraftBucketSTEPdate(id, step, bulan,tahun);
	}
	
	public void deleteNilai(int id) {
		this.repoNilai.deleteByIdDraftBucket(id);
	}
	
	public void truncateNilai() {
		this.repoNilai.truncate();
	}
	
	public void saveNilai(NilaiBucket n) {
		this.repoNilai.save(n);
	}
	
}
