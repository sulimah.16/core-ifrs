package com.hanifalbaaits.java.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.DataInput;
import com.hanifalbaaits.java.repo.DataInputRepo;

@Service
public class DataInputService {
	
	@Autowired
	DataInputRepo repo;
	
	public List<DataInput> findAll() {
		return repo.findAll();
	}
	
	public DataInput findById(int id) {
		return repo.findById(id);
	}
	
	public List<DataInput> findByDraft(int id_draft) {
		return repo.findByDraft(id_draft);
	}
	
	public List<DataInput> findByDraftPeriode(int id_draft, Date start, Date end) {
		return repo.findByDraftPeriode(id_draft, start, end);
	}
		
	public void save(DataInput audit) {
		this.repo.save(audit);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
	
	public void truncate() {
		this.repo.truncate();
	}
	
	public void deleteDataInputByDraft(int id) {
		this.repo.deleteDataInputByDraft(id);
	}
	
	public void deleteDataInputByPathFile(String path) {
		this.repo.deleteDataInputByPathFile(path);
	}
}
