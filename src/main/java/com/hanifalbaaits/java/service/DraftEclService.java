package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.DraftECL;
import com.hanifalbaaits.java.repo.DraftEclRepo;

@Service
public class DraftEclService {
	
	@Autowired
	DraftEclRepo repo;
	
	public List<DraftECL> findAll() {
		return repo.findAll();
	}
	
	public List<DraftECL> findGrupIdModel() {
		return repo.findGrupIdModel();
	}
	
	public List<DraftECL> findByIdDraftBucket(int id) {
		return repo.findByIdDraftBucket(id);
	}
	
	public DraftECL findById(int id) {
		return repo.findById(id);
	}
	
	public void save(DraftECL role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
}
