package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.ECL;
import com.hanifalbaaits.java.modelDB.ECLLIST;
import com.hanifalbaaits.java.modelDB.ECLSKENARIO;
import com.hanifalbaaits.java.modelDB.MAPE;
import com.hanifalbaaits.java.modelDB.MAPELIST;
import com.hanifalbaaits.java.repo.ECLLISTrepo;
import com.hanifalbaaits.java.repo.ECLSKENARIOrepo;
import com.hanifalbaaits.java.repo.ECLrepo;
import com.hanifalbaaits.java.repo.MAPELISTrepo;
import com.hanifalbaaits.java.repo.MAPErepo;

@Service
public class SaveECLService {
	
	@Autowired
	MAPErepo repoMape;
	@Autowired
	MAPELISTrepo repoMapeList;
	
	////////
	
	@Autowired
	ECLrepo repoEcl;
	@Autowired
	ECLSKENARIOrepo repoEclSkenario;
	@Autowired
	ECLLISTrepo repoEclList;
	
	public MAPE findMape(int id_draft_ecl) {
		return this.repoMape.findByIdDraft(id_draft_ecl);
	}
	public List<MAPELIST> findMapeList(int id_draft_ecl) {
		return this.repoMapeList.findByIdDraft(id_draft_ecl);
	}
	public List<ECL> findEcl(int id_draft_ecl) {
		return this.repoEcl.findByIdDraft(id_draft_ecl);
	}
	public ECLSKENARIO findEclSkenario(int id_draft_ecl) {
		return this.repoEclSkenario.findByIdDraft(id_draft_ecl);
	}
	public List<ECLLIST> findEclList(int id_draft_ecl) {
		return this.repoEclList.findByIdDraft(id_draft_ecl);
	}
	
	public void saveMape(MAPE pesan) {
		this.repoMape.save(pesan);
	}
	public void saveMapeList(MAPELIST pesan) {
		this.repoMapeList.save(pesan);
	}
	public void saveEcl(ECL pesan) {
		this.repoEcl.save(pesan);
	}
	public void saveEclSkenario(ECLSKENARIO pesan) {
		this.repoEclSkenario.save(pesan);
	}
	public void saveEclList(ECLLIST pesan) {
		this.repoEclList.save(pesan);
	}
	public void deleteECL(int id_draft_ecl) {
		this.repoMapeList.deleteByIdDraft(id_draft_ecl);
		this.repoMape.deleteByIdDraft(id_draft_ecl);
		this.repoEclList.deleteByIdDraft(id_draft_ecl);
		this.repoEclSkenario.deleteByIdDraft(id_draft_ecl);
		this.repoEcl.deleteByIdDraft(id_draft_ecl);
	}
}
