package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.Komentar;
import com.hanifalbaaits.java.repo.KomentarRepo;

@Service
public class KomentarService {
	
	@Autowired
	KomentarRepo repo;
	
	public List<Komentar> findAll(){
		return this.repo.findAll();
	}
	public Komentar findById(int id){
		return this.repo.findById(id);
	}
	public List<Komentar> findByTypeIdType(int id_type, int flag, int id_bucket){
		return this.repo.findByTypeIdType(id_type,flag,id_bucket);
	}
	public List<Komentar> findByTypeIdTypeAll(int id_type, int flag, int id_bucket){
		return this.repo.findByTypeIdTypeAll(id_type,flag, id_bucket);
	}
	public List<Komentar> findByTypeIdTypeAll(int id_type, int id_bucket){
		return this.repo.findByTypeIdTypeAll(id_type,id_bucket);
	}
	public void save(Komentar pesan) {
		this.repo.save(pesan);
	}
	public void delete(int id) {
		this.repo.deleteById(id);
	}
	public void deleteByIdDraft(int id_type, int id_bucket) {
		this.repo.deleteByIdDraft(id_type,id_bucket);
	}
}
