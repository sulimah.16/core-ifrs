package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.InputMEV;
import com.hanifalbaaits.java.modelDB.InputModelFL;

import com.hanifalbaaits.java.repo.InputMEVrepo;
import com.hanifalbaaits.java.repo.InputModelFLrepo;

@Service
public class InputMevFL {
	
	@Autowired
	InputMEVrepo repoMev;
	
	@Autowired
	InputModelFLrepo repoFL;

	public List<InputMEV> findMevAll(){
		return this.repoMev.findAll();
	}
	
	public InputMEV findMevByID(int id) {
		return this.repoMev.findById(id);
	}
	
	public InputMEV findSTDEV() {
		return this.repoMev.findSTDEV();
	}
	
	public InputMEV findMevByBulanTahun(int bulan, int tahun) {
		return this.repoMev.findByBulanTahun(bulan,tahun);
	}
	
	public void saveMev(InputMEV data) {
		this.repoMev.save(data);
	}
	public void deleteMev(int id) {
		this.repoMev.deleteById(id);
	}
	
	public void truncateMev() {
		this.repoMev.truncate();
	}
	
	//FL
	
	public List<InputModelFL> findFLAll(){
		return this.repoFL.findAll();
	}
	
	public InputModelFL findFLById(int id) {
		return this.repoFL.findById(id);
	}
	
	public List<InputModelFL> findFLByNmr(String nomor) {
		return this.repoFL.findByNomor(nomor);
	}
	
	public List<InputModelFL> findFLByIdSegmen(int id, int status) {
		return this.repoFL.findByIdSegmen(id , status);
	}
	
	public InputModelFL findFLByIdModelSegment(int id, int id_segment) {
		return this.repoFL.findByIdModelSegment(id,id_segment);
	}
	
	public InputModelFL findFLByNmrSegment(String nomor, int id_segment) {
		return this.repoFL.findByNomorSegment(nomor,id_segment);
	}
	
	public void saveFL(InputModelFL data) {
		this.repoFL.save(data);
	}
	public void deleteFL(int id) {
		this.repoFL.deleteById(id);
	}
	
//	public void deleteFLBySegment(int id_segment) {
//		this.repoFL.deleteBySegment(id_segment);
//	}
}
