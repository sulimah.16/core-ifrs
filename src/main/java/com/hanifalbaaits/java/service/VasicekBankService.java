package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.VasicekBank;
import com.hanifalbaaits.java.repo.VasicekBankRepo;

@Service
public class VasicekBankService {
	
	@Autowired
	VasicekBankRepo repo;
	
	public List<VasicekBank> findAll() {
		return repo.findAll();
	}
	
	public VasicekBank findById(int id) {
		return repo.findById(id);
	}
	
	public VasicekBank findBySame(String nama, String akronim, int tahun) {
		return repo.findBySame(nama,akronim,tahun);
	}
	public void save(VasicekBank role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
}
