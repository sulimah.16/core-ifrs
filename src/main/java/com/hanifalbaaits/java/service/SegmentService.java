package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.repo.SegmentRepo;

@Service
public class SegmentService {
	
	@Autowired
	SegmentRepo repo;
	
	public List<Segment> findAll() {
		return repo.findAll();
	}
	
	public Segment findById(int id) {
		return repo.findById(id);
	}
	
	public Segment findByKode(String id) {
		return repo.findByKode(id);
	}

	public void save(Segment role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
}
