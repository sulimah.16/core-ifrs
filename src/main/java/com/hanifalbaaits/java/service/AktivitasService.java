package com.hanifalbaaits.java.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.Aktivitas;
import com.hanifalbaaits.java.repo.AktivitasRepo;

@Service
public class AktivitasService {
	
	@Autowired
	AktivitasRepo repo;
	
	public List<Aktivitas> findByAdmin(Date start, Date end) {
		return repo.findByAdmin(start, end);
	}
	
	public List<Aktivitas> findByAdminDashboard(Date start, Date end) {
		return repo.findByAdminDashboard(start, end);
	}
	
	public List<Aktivitas> findByIdUser(int user, Date start, Date end) {
		return repo.findByIdUser(user, start, end);
	}
	
	public List<Aktivitas> findLastAdmin() {
		return repo.findLastAdmin();
	}
	
	public List<Aktivitas> findLastByIduser(int user) {
		return repo.findLastByIduser(user);
	}
	
	public Aktivitas findLastLoginByIdUser(int user) {
		return repo.findLastLoginByIdUser(user);
	}
	
	public void save(Aktivitas audit) {
		this.repo.save(audit);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
}
