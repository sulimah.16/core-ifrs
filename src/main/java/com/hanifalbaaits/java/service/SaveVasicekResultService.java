package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.VasicekResultAset;
import com.hanifalbaaits.java.modelDB.VasicekResultDraft;
import com.hanifalbaaits.java.modelDB.VasicekResultMacro;
import com.hanifalbaaits.java.modelDB.VasicekResultPD;
import com.hanifalbaaits.java.repo.VasicekResultPdRepo;
import com.hanifalbaaits.java.repo.VasicekResultMacroRepo;
import com.hanifalbaaits.java.repo.VasicekResultDraftRepo;
import com.hanifalbaaits.java.repo.VasicekResultAsetRepo;

@Service
public class SaveVasicekResultService {
	
	@Autowired
	VasicekResultDraftRepo repoDraft;
	@Autowired
	VasicekResultAsetRepo repoAset;
	@Autowired
	VasicekResultMacroRepo repoMacro;
	@Autowired
	VasicekResultPdRepo repoPd;
	
	public List<VasicekResultDraft> findResultDraftAll() {
		return this.repoDraft.findByAll();
	}
	
	public VasicekResultDraft findResultDraft(int id) {
		return this.repoDraft.findByIdDraft(id);
	}
	
	public List<VasicekResultAset> findResultAset(int id) {
		return this.repoAset.findByIdDraft(id);
	}
	
	public List<VasicekResultMacro> findResultMacro(int id) {
		return this.repoMacro.findByIdDraft(id);
	}
	
	public List<VasicekResultPD> findResultPd(int id) {
		return this.repoPd.findByIdDraft(id);
	}
	
	public void saveDraft(VasicekResultDraft pesan) {
		this.repoDraft.save(pesan);
	}
	public void saveAset(VasicekResultAset pesan) {
		this.repoAset.save(pesan);
	}
	public void saveMacro(VasicekResultMacro pesan) {
		this.repoMacro.save(pesan);
	}
	public void savePd(VasicekResultPD pesan) {
		this.repoPd.save(pesan);
	}
	
	public void deleteResultVasicek(int id_draft_ecl) {
		this.repoDraft.deleteByIdDraft(id_draft_ecl);
		this.repoAset.deleteByIdDraft(id_draft_ecl);
		this.repoMacro.deleteByIdDraft(id_draft_ecl);
		this.repoPd.deleteByIdDraft(id_draft_ecl);
	}
}
