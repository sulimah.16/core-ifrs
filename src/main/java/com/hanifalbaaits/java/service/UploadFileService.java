package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.UploadFile;
import com.hanifalbaaits.java.repo.UploadFileRepo;

@Service
public class UploadFileService {
	
	@Autowired
	UploadFileRepo repo;
	
	public List<UploadFile> findAll() {
		return repo.findAll();
	}
	
	public UploadFile findById(int id) {
		return repo.findById(id);
	}
	
	public UploadFile findLast() {
		return repo.findLast();
	}
	
	public void save(UploadFile dt) {
		this.repo.save(dt);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
}
