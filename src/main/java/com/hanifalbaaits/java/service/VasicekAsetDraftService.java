package com.hanifalbaaits.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hanifalbaaits.java.modelDB.VasicekAsetDraft;
import com.hanifalbaaits.java.repo.VasicekAsetDraftRepo;

@Service
public class VasicekAsetDraftService {
	
	@Autowired
	VasicekAsetDraftRepo repo;
	
	public List<VasicekAsetDraft> findAll() {
		return repo.findAll();
	}
	
	public VasicekAsetDraft findById(int id) {
		return repo.findById(id);
	}
	
	public VasicekAsetDraft findLast() {
		return repo.findLast();
	}
	
	public void deleteByIdDraft(int id) {
		this.repo.deleteByIdDraft(id);
	}
	
	public void save(VasicekAsetDraft role) {
		this.repo.save(role);
	}
	
	public void delete(int id) {
		this.repo.deleteById(id);
	}
}
