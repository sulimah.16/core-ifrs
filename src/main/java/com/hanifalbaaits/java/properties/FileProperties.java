package com.hanifalbaaits.java.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class FileProperties {
	
	private String path_profile;
	private String path_input;
	private String path_support;
	public String getPath_profile() {
		return path_profile;
	}
	public void setPath_profile(String path_profile) {
		this.path_profile = path_profile;
	}
	public String getPath_input() {
		return path_input;
	}
	public void setPath_input(String path_input) {
		this.path_input = path_input;
	}
	public String getPath_support() {
		return path_support;
	}
	public void setPath_support(String path_support) {
		this.path_support = path_support;
	}
}


