package com.hanifalbaaits.java.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.UserRole;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.service.UserRoleService;

@RestController
@RequestMapping(value="core-ifrs")
public class MasterUserRole {
	
	private Logger logger = LoggerFactory.getLogger(MasterUserRole.class);

	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	UserRoleService serviceUserRole;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/role")
	public ResponseEntity<?> getAll(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/role";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api get UserRole");
			List<UserRole> data = serviceUserRole.findAll();
			
			if (data.size() == 0) {
				logger.info(method,url,"tidak ada data UserRole");
				return new ResponseEntity(new ResponObject(2, "empty UserRole",data),HttpStatus.OK);
			}
			
			logger.info(method,url,"data UserRole");
			return new ResponseEntity(new ResponObject(1, "List UserRole",data),HttpStatus.OK);
			
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/role/{id}")
	public ResponseEntity<?> getUserRoleById(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id
			) {
		
		
		String method = "GET";
		String url = "/role/{id}";
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api get UserRole by id");
			
			UserRole data = serviceUserRole.findById(id);
			
			if (data == null) {
				logger.info(method,url,"tidak ada data UserRole");
				return new ResponseEntity(new ResponObject(2, "tidak ada data UserRole",data),HttpStatus.OK);
			}
			
			logger.info(method,url,"data UserRole");
			return new ResponseEntity(new ResponObject(1, "List UserRole",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/role")
	public ResponseEntity<?> AddUserRole(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) {
		
		String method = "POST";
		String url = "/role";
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api UserRole create");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			if (body.get("keterangan")==null || body.get("keterangan").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "keterangan null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+"nama :"+body.get("nama"));
			logger.info(method+"|"+url+"|"+"keterangan :"+body.get("keterangan"));
			
			UserRole usr = new UserRole();
			usr.setNama(body.get("nama"));
			usr.setKeterangan(body.get("keterangan"));
			this.serviceUserRole.save(usr);
			
			logger.info(method+"|"+url+"|"+"save UserRole");
			return new ResponseEntity(new ResponObject(1, "Data UserRole save."),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/role/{id}")
	public ResponseEntity<?> UpdateUserRole(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,@RequestBody Map<String,String> body) {
		
		
		String method = "PUT";
		String url = "/role";
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api UserRole create");
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			if (body.get("keterangan")==null || body.get("keterangan").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "keterangan null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+"nama :"+body.get("nama"));
			logger.info(method+"|"+url+"|"+"keterangan :"+body.get("keterangan"));
			
			UserRole usr = this.serviceUserRole.findById(id);
			usr.setNama(body.get("nama"));
			usr.setKeterangan(body.get("keterangan"));
			this.serviceUserRole.save(usr);
			logger.info(method+"|"+url+"|"+"update UserRole");	
			return new ResponseEntity(new ResponObject(1, "Update UserRole save."),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@DeleteMapping(value="/role/{id}")
	public ResponseEntity<?> DeleteUserRole(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) {
		
		
		String method = "DELETE";
		String url = "/role";
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api UserRole delete");
			logger.info(method+"|"+url+"|"+"UserRole id: "+id);
			this.serviceUserRole.delete(id);
			logger.info(method+"|"+url+"|"+"delete UserRole");	
			return new ResponseEntity(new ResponObject(1, "delete UserRole."),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
