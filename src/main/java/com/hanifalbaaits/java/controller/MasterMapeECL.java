package com.hanifalbaaits.java.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.DraftBucket;
import com.hanifalbaaits.java.modelDB.InputMEV;
import com.hanifalbaaits.java.modelDB.InputModelFL;
import com.hanifalbaaits.java.modelDB.NilaiBucket;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelRespon.DraftBucketJoin;
import com.hanifalbaaits.java.modelRespon.Ecl;
import com.hanifalbaaits.java.modelRespon.EclBucket;
import com.hanifalbaaits.java.modelRespon.EclDraft;
import com.hanifalbaaits.java.modelRespon.EclSkenario;
import com.hanifalbaaits.java.modelRespon.Mape;
import com.hanifalbaaits.java.modelRespon.MapeDraft;
import com.hanifalbaaits.java.service.DraftBucketService;
import com.hanifalbaaits.java.service.DraftService;
import com.hanifalbaaits.java.service.InputMevFL;
import com.hanifalbaaits.java.utils.Encryption;

@RestController
@RequestMapping(value="core-ifrs")
public class MasterMapeECL {
	
	private Logger logger = LoggerFactory.getLogger(MasterMapeECL.class);

	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	DraftService serviceDraft;
	
	@Autowired
	DraftBucketService serviceBucketService;
	
	@Autowired
	MasterDraftBucket serviceControllerBS;
	
	@Autowired
	MasterReport serviceReport;
	
	@Autowired
	InputMevFL serviceMevFL;
	
	@Autowired
	Encryption encrypt;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/mape")
	public ResponseEntity<?> GetMAPE(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/mape";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft_bucket")==null || body.get("id_draft_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft_bucket null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_model")==null || body.get("id_model").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_model null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_regresi")==null || body.get("id_regresi").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_regresi null!"),HttpStatus.BAD_REQUEST);
		if (body.get("periode_forecast")==null || body.get("periode_forecast").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "periode_forecast null!"),HttpStatus.BAD_REQUEST);
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		//ID REGRESI 1 -> ODR 2 -> AVERAGE 3 -> DELTA LOSS 
		
		try {
			logger.info( "hit api get get mape");

			int id_draft_bucket = Integer.parseInt(body.get("id_draft_bucket"));
			int id_model = Integer.parseInt(body.get("id_model"));
			int id_regresi = Integer.parseInt(body.get("id_regresi"));
			int periode_forecast = Integer.parseInt(body.get("periode_forecast"));
			
			
			if (id_regresi != 1 && id_regresi !=2 && id_regresi !=3 && id_regresi !=4) {
				return new ResponseEntity(new ResponObject(2, "ID regresi hanya 1 , 2, 3, 4"),HttpStatus.BAD_REQUEST);
			}
			
			DraftBucket drabu = this.serviceBucketService.findById(id_draft_bucket);
			
			if (drabu == null) {
				
				return new ResponseEntity(new ResponObject(2, "draft_bucket null"),HttpStatus.BAD_REQUEST);
			} 
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			DateFormat formatYear = new SimpleDateFormat("yyyy");  
			DateFormat formatMonth = new SimpleDateFormat("MM");
			
			int bulan_start = Integer.parseInt(formatMonth.format(date1));
			int tahun_start = Integer.parseInt(formatYear.format(date1));
			int bulan_end = Integer.parseInt(formatMonth.format(date2));
			int tahun_end = Integer.parseInt(formatYear.format(date2));
			
			//CEK TANGGAL nya harus ada sesuai dengan draft bucket
			Calendar cals = Calendar.getInstance();
			cals.setTime(date1);
			cals.set(Calendar.DATE, 1);
			
			if (cals.getTimeInMillis() < drabu.getStart_date().getTime() || cals.getTimeInMillis() > drabu.getEnd_date().getTime()) {
				return new ResponseEntity(new ResponObject(2, "Periode Start Date tidak memungkinkan dari draft bucket. Mohon periksa kembali"),HttpStatus.BAD_REQUEST);
			}
			
			Calendar cale = Calendar.getInstance();
			cale.setTime(date2);
			cale.set(Calendar.DATE, 1);
			
			if (cale.getTimeInMillis() > drabu.getEnd_date().getTime() || cale.getTimeInMillis() < cals.getTimeInMillis()) {
				return new ResponseEntity(new ResponObject(2, "Periode End Date tidak memungkinkan dari draft bucket. Mohon periksa kembali"),HttpStatus.BAD_REQUEST);	
			}
			
			//UPDATE PARAMETER MODEL JUGA ID SEGMENT
			InputModelFL model_fl = this.serviceMevFL.findFLByIdModelSegment(id_model, drabu.getId_segment());
			
			if (model_fl == null) {
				
				return new ResponseEntity(new ResponObject(2, "model_fl null"),HttpStatus.BAD_REQUEST);
			}
			
			Draft dr = this.serviceDraft.findById(drabu.getId_draft());
			List<NilaiBucket> nb = this.serviceBucketService.findNilaiBucketByIdDraftBucket(id_draft_bucket);
			DraftBucketJoin respon = serviceControllerBS.joinDraftBucket(dr, drabu, nb);
			
			int jml_bulan = serviceReport.getBulanfromDate(date1, date2) + 1; //sengaja di tambahin angka 1.
			
			logger.info( "id_draft_bucket : "+id_draft_bucket);
			logger.info( "id_model : "+id_model);
			logger.info( "id_regresi : "+id_regresi);
			logger.info( "periode_forecast : "+periode_forecast);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			logger.info(method+"|"+url+"|"+ "bulan start :"+bulan_start);
			logger.info(method+"|"+url+"|"+ "tahun start :"+tahun_start);
			logger.info(method+"|"+url+"|"+ "bulan end :"+bulan_end);
			logger.info(method+"|"+url+"|"+ "tahun end :"+tahun_end);
			logger.info(method+"|"+url+"|"+ "jumlah_bulan :"+jml_bulan);
			
			MapeDraft mape_dr = getMAPE(nb, respon, id_regresi, model_fl, periode_forecast, date1, date2, jml_bulan, tahun_start, bulan_start);
			
			if (mape_dr == null) {
				return new ResponseEntity(new ResponObject(2, "Model FL tidak tersedia untuk periode yang diambil. Tambahkan Model baru pada model FL."),HttpStatus.BAD_REQUEST);
			}
			
			logger.info("success List Data Input");
			return new ResponseEntity(new ResponObject(1, "Result MAPE",mape_dr),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/ecl")
	public ResponseEntity<?> GetECL(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/ecl";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft_bucket")==null || body.get("id_draft_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft_bucket null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_model")==null || body.get("id_model").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_model null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_regresi")==null || body.get("id_regresi").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_regresi null!"),HttpStatus.BAD_REQUEST);
		if (body.get("periode_forecast")==null || body.get("periode_forecast").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "periode_forecast null!"),HttpStatus.BAD_REQUEST);
		//if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		//if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		//FIELD yg dibutuhin untuk ECL nya.
		
		if (body.get("periode") == null || body.get("periode").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "periode null!"),HttpStatus.BAD_REQUEST);
		if (body.get("lgd")==null || body.get("lgd").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "lgd null"),HttpStatus.BAD_REQUEST);
		if (body.get("normal")==null || body.get("normal").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "normal null"),HttpStatus.BAD_REQUEST);		
		if (body.get("optimis")==null || body.get("optimis").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "optimis null"),HttpStatus.BAD_REQUEST);		
		if (body.get("pesimis")==null || body.get("pesimis").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "pesimis null"),HttpStatus.BAD_REQUEST);		
		//if (body.get("scaling")==null || body.get("scaling").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "scaling null"),HttpStatus.BAD_REQUEST);		
		
		//ID REGRESI 1 -> ODR 2 -> AVERAGE 3 -> DELTA LOSS 
		
		try {
			logger.info( "hit api get get report ecl");

			int id_draft_bucket = Integer.parseInt(body.get("id_draft_bucket"));
			int id_model = Integer.parseInt(body.get("id_model"));
			int id_regresi = Integer.parseInt(body.get("id_regresi"));
			int periode_forecast = Integer.parseInt(body.get("periode_forecast"));
			double scaling = 0.0;
			
			if (id_regresi != 1 && id_regresi !=2 && id_regresi !=3 && id_regresi !=4) {
				return new ResponseEntity(new ResponObject(2, "ID regresi hanya 1 , 2, 3, 4"),HttpStatus.BAD_REQUEST);
			}
			
			DraftBucket drabu = this.serviceBucketService.findById(id_draft_bucket);
			
			if (drabu == null) {
				
				return new ResponseEntity(new ResponObject(2, "draft_bucket null"),HttpStatus.BAD_REQUEST);
			}
			
			if (body.get("scaling") != null && !body.get("scaling").isEmpty()) {
				scaling = Double.parseDouble(body.get("scaling"));
				System.out.println("SCALING: "+scaling);
			}
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date periode = new Date();
			try {
				periode = df.parse(body.get("periode").trim()+" 00:00");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			DateFormat formatYear = new SimpleDateFormat("yyyy");  
			DateFormat formatMonth = new SimpleDateFormat("MM");
			
			int bulan_periode = Integer.parseInt(formatMonth.format(periode));
			int tahun_periode = Integer.parseInt(formatYear.format(periode));
			
			//CEK TANGGAL nya harus ada sesuai dengan draft bucket
			Calendar cals = Calendar.getInstance();
			cals.setTime(periode);
			cals.set(Calendar.DATE, 1);
			
			if (cals.getTimeInMillis() < drabu.getStart_date().getTime() || cals.getTimeInMillis() > drabu.getEnd_date().getTime()) {
				return new ResponseEntity(new ResponObject(2, "Periode Start Date tidak memungkinkan dari draft bucket. Mohon periksa kembali"),HttpStatus.BAD_REQUEST);
			}
			
			Draft dr = this.serviceDraft.findById(drabu.getId_draft());
			List<NilaiBucket> nb = this.serviceBucketService.findNilaiBucketByIdDraftBucket(id_draft_bucket);
			DraftBucketJoin respon = serviceControllerBS.joinDraftBucket(dr, drabu, nb);
			
			logger.info( "id_draft_bucket : "+id_draft_bucket);
			logger.info( "id_model : "+id_model);
			logger.info( "id_regresi : "+id_regresi);
			logger.info( "periode_forecast : "+periode_forecast);
			logger.info( "periode : "+body.get("periode"));
			logger.info(method+"|"+url+"|"+ "bulan periode :"+bulan_periode);
			logger.info(method+"|"+url+"|"+ "tahun periode :"+tahun_periode);
			
			double lgd = Double.parseDouble(body.get("lgd")) / 100;
			double normal = Double.parseDouble(body.get("normal")) / 100;
			double optimis = Double.parseDouble(body.get("optimis")) / 100;
			double pesimis = Double.parseDouble(body.get("pesimis")) / 100;
			
			//DISINI BUATIN TEMP ECL NYA
			
			EclDraft result = new EclDraft();
			String regresi = "";
			result.setInput_regresi(id_regresi);
			if (id_regresi == 1) {
				regresi = "ODR";
			} else if (id_regresi == 2) {
				regresi = "AVERAGE";
			} else if (id_regresi == 3) {
				regresi = "SUM LOSS RATE";
			} else if (id_regresi == 4) {
				regresi = "SUM DELTA LOSS RATE";
			}
			
			result.setRegresi(regresi);
			result.setDraft_bucket(respon);
			
			List<EclBucket> list_bucket = new ArrayList<EclBucket>();
			
			for (int i = 0; i < 2; i++) {
				if (i == 0) {
					
					EclBucket buc = getBucketECL(id_regresi, bulan_periode, tahun_periode, periode_forecast, 
							nb, 0, respon.getJumlah_bucket(), lgd , normal , optimis, pesimis, scaling,respon.getAraging().getHeader_bucket(), drabu.getId_segment());
					if (buc == null) {
						continue;
					}
					list_bucket.add(buc);
					if (id_model == 0) {
						break;	
					}
					
				} else {
					
					EclBucket buc = getBucketECL(id_regresi, bulan_periode, tahun_periode, periode_forecast, 
							nb, id_model, respon.getJumlah_bucket(), lgd , normal , optimis, pesimis, scaling,respon.getAraging().getHeader_bucket(), drabu.getId_segment());
					if (buc == null) {
						continue;
					}
					list_bucket.add(buc);
					
				}
			}
			
			result.setEcl_list(list_bucket);
			
			
			logger.info("success List Data ECL");
			return new ResponseEntity(new ResponObject(1, "Result ECL",result),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	public EclBucket getBucketECL(int id_regresi, int bulan_periode, int tahun_periode, int periode_forecast,
			List<NilaiBucket> nilai_bucket, int id_model, int jumlah_bucket, double lgd, double normal, double optimis, double pesimis, double scaling, List<String> header, int id_segment) {
		
		InputModelFL model_fl = new InputModelFL();
		
		if (id_model != 0) {
			//UPDATE PARAMETER MODEL JUGA ID SEGMENT
			model_fl = this.serviceMevFL.findFLByIdModelSegment(id_model, id_segment);
			if (model_fl == null) {
				return null;
			}
		}
		
		EclBucket bucket = new EclBucket();
		bucket.setId_model(id_model);
		bucket.setPeriode(serviceReport.getPeriode(bulan_periode, tahun_periode));
		bucket.setPeriode_forecast(periode_forecast);
		
		//ambil data yg dibutuhkan dulu untuk list selanjutnya.
		int jumlah_periode = 0;
		double [] ead = new double[jumlah_bucket];
		double [] sum_step7 = new double[jumlah_bucket];
		double [] sum_step9 = new double[jumlah_bucket];
		double persentase_odr = 0.0;
		double average_lossrate = 0.0;
		double sum_lossrate = 0.0;
		double sum_lossrate_67 = 0.0;
		
		for (NilaiBucket nb : nilai_bucket) {
			
			if (nb.getId_step() == 3) {
				jumlah_periode += 1;
			}
			
			if (nb.getId_step() == 3 && nb.getPeriode_bulan() == bulan_periode 
					&& nb.getPeriode_tahun() == tahun_periode) {
				JSONArray list_nilai = new JSONArray(nb.getNilai());
				for (int i = 0; i < jumlah_bucket; i++) {
					ead[i] += list_nilai.getDouble(i);
				}	
			
			} else if (nb.getId_step() == 67 && nb.getPeriode().toLowerCase().contains("loss rate")) {
				
				JSONArray list_nilai = new JSONArray(nb.getNilai());
				for (int i = 0; i < jumlah_bucket; i++) {
					sum_step7[i] += list_nilai.getDouble(i);
				}
				sum_lossrate_67 = nb.getSum();
				
			} else if (nb.getId_step() == 9) {
				
				JSONArray list_nilai = new JSONArray(nb.getNilai());
				for (int i = 0; i < jumlah_bucket; i++) {
					sum_step9[i] += list_nilai.getDouble(i);
				}	
			
			} else if (nb.getId_step() == 10 && nb.getPeriode_bulan() == bulan_periode &&
					nb.getPeriode_tahun() == tahun_periode) {
				
				average_lossrate = nb.getAverage();
				sum_lossrate = nb.getSum();
				
			} else if (nb.getId_step() == 14 && nb.getPeriode_bulan() == bulan_periode &&
					nb.getPeriode_tahun() == tahun_periode) {
				
				persentase_odr = nb.getPersentase_odr();
				
			} else {
				
				continue;
			}
		}

		//BUAT LIST UNTUK NO MODEL
		if (id_model == 0) {
			
			double sum_ead = 0.0 ;
			double sum_ecl = 0.0 ;
			
			List<Ecl> list = new ArrayList<Ecl>();
			
			for (int i = 0; i < jumlah_bucket; i++) {
				
				Ecl ecl = new Ecl();
				ecl.setBucket(header.get(i));
				
				double pd_base = 0.0;
				
				if (id_regresi == 1) {
					
					pd_base = sum_step9[i]/ (jumlah_periode -1);
					
				} else if (id_regresi == 2) {
				
					pd_base = sum_step7[i];
					
				} else if (id_regresi == 3) {
				
					pd_base = sum_step7[i];
					
				} else if (id_regresi == 4) {
				
					pd_base = sum_step7[i];
					
				}
				
				ecl.setPd_base(pd_base);
				ecl.setLgd(lgd);
				ecl.setEad(ead[i]);
				sum_ead += ead[i];
				
				double ecl_base = pd_base * lgd * ead[i];
				ecl.setEcl_base(ecl_base);
				sum_ecl += ecl_base;
				list.add(ecl);
			}
			bucket.setNo_model_list(list);
			bucket.setNo_model_sum_ead(sum_ead);
			bucket.setNo_model_sum_ecl(sum_ecl);
		
		} else {
			
			bucket.setModel(model_fl);
			
			EclSkenario skena = new EclSkenario();
			InputMEV stdev = this.serviceMevFL.findSTDEV();
			
			double stdev_var1 = getSTDEV(stdev, model_fl.getVar1());
			double stdev_var2 = getSTDEV(stdev, model_fl.getVar2());
			
			skena.setStdev_var1(stdev_var1);
			skena.setStdev_var2(stdev_var2);
			
			
			double normal_var1 = getNilaiVar(bulan_periode, tahun_periode, model_fl.getVar1(), periode_forecast - model_fl.getLag1());
			double normal_var2 = getNilaiVar(bulan_periode, tahun_periode, model_fl.getVar2(), periode_forecast - model_fl.getLag2());
			
			skena.setNormal_var1(normal_var1);
			skena.setNormal_var2(normal_var2);
			skena.setNormal_probability(normal);
			
			double optimis_var1 = 0.0;
			double optimis_var2 = 0.0;
			
			if (model_fl.getKoef_var1() > 0) {
				optimis_var1 = normal_var1 - stdev_var1 ;
			} else {
				optimis_var1 = stdev_var1 + normal_var1 ;
			}
			
			if (model_fl.getKoef_var2() > 0) {
				optimis_var2 = normal_var2 - stdev_var2 ;
			} else {
				optimis_var2 = stdev_var2 + normal_var2 ;
			}
			
			skena.setOptimis_var1(optimis_var1);
			skena.setOptimis_var2(optimis_var2);
			skena.setOptimis_probability(optimis);
			
			double pesimis_var1 = 0.0;
			double pesimis_var2 = 0.0;
			
			if (model_fl.getKoef_var1() > 0) {
				pesimis_var1 = normal_var1 + (1.5 * stdev_var1) ;
			} else {
				pesimis_var1 = normal_var1 - (1.5 * stdev_var1) ;
			}
			
			if (model_fl.getKoef_var2() > 0) {
				pesimis_var2 = normal_var2 + (1.5 * stdev_var2) ;
			} else {
				pesimis_var2 = stdev_var2 - (1.5 * stdev_var2) ;
			}
			
			skena.setPesimis_var1(pesimis_var1);
			skena.setPesimis_var2(pesimis_var2);
			skena.setPesimis_probability(pesimis);
			
			//DISINI BUAT DISTRIBUSI PROPORSIONAL
			
			double weigh_var1 = (normal_var1 * normal) + (optimis_var1 * optimis)+ (pesimis_var1 * pesimis);
			double weigh_var2 = (normal_var2 * normal) + (optimis_var2 * optimis)+ (pesimis_var2 * pesimis);
			
			skena.setWeigthed_var1(weigh_var1);
			skena.setWeigthed_var2(weigh_var2);
			
			bucket.setSkenario(skena);
			
			double logit = model_fl.getC_coef() + (model_fl.getKoef_var1() * weigh_var1) + (model_fl.getKoef_var2() * weigh_var2);
			bucket.setLogit_expected_psak(logit);
			
			double expected = 0.0;
			
			if (id_regresi == 1 || id_regresi == 2) {
				expected = Math.exp(logit) / (1 + Math.exp(logit));
			} else {
				expected = logit;
			}
			
			bucket.setExpected_psak(expected);
			bucket.setExpected(expected);
			
			double last = 0.0 ;
			if (id_regresi == 1) {
				last = persentase_odr;
			} else if (id_regresi == 2) {
				last = average_lossrate;
			} else if (id_regresi == 3) {
				last = sum_lossrate;
			} else if (id_regresi == 4) {
				last = sum_lossrate_67;
			}
			
			bucket.setLast(last);
			
			double mev_effect = expected / last;
			bucket.setMev(mev_effect);
			
			if(id_regresi != 4)	{
				
				double sum_ead = 0.0 ;
				double sum_ecl = 0.0 ;
				double sum_ecl_forward = 0.0 ;
				
				List<Ecl> list = new ArrayList<Ecl>();
				
				for (int i = 0; i < jumlah_bucket; i++) {
					
					Ecl ecl = new Ecl();
					ecl.setBucket(header.get(i));
					
					double pd_base = 0.0;
					
					if (id_regresi == 1) {
						
						pd_base = sum_step9[i]/ (jumlah_periode - 1);
						
					} else if (id_regresi == 2) {
						
						pd_base = sum_step7[i];
						
					} else if (id_regresi == 3) {
						
						pd_base = sum_step7[i];
						
					}
					
					ecl.setPd_base(pd_base);
					ecl.setMev_effect(mev_effect);
					
					double forward_looking = 0.0 ;
					if (pd_base >= 1.0) {
						forward_looking = 1.0;
					} else {
						forward_looking = pd_base * mev_effect;
						if (forward_looking >= 1.0) {
							forward_looking = 1.0;
						}
					}
					
					ecl.setPd_forward_looking(forward_looking);
					ecl.setLgd(lgd);
					ecl.setEad(ead[i]);
					sum_ead += ead[i];
					
					double ecl_base = pd_base * lgd * ead[i];
					ecl.setEcl_base(ecl_base);
					sum_ecl += ecl_base;
					
					double ecl_fl = forward_looking * lgd * ead[i];
					ecl.setEcl_forward_looking(ecl_fl);
					sum_ecl_forward += ecl_fl;
					list.add(ecl);
				}
				bucket.setProporsional_list(list);
				bucket.setProporsional_sum_ead(sum_ead);
				bucket.setProporsional_sum_ecl(sum_ecl);
				bucket.setProporsional_sum_ecl_forward_looking(sum_ecl_forward);
		
			}
			//DISINI BUAT DISTRIBUSI SOLVER
			
			if (id_regresi == 2 || id_regresi == 4) { //untuk yang regresi 2 (average) ada distribusi dengan solver.
				
				double solver_sum_pd_base = 0.0;
				double solver_sum_pd_forward_looking = 0.0;
				double solver_sum_ead = 0.0 ;
				double solver_sum_ecl = 0.0 ;
				double solver_sum_ecl_forward = 0.0 ;
				
				List<Ecl> list_solver = new ArrayList<Ecl>();
				
				for (int i = 0; i < jumlah_bucket; i++) {
					
					Ecl ecl = new Ecl();
					ecl.setBucket(header.get(i));
					
					double pd_base = 0.0;
					
					if (id_regresi == 1) {
						
						pd_base = sum_step9[i]/jumlah_bucket;
						
					} else if (id_regresi == 2 || id_regresi == 3 || id_regresi == 4) {
						
						pd_base = sum_step7[i];
					}
					solver_sum_pd_base += pd_base;
					ecl.setPd_base(pd_base);
					
					double logit_pd = 0.0;
					
					if (pd_base >= 1.0) {
						logit_pd = 1.0;
					} else if (pd_base <= 0) {
						logit_pd = 0.0;
					} else {
						logit_pd = Math.log(pd_base / (1 - pd_base)) + scaling;
					}
					
					ecl.setLogit_pd(logit_pd);
					
					double forward_looking = 0.0 ;
					if (pd_base >= 1.0) {
						forward_looking = 1.0;
					} else if (pd_base <= 0) {
						forward_looking = 0.0;
					} else {
						forward_looking = 1 / (1 + Math.exp(-1 * logit_pd));
						if (forward_looking <= 0) {
							forward_looking = 0;
						} else if (forward_looking >= 1) {
							forward_looking = 1;
						}
					}
					
					solver_sum_pd_forward_looking += forward_looking;
					ecl.setPd_forward_looking(forward_looking);
					
					ecl.setLgd(lgd);
					ecl.setEad(ead[i]);
					solver_sum_ead += ead[i];
					
					double ecl_base = pd_base * lgd * ead[i];
					ecl.setEcl_base(ecl_base);
					solver_sum_ecl += ecl_base;
					
					double ecl_fl = forward_looking * lgd * ead[i];
					ecl.setEcl_forward_looking(ecl_fl);
					solver_sum_ecl_forward += ecl_fl;
					list_solver.add(ecl);
				}
				
				double target = 0.0; 
				
				if (id_regresi == 2) {
					target = expected - (solver_sum_pd_forward_looking / jumlah_bucket);
				} else if (id_regresi == 4) {
					double pd_forward_looking = expected + last;
					target = pd_forward_looking - solver_sum_pd_forward_looking;
					bucket.setPd_forward_looking(pd_forward_looking);
				}
				
				bucket.setSolver_list(list_solver);
				bucket.setSolver_sum_ead(solver_sum_ead);
				bucket.setSolver_sum_ecl(solver_sum_ecl);
				bucket.setSolver_sum_ecl_forward_looking(solver_sum_ecl_forward);
				bucket.setSolver_average_pd_base(solver_sum_pd_base/jumlah_bucket);
				bucket.setSolver_average_pd_forward_looking(solver_sum_pd_forward_looking / jumlah_bucket);
				
				bucket.setScaling(scaling);
				bucket.setTarget(target);
			}
		}
		
		return bucket;
	}

	public MapeDraft getMAPE(List<NilaiBucket> nb, DraftBucketJoin respon, int id_regresi, InputModelFL model_fl, int periode_forecast, 
			Date start, Date end, int jml_bulan, int tahun_start, int bulan_start) {
		
		List<String> header = new ArrayList<String>();
		String regresi = "";
		int step = 0;
		
		header.add("Periode");
		header.add("Nilai Var 1");
		header.add("Nilai Var 2");
		
		if (id_regresi == 1) { //ODR
			
			regresi = "ODR";
			step = 14;
			header.add("Logit Expected PD");
			header.add("Expected PD");
			header.add("Actual PD");
			
		} else if (id_regresi == 2) { //AVERAGE
			
			regresi = "AVERAGE";
			step = 10;
			header.add("Logit Expected Loss Rate");
			header.add("Expected Average Loss Rate");
			header.add("Actual Average Loss Rate");
			
		} else if (id_regresi == 3) { //SUM LOSS RATE
			
			regresi = "SUM LOSS RATE";
			step = 10;
			header.add("Expected Sum Loss Rate");
			header.add("Actual Sum Loss Rate");
			
		} else if (id_regresi == 4) { //SUM DELTA
			
			regresi = "SUM DELTA LOSS RATE";
			step = 12;
			header.add("Expected Sum Delta Loss Rate");
			header.add("Actual Sum Delta Loss Rate");
			
		}
		
		header.add("MAPE");
		
		MapeDraft mape_dr = new MapeDraft();
		
		mape_dr.setDraft_bucket(respon);
		mape_dr.setId_regresi(id_regresi);
		mape_dr.setInput_regresi(regresi);
		mape_dr.setId_model(model_fl.getId_model());
		mape_dr.setModel(model_fl);
		mape_dr.setPeriode_forecast(periode_forecast);
		mape_dr.setPeriode_start(start);
		mape_dr.setPeriode_end(end);
		mape_dr.setHeader(header);
		
		List<Mape> list_map = new ArrayList<Mape>();
		double total_mape = 0.0;
		
		for (int i = 0; i < jml_bulan; i++) {
			
			if (i != 0) {
				if (bulan_start > 12) {
					tahun_start += 1;
					bulan_start = 1;
				}
			}
			
			Mape mape = new Mape();
			mape.setPeriode(serviceReport.getPeriode(bulan_start, tahun_start));
			mape.setPeriode_bulan(bulan_start);
			mape.setPeriode_tahun(tahun_start);
			
			Double var1 = getNilaiVar(bulan_start, tahun_start, model_fl.getVar1(), periode_forecast - model_fl.getLag1());
			Double var2 = getNilaiVar(bulan_start, tahun_start, model_fl.getVar2(), periode_forecast - model_fl.getLag2());
			
			if (var1 == null || var2 == null) {
				return null;
			}
			
			mape.setNilai_var1(var1);
			mape.setNilai_var2(var2);
			
			double logit = model_fl.getC_coef()+(model_fl.getKoef_var1() * var1)+(model_fl.getKoef_var2() * var2);
			mape.setLogit_expected(logit);
			
			double expected = 0.0;
			if (id_regresi == 1 ||id_regresi == 2) {
				expected = Math.exp(logit) / (1.0 + Math.exp(logit));
			} else {
				expected = logit;
			}
			mape.setExpected(expected);
			
			NilaiBucket nbuk = getNilaiBucket(nb, bulan_start, tahun_start, step);
			
			double actual = 0.0;
			if (id_regresi == 1) { //ODR
				actual = nbuk.getPersentase_odr();
			} else if (id_regresi == 2) { //AVERAGE
				actual = nbuk.getAverage();
			} else if (id_regresi == 3) { //SUM LOSS RATE
				actual = nbuk.getSum();
			} else if (id_regresi == 4) { //SUM DELTA LOSS RATE
				actual = nbuk.getSum();
			}
			
			mape.setActual(actual);
			
			double fmape = (expected - actual) / actual ;
			if (fmape < 0) {
				fmape = fmape * -1;
			}
			
			mape.setMape(fmape);
			total_mape += fmape;
			
			list_map.add(mape);
			bulan_start += 1;
		}
		
		mape_dr.setList_mape(list_map);
		mape_dr.setAverage_mape(total_mape / jml_bulan);
		mape_dr.setThreshold_mape(20 / 100.0);
		mape_dr.setHasil_mape("VALID");
		
		return mape_dr;
	}
	
	public NilaiBucket getNilaiBucket(List<NilaiBucket> list, int bulan, int tahun, int step) {
		NilaiBucket rsp = new NilaiBucket();
		for (NilaiBucket gb : list) {
			if (gb.getPeriode_bulan() == bulan && gb.getPeriode_tahun() == tahun && gb.getId_step() == step) {
				 rsp = gb;
				 break;
			}
		}
		return rsp;
	}
	
	public Double getNilaiVar(int bulan,int tahun,String var, int add_bln) {
		
		double var_nilai = 0.0;
		Calendar calx = Calendar.getInstance();
		calx.set(tahun, (bulan - 1), 1); // disini karena bulan nya calendar 0 itu januari.
		calx.add(Calendar.MONTH, add_bln);
		
		System.out.println("TRACE bulan: "+bulan);
		System.out.println("TRACE tahun: "+tahun);
		System.out.println("TRACE var: "+var);
		System.out.println("TRACE add_bln: "+add_bln);
		System.out.println("TRACE CHECK bulan: "+calx.get(Calendar.MONTH));
		System.out.println("TRACE CHECK tahun: "+calx.get(Calendar.YEAR));
		
		InputMEV model = this.serviceMevFL.findMevByBulanTahun(calx.get(Calendar.MONTH) + 1, calx.get(Calendar.YEAR)); //ditambah satu karena sebelumnya di kalendar januari dihitung 0.
		if (model == null) {
			return null;
		}
		
		if (var.toLowerCase().contentEquals("gdp")) {
			var_nilai = model.getGdp();
		} else if (var.toLowerCase().contentEquals("lgdp")) {
			var_nilai = model.getLgdp();
		} else if (var.toLowerCase().contentEquals("dgdp_1")) {
			var_nilai = model.getDgdp_1();
		} else if (var.toLowerCase().contentEquals("dgdp_12")) {
			var_nilai = model.getDgdp_12();
		} else if (var.toLowerCase().contentEquals("lgdp_1")) {
			var_nilai = model.getLgdp_1();
		} else if (var.toLowerCase().contentEquals("lgdp_12")) {
			var_nilai = model.getLgdp_12();
		} else if (var.toLowerCase().contentEquals("gdpgr")) {
			var_nilai = model.getGdpgr();
		} else if (var.toLowerCase().contentEquals("kurs")) {
			var_nilai = model.getKurs();
		} else if (var.toLowerCase().contentEquals("lkurs")) {
			var_nilai = model.getLkurs();
		} else if (var.toLowerCase().contentEquals("dkurs")) {
			var_nilai = model.getDkurs();
		} else if (var.toLowerCase().contentEquals("dlkurs")) {
			var_nilai = model.getDlkurs();
		} else if (var.toLowerCase().contentEquals("i7rr")) {
			var_nilai = model.getI7rr();
		} else if (var.toLowerCase().contentEquals("di7rr")) {
			var_nilai = model.getDi7rr();
		} else if (var.toLowerCase().contentEquals("inf")) {
			var_nilai = model.getInf();
		} else if (var.toLowerCase().contentEquals("dinf")) {
			var_nilai = model.getDinf();
		} else if (var.toLowerCase().contentEquals("govsp")) {
			var_nilai = model.getGovsp();
		} else if (var.toLowerCase().contentEquals("lgovsp")) {
			var_nilai = model.getLgovsp();
		} else if (var.toLowerCase().contentEquals("dgovsp")) {
			var_nilai = model.getDgovsp();
		} else if (var.toLowerCase().contentEquals("dlgovsp")) {
			var_nilai = model.getDlgovsp();
		} else if (var.toLowerCase().contentEquals("govspgr")) {
			var_nilai = model.getGovspgr();
		} else if (var.toLowerCase().contentEquals("housep")) {
			var_nilai = model.getHousep();
		} else if (var.toLowerCase().contentEquals("lhousep")) {
			var_nilai = model.getLhousep();
		} else if (var.toLowerCase().contentEquals("dhousep")) {
			var_nilai = model.getDhousep();
		} else if (var.toLowerCase().contentEquals("dlhousep")) {
			var_nilai = model.getDlhousep();
		} else if (var.toLowerCase().contentEquals("dlgdp")) {
			var_nilai = model.getDlgdp();
		} else if (var.toLowerCase().contentEquals("dgdpgr")) {
			var_nilai = model.getDgdpgr();
		} else if (var.toLowerCase().contentEquals("oil")) {
			var_nilai = model.getOil();
		} else if (var.toLowerCase().contentEquals("loil")) {
			var_nilai = model.getLoil();
		} else if (var.toLowerCase().contentEquals("doil")) {
			var_nilai = model.getDoil();
		} else if (var.toLowerCase().contentEquals("dloil")) {
			var_nilai = model.getDloil();
		}
		
		return var_nilai;
	}

	public Double getSTDEV(InputMEV model , String var) {
		
		double var_nilai = 0.0 ;
		
		if (var.toLowerCase().contentEquals("gdp")) {
			var_nilai = model.getGdp();
		} else if (var.toLowerCase().contentEquals("lgdp")) {
			var_nilai = model.getLgdp();
		} else if (var.toLowerCase().contentEquals("dgdp_1")) {
			var_nilai = model.getDgdp_1();
		} else if (var.toLowerCase().contentEquals("dgdp_12")) {
			var_nilai = model.getDgdp_12();
		} else if (var.toLowerCase().contentEquals("lgdp_1")) {
			var_nilai = model.getLgdp_1();
		} else if (var.toLowerCase().contentEquals("lgdp_12")) {
			var_nilai = model.getLgdp_12();
		} else if (var.toLowerCase().contentEquals("gdpgr")) {
			var_nilai = model.getGdpgr();
		} else if (var.toLowerCase().contentEquals("kurs")) {
			var_nilai = model.getKurs();
		} else if (var.toLowerCase().contentEquals("lkurs")) {
			var_nilai = model.getLkurs();
		} else if (var.toLowerCase().contentEquals("dkurs")) {
			var_nilai = model.getDkurs();
		} else if (var.toLowerCase().contentEquals("dlkurs")) {
			var_nilai = model.getDlkurs();
		} else if (var.toLowerCase().contentEquals("i7rr")) {
			var_nilai = model.getI7rr();
		} else if (var.toLowerCase().contentEquals("di7rr")) {
			var_nilai = model.getDi7rr();
		} else if (var.toLowerCase().contentEquals("inf")) {
			var_nilai = model.getInf();
		} else if (var.toLowerCase().contentEquals("dinf")) {
			var_nilai = model.getDinf();
		} else if (var.toLowerCase().contentEquals("govsp")) {
			var_nilai = model.getGovsp();
		} else if (var.toLowerCase().contentEquals("lgovsp")) {
			var_nilai = model.getLgovsp();
		} else if (var.toLowerCase().contentEquals("dgovsp")) {
			var_nilai = model.getDgovsp();
		} else if (var.toLowerCase().contentEquals("dlgovsp")) {
			var_nilai = model.getDlgovsp();
		} else if (var.toLowerCase().contentEquals("govspgr")) {
			var_nilai = model.getGovspgr();
		} else if (var.toLowerCase().contentEquals("housep")) {
			var_nilai = model.getHousep();
		} else if (var.toLowerCase().contentEquals("lhousep")) {
			var_nilai = model.getLhousep();
		} else if (var.toLowerCase().contentEquals("dhousep")) {
			var_nilai = model.getDhousep();
		} else if (var.toLowerCase().contentEquals("dlhousep")) {
			var_nilai = model.getDlhousep();
		} else if (var.toLowerCase().contentEquals("dlgdp")) {
			var_nilai = model.getDlgdp();
		} else if (var.toLowerCase().contentEquals("dgdpgr")) {
			var_nilai = model.getDgdpgr();
		} else if (var.toLowerCase().contentEquals("oil")) {
			var_nilai = model.getOil();
		} else if (var.toLowerCase().contentEquals("loil")) {
			var_nilai = model.getLoil();
		} else if (var.toLowerCase().contentEquals("doil")) {
			var_nilai = model.getDoil();
		} else if (var.toLowerCase().contentEquals("dloil")) {
			var_nilai = model.getDloil();
		}
		
		return var_nilai;
	}
}
