package com.hanifalbaaits.java.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelRespon.SegmentJoin;
import com.hanifalbaaits.java.service.SegmentService;
import com.hanifalbaaits.java.utils.Encryption;

@RestController
@RequestMapping(value="core-ifrs")
public class MasterSegment {
	
	private Logger logger = LoggerFactory.getLogger(MasterSegment.class);

	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	SegmentService serviceSegment;
	
	@Autowired
	Encryption encrypt;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/segment")
	public ResponseEntity<?> getSegment(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/segment";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get master user");
			
			List<Segment> data = serviceSegment.findAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data Segment");
				return new ResponseEntity(new ResponList(1, "empty Segment",data),HttpStatus.OK);
			}
			
			List<SegmentJoin> dataJoin = joinSegment(data);
			logger.info(method+"|"+url+"|"+"data user");
			return new ResponseEntity(new ResponList(1, "List Segment",dataJoin),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/segment/{id}")
	public ResponseEntity<?> getSegmentID(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id
			) {
		
		String method = "GET";
		String url = "/segment/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get master segment");
			
			Segment check = this.serviceSegment.findById(id);
		
			if (check == null) {
				logger.info(method+"|"+url+"|"+"tidak ada data Segment");
				return new ResponseEntity(new ResponList(1, "empty Segment"),HttpStatus.OK);
			}
			
			SegmentJoin segment_join = new SegmentJoin();
			segment_join.setId_segment(check.getId_segment());
			segment_join.setKode_segment(check.getKode_segment());
			segment_join.setNama_segment(check.getNama_segment());
	
			segment_join.setCreated_by(check.getCreated_by());
			segment_join.setCreated_date(configGetDate.format(check.getCreated_date()));
			
			if(check.getUpdated_date()!=null) {
				segment_join.setUpdated_by(check.getUpdated_by());
				segment_join.setUpdated_date(configGetDate.format(check.getUpdated_date()));
			}
		
			logger.info(method+"|"+url+"|"+"data segment");
			return new ResponseEntity(new ResponObject(1, "List Segment",segment_join),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/segment")
	public ResponseEntity<?> AddSegment(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) {
		
		
		String method = "POST";
		String url = "/segment";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api add segment");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("kode")==null || body.get("kode").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "kode null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "kode :"+body.get("kode"));
			logger.info(method+"|"+url+"|"+ "nama :"+body.get("nama"));
			
			Segment check = serviceSegment.findByKode(body.get("kode"));
			if(check != null) { return new ResponseEntity(new ResponObject(2, "kode telah terdaftar!"),HttpStatus.BAD_REQUEST); }
				
			Date today = new Date();
			Segment nmember = new Segment();
			User created = resp.getData();
			
			nmember.setKode_segment(body.get("kode").toUpperCase());
			nmember.setNama_segment(body.get("nama"));
			nmember.setCreated_by(created.getId_user());
			nmember.setCreated_date(today);
			this.serviceSegment.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success save segment");
			int respact = this.serviceAktivitas.createAktivitas("SEGMENT",created,method,url,nmember.getId_segment());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes add segment",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/segment/{id}")
	public ResponseEntity<?> updateSegment(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) {
		
		String method = "PUT";
		String url = "/segment/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api put user");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("kode")==null || body.get("kode").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "kode null"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "kode :"+body.get("kode"));
			logger.info(method+"|"+url+"|"+ "nama :"+body.get("nama"));
			
			Segment nmember = serviceSegment.findById(id);
			if(nmember == null ) { return new ResponseEntity(new ResponObject(2, "there is no segment"),HttpStatus.BAD_REQUEST); }
			
			if (!body.get("kode").contentEquals(nmember.getKode_segment())) {
				Segment chk = serviceSegment.findByKode(body.get("kode"));
				if (chk != null) {
					return new ResponseEntity(new ResponObject(2, "kode segment telah terpakai"),HttpStatus.BAD_REQUEST);
				}
			}
			
			Date today = new Date();
			User created = resp.getData();
			nmember.setUpdated_by(created.getId_user());
			nmember.setUpdated_date(today);
			
			nmember.setKode_segment(body.get("kode").toUpperCase());
			nmember.setNama_segment(body.get("nama"));	
			this.serviceSegment.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success update segment");
			int respact = this.serviceAktivitas.createAktivitas("SEGMENT",created,method,url,nmember.getId_segment());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update segment",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public List<SegmentJoin> joinSegment(List<Segment> data){
		
		List<SegmentJoin> resp = new ArrayList<SegmentJoin>();
//		List<Segment> dumy_mem = new ArrayList<Segment>();
		
		for (Segment check : data) {
			
			SegmentJoin segment_join = new SegmentJoin();
//			Segment mem = new Segment();
//			if(dumy_mem.size()>0) {
//				int loop = 0;
//				for(Segment a : dumy_mem) {
//					if(a.getId_user() == act.getId_user()) {
//						mem = a;
//						break;
//					} else if (loop == dumy_mem.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
//						mem = this.serviceSegment.findById(act.getId_user());
//						dumy_mem.add(mem);
//						break;
//					}	
//					loop++;
//				}
//			} else {
//				mem = this.serviceSegment.findById(act.getId_user());
//				dumy_mem.add(mem);
//			}
			
			segment_join.setId_segment(check.getId_segment());
			segment_join.setKode_segment(check.getKode_segment());
			segment_join.setNama_segment(check.getNama_segment());
			segment_join.setCreated_by(check.getCreated_by());
			segment_join.setCreated_date(configGetDate.format(check.getCreated_date()));
			
			if(check.getUpdated_date()!=null) {
				segment_join.setUpdated_by(check.getUpdated_by());
				segment_join.setUpdated_date(configGetDate.format(check.getUpdated_date()));
			}
			
			resp.add(segment_join);
		}
		return resp;
	}
	
}
