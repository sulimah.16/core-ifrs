package com.hanifalbaaits.java.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelDB.VasicekAsetDraft;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelRespon.DraftJoin;
import com.hanifalbaaits.java.service.VasicekAsetListService;
import com.hanifalbaaits.java.service.VasicekAsetDraftService;
import com.hanifalbaaits.java.utils.Encryption;

@RestController
@RequestMapping(value="core-ifrs")
public class VasicekAsetDraftCon {
	
	private Logger logger = LoggerFactory.getLogger(VasicekAsetDraftCon.class);

	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	VasicekAsetDraftService serviceDraft;
	
	@Autowired
	VasicekAsetListService serviceInput;
	
	@Autowired
	Encryption encrypt;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/vasicek/draft")
	public ResponseEntity<?> getDraft(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/vasicek/draft";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get master user");
			
			List<VasicekAsetDraft> data = serviceDraft.findAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data Draft");
				return new ResponseEntity(new ResponList(1, "empty Draft",data),HttpStatus.OK);
			}
			
			//List<VasicekAsetDraft> dataJoin = joinDraft(data);
			logger.info(method+"|"+url+"|"+"data draft");
			return new ResponseEntity(new ResponList(1, "List Draft",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/vasicek/draft/{id}")
	public ResponseEntity<?> getDraftID(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id
			) {
		
		String method = "GET";
		String url = "/vasicek/draft/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get master draft");
			
			VasicekAsetDraft check = this.serviceDraft.findById(id);
		
			if (check == null) {
				logger.info(method+"|"+url+"|"+"tidak ada data Draft");
				return new ResponseEntity(new ResponList(1, "empty Draft"),HttpStatus.OK);
			}
			
//			DraftJoin draft_join = new DraftJoin();
//			draft_join.setId_draft(check.getId_draft());
//			draft_join.setKode_draft(check.getKode_draft());
//			draft_join.setNama_draft(check.getNama_draft());
//			draft_join.setFlag_draft(check.getFlag_draft());
//			draft_join.setFlag_insert(check.getFlag_draft());
//			draft_join.setHeader_bucket(check.getHeader_bucket());
//	
//			draft_join.setCreated_by(check.getCreated_by());
//			draft_join.setCreated_date(configGetDate.format(check.getCreated_date()));
//			
//			if(check.getUpdated_date()!=null) {
//				draft_join.setUpdated_by(check.getUpdated_by());
//				draft_join.setUpdated_date(configGetDate.format(check.getUpdated_date()));
//			}
		
			logger.info(method+"|"+url+"|"+"data draft");
			return new ResponseEntity(new ResponObject(1, "List Draft",check),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/vasicek/draft")
	public ResponseEntity<?> AddDraft(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) {
		
		
		String method = "POST";
		String url = "/vasicek/draft";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api add draft");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "nama :"+body.get("nama"));
			
			VasicekAsetDraft check = serviceDraft.findLast();
			String kode = "DRA0001";
			if(check != null) {
				logger.info(method+"|"+url+"|"+ "draft terakhir : "+check.getKode_draft());
				String kd = check.getKode_draft().substring(3);
				int codenow = Integer.parseInt(kd) + 1;	
				int lcdn = String.valueOf(codenow).length();
				if(lcdn == 1) {kode = "DRA000"+String.valueOf(codenow);} 
				else if (lcdn ==2) { kode = "DRA00"+String.valueOf(codenow);}
				else if (lcdn ==3) { kode = "DRA0"+String.valueOf(codenow);}
				else { kode = "DRA"+String.valueOf(codenow);}
			}
				
			Date today = new Date();
			VasicekAsetDraft nmember = new VasicekAsetDraft();
			User created = resp.getData();
			nmember.setCreated_by(created.getId_user());
			nmember.setCreated_date(today);
					
			nmember.setKode_draft(kode);
			nmember.setNama_draft(body.get("nama"));
			this.serviceDraft.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success save draft");
			int respact = this.serviceAktivitas.createAktivitas("DRAFT ASET",created,method,url,nmember.getId_draft_vasicek());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes add draft",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/vasicek/draft/{id}")
	public ResponseEntity<?> updateDraft(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) {
		
		String method = "PUT";
		String url = "/vasicek/draft/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api put draft");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "nama :"+body.get("nama"));
			
			VasicekAsetDraft nmember = serviceDraft.findById(id);
			if(nmember == null ) { return new ResponseEntity(new ResponObject(2, "there is no draft"),HttpStatus.BAD_REQUEST); }
			
			Date today = new Date();
			User created = resp.getData();
			nmember.setUpdated_by(created.getId_user());
			nmember.setUpdated_date(today);
			nmember.setNama_draft(body.get("nama"));	
			this.serviceDraft.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success update draft");
			int respact = this.serviceAktivitas.createAktivitas("DRAFT ASET",created,method,url,nmember.getId_draft_vasicek());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update draft",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@DeleteMapping(value="/vasicek/draft/{id}")
	public ResponseEntity<?> deleteDraft(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) {
		
		String method = "DELETE";
		String url = "/vasicek/draft/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api delete draft");
			logger.info(method+"|"+url+"|"+"input data id: "+id);
			this.serviceDraft.delete(id);
			this.serviceInput.deleteDataInputByDraft(id);
			
			int respact = this.serviceAktivitas.createAktivitas("DRAFT ASET",resp.getData(),method,url,id);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			logger.info(method+"|"+url+"|"+"delete data draft");	
			return new ResponseEntity(new ResponObject(1, "delete Data Draft."),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
