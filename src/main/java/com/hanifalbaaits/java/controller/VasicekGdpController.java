package com.hanifalbaaits.java.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.hanifalbaaits.java.modelDB.UploadFile;
import com.hanifalbaaits.java.modelDB.VasicekBank;
import com.hanifalbaaits.java.modelDB.VasicekGDP;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelUtils.KoreksiImport;
import com.hanifalbaaits.java.modelUtils.KoreksiPesan;
import com.hanifalbaaits.java.service.UploadFileService;
import com.hanifalbaaits.java.service.UploadService;
import com.hanifalbaaits.java.service.VasicekGdpService;

@RestController
@RequestMapping(value="core-ifrs")
public class VasicekGdpController {
	
	private Logger logger = LoggerFactory.getLogger(VasicekGdpController.class);
	
	@Value("${file.path_input}")
	String path_file;

	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	VasicekGdpService serviceGdp;
	
	@Autowired
    UploadService uploadService;
	
	@Autowired
	UploadFileService serviceUpload;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/vas-gdp")
	public ResponseEntity<?> getGdp(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/vas-gdp";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get vasicek rating");
			
			List<VasicekGDP> data = serviceGdp.findAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data Gdp");
				return new ResponseEntity(new ResponList(1, "empty Gdp",data),HttpStatus.OK);
			}
			
			logger.info(method+"|"+url+"|"+"data user");
			return new ResponseEntity(new ResponList(1, "List Gdp",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/vas-gdp")
	public ResponseEntity<?> AddGdp(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) {
		
		
		String method = "POST";
		String url = "/vas-gdp";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api add gdp");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("bulan")==null || body.get("bulan").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "bulan null!"),HttpStatus.BAD_REQUEST);
			if (body.get("tahun")==null || body.get("tahun").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "tahun null!"),HttpStatus.BAD_REQUEST);
			if (body.get("value")==null || body.get("value").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "value null!"),HttpStatus.BAD_REQUEST);
			if (body.get("flag")==null || body.get("flag").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "flag null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "bulan :"+body.get("bulan"));
			logger.info(method+"|"+url+"|"+ "tahun :"+body.get("tahun"));
			logger.info(method+"|"+url+"|"+ "value :"+body.get("value"));
			logger.info(method+"|"+url+"|"+ "flag :"+body.get("flag"));
			
			VasicekGDP check = serviceGdp.findByBulanTahun(Integer.parseInt(body.get("bulan")), Integer.parseInt(body.get("tahun")));
			if(check != null) { return new ResponseEntity(new ResponObject(2, "Periode Bulan dan Tahun sudah terdaftar!"),HttpStatus.BAD_REQUEST); }
	
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String periode = "1/"+body.get("bulan")+"/"+body.get("tahun");
			Date dperiode = df.parse(periode);
			
			VasicekGDP nmember = new VasicekGDP();
			nmember.setPeriode(dperiode);
			nmember.setBulan(Integer.parseInt(body.get("bulan")));
			nmember.setTahun(Integer.parseInt(body.get("tahun")));
			nmember.setValue_gdp(Double.parseDouble(body.get("value")));
			nmember.setFlg_projection(Integer.parseInt(body.get("flag")));
			this.serviceGdp.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success save gdp");
			int respact = this.serviceAktivitas.createAktivitas("GDP",resp.getData(),method,url,nmember.getId_vas_gdp());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes add gdp",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/vas-gdp/{id}")
	public ResponseEntity<?> updateGdp(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) {
		
		String method = "PUT";
		String url = "/vas-gdp/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api put gdp");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			//if (body.get("bulan")==null || body.get("bulan").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "bulan null!"),HttpStatus.BAD_REQUEST);
			//if (body.get("tahun")==null || body.get("tahun").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "tahun null!"),HttpStatus.BAD_REQUEST);
			if (body.get("value")==null || body.get("value").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "value null!"),HttpStatus.BAD_REQUEST);
			if (body.get("flag")==null || body.get("flag").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "flag null!"),HttpStatus.BAD_REQUEST);
			
//			logger.info(method+"|"+url+"|"+ "bulan :"+body.get("bulan"));
//			logger.info(method+"|"+url+"|"+ "tahun :"+body.get("tahun"));
			logger.info(method+"|"+url+"|"+ "value :"+body.get("value"));
			logger.info(method+"|"+url+"|"+ "flag :"+body.get("flag"));
			
			VasicekGDP nmember = serviceGdp.findById(id);
			if(nmember == null ) { return new ResponseEntity(new ResponObject(2, "tidak adak gdp"),HttpStatus.BAD_REQUEST); }
			
//			VasicekGDP check = serviceGdp.findByBulanTahun(Integer.parseInt(body.get("bulan")), Integer.parseInt(body.get("tahun")));
//			if (check != null) {
//				return new ResponseEntity(new ResponObject(2, "Periode Bulan Tahun sudah ada"),HttpStatus.BAD_REQUEST);
//			}
			
			nmember.setValue_gdp(Double.parseDouble(body.get("value")));
			nmember.setFlg_projection(Integer.parseInt(body.get("flag")));
			this.serviceGdp.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success update GDP");
			int respact = this.serviceAktivitas.createAktivitas("GDP",resp.getData(),method,url,nmember.getId_vas_gdp());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update GDP",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@PostMapping(value="/vas-gdp/upload")
	public ResponseEntity<?> uploadFL(@RequestHeader("Authorization") String Authorization,
			@RequestParam("file") MultipartFile file
		) throws Exception, ParseException {
	
		String method = "POST";
		String url = "/vas-gdp/upload";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info("FILE original: "+file.getOriginalFilename()+" TYPE: "+file.getContentType());
		logger.info("FILE name: "+file.getName());
		
		//try untuk baca extensi file harus CSV
		String extension = "";
		try {
			String split [] = file.getOriginalFilename().split("\\.");
			extension = split[split.length-1];
			if(!extension.toLowerCase().contentEquals("csv")) {
				logger.info("validasi => File type must CSV");
					return new ResponseEntity(new ResponObject(2, "File type must CSV"),HttpStatus.BAD_REQUEST);	
				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		logger.info("Lolos validasi file csv");
	
		Date today = new Date();
		
		try {
			logger.info("UPLOAD FILE untuk INPUT");
			
			int lst = 1;
			UploadFile dmy = this.serviceUpload.findLast();
			if (dmy != null) {
				lst = dmy.getId_upload() + 1;
			}
			
			UploadFile nw = new UploadFile();
			nw.setTrue_name(file.getOriginalFilename());
			nw.setType_file(file.getContentType());
			nw.setSize_file((int) file.getSize());
			nw.setType_folder("input");
			nw.setTanggal_upload(today);
			nw.setId_user(resp.getData().getId_user());
				
			String filename = String.valueOf(lst)+today.getTime()+"_fl"+"."+extension;
			logger.info(method+"|"+url+"|"+"filename: "+filename);
			
			nw.setPath_name("input/"+filename);
			
			String uripath = uploadService.storeFile(file,filename,"input");		 
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
			.path("core-ifrs/view_file/input/")
			.path(uripath)
			.toUriString();
			
			this.serviceUpload.save(nw);
			logger.info(method+"|"+url+"|"+"success upload");
			
			logger.info("UPLOAD-FILE uripath: "+uripath);
			logger.info("UPLOAD-FILE fileDownloadUri: "+fileDownloadUri);
			
			KoreksiImport impor = new KoreksiImport();
			impor.setId_draft(0);
//			impor.setDraft(draft);
			impor.setId_segment(0);
//			impor.setSegment(ch_seg);
			impor.setCount_import(0);
			impor.setCount_success(0);
			impor.setCount_failed(0);
			impor.setTgl_import(today.toString());
			impor.setTitle("Import File Model FL");
			impor.setFile_upload(nw);
			
			List<KoreksiPesan> pesanKoreksi = new ArrayList<KoreksiPesan>();
			this.serviceGdp.truncate();
			
			try (BufferedReader br = new BufferedReader(new FileReader(path_file+"/"+uripath))) {
			    String line;
			  
			    //TEMPLATE: tanggal;idpiutang;kodecustomer;namacustomer;kodeproyek;namaproyek;pemberikerja,dll
			    int i = -1;
				
			    while ((line = br.readLine()) != null) {
			    	i++;
			    	String clean = line.replaceAll("\\P{Print}", ""); //ini perintah untuk remove unicode
			        String[] values = clean.split("\\;");
			        
			        if (i == 0) {
						continue;
					}
			       
			        logger.info("PERULANGAN KE- : "+i);
			        String nomor = String.valueOf(i);
			       
			    try {
			    	
			    	logger.info("Values length : "+values.length);
			        if (values.length != 3) {
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Jumlah field harus 2")); 
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			        
			        if (values[0] == null || values[0].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "tanggal periode tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[1] == null || values[1].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "gdp tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[2] == null || values[2].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "flag tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			       
			        String periode = values[0];
			        String gdp = values[1].replaceAll("\\,", ".");
			        String flag = values[2];
			    
			        logger.info("periode : "+periode);
					logger.info("gdp : "+gdp);
					logger.info("flag : "+flag);
							
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					Date dperiode = df.parse(periode);
					DateFormat formatYear = new SimpleDateFormat("yyyy");  
					DateFormat formatMonth = new SimpleDateFormat("MM");
					
					String bulan = formatMonth.format(dperiode);
					String tahun = formatYear.format(dperiode);
					
					logger.info(method+"|"+url+"|"+ "bulan :"+bulan);
					logger.info(method+"|"+url+"|"+ "tahun :"+tahun);
					
					VasicekGDP check = serviceGdp.findByBulanTahun(Integer.parseInt(bulan), Integer.parseInt(tahun));
					if(check != null) { 
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Periode Bulan dan Tahun sudah terdaftar!")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			
					VasicekGDP nmember = new VasicekGDP();
					nmember.setPeriode(dperiode);
					nmember.setBulan(Integer.parseInt(bulan));
					nmember.setTahun(Integer.parseInt(tahun));
					nmember.setValue_gdp(Double.parseDouble(gdp));
					nmember.setFlg_projection(Integer.parseInt(flag));
					this.serviceGdp.save(nmember);
					
					impor.setCount_import(impor.getCount_import()+1);
					impor.setCount_success(impor.getCount_success()+1);
					
			    	} catch (Exception e) {
						// TODO: handle exception
			    		logger.info(e.toString());
						logger.info(this.getClass().toGenericString(), e);	
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Pastikan sesuai format"));
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			    }
			    
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				impor.setList_pesan(pesanKoreksi);
				return new ResponseEntity(new ResponObject(3, e.getMessage(),impor),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			if (pesanKoreksi.size()> 0) {
				this.serviceGdp.truncate();
			}
			
			logger.info(method+"|"+url+"|"+"success save gdp");
			int respact = this.serviceAktivitas.createAktivitas("GDP",resp.getData(),method,url,0);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
		
			
			impor.setList_pesan(pesanKoreksi);
			return new ResponseEntity(new ResponObject(1, "Data Impor File Berhasil. Jika ada baris yang gagal. Upload Kembali",impor),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);	
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@DeleteMapping(value="/vas-gdp/{id}")
	public ResponseEntity<?> deleteGDP(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) {
		
		String method = "DELETE";
		String url = "/vas-gdp/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api delete gdp");
		
			VasicekGDP nmember = serviceGdp.findById(id);
			if (nmember == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada data gdp"),HttpStatus.BAD_REQUEST);
			}
			this.serviceGdp.delete(id);
			
			logger.info(method+"|"+url+"|"+"success delete bank");
			int respact = this.serviceAktivitas.createAktivitas("GDP",resp.getData(),method,url,nmember.getId_vas_gdp());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes delete GDP",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
