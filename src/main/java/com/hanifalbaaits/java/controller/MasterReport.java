package com.hanifalbaaits.java.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.DataInput;
import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelRespon.GetBucket;
import com.hanifalbaaits.java.modelRespon.GetBucketList;
import com.hanifalbaaits.java.modelRespon.GetBucketSolver;
import com.hanifalbaaits.java.modelRespon.GetBucketSolverHeader;
import com.hanifalbaaits.java.modelRespon.GetBucketSolverList;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.service.DataInputService;
import com.hanifalbaaits.java.service.DraftService;
import com.hanifalbaaits.java.service.SegmentService;
import com.hanifalbaaits.java.service.UserService;

@RestController
@RequestMapping(value="core-ifrs")
public class MasterReport {

	private Logger logger = LoggerFactory.getLogger(MasterReport.class);
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	UserService serviceUser;
	
	@Autowired
	DataInputService serviceInput;
	
	@Autowired
	DraftService serviceDraft;
	
	@Autowired
	SegmentService serviceSegment;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	//ARAGING / STEP 3
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/get-araging")
	public ResponseEntity<?> GetAraging(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/get-araging";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("month_first_bucket")==null || body.get("month_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_first_bucket null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("interval")==null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("jumlah_bucket")==null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
		
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		try {
			logger.info( "hit api get get araging");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				draft = new Draft();
				draft.setId_draft(0);
				
			} else if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
			
			Segment segment = this.serviceSegment.findById(id_segment);
			
			GetBucket bucket = new GetBucket();
			bucket.setTitle_bucket("AR-AGING");
			bucket.setDraft(draft);
			bucket.setSegment(segment);
			bucket.setNormalisasi("default");
			bucket.setStart_date(body.get("start_date"));
			bucket.setEnd_date(body.get("end_date"));
			bucket.setInterval_bucket(interval);
			bucket.setJumlah_bucket(jumlah_bucket);
			bucket.setHari_first_bucket(hari_first_bucket);
			if (draft.getId_draft() == 0) {
				bucket.setHeader_bucket(new ArrayList<String>());
			} else {
				bucket.setHeader_bucket(getHeader(draft.getFlag_draft(), jumlah_bucket, hari_first_bucket, interval, draft.getHeader_bucket()));
			}
			
			List<GetBucketList> fix = new ArrayList<GetBucketList>();
			if (list.size() == 0) {
				bucket.setList_bucket(fix);
				logger.info("success List Data Input Empty");
				return new ResponseEntity(new ResponObject(1, "List Data Input Empty",bucket),HttpStatus.OK);
			}
			
			bucket.setList_bucket(getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket()));
			
			logger.info("success List Data Input");
			return new ResponseEntity(new ResponObject(1, "List Data Input",bucket),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//ROLLRATE / STEP 4
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/get-rollrate")
	public ResponseEntity<?> GetRollRate(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/get-rollrate";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("month_first_bucket")==null || body.get("month_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_first_bucket null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("interval")==null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("jumlah_bucket")==null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
		
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		try {
			logger.info( "hit api get get araging");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				draft = new Draft();
				draft.setId_draft(0);
				
			} else if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
			
			Segment segment = this.serviceSegment.findById(id_segment);
			
			GetBucket bucket = new GetBucket();
			bucket.setTitle_bucket("ROLL RATE");
			bucket.setDraft(draft);
			bucket.setSegment(segment);
			bucket.setNormalisasi("default");
			bucket.setStart_date(body.get("start_date"));
			bucket.setEnd_date(body.get("end_date"));
			bucket.setInterval_bucket(interval);
			bucket.setJumlah_bucket(jumlah_bucket);
			bucket.setHari_first_bucket(hari_first_bucket);
			if (draft.getId_draft() == 0) {
				bucket.setHeader_bucket(new ArrayList<String>());
			} else {
				bucket.setHeader_bucket(getHeader(draft.getFlag_draft(), jumlah_bucket, hari_first_bucket, interval, draft.getHeader_bucket()));
			}
			
			List<GetBucketList> fix = new ArrayList<GetBucketList>();
			if (list.size() == 0) {
				bucket.setList_bucket(fix);
				logger.info("success List Data Input Empty");
				return new ResponseEntity(new ResponObject(1, "List Data Input Empty",bucket),HttpStatus.OK);
			}
			
			List<GetBucketList> araging = getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket());
			
			bucket.setList_bucket(getRollRate(araging, jumlah_bucket,0)); //0 belum normal , //1 normalisasi
			logger.info("success RollRate");
			return new ResponseEntity(new ResponObject(1, "List Roll Rate",bucket),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//NORMALISASI ROLLRATE / STEP 5
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/get-rollrate/normalisasi")
	public ResponseEntity<?> GetRollRateNormalisasi(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/get-rollrate/normalisasi";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("month_first_bucket")==null || body.get("month_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_first_bucket null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("interval")==null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("jumlah_bucket")==null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
		
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		try {
			logger.info( "hit api get get roll rate normalisasi");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				draft = new Draft();
				draft.setId_draft(0);
				
			} else if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
			
			Segment segment = this.serviceSegment.findById(id_segment);
			
			GetBucket bucket = new GetBucket();
			bucket.setTitle_bucket("ROLL RATE NORMALISASI");
			bucket.setDraft(draft);
			bucket.setSegment(segment);
			bucket.setNormalisasi("default");
			bucket.setStart_date(body.get("start_date"));
			bucket.setEnd_date(body.get("end_date"));
			bucket.setInterval_bucket(interval);
			bucket.setJumlah_bucket(jumlah_bucket);
			bucket.setHari_first_bucket(hari_first_bucket);
			if (draft.getId_draft() == 0) {
				bucket.setHeader_bucket(new ArrayList<String>());
			} else {
				bucket.setHeader_bucket(getHeader(draft.getFlag_draft(), jumlah_bucket, hari_first_bucket, interval, draft.getHeader_bucket()));
			}
			
			List<GetBucketList> fix = new ArrayList<GetBucketList>();
			if (list.size() == 0) {
				bucket.setList_bucket(fix);
				logger.info("success List Data Input Empty");
				return new ResponseEntity(new ResponObject(1, "List Data Input Empty",bucket),HttpStatus.OK);
			}
			
			List<GetBucketList> araging = getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket());
			bucket.setList_bucket(getRollRate(araging, jumlah_bucket,1)); //0 belum normal , //1 normalisasi
			
			logger.info("success RollRate");
			return new ResponseEntity(new ResponObject(1, "List Roll Rate",bucket),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//AVERAGE ROLLRATE & LOSSRATE / STEP 6&7
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/get-rollrate/average")
	public ResponseEntity<?> GetRollRateAverage(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/get-rollrate/average";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("month_first_bucket")==null || body.get("month_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_first_bucket null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("interval")==null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("jumlah_bucket")==null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
		
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		try {
			logger.info( "hit api get get roll rate average loss rate and roll rate");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				draft = new Draft();
				draft.setId_draft(0);
				
			} else if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
			
			Segment segment = this.serviceSegment.findById(id_segment);
			
			GetBucket bucket = new GetBucket();
			bucket.setTitle_bucket("ROLL RATE & LOSS RATE AVERAGE");
			bucket.setDraft(draft);
			bucket.setSegment(segment);
			bucket.setNormalisasi("default");
			bucket.setStart_date(body.get("start_date"));
			bucket.setEnd_date(body.get("end_date"));
			bucket.setInterval_bucket(interval);
			bucket.setJumlah_bucket(jumlah_bucket);
			bucket.setHari_first_bucket(hari_first_bucket);
			if (draft.getId_draft() == 0) {
				bucket.setHeader_bucket(new ArrayList<String>());
			} else {
				bucket.setHeader_bucket(getHeader(draft.getFlag_draft(), jumlah_bucket, hari_first_bucket, interval, draft.getHeader_bucket()));
			}
			
			List<GetBucketList> fix = new ArrayList<GetBucketList>();
			if (list.size() == 0) {
				bucket.setList_bucket(fix);
				logger.info("success List Data Input Empty");
				return new ResponseEntity(new ResponObject(1, "List Data Input Empty",bucket),HttpStatus.OK);
			}
			
			List<GetBucketList> araging = getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket());
			List<GetBucketList> rollrate = getRollRate(araging, jumlah_bucket,1);
			bucket.setList_bucket(getAverage(rollrate, jumlah_bucket));
			
			logger.info("success Average RollRate / LossRate");
			return new ResponseEntity(new ResponObject(1, "List Average RollRate / LossRate",bucket),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//Normalisasi By Average STEP8
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/get-rollrate/normalisasi/average")
	public ResponseEntity<?> GetRollRateNormalisasiByAverage(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/get-rollrate/normalisasi/average";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("month_first_bucket")==null || body.get("month_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_first_bucket null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("interval")==null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("jumlah_bucket")==null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
		
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		try {
			logger.info( "hit api get get normalisasi average");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				draft = new Draft();
				draft.setId_draft(0);
				
			} else if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
			
			Segment segment = this.serviceSegment.findById(id_segment);
			
			GetBucket bucket = new GetBucket();
			bucket.setTitle_bucket("NORMALISASI AVERAGE");
			bucket.setDraft(draft);
			bucket.setSegment(segment);
			bucket.setNormalisasi("average");
			bucket.setStart_date(body.get("start_date"));
			bucket.setEnd_date(body.get("end_date"));
			bucket.setInterval_bucket(interval);
			bucket.setJumlah_bucket(jumlah_bucket);
			bucket.setHari_first_bucket(hari_first_bucket);
			if (draft.getId_draft() == 0) {
				bucket.setHeader_bucket(new ArrayList<String>());
			} else {
				bucket.setHeader_bucket(getHeader(draft.getFlag_draft(), jumlah_bucket, hari_first_bucket, interval, draft.getHeader_bucket()));
			}
			
			List<GetBucketList> fix = new ArrayList<GetBucketList>();
			if (list.size() == 0) {
				bucket.setList_bucket(fix);
				logger.info("success List Data Input Empty");
				return new ResponseEntity(new ResponObject(1, "List Data Input Empty",bucket),HttpStatus.OK);
			}
			
			List<GetBucketList> araging = getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket());
			List<GetBucketList> rollrate_normal = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
			List<GetBucketList> average = getAverage(rollrate_normal, jumlah_bucket);
			bucket.setList_bucket(getRollRateByAverage(rollrate_normal, jumlah_bucket, average.get(0))); //get index 0 itu average rollrate kalo 1 average loss rate.
			
			logger.info("success List Roll Rate Normalisasi By Average");
			return new ResponseEntity(new ResponObject(1, "List Roll Rate Normalisasi By Average",bucket),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//LOSSRATE STEP 9 & 11 default / average
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/get-lossrate/{flag}")
	public ResponseEntity<?> GetLossRate(@RequestHeader("Authorization") String Authorization,
			@PathVariable String flag,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/get-lossrate/"+flag;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("month_first_bucket")==null || body.get("month_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_first_bucket null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("interval")==null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("jumlah_bucket")==null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
		
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		try {
			logger.info( "hit api get get loss rate");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				draft = new Draft();
				draft.setId_draft(0);
				
			} else if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
			
			Segment segment = this.serviceSegment.findById(id_segment);
			
			GetBucket bucket = new GetBucket();
			bucket.setTitle_bucket("LOSSRATE");
			bucket.setDraft(draft);
			bucket.setSegment(segment);
			bucket.setStart_date(body.get("start_date"));
			bucket.setEnd_date(body.get("end_date"));
			bucket.setInterval_bucket(interval);
			bucket.setJumlah_bucket(jumlah_bucket);
			bucket.setHari_first_bucket(hari_first_bucket);
			if (draft.getId_draft() == 0) {
				bucket.setHeader_bucket(new ArrayList<String>());
			} else {
				bucket.setHeader_bucket(getHeader(draft.getFlag_draft(), jumlah_bucket, hari_first_bucket, interval, draft.getHeader_bucket()));
			}
			
			List<GetBucketList> fix = new ArrayList<GetBucketList>();
			if (list.size() == 0) {
				bucket.setList_bucket(fix);
				logger.info("success List Data Input Empty");
				return new ResponseEntity(new ResponObject(1, "List Data Input Empty",bucket),HttpStatus.OK);
			}
			
			List<GetBucketList> araging = getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket());
			List<GetBucketList> rollrate = new ArrayList<GetBucketList>();
			
			if (flag.toLowerCase().contentEquals("default")) {
				bucket.setNormalisasi("default");
				rollrate = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
				
			} else {
				bucket.setNormalisasi("average");
				List<GetBucketList> rollrate_normal = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
				List<GetBucketList> average = getAverage(rollrate_normal, jumlah_bucket);
				rollrate = getRollRateByAverage(rollrate_normal, jumlah_bucket, average.get(0)); //get index 0 itu average rollrate kalo 1 average loss rate.	
			
			}
			
			bucket.setList_bucket(getLossRate(rollrate, jumlah_bucket)); 
			
			logger.info("success List Loss Rate");
			return new ResponseEntity(new ResponObject(1, "List Loss Rate",bucket),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//moving average STEP 10 & 11 default / average
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/get-lossrate/moving-average/{flag}/{titik}")
	public ResponseEntity<?> GetLossRateMovingAverage(@RequestHeader("Authorization") String Authorization,
			@PathVariable String flag,
			@PathVariable int titik,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/get-lossrate/moving-average/"+flag+"/"+titik;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("month_first_bucket")==null || body.get("month_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_first_bucket null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("interval")==null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("jumlah_bucket")==null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
		
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		try {
			logger.info( "hit api get get loss rate moving average");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				draft = new Draft();
				draft.setId_draft(0);
				
			} else if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
			
			Segment segment = this.serviceSegment.findById(id_segment);
			
			GetBucket bucket = new GetBucket();
			bucket.setTitle_bucket("LOSS RATE MOVING AVERAGE");
			bucket.setDraft(draft);
			bucket.setSegment(segment);
			bucket.setStart_date(body.get("start_date"));
			bucket.setEnd_date(body.get("end_date"));
			bucket.setInterval_bucket(interval);
			bucket.setJumlah_bucket(jumlah_bucket);
			bucket.setHari_first_bucket(hari_first_bucket);
			if (draft.getId_draft() == 0) {
				bucket.setHeader_bucket(new ArrayList<String>());
			} else {
				bucket.setHeader_bucket(getHeader(draft.getFlag_draft(), jumlah_bucket, hari_first_bucket, interval, draft.getHeader_bucket()));
			}
			
			List<GetBucketList> fix = new ArrayList<GetBucketList>();
			if (list.size() == 0) {
				bucket.setList_bucket(fix);
				logger.info("success List Data Input Empty");
				return new ResponseEntity(new ResponObject(1, "List Data Input Empty",bucket),HttpStatus.OK);
			}
			
			List<GetBucketList> araging = getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket());
			
			if (titik == 0 || titik > (araging.size() / 2 )) {
				logger.info("titik -> "+titik+" - size : "+araging.size());
				return new ResponseEntity(new ResponList(2, "Titik tidak boleh 0 dan tidak boleh lebih besar dari : "+araging.size()/2),HttpStatus.OK);
			}
			
			List<GetBucketList> rollrate = new ArrayList<GetBucketList>();
			
			if (flag.toLowerCase().contentEquals("default")) {
				bucket.setNormalisasi("default");
				rollrate = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
				
			} else {
				bucket.setNormalisasi("average");
				List<GetBucketList> rollrate_normal = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
				List<GetBucketList> average = getAverage(rollrate_normal, jumlah_bucket);
				rollrate = getRollRateByAverage(rollrate_normal, jumlah_bucket, average.get(0)); //get index 0 itu average rollrate kalo 1 average loss rate.	
			
			}
			
			
			List<GetBucketList> loss_rate = getLossRate(rollrate, jumlah_bucket);
			
			bucket.setList_bucket(getNormalisasiMoving(loss_rate,jumlah_bucket,titik));
			
			logger.info("success List Loss Rate");
			return new ResponseEntity(new ResponObject(1, "List Loss Rate",bucket),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//delta-lossrate default/average
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/get-lossrate/delta-lossrate/{flag}/{titik}")
	public ResponseEntity<?> GetDeltaLossRate(@RequestHeader("Authorization") String Authorization,
			@PathVariable String flag,
			@PathVariable int titik,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/get-lossrate/delta-lossrate/"+flag+"/"+titik;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("month_first_bucket")==null || body.get("month_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_first_bucket null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("interval")==null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("jumlah_bucket")==null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
		
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		try {
			logger.info( "hit api get get loss rate delta loss rate");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				draft = new Draft();
				draft.setId_draft(0);
				
			} else if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			logger.info( "FLAG  : "+flag);
			logger.info( "TITIK : "+titik);
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
			
			Segment segment = this.serviceSegment.findById(id_segment);
			
			GetBucket bucket = new GetBucket();
			bucket.setTitle_bucket("DELTA LOSSRATE");
			bucket.setDraft(draft);
			bucket.setSegment(segment);
			bucket.setStart_date(body.get("start_date"));
			bucket.setEnd_date(body.get("end_date"));
			bucket.setInterval_bucket(interval);
			bucket.setJumlah_bucket(jumlah_bucket);
			bucket.setHari_first_bucket(hari_first_bucket);
			if (draft.getId_draft() == 0) {
				bucket.setHeader_bucket(new ArrayList<String>());
			} else {
				bucket.setHeader_bucket(getHeader(draft.getFlag_draft(), jumlah_bucket, hari_first_bucket, interval, draft.getHeader_bucket()));
			}
			
			List<GetBucketList> fix = new ArrayList<GetBucketList>();
			if (list.size() == 0) {
				bucket.setList_bucket(fix);
				logger.info("success List Data Input Empty");
				return new ResponseEntity(new ResponObject(1, "List Data Input Empty",bucket),HttpStatus.OK);
			}
		
			List<GetBucketList> araging = getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket());
			
			if (titik == 0 || titik > (araging.size() / 2 )) {
				logger.info("titik -> "+titik+" - size : "+araging.size());
				return new ResponseEntity(new ResponList(2, "Titik tidak boleh 0 dan tidak boleh lebih besar dari : "+araging.size()/2),HttpStatus.OK);
			}
			
			List<GetBucketList> rollrate = new ArrayList<GetBucketList>();
			List<GetBucketList> average  = new ArrayList<GetBucketList>();
			
			if (flag.toLowerCase().contentEquals("default")) {
				bucket.setNormalisasi("default");
				rollrate = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
				average = getAverage(rollrate, jumlah_bucket);
				
			} else {
				bucket.setNormalisasi("average");
				List<GetBucketList> rollrate_normal = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
				average = getAverage(rollrate_normal, jumlah_bucket);
				rollrate = getRollRateByAverage(rollrate_normal, jumlah_bucket, average.get(0)); //get index 0 itu average rollrate kalo 1 average loss rate.	
			
			}
			
			
			List<GetBucketList> loss_rate = getLossRate(rollrate, jumlah_bucket);
			List<GetBucketList> normal_moving = getNormalisasiMoving(loss_rate, jumlah_bucket, titik);
			
			bucket.setList_bucket(getDeltaLossRate(normal_moving, jumlah_bucket, titik, average.get(1))); //average loss rate
			
			logger.info("success List Loss Rate");
			return new ResponseEntity(new ResponObject(1, "List Loss Rate",bucket),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/get-odr/{flag}/{titik}")
	public ResponseEntity<?> GetODR(@RequestHeader("Authorization") String Authorization,
			@PathVariable String flag,
			@PathVariable int titik,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/get-odr/"+flag+"/"+titik;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("month_first_bucket")==null || body.get("month_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_first_bucket null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("interval")==null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("jumlah_bucket")==null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
		
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		try {
			logger.info( "hit api get get odr");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				draft = new Draft();
				draft.setId_draft(0);
				
			} else if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			logger.info( "FLAG  : "+flag);
			logger.info( "TITIK : "+titik);
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
			
			Segment segment = this.serviceSegment.findById(id_segment);
			
			GetBucket bucket = new GetBucket();
			bucket.setTitle_bucket("ODR");
			bucket.setDraft(draft);
			bucket.setSegment(segment);
			bucket.setStart_date(body.get("start_date"));
			bucket.setEnd_date(body.get("end_date"));
			bucket.setInterval_bucket(interval);
			bucket.setJumlah_bucket(jumlah_bucket);
			bucket.setHari_first_bucket(hari_first_bucket);
			if (draft.getId_draft() == 0) {
				bucket.setHeader_bucket(new ArrayList<String>());
			} else {
				bucket.setHeader_bucket(getHeader(draft.getFlag_draft(), jumlah_bucket, hari_first_bucket, interval, draft.getHeader_bucket()));
			}
			
			List<GetBucketList> fix = new ArrayList<GetBucketList>();
			if (list.size() == 0) {
				bucket.setList_bucket(fix);
				logger.info("success List Data Input Empty");
				return new ResponseEntity(new ResponObject(1, "List Data Input Empty",bucket),HttpStatus.OK);
			}
			
			List<GetBucketList> araging = getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket());
			
			if (titik == 0 || titik > (araging.size() / 2 )) {
				logger.info("titik -> "+titik+" - size : "+araging.size());
				return new ResponseEntity(new ResponList(2, "Titik tidak boleh 0 dan tidak boleh lebih besar dari : "+araging.size()/2),HttpStatus.OK);
			}
			
			List<GetBucketList> rollrate = new ArrayList<GetBucketList>();
			
			if (flag.toLowerCase().contentEquals("default")) {
				bucket.setNormalisasi("default");
				rollrate = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
				
			} else {
				bucket.setNormalisasi("average");
				List<GetBucketList> rollrate_normal = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
				List<GetBucketList> average = getAverage(rollrate_normal, jumlah_bucket);
				rollrate = getRollRateByAverage(rollrate_normal, jumlah_bucket, average.get(0)); //get index 0 itu average rollrate kalo 1 average loss rate.	
			
			}
			
			List<GetBucketList> loss_rate = getLossRate(rollrate, jumlah_bucket);
			List<GetBucketList> normal_moving = getNormalisasiMoving(loss_rate, jumlah_bucket, titik);
			
			bucket.setList_bucket(getNilaiODR(normal_moving,jumlah_bucket, titik, araging));
			
			logger.info("success List Delta Loss Rate");
			return new ResponseEntity(new ResponObject(1, "List Loss Rate",bucket),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/report/get-solver/{flag}/{titik}")
	public ResponseEntity<?> GetSolver(@RequestHeader("Authorization") String Authorization,
			@PathVariable String flag,
			@PathVariable int titik,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/report/get-odr/"+flag+"/"+titik;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("month_first_bucket")==null || body.get("month_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_first_bucket null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("interval")==null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
//		if (body.get("jumlah_bucket")==null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
		
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		if (body.get("intersep")==null || body.get("intersep").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "intersep null!"),HttpStatus.BAD_REQUEST);
		if (body.get("gdp")==null || body.get("gdp").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "gdp null!"),HttpStatus.BAD_REQUEST);
		if (body.get("bi7")==null || body.get("bi7").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "bi7 null!"),HttpStatus.BAD_REQUEST);
		if (body.get("normal_gdp")==null || body.get("normal_gdp").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "normal_gdp null!"),HttpStatus.BAD_REQUEST);
		if (body.get("normal_bi7")==null || body.get("normal_bi7").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "normal_bi7 null!"),HttpStatus.BAD_REQUEST);
		if (body.get("normal_probability")==null || body.get("normal_probability").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "normal_probability null!"),HttpStatus.BAD_REQUEST);
		if (body.get("optimis_gdp")==null || body.get("optimis_gdp").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "optimis_gdp null!"),HttpStatus.BAD_REQUEST);
		if (body.get("optimis_bi7")==null || body.get("optimis_bi7").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "optimis_bi7 null!"),HttpStatus.BAD_REQUEST);
		if (body.get("optimis_probability")==null || body.get("optimis_probability").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "optimis_probability null!"),HttpStatus.BAD_REQUEST);
		if (body.get("pesimis_gdp")==null || body.get("pesimis_gdp").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "pesimis_gdp null!"),HttpStatus.BAD_REQUEST);
		if (body.get("pesimis_bi7")==null || body.get("pesimis_bi7").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "pesimis_bi7 null!"),HttpStatus.BAD_REQUEST);
		if (body.get("pesimis_probability")==null || body.get("pesimis_probability").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "pesimis_probability null!"),HttpStatus.BAD_REQUEST);
		if (body.get("default_lgd")==null || body.get("default_lgd").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "default_lgd null!"),HttpStatus.BAD_REQUEST);
		
		
		
		try {
			logger.info( "hit api get solver");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				draft = new Draft();
				draft.setId_draft(0);
				
			} else if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			Double intersep = Double.valueOf(body.get("intersep"));
			Double gdp = Double.valueOf(body.get("gdp"));
			Double bi7 = Double.valueOf(body.get("bi7"));
			Double normal_gdp = Double.valueOf(body.get("normal_gdp"));
			Double normal_bi7 = Double.valueOf(body.get("normal_bi7"));
			Double normal_probability = Double.valueOf(body.get("normal_probability"));
			Double optimis_gdp = Double.valueOf(body.get("optimis_gdp"));
			Double optimis_bi7 = Double.valueOf(body.get("optimis_bi7"));
			Double optimis_probability = Double.valueOf(body.get("optimis_probability"));
			Double pesimis_gdp = Double.valueOf(body.get("pesimis_gdp"));
			Double pesimis_bi7 = Double.valueOf(body.get("pesimis_bi7"));
			Double pesimis_probability = Double.valueOf(body.get("pesimis_probability"));
			Double default_lgd = Double.valueOf(body.get("default_lgd"));
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			logger.info( "FLAG  : "+flag);
			logger.info( "TITIK : "+titik);
			
			logger.info( "intersep : "+intersep);
			logger.info( "gdp : "+gdp);
			logger.info( "bi7 : "+bi7);
			logger.info( "normal_gdp : "+normal_gdp);
			logger.info( "normal_bi7 : "+normal_bi7);
			logger.info( "normal_probability : "+normal_probability);
			logger.info( "optimis_gdp : "+optimis_gdp);
			logger.info( "optimis_bi7 : "+optimis_bi7);
			logger.info( "optimis_probability : "+optimis_probability);
			logger.info( "pesimis_gdp : "+pesimis_gdp);
			logger.info( "pesimis_bi7 : "+pesimis_bi7);
			logger.info( "pesimis_probability : "+pesimis_probability);
			logger.info( "default_lgd : "+default_lgd);
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
		
			Segment segment = this.serviceSegment.findById(id_segment);
			
			GetBucketSolver bucket = new GetBucketSolver();
			bucket.setDraft(draft);
			bucket.setSegment(segment);
			bucket.setStart_date(body.get("start_date"));
			bucket.setEnd_date(body.get("end_date"));
			bucket.setInterval_bucket(interval);
			bucket.setJumlah_bucket(jumlah_bucket);
			bucket.setHari_first_bucket(hari_first_bucket);
			
			bucket.setIntersep(intersep);
			bucket.setGdp(gdp);
			bucket.setBi7(bi7);
			bucket.setNormal_gdp(normal_gdp);
			bucket.setNormal_bi7(normal_bi7);
			bucket.setNormal_probability(normal_probability);
			bucket.setOptimis_gdp(optimis_gdp);
			bucket.setOptimis_bi7(optimis_bi7);
			bucket.setOptimis_probability(optimis_probability);
			bucket.setPesimis_gdp(pesimis_gdp);
			bucket.setPesimis_bi7(pesimis_bi7);
			bucket.setPesimis_probability(pesimis_probability);
			bucket.setDefault_lgd(default_lgd);
			
			if (list.size() == 0) {
				logger.info("success List Delta Loss Rate Empty");
				return new ResponseEntity(new ResponObject(1, "List Delta Loss Rate Empty",bucket),HttpStatus.OK);
			}
			
			List<GetBucketList> araging = getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket());
			
			if (titik == 0 || titik > (araging.size() / 2 )) {
				logger.info("titik -> "+titik+" - size : "+araging.size());
				return new ResponseEntity(new ResponList(2, "Titik tidak boleh 0 dan tidak boleh lebih besar dari : "+araging.size()/2),HttpStatus.OK);
			}
			
			List<GetBucketList> rollrate = new ArrayList<GetBucketList>();
			
			if (flag.toLowerCase().contentEquals("default")) {
				
				rollrate = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
				
			} else {
				
			List<GetBucketList> rollrate_normal = getRollRate(araging, jumlah_bucket,1); //0 belum normal , //1 normalisasi
			List<GetBucketList> average = getAverage(rollrate_normal, jumlah_bucket);
			rollrate = getRollRateByAverage(rollrate_normal, jumlah_bucket, average.get(0)); //get index 0 itu average rollrate kalo 1 average loss rate.	
			
			}
			
			List<GetBucketList> loss_rate = getLossRate(rollrate, jumlah_bucket);
			List<GetBucketList> normal_moving = getNormalisasiMoving(loss_rate, jumlah_bucket, titik);
			List<GetBucketList> nilai_odr = getNilaiODR(normal_moving,jumlah_bucket, titik, araging);
			
			double mev_gdp = (normal_gdp*normal_probability)+(optimis_gdp*optimis_probability)+(pesimis_gdp*pesimis_probability);
			double mev_bi7 = (normal_bi7*normal_probability)+(optimis_bi7*optimis_probability)+(pesimis_bi7*pesimis_probability);
			bucket.setWeighted_mev_gdp(mev_gdp);
			bucket.setWeighted_mev_bi7(mev_bi7);
			
			double psak = intersep+(gdp*mev_gdp)+(bi7*mev_bi7);
			
			bucket.setSolver(getSolverProposional(default_lgd, psak, nilai_odr, jumlah_bucket, loss_rate, araging));
			//bucket.setSolver(getSolver(default_lgd, psak, nilai_odr, jumlah_bucket, loss_rate, araging));
			
			logger.info("success List Solver");
			return new ResponseEntity(new ResponObject(1, "List Loss Rate",bucket),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public List<String> getHeader(int flag, int jumlah_bucket, int count_bln,int interval,String head){
		
		List<String> header = new ArrayList<String>();
		
		if (flag == 1) {
			
			for (int i = 0; i < jumlah_bucket; i++) {
//				if (i == 0) {
//					header.add("< "+(count_bln)); // a <= 12
//				} else if (i == jumlah_bucket - 1) {
//					header.add("> "+count_bln);	// a > 12
//				} else {
//					header.add("> "+count_bln+" - "+(count_bln+interval)); // a > 12 && a <= 15
//					count_bln+= interval;
//				}
				
				//UPDATE HANIF
				if (i == 0) {
					if(count_bln == 0) {
						header.add(String.valueOf(count_bln)); // a <= 12
					} else {
						header.add("0 - "+(count_bln)); // a <= 12
					}
				} else if (i == jumlah_bucket - 1) {
					header.add("> "+count_bln);	// a > 12
				} else {
					header.add((1+count_bln)+" - "+(count_bln+interval)); // a > 12 && a <= 15
					count_bln+= interval;
				}
				
			}
			
		} else {
			
			JSONArray hed = new JSONArray(head);
			int heat [] = new int[jumlah_bucket];
			for (int i = 0; i < heat.length; i++) {
				heat[i] = hed.getInt(i);
			}
			
			for (int i = 0; i < jumlah_bucket; i++) {
//				if (i == 0) {
//					header.add("0 - "+heat[i]);
//				} else if (i == jumlah_bucket -1) {
//					header.add(heat[i-1]+" < ");
//				} else {
//					header.add(heat[i-1]+" - "+heat[i]);
//				}
				
				//UPDATE HANIF
				if (i == 0) {
					if(heat[i] == 0) {
						header.add("0");
					} else {
						header.add("0 - "+heat[i]);
					}
				} else if (i == jumlah_bucket -1) {
					header.add("> "+heat[i-1]);
				} else {
					header.add((heat[i-1]+1)+" - "+heat[i]);
				}
			}
		}
		
		return header;
	}
	public String getPeriode(int bln, int tahun) {
		if (bln == 1) {
			return "Jan "+tahun;
		} else if (bln == 2) {
			return "Feb "+tahun;
		} else if (bln == 3) {
			return "Mar "+tahun;
		} else if (bln == 4) {
			return "Apr "+tahun;
		} else if (bln == 5) {
			return "May "+tahun;
		} else if (bln == 6) {
			return "Jun "+tahun;
		} else if (bln == 7) {
			return "Jul "+tahun;
		} else if (bln == 8) {
			return "Aug "+tahun;
		} else if (bln == 9) {
			return "Sep "+tahun;
		} else if (bln == 10) {
			return "Oct "+tahun;
		} else if (bln == 11) {
			return "Nov "+tahun;
		} else {
			return "Dec "+tahun;
		}
	}
	public int getUmurPiutangBySegment(int index) {
		if (index == 0) {
			return 12;
		} else if (index == 1) {
			return 15;
		} else if (index == 2) {
			return 18;
		} else if (index == 3) {
			return 21;
		} else if (index == 4) {
			return 24;
		} else if (index == 5) {
			return 27;
		} else if (index == 6) {
			return 30;
		} else if (index == 7) {
			return 33;
		} else if (index == 8) {
			return 36;
		} else {
			return 39;
		}
	}
	public int getBulanfromDate(Date startDate, Date endDate) throws Exception {
		
		int bln = 0;
		
		try {
			
			Calendar start = Calendar.getInstance();
	        Calendar end = Calendar.getInstance();
	        
	        start.setTime(startDate);
	        end.setTime(endDate);
	        
	        int yearsInBetween = end.get(Calendar.YEAR) 
	                                - start.get(Calendar.YEAR);
	        int monthsDiff = end.get(Calendar.MONTH) 
	                                - start.get(Calendar.MONTH);
	        long ageInMonths = yearsInBetween*12 + monthsDiff;
	        long age = yearsInBetween;
	        
	        System.out.println("month : "+ ageInMonths);
	        System.out.println("age : " + age);
	        
	        bln = (int) ageInMonths;
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error get bln: "+e.getMessage());
		}
		
		return bln;
	}

	public List<GetBucketList> getAraging(List<DataInput> inputs,int id_segment,int first, int interval, int jumlah,int flag, String head){
		
		List<GetBucketList> resp = new ArrayList<GetBucketList>();
		
		if (flag == 1) {
			
			for (DataInput input : inputs) {
				
				if (input.getId_segment() == id_segment) {
					
					Double [] tes = new Double[jumlah];
					for (int bar = 0; bar < tes.length; bar++) {
						tes[bar] = 0.0;
					}
					
					GetBucketList bucket = new GetBucketList();
					bucket.setPeriode(getPeriode(input.getPeriode_bulan(), input.getPeriode_tahun()));
					bucket.setPeriode_bulan(input.getPeriode_bulan());
					bucket.setPeriode_tahun(input.getPeriode_tahun());
					bucket.setJumlah_outstanding(input.getSaldo_piutang());
					
					if (input.getUmur_piutang_hari() <= first) { // a <= 12
						
						tes[0] = tes[0] + input.getSaldo_piutang();
						
					} else if (input.getUmur_piutang_hari() > (first+(interval*(jumlah-2)))) {
						
						tes[jumlah-1] = tes[jumlah-1] + input.getSaldo_piutang();
						
					} else {
						
						int count_bln = first;
						for (int i = 1; i <= (jumlah-2); i++) {
							if (input.getUmur_piutang_hari() > count_bln && input.getUmur_piutang_hari() <= (count_bln+interval)) { // a > 12 && a <= 15
								tes[i] = tes[i] + input.getSaldo_piutang();
							}
							count_bln+= interval;
						}
					}	
					
					bucket.setNilai(tes);
					resp.add(bucket);
					
					if (resp.size() !=1 ) {
						if (bucket.getPeriode_bulan() == resp.get(resp.size()-2).getPeriode_bulan() 
							  && bucket.getPeriode_tahun() == resp.get(resp.size()-2).getPeriode_tahun()) {
							
							GetBucketList sebelum = resp.get(resp.size()-2);
							sebelum.setJumlah_outstanding(sebelum.getJumlah_outstanding()+bucket.getJumlah_outstanding());
							
							Double [] sebelum_n = sebelum.getNilai();
							for (int i = 0; i < sebelum_n.length; i++) {
								sebelum_n[i] = sebelum_n[i] + bucket.getNilai()[i];
							}
							sebelum.setNilai(sebelum_n);
							
							resp.remove(resp.size()-1);
						}
					}
					
				}
			}
			
		} else {
			
			JSONArray hed = new JSONArray(head);
			int heat [] = new int[jumlah];
			for (int i = 0; i < heat.length; i++) {
				heat[i] = hed.getInt(i);
			}
			
			for (DataInput input : inputs) {
				
				if (input.getId_segment() == id_segment) {
					
					Double [] tes = new Double[jumlah];
					for (int bar = 0; bar < tes.length; bar++) {
						tes[bar] = 0.0;
					}
					
					GetBucketList bucket = new GetBucketList();
					bucket.setPeriode(getPeriode(input.getPeriode_bulan(), input.getPeriode_tahun()));
					bucket.setPeriode_bulan(input.getPeriode_bulan());
					bucket.setPeriode_tahun(input.getPeriode_tahun());
					bucket.setJumlah_outstanding(input.getSaldo_piutang());
					
					if (input.getUmur_piutang_hari() <= heat[0]) { // a <= 12
						
						tes[0] = tes[0] + input.getSaldo_piutang();
						
					} else if (input.getUmur_piutang_hari() > heat[jumlah-2]) {
						
						tes[jumlah-1] = tes[jumlah-1] + input.getSaldo_piutang();
						
					} else {
						
						for (int i = 1; i <= (jumlah-2); i++) {
							if (input.getUmur_piutang_hari() > heat[i-1] && input.getUmur_piutang_hari() <= heat[i]) { // a > 12 && a <= 15
								tes[i] = tes[i] + input.getSaldo_piutang();
							}
						}
					}	
					
					bucket.setNilai(tes);
					resp.add(bucket);
					
					if (resp.size() !=1 ) {
						if (bucket.getPeriode_bulan() == resp.get(resp.size()-2).getPeriode_bulan() 
							  && bucket.getPeriode_tahun() == resp.get(resp.size()-2).getPeriode_tahun()) {
							
							GetBucketList sebelum = resp.get(resp.size()-2);
							sebelum.setJumlah_outstanding(sebelum.getJumlah_outstanding()+bucket.getJumlah_outstanding());
							
							Double [] sebelum_n = sebelum.getNilai();
							for (int i = 0; i < sebelum_n.length; i++) {
								sebelum_n[i] = sebelum_n[i] + bucket.getNilai()[i];
							}
							sebelum.setNilai(sebelum_n);
							
							resp.remove(resp.size()-1);
						}
					}
					
				}
			}
			
		}
		
		return resp;
	}
	
	public List<GetBucketList> getRollRate(List<GetBucketList> araging,int jumlah, int normal){
		
		List<GetBucketList> rollrate = new ArrayList<GetBucketList>();
		
		Double [] persen = new Double[jumlah];
		
		int i = 0;
		for (GetBucketList dmy : araging) {
			
			GetBucketList rl = new GetBucketList();
			rl.setPeriode(dmy.getPeriode());
			rl.setPeriode_bulan(dmy.getPeriode_bulan());
			rl.setPeriode_tahun(dmy.getPeriode_tahun());
			
			Double [] nilai = dmy.getNilai();
			persen = new Double[jumlah];
			for (int ii = 0; ii < persen.length; ii++) {
				persen[ii] = 0.0;
			}
			
			if (i == 0) {
				
				for (int j = 0; j < persen.length; j++) {
					persen[j] = 0.0;
				}
				
			} else {
				
				Double [] nilai_index_sebelum = araging.get(i - 1).getNilai();
				for (int j = 0; j < persen.length; j++) {
					
					if (j != (persen.length -1)) { //perulangan
						
						Double res = nilai[j+1] / nilai_index_sebelum[j];
						if (res.isNaN()) {
							persen[j] = 0.0;
						} else if (res.isInfinite() && normal == 0) {
							persen[j] = res;
						} else if (res.isInfinite() && normal != 0) {
							persen[j] = 0.0;
						} else {
							if (res > 1.0 && normal == 1) {
								persen[j] = 1.0;
							} else if(res <= 0.0 && normal == 1){
								persen[j] = 0.0;
							} else {
								persen[j] = res;
							}
						}
						
					} else {	//perulangan nilai terakhir
						persen[j] = 1.0;
					}		
				}	
			}
			rl.setNilai(persen);
			rollrate.add(rl);
			i++;
		}
		return rollrate;
		
	}
	
	public List<GetBucketList> getAverage(List<GetBucketList> rollrate,int jumlah){
		
		List<GetBucketList> average = new ArrayList<GetBucketList>();
		
		Double [] averageRol = new Double[jumlah];
		Double [] averageLos = new Double[jumlah];
		
		for (int i = 0; i < averageLos.length; i++) {
			averageRol[i] = 0.0; //karena penjumlahan
			averageLos[i] = 1.0; //karena perkalian
		}
	
		for (GetBucketList dmy : rollrate) {
			Double [] nilai = dmy.getNilai();
			for (int j = 0; j < nilai.length; j++) {
				averageRol[j] = averageRol[j] + nilai[j];
				System.out.println(averageRol[j]+" + "+nilai[j]);
			}
		}
		
//		System.out.println("JUMLAH TOT ROLLRATE: "+Arrays.toString(averageRol));
		Double sum_rol = 0.0;
		for (int i = 0; i < averageRol.length; i++) {
			averageRol[i] = averageRol[i] / (rollrate.size() - 1);
		}
		for (int i = 0; i < averageRol.length; i++) {
			sum_rol += averageRol[i];
		}
		
//		System.out.println("AVG ROLLRATE: "+Arrays.toString(averageRol));
		
		GetBucketList avgRol = new GetBucketList();
		avgRol.setPeriode("AVERAGE ROLL RATE");
		avgRol.setNilai(averageRol);
		avgRol.setSum(sum_rol);
		avgRol.setAverage(sum_rol/averageRol.length);
		average.add(avgRol);
		
		//DISINI AVERAGE YANG LOSS RATE
		
		int tot = averageRol.length;
		Double sum_los = 0.0;
        for (int i = 0; i < averageRol.length; i++) {		
        	for (int j = 0; j < tot; j++) {
				averageLos[i] *= averageRol[i+j];
			}
        	tot--;
		}
        
        for (int i = 0; i < averageLos.length; i++) {		
        	sum_los += averageLos[i];
		}
        
//        System.out.println("AVG LOSSRATE: "+Arrays.toString(averageLos));
		
		GetBucketList avgLoss = new GetBucketList();
		avgLoss.setPeriode("AVERAGE LOSS RATE");
		avgLoss.setNilai(averageLos);
		avgLoss.setSum(sum_los);
		avgLoss.setAverage(sum_los/averageLos.length);
		average.add(avgLoss);
		
		return average;
	}
	
	public List<GetBucketList> getRollRateByAverage(List<GetBucketList> rollrate_normal, int jumlah, GetBucketList average){
		
		List<GetBucketList> rollrateByAverage = new ArrayList<GetBucketList>();
		
		Double nilai_avg [] = average.getNilai();
		
		int loop = 0;
		for (GetBucketList rol_normal : rollrate_normal) {
			
			GetBucketList rol_avg = new GetBucketList();
			rol_avg.setPeriode(rol_normal.getPeriode());
			rol_avg.setPeriode_bulan(rol_normal.getPeriode_bulan());
			rol_avg.setPeriode_tahun(rol_normal.getPeriode_tahun());
			
			Double nilai [] = rol_normal.getNilai();
			
//			System.out.println(Arrays.toString(nilai));
			
			if (loop != 0) {
				for (int i = 0; i < nilai_avg.length; i++) {
					if (nilai[i] == 0) {
						nilai[i] = nilai_avg[i];
					} 
				}
			}
			
			rol_avg.setNilai(nilai);
			rollrateByAverage.add(rol_avg);
			loop++;
		}
		return rollrateByAverage;
		
	}

	public List<GetBucketList> getLossRate(List<GetBucketList> rollrate, int jumlah){
		
		List<GetBucketList> lossrate_list = new ArrayList<GetBucketList>();
		
		int loop = 0;
		for (GetBucketList roll : rollrate) {
			
			GetBucketList lossRate = new GetBucketList();
			lossRate.setPeriode(roll.getPeriode());
			lossRate.setPeriode_bulan(roll.getPeriode_bulan());
			lossRate.setPeriode_tahun(roll.getPeriode_tahun());
			
			Double nilai [] = roll.getNilai();
			Double [] averageLos = new Double[jumlah];
			
//			System.out.println(Arrays.toString(nilai));
			
			if (loop != 0) {
				for (int i = 0; i < averageLos.length; i++) {
					averageLos[i] = 1.0;
				}
				
				int tot = nilai.length;
		        for (int i = 0; i < nilai.length; i++) {		
		        	for (int j = 0; j < tot; j++) {
						averageLos[i] *= nilai[i+j];
//						System.out.println("averageLos["+i+"] * nilai[i+j "+nilai[i+j]+"] = "+averageLos[i]);
					}
		        	tot--;
				}
		       
			} else {
				for (int i = 0; i < averageLos.length; i++) {
					averageLos[i] = 0.0;
				}
			}
			
//			System.out.println(Arrays.toString(averageLos));
			
			//MENCARI AVERAGE DAN SUM
			Double sum = 0.0;
			
			for (int i = 0; i < averageLos.length; i++) {
				sum+= averageLos[i];
			}
	
			lossRate.setNilai(averageLos);
			lossRate.setSum(sum);
			lossRate.setAverage(sum/averageLos.length);
			lossrate_list.add(lossRate);
			loop++;
			
		}
		return lossrate_list;
		
	}

	public List<GetBucketList> getNormalisasiMoving(List<GetBucketList> lossrate, int jumlah, int titik){
		
		List<GetBucketList> NormalisasiMoving = new ArrayList<GetBucketList>();
		
		int loop = 0;
		for (GetBucketList loss : lossrate) {
			
			GetBucketList moving = new GetBucketList();
			moving.setPeriode(loss.getPeriode());
			moving.setPeriode_bulan(loss.getPeriode_bulan());
			moving.setPeriode_tahun(loss.getPeriode_tahun());
			
			Double nilai [] = new Double[jumlah];
			
			if (loop < titik) {
				for (int i = 0; i < jumlah; i++) {
					nilai[i] = 0.0;
				}
			
			} else {
				
				for (int i = 0; i < jumlah; i++) {
					
					Double dmy = 0.0;
					for (int j = 0; j < titik; j++) {
//						System.out.println("perulangan i: "+i);
//						System.out.println("perulangan j: "+j);
//						System.out.println("idex loss rate: "+((loop-titik+1)+j));
						GetBucketList dmy_a = lossrate.get((loop-titik+1)+j);
						dmy += dmy_a.getNilai()[i];
					}
					nilai[i] = dmy / titik;
				}
			}
			
			//MENCARI AVERAGE DAN SUM
			Double sum = 0.0;
			
			for (int i = 0; i < nilai.length; i++) {
				sum+= nilai[i];
			}
	
			moving.setNilai(nilai);
			moving.setSum(sum);
			moving.setAverage(sum/jumlah);
			NormalisasiMoving.add(moving);
			loop++;
		}
		return NormalisasiMoving;
		
	}
	
	public List<GetBucketList> getDeltaLossRate(List<GetBucketList> moving, int jumlah, int titik, GetBucketList average){
		
		List<GetBucketList> DeltaLoss = new ArrayList<GetBucketList>();
		
		Double [] avera = average.getNilai();
		
		System.out.println("AVERA: "+Arrays.toString(avera));
		
		int loop = 0;
		for (GetBucketList mov : moving) {
			
			GetBucketList delta = new GetBucketList();
			delta.setPeriode(mov.getPeriode());
			delta.setPeriode_bulan(mov.getPeriode_bulan());
			delta.setPeriode_tahun(mov.getPeriode_tahun());
			
			Double nilai [] = new Double[jumlah];
			
			if (loop < titik) {
				for (int i = 0; i < jumlah; i++) {
					nilai[i] = 0.0;
				}
			
			} else {
				
				Double [] dmy = mov.getNilai();
				System.out.println("Nilai: "+Arrays.toString(dmy));
				
				for (int i = 0; i < avera.length; i++) {
					nilai[i] = dmy[i] - avera[i];
					System.out.println("DMY["+i+"] ="+dmy[i]);
					System.out.println("AVERA["+i+"] ="+avera[i]);
				}
				
			}
			
			//MENCARI AVERAGE DAN SUM
			Double sum = 0.0;
			
			for (int i = 0; i < nilai.length; i++) {
				sum+= nilai[i];
			}
	
			delta.setNilai(nilai);
			delta.setSum(sum);
			delta.setAverage(sum/jumlah);
			DeltaLoss.add(delta);
			loop++;
		}
		return DeltaLoss;
		
	}
	
	public List<GetBucketList> getNilaiODR(List<GetBucketList> moving, int jumlah, int titik, List<GetBucketList> Araging){
		
		List<GetBucketList> DeltaLoss = new ArrayList<GetBucketList>();
		
		int loop = 0;
		for (GetBucketList mov : moving) {
			
			GetBucketList delta = new GetBucketList();
			delta.setPeriode(mov.getPeriode());
			delta.setPeriode_bulan(mov.getPeriode_bulan());
			delta.setPeriode_tahun(mov.getPeriode_tahun());
			
			Double nilai [] = new Double[jumlah];
			
			if (loop < titik) {
				for (int i = 0; i < jumlah; i++) {
					nilai[i] = 0.0;
				}
			
			} else {
				
				Double [] dmy = mov.getNilai();
				Double [] arag = Araging.get(loop).getNilai();
				System.out.println("Nilai: "+Arrays.toString(dmy));
				
				for (int i = 0; i < dmy.length; i++) {
					nilai[i] = dmy[i] * arag[i];
					System.out.println("DMY["+i+"] ="+dmy[i]);
					System.out.println("arag["+i+"] ="+arag[i]);
				}
				
			}
			
			//MENCARI AVERAGE DAN SUM
			Double sum = 0.0;
			
			for (int i = 0; i < nilai.length; i++) {
				sum+= nilai[i];
			}
	
			delta.setNilai(nilai);
			delta.setSum(sum);
			delta.setAverage(sum/jumlah);
			delta.setPersentase_odr(sum/Araging.get(loop).getJumlah_outstanding());
			DeltaLoss.add(delta);
			loop++;
		}
		return DeltaLoss;
		
	}
	
	public GetBucketSolverHeader getSolverProposional(double lgd, double psak, List<GetBucketList> odr, int jumlah_bucket, List<GetBucketList> lossrate,
			List<GetBucketList> araging) {
		
		GetBucketSolverHeader proporsional = new GetBucketSolverHeader();
		proporsional.setTitle_solver("Distribusi Penyesuaian PSAK71 proporsional");
		proporsional.setPsak71(psak);
		double last_odr = odr.get(odr.size()-1).getPersentase_odr();
		proporsional.setLast_odr(last_odr);
		proporsional.setMev_effect(psak/last_odr);
		
		String [] head = new String[7];
		head[0] = "PD Base";
		head[1] = "MEV Effect";
		head[2] = "PD Forward Looking";
		head[3] = "LGD";
		head[4] = "EAD";
		head[5] = "ECL Base";
		head[6] = "ECL Forward Looking";
		proporsional.setHeader(head);
		
		double [] sum = new double[jumlah_bucket];
		int loop = 0;
		for (GetBucketList losrat : lossrate) {
			Double [] d = losrat.getNilai();
			for (int i = 0; i < sum.length; i++) {
				sum[i] = sum[i] + d[i];
			}
			loop++;
		}
		System.out.println("LOOP: "+loop);
		
		List<GetBucketSolverList> solv = new ArrayList<GetBucketSolverList>();
		for (int i = 0; i < jumlah_bucket; i++) {
			GetBucketSolverList sol = new GetBucketSolverList();
			sol.setNama_bucket(String.valueOf(i+1));
			
			Double [] nilai = new Double[7];
			Double summ = sum[i]/(lossrate.size()-1);
			nilai[0] = summ;
			nilai[1] = psak/last_odr;
			Double pd_floking = 0.0;
			if (summ == 1) {
				pd_floking = 1.0;
			} else {
				pd_floking = (sum[i]/(lossrate.size()-1))*(psak/last_odr);
			}
			
			nilai[2] = pd_floking;
			nilai[3] = lgd;
			nilai[4] = araging.get(araging.size()-1).getNilai()[i];
			nilai[5] = (sum[i]/(lossrate.size()-1)) * lgd * (araging.get(araging.size()-1).getNilai()[i]);
			nilai[6] = pd_floking * lgd * (araging.get(araging.size()-1).getNilai()[i]);
			
			sol.setNilai(nilai);
			solv.add(sol);
		}
		
		GetBucketSolverList jumlah = new GetBucketSolverList();
		Double [] nilai = new Double[7];
		for (int i = 0; i < nilai.length; i++) {
			nilai[i] = 0.0;
		}
		
		
		for (GetBucketSolverList getb : solv) {
			
			Double [] nd = getb.getNilai();
			
			for (int i = 0; i < nilai.length; i++) {
				nilai[i] = nilai[i] + nd[i];
			}
		}
		jumlah.setNama_bucket("JUMLAH");
		jumlah.setNilai(nilai);
		solv.add(jumlah);
		proporsional.setList_solver(solv);
		return proporsional;
	
	}
	
	public GetBucketSolverHeader getSolver(double lgd, double psak, List<GetBucketList> odr, int jumlah_bucket, List<GetBucketList> lossrate,
			List<GetBucketList> araging) {
		
		GetBucketSolverHeader proporsional = new GetBucketSolverHeader();
		proporsional.setTitle_solver("Distribusi Penyesuaian PSAK71 menggunakan solver");
		
		
		double last_odr = odr.get(odr.size()-1).getPersentase_odr();
		proporsional.setPsak71(psak);
//		proporsional.setAgregat_pd_base(agregat_pd_base);
//		proporsional.setPd_forward_looking(pd_forward_looking);
		
		String [] head = new String[7];
		head[0] = "PD Base";
		head[1] = "Logit PD Ajusted";
		head[2] = "PD Forward Looking";
		head[3] = "LGD";
		head[4] = "EAD";
		head[5] = "ECL Base";
		head[6] = "ECL Forward Looking";
		proporsional.setHeader(head);
		
		double [] sum = new double[jumlah_bucket];
		int loop = 0;
		for (GetBucketList losrat : lossrate) {
			Double [] d = losrat.getNilai();
			for (int i = 0; i < sum.length; i++) {
				sum[i] = sum[i] + d[i];
			}
			loop++;
		}
		System.out.println("LOOP: "+loop);
		
		List<GetBucketSolverList> solv = new ArrayList<GetBucketSolverList>();
		for (int i = 0; i < jumlah_bucket; i++) {
			GetBucketSolverList sol = new GetBucketSolverList();
			sol.setNama_bucket(String.valueOf(i+1));
			
			Double [] nilai = new Double[7];
			
			nilai[0] = sum[i]/(lossrate.size()-1);
			nilai[1] = psak/last_odr;
			Double pd_floking = (sum[i]/(lossrate.size()-1))*(psak/last_odr);
			nilai[2] = pd_floking;
			nilai[3] = lgd;
			nilai[4] = araging.get(araging.size()-1).getNilai()[i];
			nilai[5] = (sum[i]/(lossrate.size()-1)) * lgd * (araging.get(araging.size()-1).getNilai()[i]);
			nilai[6] = pd_floking * lgd * (araging.get(araging.size()-1).getNilai()[i]);
			
			sol.setNilai(nilai);
			solv.add(sol);
		}
		
		GetBucketSolverList jumlah = new GetBucketSolverList();
		Double [] nilai = new Double[7];
		for (int i = 0; i < nilai.length; i++) {
			nilai[i] = 0.0;
		}
		
		for (GetBucketSolverList getb : solv) {
			for (int i = 0; i < nilai.length; i++) {
				nilai[i] += nilai[i] + getb.getNilai()[i];
			}
		}
		jumlah.setNama_bucket("JUMLAH");
		jumlah.setNilai(nilai);
		
		proporsional.setList_solver(solv);
		return proporsional;
	
	}
}
