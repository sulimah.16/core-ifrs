package com.hanifalbaaits.java.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.DataInput;
import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.DraftBucket;
import com.hanifalbaaits.java.modelDB.DraftECL;
import com.hanifalbaaits.java.modelDB.Komentar;
import com.hanifalbaaits.java.modelDB.NilaiBucket;
import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelRespon.DraftBucketJ;
import com.hanifalbaaits.java.modelRespon.DraftBucketJoin;
import com.hanifalbaaits.java.modelRespon.GetBucket;
import com.hanifalbaaits.java.modelRespon.GetBucketList;
import com.hanifalbaaits.java.modelRespon.KomentarJoin;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.service.DataInputService;
import com.hanifalbaaits.java.service.DraftBucketService;
import com.hanifalbaaits.java.service.DraftEclService;
import com.hanifalbaaits.java.service.DraftService;
import com.hanifalbaaits.java.service.KomentarService;
import com.hanifalbaaits.java.service.SaveECLService;
import com.hanifalbaaits.java.service.SegmentService;
import com.hanifalbaaits.java.service.UserService;

@RestController
@RequestMapping(value="core-ifrs")
public class MasterDraftBucket {

	private Logger logger = LoggerFactory.getLogger(MasterDraftBucket.class);
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	UserService serviceUser;
	
	@Autowired
	DataInputService serviceInput;
	
	@Autowired
	DraftService serviceDraft;
	
	@Autowired
	SegmentService serviceSegment;
	
	@Autowired
	KomentarService serviceKomentar;
	
	@Autowired
	DraftBucketService serviceBucketService;
	
	@Autowired
	MasterReport serviceReport;
	
	@Autowired
	SaveECLService serviceSaveECL;
	
	@Autowired
	DraftEclService serviceECL;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/draft-bucket/{flag}/{titik}")
	public ResponseEntity<?> CreateDraftBucket(@RequestHeader("Authorization") String Authorization,
			@PathVariable String flag,
			@PathVariable int titik,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/draft-bucket/"+flag+"/"+titik;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
		if (body.get("start_date")==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		if (!flag.contentEquals("default") && !flag.contentEquals("average")) {
			return new ResponseEntity(new ResponObject(2, "flag hanya ada default dan average"),HttpStatus.BAD_REQUEST);
		}
		
		try {
			logger.info( "hit api create draft_bucket");

			int id_draft = Integer.parseInt(body.get("id_draft"));
			int id_segment = Integer.parseInt(body.get("id_segment"));
			
			String nama_bucket = "";
			int hari_first_bucket = 0;
			int interval = 0;
			int jumlah_bucket = 0;
			
			Draft draft = this.serviceDraft.findById(id_draft);
			
			if (draft == null) {
				
				logger.info( "draft dengan id tersebut tidak ada");
				return new ResponseEntity(new ResponObject(2, "draft tidak ada"),HttpStatus.BAD_REQUEST);
				
			} 
			
			if (draft.getFlag_draft() == 2) {
				
				jumlah_bucket = draft.getJumlah_bucket();
				
			} else {
				
				if (body.get("hari_first_bucket")==null || body.get("hari_first_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "hari_first_bucket null!"),HttpStatus.BAD_REQUEST);
				if (body.get("interval") == null || body.get("interval").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
				if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null!"),HttpStatus.BAD_REQUEST);
				
				hari_first_bucket = Integer.parseInt(body.get("hari_first_bucket"));
				interval = Integer.parseInt(body.get("interval"));
				jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
				
				if (jumlah_bucket < 3) {
					logger.info("minimal bucket 3");
					return new ResponseEntity(new ResponList(1, "minimal bucket 3"),HttpStatus.OK);
				}
				if (interval == 0) {
					logger.info("interval tidak boleh 0");
					return new ResponseEntity(new ResponList(1, "interval tidak boleh 0"),HttpStatus.OK);
				}
			}
			
			Segment segment = this.serviceSegment.findById(id_segment);
			
			if (segment == null) {
				
				logger.info( "segment dengan id tersebut tidak ada");
				return new ResponseEntity(new ResponObject(2, "segment tidak ada"),HttpStatus.BAD_REQUEST);
				
			}
			
			logger.info( "id_draft : "+id_draft);
			logger.info( "FLAG draft : "+draft.getFlag_draft());
			logger.info( "FLAG insert : "+draft.getFlag_insert());
			logger.info( "id_segment : "+id_segment);
			logger.info( "hari_first_bucket : "+hari_first_bucket);
			logger.info( "interval : "+interval);
			logger.info( "jumlah bucket : "+jumlah_bucket);
			logger.info( "start_date : "+body.get("start_date"));
			logger.info( "end_date : "+body.get("end_date"));
			logger.info( "FLAG  : "+flag);
			logger.info( "TITIK : "+titik);
			
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = new Date();
			Date date2 = new Date();
			Date today = new Date();
			
			try {
				date1 = df.parse(body.get("start_date").trim()+" 00:00");
				date2 = df.parse(body.get("end_date").trim()+" 23:59");
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
				logger.error("PARSE EXCEPTION TANGGAL STARTDATE - ENDDATE");
			}
			
			if (body.get("start_date") == "" || body.get("end_date") == "") {
				list = serviceInput.findByDraft(id_draft);
			} else {
				list = serviceInput.findByDraftPeriode(id_draft,date1, date2);
			}
			
			if (list.size() == 0) {
				logger.info("success List Data Input Empty");
				return new ResponseEntity(new ResponObject(2, "List Data Input Empty"),HttpStatus.OK);
			}
			
			DraftBucket check = serviceBucketService.findLast();
			String kode = "BUC0001";
			if(check != null) {
				logger.info(method+"|"+url+"|"+ "draft bucket terakhir : "+check.getKode_draft_bucket());
				String kd = check.getKode_draft_bucket().substring(3);
				int codenow = Integer.parseInt(kd) + 1;	
				int lcdn = String.valueOf(codenow).length();
				if(lcdn == 1) {kode = "BUC000"+String.valueOf(codenow);} 
				else if (lcdn ==2) { kode = "BUC00"+String.valueOf(codenow);}
				else if (lcdn ==3) { kode = "BUC0"+String.valueOf(codenow);}
				else { kode = "BUC"+String.valueOf(codenow);}
			}
			

			if (body.get("nama_bucket") != null && !body.get("nama_bucket").isEmpty()) {
				nama_bucket = body.get("nama_bucket");
			} else {
				nama_bucket = kode;
			}
			
			DraftBucket drabu = new DraftBucket();
			drabu.setId_draft(id_draft);
			drabu.setId_segment(id_segment);
			drabu.setCreated_by(resp.getData().getId_user());
			drabu.setCreated_date(today);
			
			Calendar cals = Calendar.getInstance();
			cals.setTime(list.get(0).getTanggal_piutang());
			logger.info("Calendar stardate "+cals.toString());
			logger.info("Calendar stardate "+cals.getTime());
			cals.set(Calendar.DATE, 1);
			logger.info("Calendar stardate day 1"+cals.toString());
			logger.info("Calendar stardate day 1"+cals.getTime());
			drabu.setStart_date(cals.getTime());
			
			Calendar cale = Calendar.getInstance();
			cale.setTime(list.get(list.size()-1).getTanggal_piutang());
			logger.info("Calendar stardate "+cals.toString());
			logger.info("Calendar stardate "+cals.getTime());
			cale.set(Calendar.DATE, cale.getActualMaximum(Calendar.DATE));
			logger.info("Calendar stardate last day"+cals.toString());
			logger.info("Calendar stardate last day"+cals.getTime());
			drabu.setEnd_date(cale.getTime());
			drabu.setHari_first(hari_first_bucket);
			drabu.setInterval_bucket(interval);
			drabu.setJumlah_bucket(jumlah_bucket);
			drabu.setKode_draft_bucket(kode);
			drabu.setNama_draft_bucket(nama_bucket);
			drabu.setMoving_average(titik);
			
			List<GetBucketList> araging = serviceReport.getAraging(list, id_segment, hari_first_bucket, interval, jumlah_bucket, draft.getFlag_draft(),draft.getHeader_bucket());
			
			if (titik == 0 || titik > (araging.size() / 2 )) {
				logger.info("titik -> "+titik+" - size : "+araging.size());
				return new ResponseEntity(new ResponList(2, "Titik tidak boleh 0 dan tidak boleh lebih besar dari : "+(araging.size()/2)),HttpStatus.OK);
			}
			
			List<GetBucketList> rollrate_belum_normal = serviceReport.getRollRate(araging, jumlah_bucket,0);
			List<GetBucketList> rollrate_normal = serviceReport.getRollRate(araging, jumlah_bucket,1);
					
			List<GetBucketList> average = new ArrayList<GetBucketList>();
			List<GetBucketList> rollrate_normalisasi = new ArrayList<GetBucketList>();
			List<GetBucketList> loss_rate = new ArrayList<GetBucketList>();
			
			if (flag.toLowerCase().contentEquals("default")) {
				drabu.setFlag_normalisasi("default");
				average = serviceReport.getAverage(rollrate_normal, jumlah_bucket);	
				loss_rate = serviceReport.getLossRate(rollrate_normal, jumlah_bucket);
			} else {
				drabu.setFlag_normalisasi("average");
				average = serviceReport.getAverage(rollrate_normal, jumlah_bucket);
				rollrate_normalisasi = serviceReport.getRollRateByAverage(rollrate_normal, jumlah_bucket, average.get(0)); //get index 0 itu average rollrate kalo 1 average loss rate.	
				loss_rate = serviceReport.getLossRate(rollrate_normalisasi, jumlah_bucket);
			}
			
			List<GetBucketList> normal_moving = serviceReport.getNormalisasiMoving(loss_rate, jumlah_bucket, titik);
			List<GetBucketList> delta_loss = serviceReport.getDeltaLossRate(normal_moving, jumlah_bucket, titik, average.get(1));
			List<GetBucketList> odr = serviceReport.getNilaiODR(normal_moving,jumlah_bucket, titik, araging);
			
			this.serviceBucketService.save(drabu);
			
			for (int i = 0; i < 9; i++) {
				int step = 0;
				List<GetBucketList> list_data = new ArrayList<GetBucketList>();
				if (i == 0) {
					list_data = araging;
					step = 3;
				} else if (i == 1) {
					list_data = rollrate_belum_normal;
					step = 4;
				} else if (i == 2) {
					list_data = rollrate_normal;
					step = 5;
				} else if (i == 3) {
					list_data = rollrate_normalisasi;
					step = 8;
				} else if (i == 4) {
					list_data = average;
					step = 67;
				} else if (i == 5) {
					list_data = loss_rate;
					step = 9;
				} else if (i == 6) {
					list_data = normal_moving;
					step = 10;
				} else if (i == 7) {
					list_data = delta_loss;
					step = 12;
				} else {
					list_data = odr;
					step = 14;
				}
				
				for (GetBucketList dt : list_data) {
					
					NilaiBucket nil = new NilaiBucket();
					nil.setId_draft_bucket(drabu.getId_draft_bucket());
					nil.setPeriode(dt.getPeriode());
					nil.setPeriode_bulan(dt.getPeriode_bulan());
					nil.setPeriode_tahun(dt.getPeriode_tahun());
					nil.setId_step(step);
					nil.setNilai(Arrays.toString(dt.getNilai()));
					if (dt.getJumlah_outstanding() != null) { nil.setJumlah_outstanding(dt.getJumlah_outstanding()); }
					if (dt.getAverage() != null) { nil.setAverage(dt.getAverage()); }
					if (dt.getSum() != null) { nil.setSum(dt.getSum()); }
					if (dt.getPersentase_odr() != null) { nil.setPersentase_odr(dt.getPersentase_odr()); }
					this.serviceBucketService.saveNilai(nil);
				}
				
				
			}
			
//			for (GetBucketList dt : araging) {
//				
//				NilaiBucket nil = new NilaiBucket();
//				nil.setId_draft_bucket(drabu.getId_draft_bucket());
//				nil.setPeriode(dt.getPeriode());
//				nil.setPeriode_bulan(dt.getPeriode_bulan());
//				nil.setPeriode_tahun(dt.getPeriode_tahun());
//				nil.setId_step(3);
//				nil.setNilai(Arrays.toString(dt.getNilai()));
//				if (dt.getJumlah_outstanding() != null) { nil.setJumlah_outstanding(dt.getJumlah_outstanding()); }
//				if (dt.getAverage() != null) { nil.setAverage(dt.getAverage()); }
//				if (dt.getSum() != null) { nil.setSum(dt.getSum()); }
//				if (dt.getPersentase_odr() != null) { nil.setPersentase_odr(dt.getPersentase_odr()); }
//				this.serviceBucketService.saveNilai(nil);
//			}
//			
//			for (GetBucketList dt : rollrate_belum_normal) {
//				
//				NilaiBucket nil = new NilaiBucket();
//				nil.setId_draft_bucket(drabu.getId_draft_bucket());
//				nil.setPeriode(dt.getPeriode());
//				nil.setPeriode_bulan(dt.getPeriode_bulan());
//				nil.setPeriode_tahun(dt.getPeriode_tahun());
//				nil.setId_step(4);
//				nil.setNilai(Arrays.toString(dt.getNilai()));
//				if (dt.getJumlah_outstanding() != null) { nil.setJumlah_outstanding(dt.getJumlah_outstanding()); }
//				if (dt.getAverage() != null) { nil.setAverage(dt.getAverage()); }
//				if (dt.getSum() != null) { nil.setSum(dt.getSum()); }
//				if (dt.getPersentase_odr() != null) { nil.setPersentase_odr(dt.getPersentase_odr()); }
//				this.serviceBucketService.saveNilai(nil);
//			}
//			
//			for (GetBucketList dt : rollrate_normal) {
//				
//				NilaiBucket nil = new NilaiBucket();
//				nil.setId_draft_bucket(drabu.getId_draft_bucket());
//				nil.setPeriode(dt.getPeriode());
//				nil.setPeriode_bulan(dt.getPeriode_bulan());
//				nil.setPeriode_tahun(dt.getPeriode_tahun());
//				nil.setId_step(5);
//				nil.setNilai(Arrays.toString(dt.getNilai()));
//				if (dt.getJumlah_outstanding() != null) { nil.setJumlah_outstanding(dt.getJumlah_outstanding()); }
//				if (dt.getAverage() != null) { nil.setAverage(dt.getAverage()); }
//				if (dt.getSum() != null) { nil.setSum(dt.getSum()); }
//				if (dt.getPersentase_odr() != null) { nil.setPersentase_odr(dt.getPersentase_odr()); }
//				this.serviceBucketService.saveNilai(nil);
//			}
//			
//			for (GetBucketList dt : rollrate_normalisasi) {
//				
//				NilaiBucket nil = new NilaiBucket();
//				nil.setId_draft_bucket(drabu.getId_draft_bucket());
//				nil.setPeriode(dt.getPeriode());
//				nil.setPeriode_bulan(dt.getPeriode_bulan());
//				nil.setPeriode_tahun(dt.getPeriode_tahun());
//				nil.setId_step(8);
//				nil.setNilai(Arrays.toString(dt.getNilai()));
//				if (dt.getJumlah_outstanding() != null) { nil.setJumlah_outstanding(dt.getJumlah_outstanding()); }
//				if (dt.getAverage() != null) { nil.setAverage(dt.getAverage()); }
//				if (dt.getSum() != null) { nil.setSum(dt.getSum()); }
//				if (dt.getPersentase_odr() != null) { nil.setPersentase_odr(dt.getPersentase_odr()); }
//				this.serviceBucketService.saveNilai(nil);
//			}
//			
//			for (GetBucketList dt : average) {
//				
//				NilaiBucket nil = new NilaiBucket();
//				nil.setId_draft_bucket(drabu.getId_draft_bucket());
//				nil.setPeriode(dt.getPeriode());
//				nil.setPeriode_bulan(dt.getPeriode_bulan());
//				nil.setPeriode_tahun(dt.getPeriode_tahun());
//				nil.setId_step(67);
//				nil.setNilai(Arrays.toString(dt.getNilai()));
//				if (dt.getJumlah_outstanding() != null) { nil.setJumlah_outstanding(dt.getJumlah_outstanding()); }
//				if (dt.getAverage() != null) { nil.setAverage(dt.getAverage()); }
//				if (dt.getSum() != null) { nil.setSum(dt.getSum()); }
//				if (dt.getPersentase_odr() != null) { nil.setPersentase_odr(dt.getPersentase_odr()); }
//				this.serviceBucketService.saveNilai(nil);
//			}
//			
//			for (GetBucketList dt : loss_rate) {
//				
//				NilaiBucket nil = new NilaiBucket();
//				nil.setId_draft_bucket(drabu.getId_draft_bucket());
//				nil.setPeriode(dt.getPeriode());
//				nil.setPeriode_bulan(dt.getPeriode_bulan());
//				nil.setPeriode_tahun(dt.getPeriode_tahun());
//				nil.setId_step(9);
//				nil.setNilai(Arrays.toString(dt.getNilai()));
//				if (dt.getJumlah_outstanding() != null) { nil.setJumlah_outstanding(dt.getJumlah_outstanding()); }
//				if (dt.getAverage() != null) { nil.setAverage(dt.getAverage()); }
//				if (dt.getSum() != null) { nil.setSum(dt.getSum()); }
//				if (dt.getPersentase_odr() != null) { nil.setPersentase_odr(dt.getPersentase_odr()); }
//				this.serviceBucketService.saveNilai(nil);
//			}
//			
//			for (GetBucketList dt : normal_moving) {
//				
//				NilaiBucket nil = new NilaiBucket();
//				nil.setId_draft_bucket(drabu.getId_draft_bucket());
//				nil.setPeriode(dt.getPeriode());
//				nil.setPeriode_bulan(dt.getPeriode_bulan());
//				nil.setPeriode_tahun(dt.getPeriode_tahun());
//				nil.setId_step(10);
//				nil.setNilai(Arrays.toString(dt.getNilai()));
//				if (dt.getJumlah_outstanding() != null) { nil.setJumlah_outstanding(dt.getJumlah_outstanding()); }
//				if (dt.getAverage() != null) { nil.setAverage(dt.getAverage()); }
//				if (dt.getSum() != null) { nil.setSum(dt.getSum()); }
//				if (dt.getPersentase_odr() != null) { nil.setPersentase_odr(dt.getPersentase_odr()); }
//				this.serviceBucketService.saveNilai(nil);
//			}
//			
//			for (GetBucketList dt : delta_loss) {
//				
//				NilaiBucket nil = new NilaiBucket();
//				nil.setId_draft_bucket(drabu.getId_draft_bucket());
//				nil.setPeriode(dt.getPeriode());
//				nil.setPeriode_bulan(dt.getPeriode_bulan());
//				nil.setPeriode_tahun(dt.getPeriode_tahun());
//				nil.setId_step(12);
//				nil.setNilai(Arrays.toString(dt.getNilai()));
//				if (dt.getJumlah_outstanding() != null) { nil.setJumlah_outstanding(dt.getJumlah_outstanding()); }
//				if (dt.getAverage() != null) { nil.setAverage(dt.getAverage()); }
//				if (dt.getSum() != null) { nil.setSum(dt.getSum()); }
//				if (dt.getPersentase_odr() != null) { nil.setPersentase_odr(dt.getPersentase_odr()); }
//				this.serviceBucketService.saveNilai(nil);
//			}
//			
//			for (GetBucketList dt : odr) {
//				
//				NilaiBucket nil = new NilaiBucket();
//				nil.setId_draft_bucket(drabu.getId_draft_bucket());
//				nil.setPeriode(dt.getPeriode());
//				nil.setPeriode_bulan(dt.getPeriode_bulan());
//				nil.setPeriode_tahun(dt.getPeriode_tahun());
//				nil.setId_step(14);
//				nil.setNilai(Arrays.toString(dt.getNilai()));
//				if (dt.getJumlah_outstanding() != null) { nil.setJumlah_outstanding(dt.getJumlah_outstanding()); }
//				if (dt.getAverage() != null) { nil.setAverage(dt.getAverage()); }
//				if (dt.getSum() != null) { nil.setSum(dt.getSum()); }
//				if (dt.getPersentase_odr() != null) { nil.setPersentase_odr(dt.getPersentase_odr()); }
//				this.serviceBucketService.saveNilai(nil);
//			}
			
			DraftBucketJoin result = new DraftBucketJoin();
			result.setId_draft(id_draft);
			result.setId_draft_bucket(drabu.getId_draft_bucket());
			result.setId_segment(id_segment);
			result.setSegment(segment);
			result.setCreated_by(drabu.getCreated_by());
			result.setCreated_user(resp.getData());
			result.setCreated_date(drabu.getCreated_date());
			result.setStart_date(this.configGetDate.format(drabu.getStart_date()));
			result.setEnd_date(this.configGetDate.format(drabu.getEnd_date()));
			result.setFlag_normalisasi(flag);
			result.setHari_first(hari_first_bucket);
			result.setInterval_bucket(interval);
			result.setJumlah_bucket(jumlah_bucket);
			result.setMoving_titik(titik);
			result.setKode_draft_bucket(drabu.getKode_draft_bucket());
			result.setNama_draft_bucket(drabu.getNama_draft_bucket());
			
			List<String> head = serviceReport.getHeader(draft.getFlag_draft(), jumlah_bucket, hari_first_bucket, interval, draft.getHeader_bucket()); 
			
			for (int i = 0; i < 9; i++) {
				GetBucket resp_buk = new GetBucket();
				resp_buk.setHeader_bucket(head);
				
				if ( i== 0) {
					resp_buk.setTitle_bucket("AR aging");
					resp_buk.setList_bucket(araging);
					result.setAraging(resp_buk);
				} else if (i == 1) {
					resp_buk.setTitle_bucket("Rollrate belum normal");
					resp_buk.setList_bucket(rollrate_belum_normal);
					result.setRollrate_belum_normal(resp_buk);
				} else if (i == 2) {
					resp_buk.setTitle_bucket("Rollrate normal");
					resp_buk.setList_bucket(rollrate_normal);
					result.setRollrate_normal(resp_buk);
				} else if (i == 3) {
					resp_buk.setTitle_bucket("Rollrate normalisasi average");
					resp_buk.setList_bucket(rollrate_normalisasi);
					result.setRollrate_normalisasi_average(resp_buk);
				} else if (i == 4) {
					resp_buk.setTitle_bucket("Average Rollrate dan Lossrate");
					resp_buk.setList_bucket(average);
					result.setAverage_rollrate_lossrate(resp_buk);
				} else if (i == 5) {
					resp_buk.setTitle_bucket("Lossrate");
					resp_buk.setList_bucket(loss_rate);
					result.setLossrate(resp_buk);
				} else if (i == 6) {
					resp_buk.setTitle_bucket("Normalisasi Moving average");
					resp_buk.setList_bucket(normal_moving);
					result.setMoving_average(resp_buk);
				} else if (i == 7) {
					resp_buk.setTitle_bucket("Sum Delta Lossrate");
					resp_buk.setList_bucket(delta_loss);
					result.setSum_delta_lossrate(resp_buk);
				} else {
					resp_buk.setTitle_bucket("ODR");
					resp_buk.setList_bucket(odr);
					result.setOdr(resp_buk);	
				}
			}
			
			logger.info("success List Delta Loss Rate");
			return new ResponseEntity(new ResponObject(1, "List Draft Bucket",result),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/draft-bucket/{id}")
	public ResponseEntity<?> updateDraftBucket(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) {
		
		String method = "PUT";
		String url = "/draft-bucket/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api put draft");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "nama :"+body.get("nama"));
			
			DraftBucket nmember = serviceBucketService.findById(id);
			if(nmember == null ) { return new ResponseEntity(new ResponObject(2, "there is no draft"),HttpStatus.BAD_REQUEST); }
			
			User created = resp.getData();
			nmember.setNama_draft_bucket(body.get("nama"));	
			this.serviceBucketService.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success update draft");
			int respact = this.serviceAktivitas.createAktivitas("DRAFT",created,method,url,nmember.getId_draft());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update Draft Bucket",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/draft-bucket")
	public ResponseEntity<?> getDraftBucket(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/draft-bucket";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get draft bucket");
			
			List<DraftBucket> datas = serviceBucketService.findAll();
			
			List<DraftBucketJ> data = new ArrayList<DraftBucketJ>();
			List<Segment> segments = this.serviceSegment.findAll();
			List<User> users = this.serviceUser.findAll();
			
			for (DraftBucket dt : datas) {
			
				DraftBucketJ dta = new DraftBucketJ();
				dta.setId_draft_bucket(dt.getId_draft_bucket());
				dta.setId_draft(dt.getId_draft());
				dta.setId_segment(dt.getId_segment());
				
				for (Segment segment : segments) {
					if (segment.getId_segment() == dt.getId_segment()) {
						dta.setSegment(segment);
						break;
					}
				}
				
				for (User user : users) {
					if (user.getId_user() == dt.getCreated_by()) {
						dta.setDibuat_oleh(user);
						break;
					}
				}
				
				dta.setKode_draft_bucket(dt.getKode_draft_bucket());
				dta.setNama_draft_bucket(dt.getNama_draft_bucket());
				dta.setHari_first(dt.getHari_first());
				dta.setInterval_bucket(dt.getInterval_bucket());
				dta.setJumlah_bucket(dt.getJumlah_bucket());
				dta.setFlag_normalisasi(dt.getFlag_normalisasi());
				dta.setMoving_average(dt.getMoving_average());
				dta.setStart_date(dt.getStart_date());
				dta.setEnd_date(dt.getEnd_date());
				dta.setCreated_by(dt.getCreated_by());
				dta.setCreated_date(dt.getCreated_date());
				dta.setDibuat_tgl(this.configGetDate.format(dt.getCreated_date()));
				
				List<KomentarJoin> komjo = new ArrayList<KomentarJoin>();
				List<Komentar> komens = this.serviceKomentar.findByTypeIdTypeAll(1, dt.getId_draft_bucket());
				
				for (Komentar komen : komens) {
					KomentarJoin kojo = new KomentarJoin();
					kojo.setId_komentar_bucket(komen.getId_komentar_bucket());
					kojo.setId_type(komen.getId_type());
					kojo.setFlag(komen.getFlag());
					kojo.setId_bucket(komen.getId_bucket());
					kojo.setKomentar(komen.getKomentar());
					kojo.setCreated_by(komen.getCreated_by());
					kojo.setCreated_date(komen.getCreated_date());
					kojo.setUpdated_date(komen.getUpdated_date());
					
					for(User a : users) {
						if(a.getId_user() == komen.getCreated_by()) {
							kojo.setUser(a);
							break;
						}
					}
					komjo.add(kojo);
				}
				dta.setList_komentar(komjo);
				data.add(dta);
			}
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data Draft Bucket");
				return new ResponseEntity(new ResponList(1, "empty draft bucket",data),HttpStatus.OK);
			}
			
			logger.info(method+"|"+url+"|"+"data draft");
			return new ResponseEntity(new ResponList(1, "List Draft",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/draft-bucket/{id}")
	public ResponseEntity<?> getDraftID(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id
			) {
		
		String method = "GET";
		String url = "/draft-bucket/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get draft bucket");
			
			DraftBucket db = this.serviceBucketService.findById(id);
			
			if (db == null) {
				logger.info(method+"|"+url+"|"+"tidak ada data draft bucket");
				return new ResponseEntity(new ResponList(2, "empty draft bucket"),HttpStatus.OK);
			}
			
			Draft dr = this.serviceDraft.findById(db.getId_draft());
			List<NilaiBucket> nb = this.serviceBucketService.findNilaiBucketByIdDraftBucket(id);
			
			DraftBucketJoin respon = joinDraftBucket(dr, db, nb);
			
			logger.info(method+"|"+url+"|"+"data draft bucket");
			return new ResponseEntity(new ResponObject(1, "List Draft Bucket",respon),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/draft-bucket/kode/{id}")
	public ResponseEntity<?> getDraftByFlag(@RequestHeader("Authorization") String Authorization,
			@PathVariable String id
			) {
		
		String method = "GET";
		String url = "/draft-bucket/kode/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get draft bucket");
			
			DraftBucket db = this.serviceBucketService.findDraftBucketByKode(id);
			
			if (db == null) {
				logger.info(method+"|"+url+"|"+"tidak ada data draft bucket");
				return new ResponseEntity(new ResponList(2, "empty draft bucket"),HttpStatus.OK);
			}
			
			Draft dr = this.serviceDraft.findById(db.getId_draft());
			List<NilaiBucket> nb = this.serviceBucketService.findNilaiBucketByIdDraftBucket(db.getId_draft_bucket());
			
			DraftBucketJoin respon = joinDraftBucket(dr, db, nb);
			
			logger.info(method+"|"+url+"|"+"data draft bucket");
			return new ResponseEntity(new ResponObject(1, "List Draft Bucket",respon),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@DeleteMapping(value="/draft-bucket/{id}")
	public ResponseEntity<?> deleteDraftBucket(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) {
		
		String method = "DELETE";
		String url = "/draft-bucket/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api delete draft-bucket");
			logger.info(method+"|"+url+"|"+"input data id: "+id);
			
			List<DraftECL> listECL = this.serviceECL.findByIdDraftBucket(id);
			for (DraftECL draftECL : listECL) {
				this.serviceECL.delete(draftECL.getId_draft_ecl());
				this.serviceSaveECL.deleteECL(draftECL.getId_draft_ecl());
				this.serviceKomentar.deleteByIdDraft(2, draftECL.getId_draft_ecl());	
			}
			
			this.serviceBucketService.delete(id);
			this.serviceBucketService.deleteNilai(id);
			this.serviceKomentar.deleteByIdDraft(1, id);
			
			
			int respact = this.serviceAktivitas.createAktivitas("DRAFT BUCKET",resp.getData(),method,url,id);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			logger.info(method+"|"+url+"|"+"delete data draft");	
			return new ResponseEntity(new ResponObject(1, "delete Data Draft Bucket."),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public DraftBucketJoin joinDraftBucket(Draft draft,DraftBucket drabu,List<NilaiBucket> nilais) {
		
		List<User> users = this.serviceUser.findAll();
		
		DraftBucketJoin result = new DraftBucketJoin();
		result.setId_draft(drabu.getId_draft());
		result.setId_draft_bucket(drabu.getId_draft_bucket());
		result.setId_segment(drabu.getId_segment());
		result.setCreated_by(drabu.getCreated_by());
		
		for(User a : users) {
			if(a.getId_user() == drabu.getCreated_by()) {
				result.setCreated_user(a);
				break;
			}
		}
		
		result.setCreated_date(drabu.getCreated_date());
		result.setStart_date(this.configGetDate.format(drabu.getStart_date()));
		result.setEnd_date(this.configGetDate.format(drabu.getEnd_date()));
		result.setFlag_normalisasi(drabu.getFlag_normalisasi());
		result.setHari_first(drabu.getHari_first());
		result.setInterval_bucket(drabu.getInterval_bucket());
		result.setJumlah_bucket(drabu.getJumlah_bucket());
		result.setMoving_titik(drabu.getMoving_average());
		result.setKode_draft_bucket(drabu.getKode_draft_bucket());
		result.setNama_draft_bucket(drabu.getNama_draft_bucket());
		result.setSegment(this.serviceSegment.findById(drabu.getId_segment()));
		
		List<KomentarJoin> komjo = new ArrayList<KomentarJoin>();
		List<Komentar> komens = this.serviceKomentar.findByTypeIdTypeAll(1, drabu.getId_draft_bucket());
		for (Komentar komen : komens) {
			KomentarJoin kojo = new KomentarJoin();
			kojo.setId_komentar_bucket(komen.getId_komentar_bucket());
			kojo.setId_type(komen.getId_type());
			kojo.setFlag(komen.getFlag());
			kojo.setId_bucket(komen.getId_bucket());
			kojo.setKomentar(komen.getKomentar());
			kojo.setCreated_by(komen.getCreated_by());
			kojo.setCreated_date(komen.getCreated_date());
			kojo.setUpdated_date(komen.getUpdated_date());
			
			for(User a : users) {
				if(a.getId_user() == komen.getCreated_by()) {
					kojo.setUser(a);
					break;
				}
			}
			komjo.add(kojo);
		}
		result.setList_komentar(komjo);
		
		List<GetBucketList> araging = new ArrayList<GetBucketList>();
		List<GetBucketList> rollrate_belum_normal = new ArrayList<GetBucketList>();
		List<GetBucketList> rollrate_normal = new ArrayList<GetBucketList>();
		List<GetBucketList> rollrate_normalisasi = new ArrayList<GetBucketList>();
		List<GetBucketList> average = new ArrayList<GetBucketList>();
		List<GetBucketList> loss_rate = new ArrayList<GetBucketList>();
		List<GetBucketList> normal_moving = new ArrayList<GetBucketList>();
		List<GetBucketList> delta_loss = new ArrayList<GetBucketList>();
		List<GetBucketList> odr = new ArrayList<GetBucketList>();
		
		for (NilaiBucket nilai : nilais) {
			GetBucketList nil = new GetBucketList();
			if (nilai.getId_step() == 67) {
				if (nilai.getPeriode().toLowerCase().contains("roll rate")) {
					nil.setPeriode("AVERAGE ROLL RATE");
				} else {
					nil.setPeriode("AVERAGE LOSS RATE");
				}
				
			} else {
				nil.setPeriode(serviceReport.getPeriode(nilai.getPeriode_bulan(), nilai.getPeriode_tahun()));	
			}
			nil.setPeriode_bulan(nilai.getPeriode_bulan());
			nil.setPeriode_tahun(nilai.getPeriode_tahun());
			
			JSONArray list_nilai = new JSONArray(nilai.getNilai());
			Double heat [] = new Double[drabu.getJumlah_bucket()];
			for (int i = 0; i < heat.length; i++) {
				heat[i] = list_nilai.getDouble(i);
			}
	
			nil.setNilai(heat);
			nil.setJumlah_outstanding(nilai.getJumlah_outstanding());
			nil.setAverage(nilai.getAverage());
			nil.setSum(nilai.getSum());
			nil.setPersentase_odr(nilai.getPersentase_odr());
			
			if (nilai.getId_step() == 3) {
				araging.add(nil);
			} else if (nilai.getId_step() == 4) {
				rollrate_belum_normal.add(nil);
			} else if (nilai.getId_step() == 5) {
				rollrate_normal.add(nil);
			} else if (nilai.getId_step() == 8) {
				rollrate_normalisasi.add(nil);
			} else if (nilai.getId_step() == 67) {
				average.add(nil);
			} else if (nilai.getId_step() == 9) {
				loss_rate.add(nil);
			} else if (nilai.getId_step() == 10) {
				normal_moving.add(nil);
			} else if (nilai.getId_step() == 12) {
				delta_loss.add(nil);
			} else if (nilai.getId_step() == 14) {
				odr.add(nil);
			} 
		}
		
		List<String> head = serviceReport.getHeader(draft.getFlag_draft(),drabu.getJumlah_bucket(),drabu.getHari_first(),drabu.getInterval_bucket(), draft.getHeader_bucket()); 
		
		for (int i = 0; i < 9; i++) {
			GetBucket resp_buk = new GetBucket();
			resp_buk.setHeader_bucket(head);
			
			if ( i== 0) {
				resp_buk.setTitle_bucket("AR aging");
				resp_buk.setList_bucket(araging);
				result.setAraging(resp_buk);
			} else if (i == 1) {
				resp_buk.setTitle_bucket("Rollrate belum normal");
				resp_buk.setList_bucket(rollrate_belum_normal);
				result.setRollrate_belum_normal(resp_buk);
			} else if (i == 2) {
				resp_buk.setTitle_bucket("Rollrate normal");
				resp_buk.setList_bucket(rollrate_normal);
				result.setRollrate_normal(resp_buk);
			} else if (i == 3) {
				resp_buk.setTitle_bucket("Rollrate normalisasi average");
				resp_buk.setList_bucket(rollrate_normalisasi);
				result.setRollrate_normalisasi_average(resp_buk);
			} else if (i == 4) {
				resp_buk.setTitle_bucket("Average Rollrate dan Lossrate");
				resp_buk.setList_bucket(average);
				result.setAverage_rollrate_lossrate(resp_buk);
			} else if (i == 5) {
				resp_buk.setTitle_bucket("Lossrate");
				resp_buk.setList_bucket(loss_rate);
				result.setLossrate(resp_buk);
			} else if (i == 6) {
				resp_buk.setTitle_bucket("Normalisasi Moving average");
				resp_buk.setList_bucket(normal_moving);
				result.setMoving_average(resp_buk);
			} else if (i == 7) {
				resp_buk.setTitle_bucket("Sum Delta Lossrate");
				resp_buk.setList_bucket(delta_loss);
				result.setSum_delta_lossrate(resp_buk);
			} else {
				resp_buk.setTitle_bucket("ODR");
				resp_buk.setList_bucket(odr);
				result.setOdr(resp_buk);	
			}
		}
	
		return result;
	}
	
}
