package com.hanifalbaaits.java.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.Aktivitas;
import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.UploadFile;
import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.service.AktivitasService;
import com.hanifalbaaits.java.service.DraftService;
import com.hanifalbaaits.java.service.SegmentService;
import com.hanifalbaaits.java.service.UploadFileService;
import com.hanifalbaaits.java.service.UserService;
import com.hanifalbaaits.java.utils.Encryption;

@RestController
@RequestMapping(value="core-ifrs")
public class MasterDashboard {
	
	private Logger logger = LoggerFactory.getLogger(MasterDashboard.class);

	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	AktivitasService serviceAktivitasRepo;
	
	@Autowired
	UserService serviceUser;
	
	@Autowired
	SegmentService serviceSegment;
	
	@Autowired
	DraftService serviceDraft;
	
	@Autowired
	UploadFileService serviceUpload;
	
	@Autowired
	Encryption encrypt;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/dashboard/data-master")
	public ResponseEntity<?> getDataMaster(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/dashboard/data-master";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get dashboard data-master");
			
			int total_user = 0 ;
			int total_user_nonactive = 0;
			int total_segment = 0;
			int total_file_upload = 0;
			int total_file_size = 0;
			
			List<User> users = serviceUser.findAll();
			List<Segment> segments = serviceSegment.findAll();
			List<UploadFile> uploads = serviceUpload.findAll();
			
			for (User user : users) {
				total_user += 1;
				if (user.getStatus() == 0) {
					total_user_nonactive += 1;
				}
			}
			for (UploadFile upload : uploads) {
				total_file_upload += 1;
				total_file_size += upload.getSize_file();
			}
			
			total_segment = segments.size();
			
			double kb = total_file_size /1024;
			double mb = kb / 1024;
			
			Map map=new HashMap();
			map.put("total_user", total_user);
			map.put("total_user_nonactive",total_user_nonactive);
			map.put("total_segment", total_segment);
			map.put("total_file",total_file_upload);
			map.put("total_size",total_file_size);
			map.put("total_kb", kb);
			map.put("total_mb", mb);
			
			logger.info(method+"|"+url+"|"+"dashboard data master");
			return new ResponseEntity(new ResponObject(1, "Dashboard Data Master", map),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/dashboard/data-draft")
	public ResponseEntity<?> getDataDrat(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/dashboard/data-draft";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get dashboard data draft");
			
			int total_draft = 0 ;
			int total_draft_hari = 0;
			int total_draft_bucket = 0;
			
			List<Draft> drafts = serviceDraft.findAll();
			
			for (Draft draft : drafts) {
				total_draft += 1;
				if (draft.getFlag_draft() == 1) {
					total_draft_hari += 1;
				}
				if (draft.getFlag_draft() == 2) {
					total_draft_bucket += 1;
				}
			}
			
			Map map=new HashMap();
			map.put("total_draft", total_draft);
			map.put("total_draft_hari",total_draft_hari);
			map.put("total_draft_bucket", total_draft_bucket);
			logger.info(method+"|"+url+"|"+"Dashboard Data Draft");
			return new ResponseEntity(new ResponObject(1, "Dashboard Data Draft",map),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/dashboard/data-login")
	public ResponseEntity<?> getDataLogin(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/dashboard/data-login";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get dashboard data draft");
			
			Date today = new Date();
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -6);
			Date start = cal.getTime();
			
			int nilai [] = new int[7];
			int list_bulan [] = new int[7];
			int list_tahun [] = new int[7];
			
			DateFormat formatMonth = new SimpleDateFormat("MM");
			DateFormat formatYear = new SimpleDateFormat("yyyy");
			int month = Integer.parseInt(formatMonth.format(start));
			int year = Integer.parseInt(formatYear.format(start));
			
			for (int i = 0; i <= 6; i++) {
				
				
				list_bulan[i] = month;
				list_tahun[i] = year;
			
				if (month == 12) {
					month = 1;
					year += 1;
				} else {
					month += 1;
				}
			}
		
			List<Aktivitas> aktivitass = this.serviceAktivitasRepo.findByAdminDashboard(start, today);
			
			System.out.println(Arrays.toString(list_bulan));
			System.out.println(Arrays.toString(list_tahun));
			
			for (Aktivitas aktivitas : aktivitass) {
				if (aktivitas.getCaption().toLowerCase().contentEquals("login")) {
					
					Date created = aktivitas.getTanggal_aktivitas();
					String month_s = formatMonth.format(created);
					String year_s = formatYear.format(created);
					
					System.out.println("month_s: "+month_s+" | year_s: "+year_s);
					
					for (int i = 0; i < list_bulan.length; i++) {
						if (Integer.parseInt(month_s) == list_bulan[i] &&      
								Integer.parseInt(year_s) == list_tahun[i] ) {
							nilai[i] += 1;
						}
					}
				}
			}
			
			
			List<Map> aktifitas_login = new ArrayList<Map>();
			
			for (int j = 0; j < list_bulan.length; j++) {
				
				Map map=new HashMap();
				map.put("tahun", list_tahun[j]);
				map.put("bulan",list_bulan[j]);
				map.put("jumlah_aktivitas", nilai[j]);
				aktifitas_login.add(map);
			}
			
			logger.info(method+"|"+url+"|"+"Dashboard Data Aktifitas");
			return new ResponseEntity(new ResponObject(1, "Dashboard Data Aktifitas",aktifitas_login),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
}
