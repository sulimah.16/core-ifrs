package com.hanifalbaaits.java.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.ModelFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.hanifalbaaits.java.modelDB.DraftECL;
import com.hanifalbaaits.java.modelDB.InputMEV;
import com.hanifalbaaits.java.modelDB.InputModelFL;
import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.UploadFile;
import com.hanifalbaaits.java.modelRespon.InputModelFLJoin;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelUtils.KoreksiImport;
import com.hanifalbaaits.java.modelUtils.KoreksiPesan;
import com.hanifalbaaits.java.service.DraftEclService;
import com.hanifalbaaits.java.service.InputMevFL;
import com.hanifalbaaits.java.service.SegmentService;
import com.hanifalbaaits.java.service.UploadFileService;
import com.hanifalbaaits.java.service.UploadService;

@RestController
@RequestMapping(value="core-ifrs")
public class MasterInputMevFL {

	private Logger logger = LoggerFactory.getLogger(MasterInputMevFL.class);
	
	@Value("${file.path_input}")
	String path_file;
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	InputMevFL serviceMevFL;
	
	@Autowired
	AktivitasController serviceAktifitas;
	
	@Autowired
    UploadService uploadService;
	
	@Autowired
	UploadFileService serviceUpload;
	
	@Autowired
	SegmentService serviceSegment;
	
	@Autowired
	DraftEclService serviceECL;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/input-mev")
	public ResponseEntity<?> getInputData(@RequestHeader("Authorization") String Authorization) throws Exception {
		
		String method = "GET";
		String url = "/input-mev";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			List<InputMEV> data = serviceMevFL.findMevAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponList(1, "empty input",data),HttpStatus.OK);
			}
			
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponList(1, "List Input",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/input-mev/{bulan}/{tahun}")
	public ResponseEntity<?> searchInputMEV(@RequestHeader("Authorization") String Authorization,
			@PathVariable int bulan,
			@PathVariable int tahun) throws Exception {
		
		String method = "POST";
		String url = "/input-mev/"+bulan+"/"+tahun;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info( "hit api get data input");
			
			logger.info( "bulan : "+bulan);
			logger.info( "tahun : "+tahun);
		
			InputMEV list = serviceMevFL.findMevByBulanTahun(bulan, tahun);
			
			if (list == null) {
				logger.info("tidak ada Data Input");
				return new ResponseEntity(new ResponObject(1, "empty Data Input",list),HttpStatus.OK);
			}
			
			logger.info("success List Data Input");
			return new ResponseEntity(new ResponObject(1, "List Data Input",list),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@PostMapping(value="/input-mev/upload")
	public ResponseEntity<?> uploadHarian(@RequestHeader("Authorization") String Authorization,
		@RequestParam("file") MultipartFile file
		) throws Exception, ParseException {
	
		String method = "POST";
		String url = "/input-mev/upload";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info("FILE original: "+file.getOriginalFilename()+" TYPE: "+file.getContentType());
		logger.info("FILE name: "+file.getName());
		
		//try untuk baca extensi file harus CSV
		String extension = "";
		try {
			String split [] = file.getOriginalFilename().split("\\.");
			extension = split[split.length-1];
			if(!extension.toLowerCase().contentEquals("csv")) {
				logger.info("validasi => File type must CSV");
					return new ResponseEntity(new ResponObject(2, "File type must CSV"),HttpStatus.BAD_REQUEST);	
				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		logger.info("Lolos validasi file csv");
	
		Date today = new Date();
		
		try {
			logger.info("UPLOAD FILE untuk INPUT");
			
			int lst = 1;
			UploadFile dmy = this.serviceUpload.findLast();
			if (dmy != null) {
				lst = dmy.getId_upload() + 1;
			}
			
			UploadFile nw = new UploadFile();
			nw.setTrue_name(file.getOriginalFilename());
			nw.setType_file(file.getContentType());
			nw.setSize_file((int) file.getSize());
			nw.setType_folder("input");
			nw.setTanggal_upload(today);
			nw.setId_user(resp.getData().getId_user());
				
			String filename = String.valueOf(lst)+today.getTime()+"_mev"+"."+extension;
			logger.info(method+"|"+url+"|"+"filename: "+filename);
			
			nw.setPath_name("input/"+filename);
			
			String uripath = uploadService.storeFile(file,filename,"input");		 
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
			.path("core-ifrs/view_file/input/")
			.path(uripath)
			.toUriString();
			
			this.serviceUpload.save(nw);
			logger.info(method+"|"+url+"|"+"success upload");
			
			logger.info("UPLOAD-FILE uripath: "+uripath);
			logger.info("UPLOAD-FILE fileDownloadUri: "+fileDownloadUri);
			
			KoreksiImport impor = new KoreksiImport();
			impor.setId_draft(0);
//			impor.setDraft(draft);
			impor.setId_segment(0);
//			impor.setSegment(ch_seg);
			impor.setCount_import(0);
			impor.setCount_success(0);
			impor.setCount_failed(0);
			impor.setTgl_import(today.toString());
			impor.setTitle("Import File MEV");
			impor.setFile_upload(nw);
			
			List<KoreksiPesan> pesanKoreksi = new ArrayList<KoreksiPesan>();
			
			this.serviceMevFL.truncateMev();
			
			try (BufferedReader br = new BufferedReader(new FileReader(path_file+"/"+uripath))) {
			    String line;
			  
			    //TEMPLATE: tanggal;idpiutang;kodecustomer;namacustomer;kodeproyek;namaproyek;pemberikerja,dll
			    int i = -1;
				
			    while ((line = br.readLine()) != null) {
			    	i++;
			    	String clean = line.replaceAll("\\P{Print}", ""); //ini perintah untuk remove unicode
			        String[] values = clean.split("\\;");
			        
			        if (i == 0) {
						continue;
					}
			       
			        logger.info("PERULANGAN KE- : "+i);
			        String nomor = String.valueOf(i);
			       
			    try {
			    	
			    	logger.info("Values length : "+values.length);
			        if (values.length != 31) {
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Jumlah field harus 31")); 
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			        
			        if (values[0] == null || values[0].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "tanggal periode tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[1] == null || values[1].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "gdp tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[2] == null || values[2].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "lgdp tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[3] == null || values[3].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dgdp_1 tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[4] == null || values[4].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dgdp_12 tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[5] == null || values[5].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "lgdp_1 tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[6] == null || values[6].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "lgdp_12 tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[7] == null || values[7].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "gdpgr tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[8] == null || values[8].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "kurs tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[9] == null || values[9].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "lkurs tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[10] == null || values[10].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dkurs tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[11] == null || values[11].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dlkurs tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[12] == null || values[12].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "i7rr tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[13] == null || values[13].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "di7rr tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        if (values[14] == null || values[14].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "inf tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[15] == null || values[15].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dinf tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[16] == null || values[16].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "govsp tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        if (values[17] == null || values[17].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "lgovsp tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[18] == null || values[18].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dgovsp tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[19] == null || values[19].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dlgovsp tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        
			        if (values[20] == null || values[20].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "govspgr tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        if (values[21] == null || values[21].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "housep tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[22] == null || values[22].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "lhousep tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[23] == null || values[23].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dhousep tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        if (values[24] == null || values[24].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dlhousep tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        
			        if (values[25] == null || values[25].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dlgdp tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        if (values[26] == null || values[26].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dgdpgr tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        if (values[27] == null || values[27].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "oil tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        if (values[28] == null || values[28].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "loil tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        if (values[29] == null || values[29].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "doil tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        if (values[30] == null || values[30].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dloil tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        
			       
			        String tgl = values[0];
			        double gdp = Double.parseDouble(values[1].replaceAll("\\,", "."));
			        double lgdp = Double.parseDouble(values[2].replaceAll("\\,", "."));
			        double dgdp_1 = Double.parseDouble(values[3].replaceAll("\\,", "."));
			        double dgdp_12 = Double.parseDouble(values[4].replaceAll("\\,", "."));
			        double lgdp_1 = Double.parseDouble(values[5].replaceAll("\\,", "."));
			        double lgdp_12 = Double.parseDouble(values[6].replaceAll("\\,", "."));
			        double gdpgr = Double.parseDouble(values[7].replaceAll("\\,", "."));
			        double kurs = Double.parseDouble(values[8].replaceAll("\\,", "."));
			        double lkurs = Double.parseDouble(values[9].replaceAll("\\,", "."));
			        double dkurs = Double.parseDouble(values[10].replaceAll("\\,", "."));
			        double dlkurs = Double.parseDouble(values[11].replaceAll("\\,", "."));
			        double i7rr = Double.parseDouble(values[12].replaceAll("\\,", "."));
			        double di7rr = Double.parseDouble(values[13].replaceAll("\\,", "."));
			        double inf = Double.parseDouble(values[14].replaceAll("\\,", "."));
			        double dinf = Double.parseDouble(values[15].replaceAll("\\,", "."));
			        double govsp = Double.parseDouble(values[16].replaceAll("\\,", "."));
			        double lgovsp = Double.parseDouble(values[17].replaceAll("\\,", "."));
			        double dgovsp = Double.parseDouble(values[18].replaceAll("\\,", "."));
			        double dlgovsp = Double.parseDouble(values[19].replaceAll("\\,", "."));
			        double govspgr = Double.parseDouble(values[20].replaceAll("\\,", "."));
			        double housep = Double.parseDouble(values[21].replaceAll("\\,", "."));
			        double lhousep = Double.parseDouble(values[22].replaceAll("\\,", "."));
			        double dhousep = Double.parseDouble(values[23].replaceAll("\\,", "."));
			        double dlhousep = Double.parseDouble(values[24].replaceAll("\\,", "."));
			        
			        double dlgdp = Double.parseDouble(values[25].replaceAll("\\,", "."));
			        double dgdpgr = Double.parseDouble(values[26].replaceAll("\\,", "."));
			        double oil = Double.parseDouble(values[27].replaceAll("\\,", "."));
			        double loil = Double.parseDouble(values[28].replaceAll("\\,", "."));
			        double doil = Double.parseDouble(values[29].replaceAll("\\,", "."));
			        double dloil = Double.parseDouble(values[30].replaceAll("\\,", "."));
			    
			        logger.info("tgl : "+tgl);
					logger.info("gdp : "+gdp);
					logger.info("lgdp : "+lgdp);
					logger.info("dgdp_1 : "+dgdp_1);
					logger.info("dgdp_12: "+dgdp_12);
					logger.info("lgdp_1 : "+lgdp_1);
					logger.info("lgdp_12 : "+lgdp_12);
					logger.info("gdpgr : "+gdpgr);
					logger.info("kurs : "+kurs);
					logger.info("lkurs : "+lkurs);
					logger.info("dkurs : "+dkurs);
					logger.info("dlkurs : "+dlkurs);
					logger.info("i7rr : "+i7rr);
					logger.info("di7rr : "+di7rr);
					logger.info("inf : "+inf);
					logger.info("dinf : "+dinf);
					logger.info("govsp : "+govsp);
					logger.info("lgovsp : "+lgovsp);
					logger.info("dgovsp : "+dgovsp);
					logger.info("dlgovsp : "+dlgovsp);
					logger.info("govspgr : "+govspgr);
					logger.info("govsp : "+govsp);
					logger.info("housep : "+housep);
					logger.info("lhousep : "+lhousep);
					logger.info("dhousep : "+dhousep);
					logger.info("dlhousep : "+dlhousep);
					
					logger.info("dlgdp : "+dlgdp);
					logger.info("dgdpgr : "+dgdpgr);
					logger.info("oil : "+oil);
					logger.info("loil : "+loil);
					logger.info("doil : "+doil);
					logger.info("dloil : "+dloil);
							
					DateFormat df = new SimpleDateFormat("dd/MM/yy");
					Date tgl_parse = df.parse(tgl);
					DateFormat formatYear = new SimpleDateFormat("yyyy");  
					DateFormat formatMonth = new SimpleDateFormat("MM");
						
					String bulan = formatMonth.format(tgl_parse);
					String tahun = formatYear.format(tgl_parse);
						
					logger.info(method+"|"+url+"|"+ "bulan :"+bulan);
					logger.info(method+"|"+url+"|"+ "tahun :"+tahun);
						
					int bln = Integer.parseInt(bulan);
					int thn = Integer.parseInt(tahun);
					
					InputMEV mev = new InputMEV();
					mev.setTanggal_mev(tgl_parse);
					mev.setBulan(bln);
					mev.setTahun(thn);
					mev.setGdp(gdp);
					mev.setLgdp(lgdp);
					mev.setDgdp_1(dgdp_1);
					mev.setDgdp_12(dgdp_12);
					mev.setLgdp_1(lgdp_1);
					mev.setLgdp_12(lgdp_12);
					mev.setGdpgr(gdpgr);
					mev.setKurs(kurs);
					mev.setLkurs(lkurs);
					mev.setDkurs(dkurs);
					mev.setDlkurs(dlkurs);
					mev.setI7rr(i7rr);
					mev.setDi7rr(di7rr);
					mev.setInf(inf);
					mev.setDinf(dinf);
					mev.setGovsp(govsp);
					mev.setLgovsp(lgovsp);
					mev.setDgovsp(dgovsp);
					mev.setDlgovsp(dlgovsp);
					mev.setGovspgr(govspgr);
					mev.setHousep(housep);
					mev.setLhousep(lhousep);
					mev.setDhousep(dhousep);
					mev.setDlhousep(dlhousep);
					
					mev.setDlgdp(dlgdp);
					mev.setDgdpgr(dgdpgr);
					mev.setOil(oil);
					mev.setLoil(loil);
					mev.setDoil(doil);
					mev.setDloil(dloil);
					this.serviceMevFL.saveMev(mev);
					
					impor.setCount_import(impor.getCount_import()+1);
					impor.setCount_success(impor.getCount_success()+1);
					
			    	} catch (Exception e) {
						// TODO: handle exception
			    		logger.info(e.toString());
						logger.info(this.getClass().toGenericString(), e);	
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Pastikan sesuai format"));
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			    }
			    
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				impor.setList_pesan(pesanKoreksi);
				return new ResponseEntity(new ResponObject(3, e.getMessage(),impor),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			
			logger.info(method+"|"+url+"|"+"success save input");
			int respact = this.serviceAktifitas.createAktivitas("INPUT-FILE",resp.getData(),method,url,0);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			if (pesanKoreksi.size()> 0) {
				this.serviceMevFL.truncateMev();
			} else {
				
				List<InputMEV> list_mev = serviceMevFL.findMevAll();
				
				double gdp [] = new double[list_mev.size()]; 
		        double lgdp [] = new double[list_mev.size()];
		        double dgdp_1 [] = new double[list_mev.size()];
		        double dgdp_12 [] = new double[list_mev.size()];
		        double lgdp_1 [] = new double[list_mev.size()];
		        double lgdp_12 [] = new double[list_mev.size()];
		        double gdpgr [] = new double[list_mev.size()];
		        double kurs [] = new double[list_mev.size()];
		        double lkurs [] = new double[list_mev.size()];
		        double dkurs [] = new double[list_mev.size()];
		        double dlkurs [] = new double[list_mev.size()];
		        double i7rr [] = new double[list_mev.size()];
		        double di7rr [] = new double[list_mev.size()];
		        double inf [] = new double[list_mev.size()];
		        double dinf [] = new double[list_mev.size()];
		        double govsp [] = new double[list_mev.size()];
		        double lgovsp [] = new double[list_mev.size()];
		        double dgovsp [] = new double[list_mev.size()];
		        double dlgovsp [] = new double[list_mev.size()];
		        double govspgr [] = new double[list_mev.size()];
		        double housep [] = new double[list_mev.size()];
		        double lhousep [] = new double[list_mev.size()];
		        double dhousep [] = new double[list_mev.size()];
		        double dlhousep [] = new double[list_mev.size()];
		        
		        double dlgdp [] = new double[list_mev.size()];
		        double dgdpgr [] = new double[list_mev.size()];
		        double oil [] = new double[list_mev.size()];
		        double loil [] = new double[list_mev.size()];
		        double doil [] = new double[list_mev.size()];
		        double dloil [] = new double[list_mev.size()];
		        
		        int ux = 0 ;
		        for (InputMEV mev : list_mev) {
		        	gdp[ux] = mev.getGdp();
		        	lgdp[ux] = mev.getLgdp();
		        	dgdp_1[ux] = mev.getDgdp_1();
		        	dgdp_12[ux] = mev.getDgdp_12();
		        	lgdp_1[ux] = mev.getLgdp_1();
		        	lgdp_12[ux] = mev.getLgdp_12();
		        	gdpgr[ux] = mev.getGdpgr();
		        	kurs[ux] = mev.getKurs();
		        	lkurs[ux] = mev.getLkurs();
		        	dkurs[ux] = mev.getDkurs();
		        	dlkurs[ux] = mev.getDlkurs();
		        	i7rr[ux] = mev.getI7rr();
		        	di7rr[ux] = mev.getDi7rr();
		        	inf[ux] = mev.getInf();
		        	dinf[ux] = mev.getDinf();
		        	govsp[ux] = mev.getGovsp();
		        	lgovsp[ux] = mev.getLgovsp();
		        	dgovsp[ux] = mev.getDgovsp();
		        	dlgovsp[ux] = mev.getDlgovsp();
		        	govspgr[ux] = mev.getGovspgr();
		        	housep[ux] = mev.getHousep();
		        	lhousep[ux] = mev.getLhousep();
		        	dhousep[ux] = mev.getDhousep();
		        	dlhousep[ux] = mev.getDlhousep();
		        	
		        	dlgdp[ux] = mev.getDlgdp();
		        	dgdpgr[ux] = mev.getDgdpgr();
		        	oil[ux] = mev.getOil();
		        	loil[ux] = mev.getLoil();
		        	doil[ux] = mev.getDoil();
		        	dloil[ux] = mev.getDloil();
		        	ux++;
				}
		        
		        InputMEV mev = new InputMEV();
				mev.setTanggal_mev(today);
				mev.setBulan(0);
				mev.setTahun(0);
				mev.setGdp(calculateSD(gdp));
				mev.setLgdp(calculateSD(lgdp));
				mev.setDgdp_1(calculateSD(dgdp_1));
				mev.setDgdp_12(calculateSD(dgdp_12));
				mev.setLgdp_1(calculateSD(lgdp_1));
				mev.setLgdp_12(calculateSD(lgdp_12));
				mev.setGdpgr(calculateSD(gdpgr));
				mev.setKurs(calculateSD(kurs));
				mev.setLkurs(calculateSD(lkurs));
				mev.setDkurs(calculateSD(dkurs));
				mev.setDlkurs(calculateSD(dlkurs));
				mev.setI7rr(calculateSD(i7rr));
				mev.setDi7rr(calculateSD(di7rr));
				mev.setInf(calculateSD(inf));
				mev.setDinf(calculateSD(dinf));
				mev.setGovsp(calculateSD(govsp));
				mev.setLgovsp(calculateSD(lgovsp));
				mev.setDgovsp(calculateSD(dgovsp));
				mev.setDlgovsp(calculateSD(dlgovsp));
				mev.setGovspgr(calculateSD(govspgr));
				mev.setHousep(calculateSD(housep));
				mev.setLhousep(calculateSD(lhousep));
				mev.setDhousep(calculateSD(dhousep));
				mev.setDlhousep(calculateSD(dlhousep));
				
				mev.setDlgdp(calculateSD(dlgdp));
				mev.setDgdpgr(calculateSD(dgdpgr));
				mev.setOil(calculateSD(oil));
				mev.setLoil(calculateSD(loil));
				mev.setDoil(calculateSD(doil));
				mev.setDloil(calculateSD(dloil));
				this.serviceMevFL.saveMev(mev);
			}
			
			impor.setList_pesan(pesanKoreksi);
			return new ResponseEntity(new ResponObject(1, "Data Impor File Berhasil. Silahkan Unggah kembali baris yang gagal.",impor),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);	
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//////////////////DISINI FL MODEL ///////////////////
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/input-fl")
	public ResponseEntity<?> getFLall(@RequestHeader("Authorization") String Authorization) throws Exception {
		
		String method = "GET";
		String url = "/input-fl";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			List<InputModelFL> data = serviceMevFL.findFLAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponList(1, "empty input",data),HttpStatus.OK);
			}
			
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponList(1, "List Input",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/input-fl/nomor/{nomor}")
	public ResponseEntity<?> getFLByNomor(@RequestHeader("Authorization") String Authorization,
			@PathVariable String nomor
			) throws Exception {
		
		String method = "GET";
		String url = "/input-fl/nomor"+nomor;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			List<InputModelFL> data = serviceMevFL.findFLByNmr(nomor);
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponObject(1, "empty input",data),HttpStatus.OK);
			}
			
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponObject(1, "List Input",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/input-fl/{id}")
	public ResponseEntity<?> getFLByID(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id
			) throws Exception {
		
		String method = "GET";
		String url = "/input-fl/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			InputModelFL data = serviceMevFL.findFLById(id);
			
			if (data == null) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponObject(1, "empty input",data),HttpStatus.OK);
			}
			
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponObject(1, "List Input",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/input-fl/segment/{id}")
	public ResponseEntity<?> getFLByIdSegment(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id
			) throws Exception {
		
		String method = "GET";
		String url = "/input-fl/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			List<InputModelFL> data = serviceMevFL.findFLByIdSegmen(id,1);
			List<InputModelFLJoin> join = joinFL(data);
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponObject(1, "empty input",join),HttpStatus.OK);
			}
			
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponObject(1, "List Input",join),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@PostMapping(value="/input-fl/upload")
	public ResponseEntity<?> uploadFL(@RequestHeader("Authorization") String Authorization,
		@RequestParam Map<String, String> body,
		@RequestParam("file") MultipartFile file
		) throws Exception, ParseException {
	
		String method = "POST";
		String url = "/input-fl/upload";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info("FILE original: "+file.getOriginalFilename()+" TYPE: "+file.getContentType());
		logger.info("FILE name: "+file.getName());
		
		//try untuk baca extensi file harus CSV
		String extension = "";
		try {
			String split [] = file.getOriginalFilename().split("\\.");
			extension = split[split.length-1];
			if(!extension.toLowerCase().contentEquals("csv")) {
				logger.info("validasi => File type must CSV");
					return new ResponseEntity(new ResponObject(2, "File type must CSV"),HttpStatus.BAD_REQUEST);	
				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		logger.info("Lolos validasi file csv");
	
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null"),HttpStatus.BAD_REQUEST);	
		if (body.get("id_segment") == null || body.get("id_segment").isEmpty()) return new ResponseEntity(new ResponObject(2, "segment null"),HttpStatus.BAD_REQUEST);	
	
		Date today = new Date();
		
		Segment ch_seg = this.serviceSegment.findById(Integer.parseInt(body.get("id_segment")));
		if (ch_seg == null) {
			return new ResponseEntity(new ResponObject(2, "Tidak ada ID segment tersebut"),HttpStatus.BAD_REQUEST);
		}
		
		try {
			logger.info("UPLOAD FILE untuk INPUT");
			logger.info("Id Segment : "+ch_seg.getId_segment());
			
			int lst = 1;
			UploadFile dmy = this.serviceUpload.findLast();
			if (dmy != null) {
				lst = dmy.getId_upload() + 1;
			}
			
			UploadFile nw = new UploadFile();
			nw.setTrue_name(file.getOriginalFilename());
			nw.setType_file(file.getContentType());
			nw.setSize_file((int) file.getSize());
			nw.setType_folder("input");
			nw.setTanggal_upload(today);
			nw.setId_user(resp.getData().getId_user());
				
			String filename = String.valueOf(lst)+today.getTime()+"_fl"+"."+extension;
			logger.info(method+"|"+url+"|"+"filename: "+filename);
			
			nw.setPath_name("input/"+filename);
			
			String uripath = uploadService.storeFile(file,filename,"input");		 
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
			.path("core-ifrs/view_file/input/")
			.path(uripath)
			.toUriString();
			
			this.serviceUpload.save(nw);
			logger.info(method+"|"+url+"|"+"success upload");
			
			logger.info("UPLOAD-FILE uripath: "+uripath);
			logger.info("UPLOAD-FILE fileDownloadUri: "+fileDownloadUri);
			
			KoreksiImport impor = new KoreksiImport();
			impor.setId_draft(0);
//			impor.setDraft(draft);
			impor.setId_segment(0);
//			impor.setSegment(ch_seg);
			impor.setCount_import(0);
			impor.setCount_success(0);
			impor.setCount_failed(0);
			impor.setTgl_import(today.toString());
			impor.setTitle("Import File Model FL");
			impor.setFile_upload(nw);
			
			List<KoreksiPesan> pesanKoreksi = new ArrayList<KoreksiPesan>();
//			this.serviceMevFL.deleteFLBySegment(ch_seg.getId_segment());
			this.deleteModelFL(ch_seg.getId_segment());
			
			try (BufferedReader br = new BufferedReader(new FileReader(path_file+"/"+uripath))) {
			    String line;
			  
			    //TEMPLATE: tanggal;idpiutang;kodecustomer;namacustomer;kodeproyek;namaproyek;pemberikerja,dll
			    int i = -1;
				
			    while ((line = br.readLine()) != null) {
			    	i++;
			    	String clean = line.replaceAll("\\P{Print}", ""); //ini perintah untuk remove unicode
			        String[] values = clean.split("\\;");
			        
			        if (i == 0) {
						continue;
					}
			       
			        logger.info("PERULANGAN KE- : "+i);
			        String nomor = String.valueOf(i);
			       
			    try {
			    	
			    	logger.info("Values length : "+values.length);
			        if (values.length != 15) {
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Jumlah field harus 12")); 
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			        
			        if (values[0] == null || values[0].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "tanggal periode tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[1] == null || values[1].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "gdp tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[2] == null || values[2].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "lgdp tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[3] == null || values[3].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dgdp_1 tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[4] == null || values[4].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dgdp_12 tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[5] == null || values[5].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "lgdp_1 tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[6] == null || values[6].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "lgdp_12 tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[7] == null || values[7].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "gdpgr tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[8] == null || values[8].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "kurs tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[9] == null || values[9].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "lkurs tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[10] == null || values[10].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dkurs tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[11] == null || values[11].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "dlkurs tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[12] == null || values[12].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "i7rr tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[13] == null || values[13].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "di7rr tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; } 
			        if (values[14] == null || values[14].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "inf tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			       
			        String no_model = values[0];
			        String var1 = values[1];
			        String var2 = values[2];
			        int lag1 = Integer.parseInt(values[3]);
			        int lag2 = Integer.parseInt(values[4]);
			        double c_coef = Double.parseDouble(values[5].replaceAll("\\,", "."));
			        double c_coef_pvalue = Double.parseDouble(values[6].replaceAll("\\,", "."));
			        double koef_var1 = Double.parseDouble(values[7].replaceAll("\\,", "."));
			        double koef_var1_pvalue = Double.parseDouble(values[8].replaceAll("\\,", "."));
			        double koef_var2 = Double.parseDouble(values[9].replaceAll("\\,", "."));
			        double koef_var2_pvalue = Double.parseDouble(values[10].replaceAll("\\,", "."));
			        double adjr_square = Double.parseDouble(values[11].replaceAll("\\,", "."));
			        double f_prob = Double.parseDouble(values[12].replaceAll("\\,", "."));
			        int tanda = Integer.parseInt(values[13]);
			        int signifikan = Integer.parseInt(values[14]);
			    
			        logger.info("no_model : "+no_model);
					logger.info("var1 : "+var1);
					logger.info("var2 : "+var2);
					logger.info("lag1 : "+lag1);
					logger.info("lag2: "+lag2);
					logger.info("c_coef : "+c_coef);
					logger.info("koef_var1 : "+koef_var1);
					logger.info("koef_var1_pvalue : "+koef_var1_pvalue);
					logger.info("koef_var2 : "+koef_var2);
					logger.info("koef_var2_pvalue : "+koef_var2_pvalue);
					logger.info("adjr_square : "+adjr_square);
					logger.info("f_prob : "+f_prob);
					logger.info("tanda : "+tanda);
					logger.info("signifikan : "+signifikan);
							
					InputModelFL fl = new InputModelFL();
					fl.setNomor(no_model);
					fl.setVar1(var1);
					fl.setVar2(var2);
					fl.setLag1(lag1);
					fl.setLag2(lag2);
					fl.setC_coef(c_coef);
					fl.setC_coef_pvalue(c_coef_pvalue);
					fl.setKoef_var1(koef_var1);
					fl.setKoef_var1_pvalue(koef_var1_pvalue);
					fl.setKoef_var2(koef_var2);
					fl.setKoef_var2_pvalue(koef_var2_pvalue);
					fl.setAdjr_square(adjr_square);
					fl.setF_prob(f_prob);
					fl.setTanda(tanda);
					fl.setSignifikan(signifikan);
					fl.setId_segment(Integer.parseInt(body.get("id_segment")));
					fl.setStatus(1);
					this.serviceMevFL.saveFL(fl);
					
					impor.setCount_import(impor.getCount_import()+1);
					impor.setCount_success(impor.getCount_success()+1);
					
			    	} catch (Exception e) {
						// TODO: handle exception
			    		logger.info(e.toString());
						logger.info(this.getClass().toGenericString(), e);	
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Pastikan sesuai format"));
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			    }
			    
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				impor.setList_pesan(pesanKoreksi);
				return new ResponseEntity(new ResponObject(3, e.getMessage(),impor),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			
			logger.info(method+"|"+url+"|"+"success save input");
			int respact = this.serviceAktifitas.createAktivitas("INPUT-FILE",resp.getData(),method,url,0);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			if (pesanKoreksi.size()> 0) {
//				this.serviceMevFL.deleteFLBySegment(ch_seg.getId_segment());
				this.deleteModelFL(ch_seg.getId_segment());
			}
			
			impor.setList_pesan(pesanKoreksi);
			return new ResponseEntity(new ResponObject(1, "Data Impor File Berhasil. Silahkan Unggah kembali baris yang gagal.",impor),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);	
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public void deleteModelFL(int id_segment) {
		
		List<DraftECL> draft_ecl = this.serviceECL.findGrupIdModel();
		List<InputModelFL> del_list = this.serviceMevFL.findFLByIdSegmen(id_segment, 1);
		for (InputModelFL input : del_list) {
			
			
			boolean ada = false;
			for (DraftECL ecl : draft_ecl) {
				if (input.getId_model() == ecl.getId_model()) {
					ada = true;
					break;
				}
			}
			
			if (ada) {
				
				input.setStatus(0);
				this.serviceMevFL.saveFL(input);
				
			} else {
				
				this.serviceMevFL.deleteFL(input.getId_model());
				
			}
		}
	}
	
	public double calculateSD(double numArray[]) {
        double sum = 0.0, standardDeviation = 0.0;
        int length = numArray.length;

        for(double num : numArray) {
            sum += num;
        }

        double mean = sum/length;

        for(double num: numArray) {
            standardDeviation += Math.pow(num - mean, 2);
        }

        return Math.sqrt(standardDeviation/ (length - 1));
    }
	
	public List<InputModelFLJoin> joinFL(List<InputModelFL> list_model) {
		
		List<InputModelFLJoin> list_join = new ArrayList<InputModelFLJoin>();
		Segment segment = new Segment();
		segment.setId_segment(0);
		
		for (InputModelFL dt : list_model) {
			
			InputModelFLJoin inp = new InputModelFLJoin();
			inp.setId_model(dt.getId_model());
			inp.setNomor(dt.getNomor());
			inp.setVar1(dt.getVar1());
			inp.setVar2(dt.getVar2());
			inp.setLag1(dt.getLag1());
			inp.setLag2(dt.getLag2());
			inp.setC_coef(dt.getC_coef());
			inp.setC_coef_pvalue(dt.getC_coef_pvalue());
			inp.setKoef_var1(dt.getKoef_var1());
			inp.setKoef_var1_pvalue(dt.getKoef_var1_pvalue());
			inp.setKoef_var2(dt.getKoef_var2());
			inp.setKoef_var2_pvalue(dt.getKoef_var2_pvalue());
			inp.setAdjr_square(dt.getAdjr_square());
			inp.setF_prob(dt.getF_prob());
			inp.setTanda(dt.getTanda());
			inp.setSignifikan(dt.getSignifikan());
			inp.setStatus(dt.getStatus());
			
			if (segment.getId_segment() == 0) {
				segment = this.serviceSegment.findById(dt.getId_segment());
				inp.setSegment(segment);
			} else {
				inp.setSegment(segment);
			}
			list_join.add(inp);
		}
		return list_join;
	}

}
