package com.hanifalbaaits.java.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.hanifalbaaits.java.modelDB.DataInput;
import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.UploadFile;
import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelRespon.DataInputJoin;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelUtils.KoreksiImport;
import com.hanifalbaaits.java.modelUtils.KoreksiPesan;
import com.hanifalbaaits.java.service.DataInputService;
import com.hanifalbaaits.java.service.DraftService;
import com.hanifalbaaits.java.service.SegmentService;
import com.hanifalbaaits.java.service.UploadFileService;
import com.hanifalbaaits.java.service.UploadService;
import com.hanifalbaaits.java.service.UserService;

@RestController
@RequestMapping(value="core-ifrs")
public class MasterDataInput {

	private Logger logger = LoggerFactory.getLogger(MasterDataInput.class);
	
	@Value("${file.path_input}")
	String path_file;
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	UserService serviceUser;
	
	@Autowired
	DataInputService serviceInput;
	
	@Autowired
    UploadService uploadService;
	
	@Autowired
	UploadFileService serviceUpload;
	
	@Autowired
	DraftService serviceDraft;
	
	@Autowired
	SegmentService serviceSegment;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/input-data")
	public ResponseEntity<?> getInputData(@RequestHeader("Authorization") String Authorization) throws Exception {
		
		String method = "GET";
		String url = "/input-data";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			List<DataInput> data = serviceInput.findAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponList(1, "empty input",data),HttpStatus.OK);
			}
			
			List<DataInputJoin> dataJoin = getDataInputJoin(data);
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponList(1, "List Input",dataJoin),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@GetMapping(value="/input-data/{id}")
	public ResponseEntity<?> getInputData(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) throws Exception {
		
		String method = "GET";
		String url = "/input-data/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get master data input");
			
			DataInput check = this.serviceInput.findById(id);
		
			if (check == null) {
				logger.info(method+"|"+url+"|"+"tidak ada data Input");
				return new ResponseEntity(new ResponList(1, "empty Input"),HttpStatus.OK);
			}
			
			DataInputJoin input_join = new DataInputJoin();
			input_join.setId_input(check.getId_input());
			input_join.setId_draft(check.getId_draft());
			input_join.setId_segment(check.getId_segment());
			
			input_join.setDraft(this.serviceDraft.findById(check.getId_draft()));
			input_join.setSegment(this.serviceSegment.findById(check.getId_segment()));
			
			input_join.setTanggal_piutang(configGetDate.format(check.getTanggal_piutang()));
			input_join.setPeriode_bulan(check.getPeriode_bulan());
			input_join.setPeriode_tahun(check.getPeriode_tahun());
			input_join.setPeriode(getPeriode(check.getPeriode_bulan(), check.getPeriode_tahun()));
			
			input_join.setId_piutang(check.getId_piutang());
			input_join.setKode_customer(check.getKode_customer());
			input_join.setNama_customer(check.getNama_customer());
			input_join.setKode_proyek(check.getKode_proyek());
			input_join.setNama_proyek(check.getNama_proyek());
			input_join.setPemberi_kerja(check.getPemberi_kerja());
			input_join.setDoc_number(check.getDoc_number());
			input_join.setDoc_type(check.getDoc_type());
			
			input_join.setJenis_piutang(check.getJenis_piutang());
			input_join.setUmur_piutang_hari(check.getUmur_piutang_hari());
			input_join.setSaldo_piutang(check.getSaldo_piutang());
			
			input_join.setPath_document(check.getPath_document());
			input_join.setCreated_by(check.getCreated_by());
			input_join.setCreated_date(configGetDate.format(check.getCreated_date()));
			if(check.getUpdated_date()!=null) {
				input_join.setUpdated_by(check.getUpdated_by());
				input_join.setUpdated_date(configGetDate.format(check.getUpdated_date()));
			}
		
			logger.info(method+"|"+url+"|"+"data segment");
			return new ResponseEntity(new ResponObject(1, "List DataInput",input_join),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@GetMapping(value="/input-data/draft/{id}")
	public ResponseEntity<?> getInputByDraft(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) throws Exception {
		
		String method = "GET";
		String url = "/input-data/draft/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			List<DataInput> data = serviceInput.findByDraft(id);
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponList(1, "empty input",data),HttpStatus.OK);
			}
			
			List<DataInputJoin> dataJoin = getDataInputJoin(data);
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponList(1, "List Input",dataJoin),HttpStatus.OK);
			
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@DeleteMapping(value="/input-data/truncate")
	public ResponseEntity<?> truncateDataInput(@RequestHeader("Authorization") String Authorization) throws Exception {
		
		String method = "DELETE";
		String url = "/input-data/truncate";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			
			this.serviceInput.truncate();
			
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponList(1, "Truncate Berhasil"),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/input-data/search")
	public ResponseEntity<?> searchDataInput(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/input-data/search";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id_draft")==null || body.get("id_draft").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft null!"),HttpStatus.BAD_REQUEST);
		if (body.get("start_date")==null || body.get("start_date").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null || body.get("end_date").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);
		
		
		try {
			logger.info( "hit api get data input");

			String id = body.get("id_draft");
			String start_date = body.get("start_date").trim();
			String end_date = body.get("end_date").trim();
			
			logger.info( "id : "+id);
			logger.info( "start_date : "+start_date);
			logger.info( "end_date : "+end_date);
		
			List<DataInput> list = new ArrayList<DataInput>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = df.parse(start_date+" 00:00");
			Date date2 = df.parse(end_date+" 23:59");
			
			list = serviceInput.findByDraftPeriode(Integer.parseInt(id),date1, date2);
			
			if (list.size() == 0) {
				logger.info("tidak ada Data Input");
				return new ResponseEntity(new ResponList(1, "empty Data Input",list),HttpStatus.OK);
			}
			List<DataInputJoin> data = getDataInputJoin(list);
			logger.info("success List Data Input");
			return new ResponseEntity(new ResponList(1, "List Data Input",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(value="/input-data")
	public ResponseEntity<?> createInputData(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/input-data";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api add data-input");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
			if (body.get("tanggal_piutang")==null || body.get("tanggal_piutang").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "tanggal_piutang null!"),HttpStatus.BAD_REQUEST);
			if (body.get("umur_piutang")==null || body.get("umur_piutang").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "umur_piutang null!"),HttpStatus.BAD_REQUEST);
			if (body.get("saldo_piutang")==null || body.get("saldo_piutang").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "saldo_piutang null!"),HttpStatus.BAD_REQUEST);
			
			
			logger.info(method+"|"+url+"|"+ "id_draft :"+body.get("id_draft"));
			logger.info(method+"|"+url+"|"+ "nama_draft :"+body.get("nama_draft"));
			logger.info(method+"|"+url+"|"+ "id_segment :"+body.get("id_segment"));
			logger.info(method+"|"+url+"|"+ "tanggal_piutang :"+body.get("tanggal_piutang"));
			logger.info(method+"|"+url+"|"+ "id_piutang :"+body.get("id_piutang"));
			logger.info(method+"|"+url+"|"+ "kode_customer :"+body.get("kode_customer"));
			logger.info(method+"|"+url+"|"+ "nama_customer :"+body.get("nama_customer"));
			logger.info(method+"|"+url+"|"+ "kode_proyek :"+body.get("kode_proyek"));
			logger.info(method+"|"+url+"|"+ "nama_proyek :"+body.get("nama_proyek"));
			logger.info(method+"|"+url+"|"+ "pemberi_kerja :"+body.get("pemberi_kerja"));
			logger.info(method+"|"+url+"|"+ "doc_number :"+body.get("doc_number"));
			logger.info(method+"|"+url+"|"+ "doc_type :"+body.get("doc_type"));
			logger.info(method+"|"+url+"|"+ "jenis_piutang :"+body.get("jenis_piutang"));
			logger.info(method+"|"+url+"|"+ "umur_piutang :"+body.get("umur_piutang"));
			logger.info(method+"|"+url+"|"+ "saldo_piutang :"+body.get("saldo_piutang"));
			
			
			Segment ch_seg = this.serviceSegment.findById(Integer.parseInt(body.get("id_segment")));
			if (ch_seg == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada ID segment tersebut"),HttpStatus.BAD_REQUEST);
			}
			
			int bln = 0;
			int thn = 0;
			Date tgl_piutang = null;
			int umur = 0;
			double saldo = 0.0;
			
			try {
				
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				tgl_piutang = df.parse(body.get("tanggal_piutang"));
				DateFormat formatYear = new SimpleDateFormat("yyyy");  
				DateFormat formatMonth = new SimpleDateFormat("MM");
				
				String bulan = formatMonth.format(tgl_piutang);
				String tahun = formatYear.format(tgl_piutang);
				
				logger.info(method+"|"+url+"|"+ "bulan piutang :"+bulan);
				logger.info(method+"|"+url+"|"+ "tahun piutang :"+tahun);
				
				bln = Integer.parseInt(bulan);
				thn = Integer.parseInt(tahun);
				umur = Integer.parseInt(body.get("umur_piutang"));
				saldo = Double.parseDouble(body.get("saldo_piutang"));
				
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);
				return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			Date today = new Date();
			Draft nmember = new Draft();
			
			if (body.get("id_draft") == null || body.get("id_draft").isEmpty()) {
				
				Draft check = serviceDraft.findLast();
				String kode = "DRF0001";
				if(check != null) {
					logger.info(method+"|"+url+"|"+ "draft terakhir : "+check.getKode_draft());
					String kd = check.getKode_draft().substring(3);
					int codenow = Integer.parseInt(kd) + 1;	
					int lcdn = String.valueOf(codenow).length();
					if(lcdn == 1) {kode = "DRF000"+String.valueOf(codenow);} 
					else if (lcdn ==2) { kode = "DRF00"+String.valueOf(codenow);}
					else if (lcdn ==3) { kode = "DRF0"+String.valueOf(codenow);}
					else { kode = "DRF"+String.valueOf(codenow);}
				}
				
				String nm_draft = body.get("nama_draft");
				if (body.get("nama_draft") == null || body.get("nama_draft").isEmpty()) {
					nm_draft = kode;
				}
				
				User created = resp.getData();
				nmember.setCreated_by(created.getId_user());
				nmember.setCreated_date(today);
				nmember.setKode_draft(kode);
				nmember.setNama_draft(nm_draft);
				nmember.setFlag_draft(0);
				nmember.setFlag_insert(0);
				nmember.setHeader_bucket("");
				this.serviceDraft.save(nmember);
				
				logger.info(method+"|"+url+"|"+"success save draft");
				int respact = this.serviceAktivitas.createAktivitas("DRAFT",created,method,url,nmember.getId_draft());
				logger.info(method+"|"+url+"|"+"respact: "+respact);
				
			} else {
				nmember = this.serviceDraft.findById(Integer.parseInt(body.get("id_draft")));
			}
			
			if (nmember == null || nmember.getKode_draft() == null || nmember.getKode_draft().isEmpty()) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada ID Draft tersebut"),HttpStatus.BAD_REQUEST);
			} else if (nmember.getFlag_draft() == 2) {
				return new ResponseEntity(new ResponObject(2, "Penambahan Draft ini untuk tipe harian"),HttpStatus.BAD_REQUEST);
			} 
//			else if (nmember.getFlag_insert() == 1) {
//				return new ResponseEntity(new ResponObject(2, "Draft sudah tidak dapat di tambah data."),HttpStatus.BAD_REQUEST);
//			}
			
			DataInput input = new DataInput();
			
			User created = resp.getData();
			input.setCreated_by(created.getId_user());
			input.setCreated_date(today);
					
			input.setId_draft(nmember.getId_draft());
			input.setId_segment(Integer.parseInt(body.get("id_segment")));
			input.setTanggal_piutang(tgl_piutang);
			input.setPeriode_bulan(bln);
			input.setPeriode_tahun(thn);
			input.setId_piutang(body.get("id_piutang").toUpperCase());
			input.setKode_customer(body.get("kode_customer").toUpperCase());
			input.setNama_customer(body.get("nama_customer").toUpperCase());
			input.setKode_proyek(body.get("kode_proyek").toUpperCase());
			input.setNama_proyek(body.get("nama_proyek").toUpperCase());
			input.setPemberi_kerja(body.get("pemberi_kerja").toUpperCase());
			input.setDoc_number(body.get("doc_number"));
			input.setDoc_type(body.get("doc_type").toUpperCase());
			input.setJenis_piutang(body.get("jenis_piutang").toUpperCase());
			input.setUmur_piutang_hari(umur);
			input.setSaldo_piutang(saldo);
			this.serviceInput.save(input);
			
			nmember.setFlag_draft(1);
			this.serviceDraft.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success save draft");
			int respact = this.serviceAktivitas.createAktivitas("INPUT",created,method,url,input.getId_input());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes add data input",input),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PutMapping(value="/input-data/{id}")
	public ResponseEntity<?> updateInputData(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "PUT";
		String url = "/input-data/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api add data-input");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_segment")==null || body.get("id_segment").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null!"),HttpStatus.BAD_REQUEST);
			if (body.get("tanggal_piutang")==null || body.get("tanggal_piutang").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "tanggal_piutang null!"),HttpStatus.BAD_REQUEST);
			if (body.get("umur_piutang")==null || body.get("umur_piutang").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "umur_piutang null!"),HttpStatus.BAD_REQUEST);
			if (body.get("saldo_piutang")==null || body.get("saldo_piutang").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "saldo_piutang null!"),HttpStatus.BAD_REQUEST);
			
			
			logger.info(method+"|"+url+"|"+ "id_draft :"+body.get("id_draft"));
			logger.info(method+"|"+url+"|"+ "nama_draft :"+body.get("nama_draft"));
			logger.info(method+"|"+url+"|"+ "id_segment :"+body.get("id_segment"));
			logger.info(method+"|"+url+"|"+ "tanggal_piutang :"+body.get("tanggal_piutang"));
			logger.info(method+"|"+url+"|"+ "id_piutang :"+body.get("id_piutang"));
			logger.info(method+"|"+url+"|"+ "kode_customer :"+body.get("kode_customer"));
			logger.info(method+"|"+url+"|"+ "nama_customer :"+body.get("nama_customer"));
			logger.info(method+"|"+url+"|"+ "kode_proyek :"+body.get("kode_proyek"));
			logger.info(method+"|"+url+"|"+ "nama_proyek :"+body.get("nama_proyek"));
			logger.info(method+"|"+url+"|"+ "pemberi_kerja :"+body.get("pemberi_kerja"));
			logger.info(method+"|"+url+"|"+ "doc_number :"+body.get("doc_number"));
			logger.info(method+"|"+url+"|"+ "doc_type :"+body.get("doc_type"));
			logger.info(method+"|"+url+"|"+ "jenis_piutang :"+body.get("jenis_piutang"));
			logger.info(method+"|"+url+"|"+ "umur_piutang :"+body.get("umur_piutang"));
			logger.info(method+"|"+url+"|"+ "saldo_piutang :"+body.get("saldo_piutang"));
			
			DataInput input = this.serviceInput.findById(id);
			if (input == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada ID DATA INPUT tersebut"),HttpStatus.BAD_REQUEST);
			}
			
			Segment ch_seg = this.serviceSegment.findById(Integer.parseInt(body.get("id_segment")));
			if (ch_seg == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada ID segment tersebut"),HttpStatus.BAD_REQUEST);
			}
			
			int bln = 0;
			int thn = 0;
			Date tgl_piutang = null;
			int umur = 0;
			double saldo = 0.0;
			
			try {
				
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				tgl_piutang = df.parse(body.get("tanggal_piutang"));
				DateFormat formatYear = new SimpleDateFormat("yyyy");  
				DateFormat formatMonth = new SimpleDateFormat("MM");
				
				String bulan = formatMonth.format(tgl_piutang);
				String tahun = formatYear.format(tgl_piutang);
				
				logger.info(method+"|"+url+"|"+ "bulan piutang :"+bulan);
				logger.info(method+"|"+url+"|"+ "tahun piutang :"+tahun);
				
				bln = Integer.parseInt(bulan);
				thn = Integer.parseInt(tahun);
				umur = Integer.parseInt(body.get("umur_piutang"));
				saldo = Double.parseDouble(body.get("saldo_piutang"));
				
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);
				return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			Date today = new Date();
			Draft nmember = new Draft();
			
			if (body.get("id_draft") == null || body.get("id_draft").isEmpty()) {
				
				Draft check = serviceDraft.findLast();
				String kode = "DRF0001";
				if(check != null) {
					logger.info(method+"|"+url+"|"+ "draft terakhir : "+check.getKode_draft());
					String kd = check.getKode_draft().substring(3);
					int codenow = Integer.parseInt(kd) + 1;	
					int lcdn = String.valueOf(codenow).length();
					if(lcdn == 1) {kode = "DRF000"+String.valueOf(codenow);} 
					else if (lcdn ==2) { kode = "DRF00"+String.valueOf(codenow);}
					else if (lcdn ==3) { kode = "DRF0"+String.valueOf(codenow);}
					else { kode = "DRF"+String.valueOf(codenow);}
				}
				
				String nm_draft = body.get("nama_draft");
				if (body.get("nama_draft") == null || body.get("nama_draft").isEmpty()) {
					nm_draft = kode;
				}
				
				User created = resp.getData();
				nmember.setCreated_by(created.getId_user());
				nmember.setCreated_date(today);
				nmember.setKode_draft(kode);
				nmember.setNama_draft(nm_draft);
				nmember.setHeader_bucket("");
				nmember.setFlag_draft(0);
				nmember.setFlag_insert(0);
				this.serviceDraft.save(nmember);
				
				logger.info(method+"|"+url+"|"+"success save draft");
				int respact = this.serviceAktivitas.createAktivitas("DRAFT",created,method,url,nmember.getId_draft());
				logger.info(method+"|"+url+"|"+"respact: "+respact);
				
			} else {
				nmember = this.serviceDraft.findById(Integer.parseInt(body.get("id_draft")));
			}
			
			if (nmember == null || nmember.getKode_draft() == null || nmember.getKode_draft().isEmpty()) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada ID Draft tersebut"),HttpStatus.BAD_REQUEST);
			} else if (nmember.getFlag_draft() == 2) {
				return new ResponseEntity(new ResponObject(2, "Draft ini tipe bucket, tidak dapat tambah data."),HttpStatus.BAD_REQUEST);
			} 
//			else if (nmember.getFlag_insert() == 1) {
//				return new ResponseEntity(new ResponObject(2, "Draft sudah tidak dapat di tambah data."),HttpStatus.BAD_REQUEST);
//			}
			
			User created = resp.getData();
			input.setUpdated_by(created.getId_user());
			input.setUpdated_date(today);
					
			input.setId_draft(nmember.getId_draft());
			input.setId_segment(Integer.parseInt(body.get("id_segment")));
			input.setTanggal_piutang(tgl_piutang);
			input.setPeriode_bulan(bln);
			input.setPeriode_tahun(thn);
			input.setId_piutang(body.get("id_piutang").toUpperCase());
			input.setKode_customer(body.get("kode_customer").toUpperCase());
			input.setNama_customer(body.get("nama_customer").toUpperCase());
			input.setKode_proyek(body.get("kode_proyek").toUpperCase());
			input.setNama_proyek(body.get("nama_proyek").toUpperCase());
			input.setPemberi_kerja(body.get("pemberi_kerja").toUpperCase());
			input.setDoc_number(body.get("doc_number"));
			input.setDoc_type(body.get("doc_type").toUpperCase());
			input.setJenis_piutang(body.get("jenis_piutang").toUpperCase());
			input.setUmur_piutang_hari(umur);
			input.setSaldo_piutang(saldo);
			this.serviceInput.save(input);
			
			nmember.setFlag_draft(1);
			this.serviceDraft.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success save draft");
			int respact = this.serviceAktivitas.createAktivitas("INPUT",created,method,url,input.getId_input());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update data input",input),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@DeleteMapping(value="/input-data/{id}")
	public ResponseEntity<?> deleteInput(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) throws Exception {
		
		String method = "DELETE";
		String url = "/input-data/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api delete data input");
			logger.info(method+"|"+url+"|"+"input data id: "+id);
			this.serviceInput.delete(id);
			
			int respact = this.serviceAktivitas.createAktivitas("INPUT",resp.getData(),method,url,id);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			logger.info(method+"|"+url+"|"+"delete data input");	
			return new ResponseEntity(new ResponObject(1, "delete Data Input."),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@PostMapping(value="/input-data/upload")
	public ResponseEntity<?> uploadHarian(@RequestHeader("Authorization") String Authorization,
		@RequestParam Map<String, String> body,
		@RequestParam("file") MultipartFile file
		) throws Exception, ParseException {
	
		String method = "POST";
		String url = "/input-data/upload";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info("CREATE INBOUND FILE: "+body.toString());
		logger.info("FILE original: "+file.getOriginalFilename()+" TYPE: "+file.getContentType());
		logger.info("FILE name: "+file.getName());
		
		//try untuk baca extensi file harus CSV
		String extension = "";
		try {
			String split [] = file.getOriginalFilename().split("\\.");
			extension = split[split.length-1];
			if(!extension.toLowerCase().contentEquals("csv")) {
				logger.info("validasi => File type must CSV");
					return new ResponseEntity(new ResponObject(2, "File type must CSV"),HttpStatus.BAD_REQUEST);	
				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		logger.info("Lolos validasi file csv");
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null"),HttpStatus.BAD_REQUEST);	
		if (body.get("id_segment") == null || body.get("id_segment").isEmpty()) return new ResponseEntity(new ResponObject(2, "segment null"),HttpStatus.BAD_REQUEST);	
		
		Date today = new Date();
		Draft draft = new Draft();
		
		Segment ch_seg = this.serviceSegment.findById(Integer.parseInt(body.get("id_segment")));
		if (ch_seg == null) {
			return new ResponseEntity(new ResponObject(2, "Tidak ada ID segment tersebut"),HttpStatus.BAD_REQUEST);
		}
		
		if (body.get("id_draft") == null || body.get("id_draft").isEmpty()) {
			
			Draft check = serviceDraft.findLast();
			String kode = "DRF0001";
			if(check != null) {
				logger.info(method+"|"+url+"|"+ "draft terakhir : "+check.getKode_draft());
				String kd = check.getKode_draft().substring(3);
				int codenow = Integer.parseInt(kd) + 1;	
				int lcdn = String.valueOf(codenow).length();
				if(lcdn == 1) {kode = "DRF000"+String.valueOf(codenow);} 
				else if (lcdn ==2) { kode = "DRF00"+String.valueOf(codenow);}
				else if (lcdn ==3) { kode = "DRF0"+String.valueOf(codenow);}
				else { kode = "DRF"+String.valueOf(codenow);}
			}
			
			String nm_draft = body.get("nama_draft");
			if (body.get("nama_draft") == null || body.get("nama_draft").isEmpty()) {
				nm_draft = kode;
			}
			
			User created = resp.getData();
			draft.setCreated_by(created.getId_user());
			draft.setCreated_date(today);
			draft.setKode_draft(kode);
			draft.setNama_draft(nm_draft);
			draft.setHeader_bucket("");
			draft.setFlag_draft(0);
			draft.setFlag_insert(0);
			this.serviceDraft.save(draft);
			
			logger.info(method+"|"+url+"|"+"success save draft");
			int respact = this.serviceAktivitas.createAktivitas("DRAFT",created,method,url,draft.getId_draft());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
		} else {
			
			draft = this.serviceDraft.findById(Integer.parseInt(body.get("id_draft")));
		}
		
		if (draft == null || draft.getKode_draft() == null || draft.getKode_draft().isEmpty()) {
			return new ResponseEntity(new ResponObject(2, "Tidak ada ID Draft tersebut"),HttpStatus.BAD_REQUEST);
		} else if (draft.getFlag_draft() == 2) {
			return new ResponseEntity(new ResponObject(2, "Penambahan Draft ini untuk tipe harian"),HttpStatus.BAD_REQUEST);
		}
//		else if (draft.getFlag_insert() == 1) {
//			return new ResponseEntity(new ResponObject(2, "Draft sudah tidak dapat di tambah data."),HttpStatus.BAD_REQUEST);
//		}
		
		try {
			logger.info("UPLOAD FILE untuk INPUT");
			logger.info("UPLOAD-FILE id_draft: "+draft.getId_draft());
			logger.info("UPLOAD-FILE kode_draft: "+draft.getKode_draft());
			logger.info("SEGMENT id: "+ch_seg.getId_segment());
			logger.info("SEGMENT kode: "+ch_seg.getKode_segment());
			logger.info("SEGMENT nama: "+ch_seg.getNama_segment());
			
			int lst = 1;
			UploadFile dmy = this.serviceUpload.findLast();
			if (dmy != null) {
				lst = dmy.getId_upload() + 1;
			}
			
			UploadFile nw = new UploadFile();
			nw.setTrue_name(file.getOriginalFilename());
			nw.setType_file(file.getContentType());
			nw.setSize_file((int) file.getSize());
			nw.setType_folder("input");
			nw.setTanggal_upload(today);
			nw.setId_user(resp.getData().getId_user());
				
			String filename = String.valueOf(lst)+today.getTime()+"_"+draft.getKode_draft()+"."+extension;
			logger.info(method+"|"+url+"|"+"filename: "+filename);
			
			nw.setPath_name("input/"+filename);
			
			String uripath = uploadService.storeFile(file,filename,"input");		 
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
			.path("core-ifrs/view_file/input/")
			.path(uripath)
			.toUriString();
			
			this.serviceUpload.save(nw);
			logger.info(method+"|"+url+"|"+"success upload");
			
			logger.info("UPLOAD-FILE uripath: "+uripath);
			logger.info("UPLOAD-FILE fileDownloadUri: "+fileDownloadUri);
			
			KoreksiImport impor = new KoreksiImport();
			impor.setId_draft(draft.getId_draft());
			impor.setDraft(draft);
			impor.setId_segment(ch_seg.getId_segment());
			impor.setSegment(ch_seg);
			impor.setCount_import(0);
			impor.setCount_success(0);
			impor.setCount_failed(0);
			impor.setTgl_import(today.toString());
			impor.setTitle("Import File Type Harian");
			impor.setFile_upload(nw);
			
			List<KoreksiPesan> pesanKoreksi = new ArrayList<KoreksiPesan>();
			
			try (BufferedReader br = new BufferedReader(new FileReader(path_file+"/"+uripath))) {
			    String line;
			  
			    //TEMPLATE: tanggal;idpiutang;kodecustomer;namacustomer;kodeproyek;namaproyek;pemberikerja,dll
			    int i = -1;
				
			    while ((line = br.readLine()) != null) {
			    	i++;
			    	String clean = line.replaceAll("\\P{Print}", ""); //ini perintah untuk remove unicode
			        String[] values = clean.split("\\;");
			        
			        if (i == 0) {
						continue;
					}
			       
			        logger.info("PERULANGAN KE- : "+i);
			        String nomor = String.valueOf(i);
			       
			    try {
			    	
			    	logger.info("Values length : "+values.length);
			        if (values.length != 12) {
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Jumlah field harus 12")); 
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			        
			        String id_piutang = "";String kode_cus = "";String nama_cus = "";String kode_pro = "";String nama_pro = "";String pemberi_kerja = "";
			        String doc_number = "";String doc_type = "";String jenis_piutang = "";
			        		
			        if (values[0] == null || values[0].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "tanggal piutang tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[1] != null) { id_piutang = values[1]; }
			        if (values[2] != null) { kode_cus = values[2]; }
			        if (values[3] != null) { nama_cus = values[3]; }
			        if (values[4] != null) { kode_pro = values[4]; }
			        if (values[5] != null) { nama_pro = values[5]; }
			        if (values[6] != null) { pemberi_kerja = values[6]; }
			        if (values[7] != null) { doc_number = values[7]; }
			        if (values[8]!= null) { doc_type = values[8]; }
			        if (values[9]!= null) { jenis_piutang = values[9]; }
			        if (values[10]== null || values[10].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "umur piutang tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[11]== null || values[11].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "saldo piutang tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        
			        logger.info("Nomor input : "+nomor);
					logger.info("Tanggal piutang : "+values[0]);
					logger.info("ID Piutang : "+id_piutang);
					logger.info("Kode Customer : "+kode_cus);
					logger.info("Nama Customer : "+nama_cus);
					logger.info("Kode Proyek; : "+kode_pro);
					logger.info("Nama Proyek; : "+nama_pro);
					logger.info("Pemberi Kerja : "+pemberi_kerja);
					logger.info("Doc Number : "+doc_number);
					logger.info("Doc Type : "+doc_type);
					logger.info("Jenis Piutang : "+jenis_piutang);
					logger.info("Umur Piutang : "+values[10]);
					logger.info("Saldo Piutang : "+values[11]);
					
					int bln = 0;
					int thn = 0;
					Date tgl_piutang = null;
					int umur = 0;
					double saldo = 0.0;
					
					try {
						
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						tgl_piutang = df.parse(values[0]);
						DateFormat formatYear = new SimpleDateFormat("yyyy");  
						DateFormat formatMonth = new SimpleDateFormat("MM");
						
						String bulan = formatMonth.format(tgl_piutang);
						String tahun = formatYear.format(tgl_piutang);
						
						logger.info(method+"|"+url+"|"+ "bulan piutang :"+bulan);
						logger.info(method+"|"+url+"|"+ "tahun piutang :"+tahun);
						
						bln = Integer.parseInt(bulan);
						thn = Integer.parseInt(tahun);
						umur = Integer.parseInt(values[10]);
						saldo = Double.parseDouble(values[11].replaceAll("\\,", "."));
//						saldo = Double.parseDouble(values[11]);
						
					} catch (Exception e) {
						// TODO: handle exception
						logger.info(e.toString());
						logger.info(this.getClass().toGenericString(), e);
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Pastikan tanggal sesuai format")); 
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
					
					DataInput input = new DataInput();
					User created = resp.getData();
					input.setCreated_by(created.getId_user());
					input.setCreated_date(today);
					input.setId_draft(draft.getId_draft());
					input.setId_segment(ch_seg.getId_segment());
					input.setTanggal_piutang(tgl_piutang);
					input.setPeriode_bulan(bln);
					input.setPeriode_tahun(thn);
					input.setId_piutang(id_piutang.toUpperCase());
					input.setKode_customer(kode_cus.toUpperCase());
					input.setNama_customer(nama_cus.toUpperCase());
					input.setKode_proyek(kode_pro.toUpperCase());
					input.setNama_proyek(nama_pro.toUpperCase());
					input.setPemberi_kerja(pemberi_kerja.toUpperCase());
					input.setDoc_number(doc_number);
					input.setDoc_type(doc_type.toUpperCase());
					input.setJenis_piutang(jenis_piutang.toUpperCase());
					input.setUmur_piutang_hari(umur);
					input.setSaldo_piutang(saldo);
					input.setPath_document("/input/"+uripath);
					this.serviceInput.save(input);
					
					logger.info(method+"|"+url+"|"+"success save input");
					int respact = this.serviceAktivitas.createAktivitas("INPUT-FILE",created,method,url,input.getId_input());
					logger.info(method+"|"+url+"|"+"respact: "+respact);
					
					impor.setCount_import(impor.getCount_import()+1);
					impor.setCount_success(impor.getCount_success()+1);
					
			    	} catch (Exception e) {
						// TODO: handle exception
			    		logger.info(e.toString());
						logger.info(this.getClass().toGenericString(), e);	
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Pastikan sesuai format"));
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			    }
			    
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				impor.setList_pesan(pesanKoreksi);
				return new ResponseEntity(new ResponObject(3, e.getMessage(),impor),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			draft.setFlag_draft(1);
			this.serviceDraft.save(draft);
			impor.setList_pesan(pesanKoreksi);
			return new ResponseEntity(new ResponObject(1, "Data Impor File Berhasil. Silahkan Unggah kembali baris yang gagal.",impor),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);	
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@PostMapping(value="/input-data/upload-bucket")
	public ResponseEntity<?> uploadBucket(@RequestHeader("Authorization") String Authorization,
		@RequestParam Map<String, String> body,
		@RequestParam("file") MultipartFile file
		) throws Exception, ParseException {
	
		String method = "POST";
		String url = "/input-data/upload-bucket";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info("CREATE INBOUND FILE: "+body.toString());
		logger.info("FILE original: "+file.getOriginalFilename()+" TYPE: "+file.getContentType());
		logger.info("FILE name: "+file.getName());
		
		//try untuk baca extensi file harus CSV
		String extension = "";
		try {
			String split [] = file.getOriginalFilename().split("\\.");
			extension = split[split.length-1];
			if(!extension.toLowerCase().contentEquals("csv")) {
				logger.info("validasi => File type must CSV");
					return new ResponseEntity(new ResponObject(2, "File type must CSV"),HttpStatus.BAD_REQUEST);	
				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		logger.info("Lolos validasi file csv");
		
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null"),HttpStatus.BAD_REQUEST);	
		if (body.get("id_segment") == null || body.get("id_segment").isEmpty()) return new ResponseEntity(new ResponObject(2, "id_segment null"),HttpStatus.BAD_REQUEST);	
		if (body.get("jumlah_bucket") == null || body.get("jumlah_bucket").isEmpty()) return new ResponseEntity(new ResponObject(2, "jumlah_bucket null"),HttpStatus.BAD_REQUEST);	
		
		Date today = new Date();
		Draft draft = new Draft();
		
		Segment ch_seg = this.serviceSegment.findById(Integer.parseInt(body.get("id_segment")));
		if (ch_seg == null) {
			return new ResponseEntity(new ResponObject(2, "Tidak ada ID segment tersebut"),HttpStatus.BAD_REQUEST);
		}
		
		int jumlah_bucket = Integer.parseInt(body.get("jumlah_bucket"));
		int flg_new_draft = 0;
		if (body.get("id_draft") == null || body.get("id_draft").isEmpty()) {
			
			Draft check = serviceDraft.findLast();
			String kode = "DRF0001";
			if(check != null) {
				logger.info(method+"|"+url+"|"+ "draft terakhir : "+check.getKode_draft());
				String kd = check.getKode_draft().substring(3);
				int codenow = Integer.parseInt(kd) + 1;	
				int lcdn = String.valueOf(codenow).length();
				if(lcdn == 1) {kode = "DRF000"+String.valueOf(codenow);} 
				else if (lcdn ==2) { kode = "DRF00"+String.valueOf(codenow);}
				else if (lcdn ==3) { kode = "DRF0"+String.valueOf(codenow);}
				else { kode = "DRF"+String.valueOf(codenow);}
			}
			
			String nm_draft = body.get("nama_draft");
			if (body.get("nama_draft") == null || body.get("nama_draft").isEmpty()) {
				nm_draft = kode;
			}
			
			User created = resp.getData();
			draft.setCreated_by(created.getId_user());
			draft.setCreated_date(today);
			draft.setKode_draft(kode);
			draft.setNama_draft(nm_draft);
			draft.setHeader_bucket("");
			this.serviceDraft.save(draft);
			
			logger.info(method+"|"+url+"|"+"success save draft");
			int respact = this.serviceAktivitas.createAktivitas("DRAFT",created,method,url,draft.getId_draft());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
		} else {
			
			draft = this.serviceDraft.findById(Integer.parseInt(body.get("id_draft")));
			flg_new_draft = 1;
		}
		
		if (draft == null || draft.getKode_draft() == null || draft.getKode_draft().isEmpty()) {
			return new ResponseEntity(new ResponObject(2, "Tidak ada ID Draft tersebut"),HttpStatus.BAD_REQUEST);
		} else if (draft.getFlag_draft() == 1) {
			return new ResponseEntity(new ResponObject(2, "Penambahan Draft ini untuk tipe bucket"),HttpStatus.BAD_REQUEST);
		}
		
		try {
			logger.info("UPLOAD FILE untuk INPUT");
			logger.info("UPLOAD-FILE id_draft: "+draft.getId_draft());
			logger.info("UPLOAD-FILE kode_draft: "+draft.getKode_draft());
			logger.info("SEGMENT id: "+ch_seg.getId_segment());
			logger.info("SEGMENT kode: "+ch_seg.getKode_segment());
			logger.info("SEGMENT nama: "+ch_seg.getNama_segment());
			logger.info("Jumlah Bucket: "+jumlah_bucket);
			logger.info("Header Bucket: "+draft.getHeader_bucket());
			
			int lst = 1;
			UploadFile dmy = this.serviceUpload.findLast();
			if (dmy != null) {
				lst = dmy.getId_upload() + 1;
			}
			
			UploadFile nw = new UploadFile();
			nw.setTrue_name(file.getOriginalFilename());
			nw.setType_file(file.getContentType());
			nw.setSize_file((int) file.getSize());
			nw.setType_folder("input");
			nw.setTanggal_upload(today);
			nw.setId_user(resp.getData().getId_user());			
				
			String filename = String.valueOf(lst)+today.getTime()+"_"+draft.getKode_draft()+"."+extension;
			logger.info(method+"|"+url+"|"+"filename: "+filename);
			
			nw.setPath_name("input/"+filename);
			
			String uripath = uploadService.storeFile(file,filename,"input");		 
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
			.path("core-ifrs/view_file/input/")
			.path(uripath)
			.toUriString();
			
			this.serviceUpload.save(nw);
			logger.info(method+"|"+url+"|"+"success upload");
			
			logger.info("UPLOAD-FILE uripath: "+uripath);
			logger.info("UPLOAD-FILE fileDownloadUri: "+fileDownloadUri);
			
			KoreksiImport impor = new KoreksiImport();
			impor.setId_draft(draft.getId_draft());
			impor.setDraft(draft);
			impor.setJumlah_bucket(jumlah_bucket);
			impor.setId_segment(ch_seg.getId_segment());
			impor.setSegment(ch_seg);
			impor.setCount_import(0);
			impor.setCount_success(0);
			impor.setCount_failed(0);
			impor.setTgl_import(today.toString());
			impor.setTitle("Import File Type Bucket");
			impor.setFile_upload(nw);
			
			List<KoreksiPesan> pesanKoreksi = new ArrayList<KoreksiPesan>();
			
			try (BufferedReader br = new BufferedReader(new FileReader(path_file+"/"+uripath))) {
			    String line;
			  
			    int [] interval = new int[jumlah_bucket];
			    
			    //TEMPLATE: tanggal;kodesegment;idpiutang;kodecustomer;namacustomer;kodeproyek;namaproyek;pemberikerja,dll
			    int i = -1;
			    while ((line = br.readLine()) != null) {
			    	i++;
			    	String clean = line.replaceAll("\\P{Print}", ""); //ini perintah untuk remove unicode
			        String[] values = clean.split("\\;");
			        
			        //SELESAIKAN HEADER
			        if (i == 0) {

		        		logger.info("HEADER");
			        	for (int j = 0; j < 10; j++) {
			        		
			        		if (values[j] == null || values[j].isEmpty()) {
			        			deleteDraftUpload(flg_new_draft,draft.getId_draft(),"/input/"+uripath);
			        			return new ResponseEntity(new ResponObject(2, "Header index ["+j+"] NULL"),HttpStatus.BAD_REQUEST);
							}
				        	logger.info("index["+j+"] :"+values[j]);
						}
			        	
			        	logger.info("PENGECEKAN INTERVAL BUCKET");
			        	for (int k = 10; k < (jumlah_bucket +10); k++) {
			        		if (values[k] == null || values[k].isEmpty()) {
			        			deleteDraftUpload(flg_new_draft,draft.getId_draft(),"/input/"+uripath);
			        			return new ResponseEntity(new ResponObject(2, "Header index ["+k+"] NULL"),HttpStatus.BAD_REQUEST);
							}
				        	logger.info("index["+k+"] :"+values[k]);
				        		
							try {
								if (k == 10) {
									interval[0] = Integer.parseInt(values[k]);
								} else if (k == (jumlah_bucket+9)) {
									interval[jumlah_bucket-1] = Integer.parseInt(values[k-1])+1;
								} else {
									
									if (Integer.parseInt(values[k]) <= interval[k-10]) {
										deleteDraftUpload(flg_new_draft,draft.getId_draft(),"/input/"+uripath);
										return new ResponseEntity(new ResponObject(2, "Header index ["+k+"] Pastikan Angka dan lebih besar dari field sebelumnya."),HttpStatus.BAD_REQUEST);
									} else {
										interval[k-10] = Integer.parseInt(values[k]);
									}	
								}
							} catch (Exception e) {
								// TODO: handle exception
								deleteDraftUpload(flg_new_draft,draft.getId_draft(),"/input/"+uripath);
								return new ResponseEntity(new ResponObject(2, "Header index ["+k+"] Pastikan Angka dan lebih besar dari field sebelumnya."),HttpStatus.BAD_REQUEST);
							}
				        	
						}
			        	logger.info("INTERVAL FIXED: "+Arrays.toString(interval));
			        	if (flg_new_draft == 1 && draft.getHeader_bucket() !=null) {
			        		
			        		logger.info("CEK BUCKET SESUAI ENGGA");
			        		if(!Arrays.toString(interval).contentEquals(draft.getHeader_bucket())) {
			        			deleteDraftUpload(flg_new_draft,draft.getId_draft(),"/input/"+uripath);
			        			return new ResponseEntity(new ResponObject(2, "Header Bucket tidak sama dengan data sebelumnya."),HttpStatus.BAD_REQUEST);
			        		}
						}
						continue;
					}
			        
			        logger.info("PERULANGAN KE- : "+i);
			        String nomor = String.valueOf(i);
			    
			    try {
			    	
			    	logger.info("Values length : "+values.length);
			        if (values.length != (10+jumlah_bucket)) {
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Jumlah field harus sesuai. Sistem membaca: "+(10+jumlah_bucket)+" field."));
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			        
			        String id_piutang = "";String kode_cus = "";String nama_cus = "";String kode_pro = "";String nama_pro = "";String pemberi_kerja = "";
			        String doc_number = "";String doc_type = "";String jenis_piutang = "";
			        
			        if (values[0] == null || values[0].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "tanggal piutang tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[1] != null) { id_piutang = values[1]; }
			        if (values[2] != null) { kode_cus = values[2]; }
			        if (values[3] != null) { nama_cus = values[3]; }
			        if (values[4] != null) { kode_pro = values[4]; }
			        if (values[5] != null) { nama_pro = values[5]; }
			        if (values[6] != null) { pemberi_kerja = values[6]; }
			        if (values[7] != null) { doc_number = values[7]; }
			        if (values[8]!= null) { doc_type = values[8]; }
			        if (values[9]!= null) { jenis_piutang = values[9]; }
			        
			        double [] saldo = new double[jumlah_bucket];
			        
			        //saldo nya sekarang.
			        for (int l = 0; l < jumlah_bucket; l++) {
			        	if (values[10+l] != null) {
//							saldo[l] = Double.parseDouble(values[10+l]);
							saldo[l] = Double.parseDouble(values[10+l].replaceAll("\\,", "."));
						}
					}
			        
			        logger.info("Nomor input : "+nomor);
					logger.info("Tanggal piutang : "+values[0]);
					logger.info("ID Piutang : "+id_piutang);
					logger.info("Kode Customer : "+kode_cus);
					logger.info("Nama Customer : "+nama_cus);
					logger.info("Kode Proyek; : "+kode_pro);
					logger.info("Nama Proyek; : "+nama_pro);
					logger.info("Pemberi Kerja : "+pemberi_kerja);
					logger.info("Doc Number : "+doc_number);
					logger.info("Doc Type : "+doc_type);
					logger.info("Jenis Piutang : "+jenis_piutang);
					logger.info("SALDO : "+Arrays.toString(saldo));
					
					int bln = 0;
					int thn = 0;
					Date tgl_piutang = null;
					
					try {
						
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						tgl_piutang = df.parse(values[0]);
						DateFormat formatYear = new SimpleDateFormat("yyyy");  
						DateFormat formatMonth = new SimpleDateFormat("MM");
						
						String bulan = formatMonth.format(tgl_piutang);
						String tahun = formatYear.format(tgl_piutang);
						
						logger.info(method+"|"+url+"|"+ "bulan piutang :"+bulan);
						logger.info(method+"|"+url+"|"+ "tahun piutang :"+tahun);
						
						bln = Integer.parseInt(bulan);
						thn = Integer.parseInt(tahun);
						
					} catch (Exception e) {
						// TODO: handle exception
						logger.info(e.toString());
						logger.info(this.getClass().toGenericString(), e);
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Pastikan tanggal sesuai format")); 
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
					
					for (int j = 0; j < jumlah_bucket; j++) {
						
						if (values[10+j] != null && !values[10+j].contentEquals("0") && !values[j].contentEquals("-")) {
							
							DataInput input = new DataInput();
							User created = resp.getData();
							input.setCreated_by(created.getId_user());
							input.setCreated_date(today);
							input.setId_draft(draft.getId_draft());
							input.setId_segment(ch_seg.getId_segment());
							input.setTanggal_piutang(tgl_piutang);
							input.setPeriode_bulan(bln);
							input.setPeriode_tahun(thn);
							input.setId_piutang(id_piutang.toUpperCase());
							input.setKode_customer(kode_cus.toUpperCase());
							input.setNama_customer(nama_cus.toUpperCase());
							input.setKode_proyek(kode_pro.toUpperCase());
							input.setNama_proyek(nama_pro.toUpperCase());
							input.setPemberi_kerja(pemberi_kerja.toUpperCase());
							input.setDoc_number(doc_number);
							input.setDoc_type(doc_type.toUpperCase());
							input.setJenis_piutang(jenis_piutang.toUpperCase());
							input.setPath_document("/input/"+uripath);
							
							input.setUmur_piutang_hari(interval[j]);
							input.setSaldo_piutang(saldo[j]);
							this.serviceInput.save(input);
							
							logger.info(method+"|"+url+"|"+"success save input");
							int respact = this.serviceAktivitas.createAktivitas("INPUT-FILE",created,method,url,input.getId_input());
							logger.info(method+"|"+url+"|"+"respact: "+respact);
								
						}
					}
					
					
			    	} catch (Exception e) {
						// TODO: handle exception
			    		logger.info(e.toString());
						logger.info(this.getClass().toGenericString(), e);	
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Pastikan sesuai format"));
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			    
				    impor.setCount_import(impor.getCount_import()+1);
					impor.setCount_success(impor.getCount_success()+1);
			    }
			    draft.setFlag_draft(2);
			    draft.setFlag_insert(1);
			    draft.setHeader_bucket(Arrays.toString(interval));
			    draft.setJumlah_bucket(jumlah_bucket);
				this.serviceDraft.save(draft);
			    
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);
				deleteDraftUpload(flg_new_draft,draft.getId_draft(),"/input/"+uripath);
				
				impor.setList_pesan(pesanKoreksi);
				return new ResponseEntity(new ResponObject(3,"Data Impor File Gagal. Mohon koreksi File Impor", impor),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			impor.setList_pesan(pesanKoreksi);
			if (pesanKoreksi.size()>0) {
				deleteDraftUpload(flg_new_draft,draft.getId_draft(),"/input/"+uripath);
				return new ResponseEntity(new ResponObject(1, "Data Impor File Gagal. Mohon koreksi File Impor",impor),HttpStatus.OK);
				
			} else {
				return new ResponseEntity(new ResponObject(1, "Data Impor File Berhasil",impor),HttpStatus.OK);
			}
								
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public List<DataInputJoin> getDataInputJoin(List<DataInput> data){
		
		List<DataInputJoin> result = new ArrayList<DataInputJoin>();
		List<User> dumyUser = new ArrayList<User>();
		List<Draft> dumyDraft = new ArrayList<Draft>();
		List<Segment> dumySegment = new ArrayList<Segment>();
		
		for (DataInput dt : data) {
			DataInputJoin rst = new DataInputJoin();
			
			rst.setId_input(dt.getId_input());
			rst.setId_draft(dt.getId_draft());
			rst.setId_segment(dt.getId_segment());
			
			Draft draft = new Draft();
			if(dumyDraft.size()>0) {
				int loop = 0;
				for(Draft a : dumyDraft) {
					if(a.getId_draft() == dt.getId_draft()) {
						draft = a;
						break;
					} else if (loop == dumyDraft.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						draft = this.serviceDraft.findById(dt.getId_draft());
						dumyDraft.add(draft);
						break;
					}	
					loop++;
				}
			} else {
				draft = this.serviceDraft.findById(dt.getId_draft());
				dumyDraft.add(draft);
			}
			rst.setDraft(draft);
			
			Segment segment = new Segment();
			if(dumySegment.size()>0) {
				int loop = 0;
				for(Segment a : dumySegment) {
					if(a.getId_segment() == dt.getId_segment()) {
						segment = a;
						break;
					} else if (loop == dumySegment.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						segment = this.serviceSegment.findById(dt.getId_segment());
						dumySegment.add(segment);
						break;
					}	
					loop++;
				}
			} else {
				segment = this.serviceSegment.findById(dt.getId_segment());
				dumySegment.add(segment);
			}
			rst.setSegment(segment);
			
			rst.setTanggal_piutang(configGetDate.format(dt.getTanggal_piutang()));
			rst.setPeriode(getPeriode(dt.getPeriode_bulan(), dt.getPeriode_tahun()));
			rst.setPeriode_bulan(dt.getPeriode_bulan());
			rst.setPeriode_tahun(dt.getPeriode_tahun());
		
			rst.setId_piutang(dt.getId_piutang());
			rst.setKode_customer(dt.getKode_customer());
			rst.setNama_customer(dt.getNama_customer());
			rst.setKode_proyek(dt.getKode_proyek());
			rst.setNama_proyek(dt.getNama_proyek());
			rst.setPemberi_kerja(dt.getPemberi_kerja());
			rst.setDoc_number(dt.getDoc_number());
			rst.setDoc_type(dt.getDoc_type());
			rst.setJenis_piutang(dt.getJenis_piutang());
			rst.setUmur_piutang_hari(dt.getUmur_piutang_hari());
			rst.setSaldo_piutang(dt.getSaldo_piutang());
			rst.setPath_document(dt.getPath_document());
			
			rst.setCreated_by(dt.getCreated_by());
			rst.setCreated_date(configGetDate.format(dt.getCreated_date()));
			if(dt.getUpdated_date()!=null) {
				rst.setUpdated_by(dt.getUpdated_by());
				rst.setUpdated_date(configGetDate.format(dt.getUpdated_date()));
			}
			
			User user = new User();
			if(dumyUser.size()>0) {
				int loop = 0;
				for(User a : dumyUser) {
					if(a.getId_user() == dt.getCreated_by()) {
						user = a;
						break;
					} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						user = this.serviceUser.findById(dt.getCreated_by());
						dumyUser.add(user);
						break;
					}	
					loop++;
				}
			} else {
				user = this.serviceUser.findById(dt.getCreated_by());
				dumyUser.add(user);
			}
			rst.setCreated_user(user);
			result.add(rst);
		}
		return result;
	}	
	public String getPeriode(int bln, int tahun) {
		if (bln == 1) {
			return "Jan "+tahun;
		} else if (bln == 2) {
			return "Feb "+tahun;
		} else if (bln == 3) {
			return "Mar "+tahun;
		} else if (bln == 4) {
			return "Apr "+tahun;
		} else if (bln == 5) {
			return "May "+tahun;
		} else if (bln == 6) {
			return "Jun "+tahun;
		} else if (bln == 7) {
			return "Jul "+tahun;
		} else if (bln == 8) {
			return "Aug "+tahun;
		} else if (bln == 9) {
			return "Sep "+tahun;
		} else if (bln == 10) {
			return "Oct "+tahun;
		} else if (bln == 11) {
			return "Nov "+tahun;
		} else {
			return "Dec "+tahun;
		}
	}
	
	public void deleteDraftUpload(int flag,int id, String path) {
		logger.info("DELETE DATA FILE");
		logger.info("FLAG : "+flag);
		logger.info("ID : "+id);
		logger.info("PATH : "+path);
		if (flag == 0) {
			this.serviceDraft.delete(id);
			this.serviceInput.deleteDataInputByPathFile(path);
		} else {
			this.serviceInput.deleteDataInputByPathFile(path);
		}
	}

}
