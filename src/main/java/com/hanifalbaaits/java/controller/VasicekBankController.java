package com.hanifalbaaits.java.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.VasicekBank;
import com.hanifalbaaits.java.modelDB.VasicekRating;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelRespon.VasicekBankJoin;
import com.hanifalbaaits.java.service.VasicekBankService;
import com.hanifalbaaits.java.service.VasicekRatingService;

@RestController
@RequestMapping(value="core-ifrs")
public class VasicekBankController {
	
	private Logger logger = LoggerFactory.getLogger(VasicekBankController.class);

	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	VasicekBankService serviceBank;
	
	@Autowired
	VasicekRatingService serviceRating;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/vas-bank")
	public ResponseEntity<?> getBank(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/vas-bank";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get vasicek bank");
			
			List<VasicekBank> data = serviceBank.findAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data Bank");
				return new ResponseEntity(new ResponList(1, "empty Bank",data),HttpStatus.OK);
			}
			
			List<VasicekBankJoin> res = joinBank(data);
			
			logger.info(method+"|"+url+"|"+"data user");
			return new ResponseEntity(new ResponList(1, "List Bank",res),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/vas-bank")
	public ResponseEntity<?> AddBank(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) {
		
		
		String method = "POST";
		String url = "/vas-bank";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api add rating");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			if (body.get("akronim")==null || body.get("akronim").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "akronim null!"),HttpStatus.BAD_REQUEST);
			if (body.get("tahun")==null || body.get("tahun").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "tahun null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_rating")==null || body.get("akronim").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "akronim null!"),HttpStatus.BAD_REQUEST);
			if (body.get("referensi")==null || body.get("referensi").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "referensi null!"),HttpStatus.BAD_REQUEST);
			
			
			logger.info(method+"|"+url+"|"+ "nama :"+body.get("nama"));
			logger.info(method+"|"+url+"|"+ "akronim :"+body.get("akronim"));
			logger.info(method+"|"+url+"|"+ "id_rating :"+body.get("id_rating"));
			logger.info(method+"|"+url+"|"+ "tahun :"+body.get("tahun"));
			logger.info(method+"|"+url+"|"+ "referensi :"+body.get("referensi"));
			
			
			//check terlebih dahulu.
			VasicekBank chk = this.serviceBank.findBySame(body.get("nama").toUpperCase(), body.get("akronim").toUpperCase(), Integer.parseInt(body.get("tahun")));
			if (chk != null) {
				return new ResponseEntity(new ResponObject(2, "Data Bank sudah ada, pastikan kembali Nama, Rating , dan Tahun"),HttpStatus.BAD_REQUEST);
			}
			
			
			VasicekRating rat = this.serviceRating.findById(Integer.parseInt(body.get("id_rating")));
			
			VasicekBank nmember = new VasicekBank();
			nmember.setNama_bank(body.get("nama").toUpperCase());
			nmember.setAkronim(body.get("akronim").toUpperCase());
			nmember.setId_rating(Integer.parseInt(body.get("id_rating")));
			nmember.setTahun(Integer.parseInt(body.get("tahun")));
			nmember.setReferensi(body.get("referensi"));
			nmember.setNilai(rat.getValue());
			this.serviceBank.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success save rating");
			int respact = this.serviceAktivitas.createAktivitas("BANK",resp.getData(),method,url,nmember.getId_vas_bank());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes add bank",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/vas-bank/{id}")
	public ResponseEntity<?> updateBank(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) {
		
		String method = "PUT";
		String url = "/vas-bank/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api put user");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			if (body.get("akronim")==null || body.get("akronim").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "akronim null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_rating")==null || body.get("akronim").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "akronim null!"),HttpStatus.BAD_REQUEST);
			if (body.get("referensi")==null || body.get("referensi").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "referensi null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "referensi :"+body.get("referensi"));
			logger.info(method+"|"+url+"|"+ "nama :"+body.get("nama"));
			logger.info(method+"|"+url+"|"+ "akronim :"+body.get("akronim"));
			logger.info(method+"|"+url+"|"+ "id_rating :"+body.get("id_rating"));
		
			VasicekBank nmember = this.serviceBank.findById(id);
			if (nmember == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada data bank"),HttpStatus.BAD_REQUEST);
			}
			
			//check terlebih dahulu.
			VasicekBank chk = this.serviceBank.findBySame(body.get("nama").toUpperCase(), body.get("akronim").toUpperCase(), Integer.parseInt(body.get("tahun")));
			if (chk != null) {
				return new ResponseEntity(new ResponObject(2, "Data Bank sudah ada, pastikan kembali Nama, Rating , dan Tahun"),HttpStatus.BAD_REQUEST);
			}
			
			VasicekRating rat = this.serviceRating.findById(Integer.parseInt(body.get("id_rating")));
			
			nmember.setNama_bank(body.get("nama").toUpperCase());
			nmember.setAkronim(body.get("akronim").toUpperCase());
			nmember.setReferensi(body.get("referensi"));
			nmember.setId_rating(Integer.parseInt(body.get("id_rating")));
			nmember.setTahun(Integer.parseInt(body.get("tahun")));
			nmember.setNilai(rat.getValue());
			this.serviceBank.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success update rating");
			int respact = this.serviceAktivitas.createAktivitas("BANK",resp.getData(),method,url,nmember.getId_vas_bank());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update bank",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@DeleteMapping(value="/vas-bank/{id}")
	public ResponseEntity<?> deleteBank(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) {
		
		String method = "DELETE";
		String url = "/vas-bank/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api delete bank");
		
			VasicekBank nmember = this.serviceBank.findById(id);
			if (nmember == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada data bank"),HttpStatus.BAD_REQUEST);
			}
			this.serviceBank.delete(id);
			
			logger.info(method+"|"+url+"|"+"success delete bank");
			int respact = this.serviceAktivitas.createAktivitas("BANK",resp.getData(),method,url,nmember.getId_vas_bank());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes delete bank",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public List<VasicekBankJoin> joinBank(List<VasicekBank> banks){
		
		List<VasicekBankJoin> res = new ArrayList<VasicekBankJoin>();
		for (VasicekBank bank : banks) {
			VasicekBankJoin bk = new VasicekBankJoin();
			bk.setId_vas_bank(bank.getId_vas_bank());
			bk.setNama_bank(bank.getNama_bank());
			bk.setAkronim(bank.getAkronim());
			bk.setId_rating(bank.getId_rating());
			bk.setTahun(bank.getTahun());
			bk.setNilai(bank.getNilai());
			bk.setReferensi(bank.getReferensi());
	
			VasicekRating rt = this.serviceRating.findById(bank.getId_rating());
			if (rt != null) {
				bk.setRating(rt);
			}
			res.add(bk);
		}
		return res;
	}
	
}
