package com.hanifalbaaits.java.controller;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelDB.UploadFile;
import com.hanifalbaaits.java.service.UploadFileService;
import com.hanifalbaaits.java.service.UploadService;

@RestController
@RequestMapping(value="core-ifrs")
public class InboundFileController {
	
	private Logger logger = LoggerFactory.getLogger(InboundFileController.class);

	@Value("${file.path}")
	String path_file;
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
    UploadService uploadService;
	
	@Autowired
	UploadFileService serviceUpload;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/upload_file")
	public ResponseEntity<?> getAll(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/upload_file";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+ "hit api get master upload_file");
			
			List<UploadFile> data = serviceUpload.findAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"tidak ada data upload_file");
				return new ResponseEntity(new ResponList(1, "empty upload_file",data),HttpStatus.OK);
			}
			
			logger.info(method+"|"+url+"success List upload_file");
			return new ResponseEntity(new ResponList(1, "success List upload_file",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@DeleteMapping(value="/upload_file/{id}")
	public ResponseEntity<?> DeleteFile(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) {
		
		String method = "GET";
		String url = "/upload_file";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+ "hit api master upload_file delete");
			logger.info(method+"|"+url+ "upload_file id: "+id);
			UploadFile file = this.serviceUpload.findById(id);
			this.serviceUpload.delete(id);

			logger.info(method+"|"+url+ "path : "+path_file+"/"+file.getPath_name());	
			File filelama = new File(path_file+"/"+file.getPath_name());	
			filelama.delete();			
	
			logger.info(method+"|"+url+ "success delete upload_file");	
			int respact = this.serviceAktivitas.createAktivitas("FILE",resp.getData(),method,url,id);
			logger.info(method+"|"+url+"respact: "+respact);	
			
			return new ResponseEntity(new ResponList(1, "success delete upload_file"),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/upload_file")
	public ResponseEntity<?> UploadFile(@RequestHeader("Authorization") String Authorization,
		@RequestParam Map<String, String> body,
		@RequestParam("file") MultipartFile file
			) {
		
		String method = "GET";
		String url = "/upload_file";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api upload");
			logger.info(method+"|"+url+"|"+"detail: "+file.toString());
			logger.info(method+"|"+url+"|"+"detail: "+file.getInputStream());
			logger.info(method+"|"+url+"|"+"detail: "+file.getOriginalFilename());
			logger.info(method+"|"+url+"|"+"detail: "+file.getContentType());
			
			if (body.get("type").isEmpty() || body.get("type") == null) {
				logger.info(method+"|"+url+"|"+resp.getMessage());
				return new ResponseEntity(new ResponObject(2, "type null!"),HttpStatus.BAD_REQUEST);
			}
			
			if (!body.get("type").contentEquals("profile") && !body.get("type").contentEquals("input")) {
				logger.info(method+"|"+url+"|"+resp.getMessage());
				return new ResponseEntity(new ResponObject(2, "type doesnt exist!"),HttpStatus.BAD_REQUEST);
			}
			
			String[] fileFrags = file.getOriginalFilename().split("\\.");
			String extension = fileFrags[fileFrags.length-1];
			Date today = new Date();
			
			int lst = 1;
			UploadFile dmy = this.serviceUpload.findLast();
			if (dmy != null) {
				lst = dmy.getId_upload() + 1;
			}
			
			UploadFile nw = new UploadFile();
			
			nw.setTrue_name(file.getOriginalFilename());
			nw.setType_file(file.getContentType());
			nw.setType_folder(body.get("type"));
			nw.setSize_file((int) file.getSize()); 
			nw.setTanggal_upload(today);
			nw.setId_user(resp.getData().getId_user());
				
			String filename = String.valueOf(lst)+today.getTime()+"."+extension;
			logger.info(method+"|"+url+"|"+"filename: "+filename);
			nw.setPath_name(body.get("type")+"/"+filename);
			
			String uripath = uploadService.storeFile(file,filename,body.get("type"));		 
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
			.path("core-ifrs/view_file/"+body.get("type")+"/")
			.path(uripath)
			.toUriString();
			
			this.serviceUpload.save(nw);
			logger.info(method+"|"+url+"|"+"success upload");
			return new ResponseEntity(new ResponObject(1, fileDownloadUri,nw),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/view_file/{type}/{fileName:.+}")
    public ResponseEntity<Resource> viewFile(@PathVariable String type,
    	@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = uploadService.loadFileAsResource(fileName,type);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
        	logger.info("Couldnt Determine type file");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
	
	@GetMapping("/download_file/{type}/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String type,
    		@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = uploadService.loadFileAsResource(fileName,type);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
        	logger.info("Couldnt Determine type file");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
	
}
