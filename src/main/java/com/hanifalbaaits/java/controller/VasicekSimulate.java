package com.hanifalbaaits.java.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.VasicekAsetList;
import com.hanifalbaaits.java.modelDB.VasicekBank;
import com.hanifalbaaits.java.modelDB.VasicekGDP;
import com.hanifalbaaits.java.modelDB.VasicekRating;
import com.hanifalbaaits.java.modelDB.VasicekResultAset;
import com.hanifalbaaits.java.modelDB.VasicekResultDraft;
import com.hanifalbaaits.java.modelDB.VasicekResultMacro;
import com.hanifalbaaits.java.modelDB.VasicekResultPD;
import com.hanifalbaaits.java.modelRespon.GetVasicek;
import com.hanifalbaaits.java.modelRespon.GetVasicekAset;
import com.hanifalbaaits.java.modelRespon.GetVasicekMacroList;
import com.hanifalbaaits.java.modelRespon.GetVasicekPD;
import com.hanifalbaaits.java.modelRespon.GetVasicekSummary;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelRespon.VasicekBankJoin;
import com.hanifalbaaits.java.modelUtils.RequestAsetECL;
import com.hanifalbaaits.java.modelUtils.RequestVasicek;
import com.hanifalbaaits.java.service.VasicekGdpService;
import com.hanifalbaaits.java.service.VasicekRatingService;
import com.hanifalbaaits.java.service.SaveVasicekResultService;
import com.hanifalbaaits.java.service.UserService;
import com.hanifalbaaits.java.service.VasicekAsetListService;
import com.hanifalbaaits.java.service.VasicekAsetService;
import com.hanifalbaaits.java.service.VasicekBankService;

@RestController
@RequestMapping(value="core-ifrs")
public class VasicekSimulate {

	private Logger logger = LoggerFactory.getLogger(VasicekSimulate.class);
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	UserService serviceUser;
	
	@Autowired
	VasicekGdpService serviceGDP;
	
	@Autowired
	MasterDataInput serviceInput;
	
	@Autowired
	MasterInputMevFL serviceMevFL;
	
	@Autowired
	VasicekRatingService serviceRating;
	
	@Autowired
	VasicekAsetService serviceAsset;
	
	@Autowired
	VasicekAsetListService serviceListAsset;
	
	@Autowired
	VasicekBankService serviceBank;
	
	@Autowired
	SaveVasicekResultService serviceVasicek;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	//SIMULATE MacroEconomic Agustmen ASET di BODY
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/vasicek/simulate")
	public ResponseEntity<?> vasicekSimulate(@RequestHeader("Authorization") String Authorization,
			@RequestBody RequestVasicek body) throws Exception {
		
		String method = "POST";
		String url = "/vasicek/simulate";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.getInterval()==null || body.getInterval().toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
		if (body.getStart_date()==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.getEnd_date()==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		if (body.getPersen_normal()==null ) return new ResponseEntity(new ResponObject(2, "persen_normal null"),HttpStatus.BAD_REQUEST);
		if (body.getPersen_upturn()==null ) return new ResponseEntity(new ResponObject(2, "persen_upturn null"),HttpStatus.BAD_REQUEST);		
		if (body.getPersen_downturn()==null ) return new ResponseEntity(new ResponObject(2, "persen_downturn null"),HttpStatus.BAD_REQUEST);
		
		if (body.getLgd()==null ) return new ResponseEntity(new ResponObject(2, "LGD null"),HttpStatus.BAD_REQUEST);		
		if (body.getPeriode_ecl()==null ) return new ResponseEntity(new ResponObject(2, "Periode ECL null"),HttpStatus.BAD_REQUEST);
		
		try {
			logger.info( "hit api get get simulate vasicek");

			int interval = Integer.parseInt(body.getInterval());
			if (interval != 3 && interval != 6 && interval != 12) {
				logger.info("interval hanya di quartal , semester , tahun."); //3 6 9 12.
				return new ResponseEntity(new ResponList(2, "interval hanya tersedia 3 , 6 ,12"),HttpStatus.BAD_REQUEST);
			}
			
			logger.info( "interval : "+interval);
			logger.info( "start_date : "+body.getStart_date());
			logger.info( "end_date : "+body.getEnd_date());
			
			List<VasicekGDP> list = new ArrayList<VasicekGDP>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date date1 = new Date();
			Date date2 = new Date();
			
			double per_normal = 1;
			double per_upturn = 0.0;
			double per_downturn = 0.0;
			try {
				if (!body.getStart_date().isEmpty() && !body.getEnd_date().isEmpty()) {
					

					date1 = df.parse(body.getStart_date());
					date2 = df.parse(body.getEnd_date());
					
					logger.info( "start : "+date1);
					logger.info( "end : "+date2);	
				}
				
				if (!body.getPersen_normal().isEmpty()) {
					
					per_normal = Double.parseDouble(body.getPersen_normal()) / 100;
					per_upturn = Double.parseDouble(body.getPersen_upturn()) / 100;
					per_downturn = Double.parseDouble(body.getPersen_downturn()) /100;
					
					if ((per_normal + per_upturn + per_downturn) != 1) {
						return new ResponseEntity(new ResponList(2, "hasil persentase ketika di jumlahkan harus 100"),HttpStatus.BAD_REQUEST);
					}
				}
				
			} catch (Exception e) {
				// TODO: handle exception
//				e.printStackTrace();
			}
			
			if (body.getStart_date() == "" || body.getEnd_date() == "") {
				list = serviceGDP.findAll();
			} else {
				list = serviceGDP.findStartEnd(date1 , date2);
			}
			
			GetVasicek mcr = new GetVasicek();
			mcr.setTitle_bucket("MacroEconomic Adjustment");
			mcr.setPeriode_mulai(body.getStart_date());
			mcr.setPeriode_selesai(body.getEnd_date());
			mcr.setInterval_bucket(interval);
			mcr.setPersen_normal(per_normal);
			mcr.setPersen_uptum(per_upturn);
			mcr.setPersen_downtum(per_downturn);
			
			List<String> header = new ArrayList<String>();
			
			//sengaja karena data tidak ada per bulan, interval dibagi 3.
			interval = interval / 3;
			
			double count_dt = list.size() / (double) interval;
			
			System.out.println("COUNT DT before: "+count_dt);
			
			if (count_dt > (list.size() / interval)) {
				count_dt += 1;
			}
			
			double [] nilai = new double[(int) count_dt];
			int [] flg_projection = new int[(int) count_dt];
			double t_nilai = 0.0;
			
			System.out.println("SIZE SAMPLE : "+list.size());
			System.out.println("INTERVAL : "+interval);
			System.out.println("STEP : "+(list.size() / interval)+1);
			
			List<GetVasicekMacroList> list_macro = new ArrayList<GetVasicekMacroList>();
			
			int index_nilai = 0;
			int i = 0;
			int count_projection = 0;
			for (VasicekGDP vasicekGDP : list) {
				if ((i % interval) == 0) {
					System.out.println("BULAN TAHUN : "+vasicekGDP.getBulan()+" "+vasicekGDP.getTahun());
					header.add(	serviceInput.getPeriode(vasicekGDP.getBulan(), vasicekGDP.getTahun()));
					nilai[index_nilai] = vasicekGDP.getValue_gdp();
					flg_projection[index_nilai] = vasicekGDP.getFlg_projection();
					if (vasicekGDP.getFlg_projection() == 1) {
						count_projection += 1;
					}
					t_nilai += vasicekGDP.getValue_gdp();
					index_nilai++;
				} 
				i++;
			}
			
			//hitung model hanya yg bukan projeksion.
			double [] list_noprojection = new double[(nilai.length-count_projection)];
			double t_noprojection = 0.0;
			int ix = 0;
			for (int j = 0; j < nilai.length; j++) {
				if (flg_projection[j] == 0) {
					list_noprojection[ix] = nilai[j];
					t_noprojection += nilai[j];
					ix++;
				}
			}
			
			
			double mean = (t_noprojection / list_noprojection.length);
			double stdev = serviceMevFL.calculateSD(list_noprojection);
			double confidence_level = 0.99;
			double t_distribution = 2.57582939565744; //TINV(1-99%, 9999999999) statis / rumus.
			
			double [] list_upturn = new double [nilai.length];
			double [] list_downturn = new double [nilai.length];
			double [] list_weighted = new double [nilai.length];
			double [] list_smev_normal = new double [nilai.length];
			double [] list_smev_weight = new double [nilai.length];
			
			for (int j = 0; j < nilai.length; j++) {
				
				double standard = (nilai[j] - mean) / stdev;
				
				if (standard > t_distribution) {
					standard = t_distribution;
				} else if (standard < (-1 * t_distribution)) {
					standard = t_distribution;
				}
				
				list_smev_normal[j] = standard;
				
				if (flg_projection[j] == 1) {
					
					list_upturn[j] = nilai[j] + (1 * stdev);
					list_downturn[j] = nilai[j] - (1.5 * stdev);
					double weigted = (per_normal * nilai[j]) + (per_upturn * (nilai[j] + (1 * stdev))) + (per_downturn * (nilai[j] - (1.5 * stdev)));
					list_weighted[j] = weigted;
					double standard_weight = (weigted - mean) / stdev;
					
					if (standard_weight > t_distribution) {
						standard_weight = t_distribution;
					} else if (standard_weight < (-1 * t_distribution)) {
						standard_weight = t_distribution;
					}
					list_smev_weight[j] = standard_weight;			
				} 
			}
			
			GetVasicekMacroList macro_normal = new GetVasicekMacroList();
			macro_normal.setTitle("GDP_IND (Normal)");
			macro_normal.setNilai(nilai);
			list_macro.add(macro_normal);
			
			GetVasicekMacroList macro_upturn = new GetVasicekMacroList();
			macro_upturn.setTitle("GDP_IND (Upturn)");
			macro_upturn.setNilai(list_upturn);
			list_macro.add(macro_upturn);
			
			GetVasicekMacroList macro_downturn = new GetVasicekMacroList();
			macro_downturn.setTitle("GDP_IND (Downturn)");
			macro_downturn.setNilai(list_downturn);
			list_macro.add(macro_downturn);
			
			GetVasicekMacroList macro_weighted = new GetVasicekMacroList();
			macro_weighted.setTitle("GDP_IND (Weighted)");
			macro_weighted.setNilai(list_weighted);
			list_macro.add(macro_weighted);
			
			GetVasicekMacroList macro_smev_normal = new GetVasicekMacroList();
			macro_smev_normal.setTitle("SMEV (Normal)");
			macro_smev_normal.setNilai(list_smev_normal);
			list_macro.add(macro_smev_normal);
			
			GetVasicekMacroList macro_smev_weigted = new GetVasicekMacroList();
			macro_smev_weigted.setTitle("SMEV (Weighted)");
			macro_smev_weigted.setNilai(list_smev_weight);
			list_macro.add(macro_smev_weigted);
			
			mcr.setHeader_macroeconomic(header);
			mcr.setList_flg_projection(flg_projection);
			mcr.setList_macroeconomic(list_macro);
			mcr.setHistorical_mean(mean);
			mcr.setHistorical_stdev(stdev);
			mcr.setConfidence_level(confidence_level);
			mcr.setT_distribution(t_distribution);
		
			//DISINI SET PD VASICEK
			String periode_projection = header.get(header.size()-1);
			List<VasicekRating> activeRatings = this.serviceRating.findByStatus(1);
			List<GetVasicekPD> vasicek_pd = new ArrayList<GetVasicekPD>();
			
			//disini masih kacau langsung get akhir index, harusnya liat projection nya.
			double macro_adjustment_normal = list_smev_normal[list_smev_normal.length-1];
			double macro_adjustment_weighted = list_smev_weight[list_smev_weight.length-1]; 
			
			double check_min_normal = 1.0; //hanya sebagai tanda awal.
			double check_min_weighted = 1.0; //hanya sebagai tanda awal.
			
			for (VasicekRating activeRating : activeRatings) {
				
				GetVasicekPD rat = new GetVasicekPD();
				rat.setId_rating(activeRating.getId_rating());
				rat.setRating(activeRating);
				rat.setValue_rating(activeRating.getValue());
				rat.setPeriode_projection(periode_projection);
				
				double base_formula = (0.12 * (1 - Math.exp(-50 * activeRating.getValue())) / (1 - Math.exp(-50))) + 
						(0.24 * (1 - (1 - Math.exp(-50 * activeRating.getValue()))) / (1 - Math.exp(-50)));
				
				rat.setValue_base_formula(base_formula);
				
				//VASICEK NORMAL.
				double vasicekNormal = 0.0;
				if (activeRating.getValue() <= 0 || activeRating.getValue() >= 1.0) {
					vasicekNormal = activeRating.getValue();
				} else {
					
					//NORMSDIST( (NORMSINV(valueRating) - SQRT(BASE FORMULA) * MacroAdjustment) / SQRT(1 - BASE FORMULA));
					double v_normsinv = normsInv(activeRating.getValue()) - (Math.sqrt(base_formula) * macro_adjustment_normal);
					
					vasicekNormal = normsDist(v_normsinv / Math.sqrt(1 - base_formula));
				}
				rat.setVasicek_normal(vasicekNormal);
				
				if (vasicekNormal <= 0 || vasicekNormal >= 1) {
					//no condition.
				} else {
					if (vasicekNormal < check_min_normal) {
						check_min_normal = vasicekNormal;
					}
				}
				
				//VASICEK Weighted.
				double vasicekWeigted = 0.0;
				if (activeRating.getValue() <= 0 || activeRating.getValue() >= 1.0) {
					vasicekWeigted = activeRating.getValue();
				} else {
					
					//NORMSDIST( (NORMSINV(valueRating) - SQRT(BASE FORMULA) * MacroAdjustment) / SQRT(1 - BASE FORMULA));
					double v_normsinv = normsInv(activeRating.getValue()) - (Math.sqrt(base_formula) * macro_adjustment_weighted);
					
					vasicekWeigted = normsDist(v_normsinv / Math.sqrt(1 - base_formula));
				}
				rat.setVasicek_weighted(vasicekWeigted);
				
				if (vasicekWeigted <= 0 || vasicekWeigted >= 1) {
					//no condition.
				} else {
					if (vasicekWeigted < check_min_weighted) {
						check_min_weighted = vasicekWeigted;
					}
				}
				
				vasicek_pd.add(rat);
			}
			
			
			//KARENA DIATAS BARU KETAUAN NILAI MINIMAL NYA , FOREACH KEMBALI UNTUK ISI ADJUSTED.
			for (GetVasicekPD getVasicekPD : vasicek_pd) {
				if (getVasicekPD.getVasicek_normal() == 0) {
//					getVasicekPD.setVasicek_adjusted_normal(check_min_normal); update biarkan jadi 0 ngga usah di adjusted ke minimal
					getVasicekPD.setVasicek_adjusted_normal(getVasicekPD.getVasicek_normal());
				} else if (getVasicekPD.getVasicek_normal() == 1.0) {
					getVasicekPD.setVasicek_adjusted_normal(1.0);
				} else {
					getVasicekPD.setVasicek_adjusted_normal(getVasicekPD.getVasicek_normal());
				}
				
				if (getVasicekPD.getVasicek_weighted() == 0) {
//					getVasicekPD.setVasicek_adjusted_weighted(check_min_weighted); update biarkan jadi 0 ngga usah di adjusted ke minimal
					getVasicekPD.setVasicek_adjusted_weighted(getVasicekPD.getVasicek_weighted());
				} else if (getVasicekPD.getVasicek_weighted() == 1.0) {
					getVasicekPD.setVasicek_adjusted_weighted(1.0);
				} else {
					getVasicekPD.setVasicek_adjusted_weighted(getVasicekPD.getVasicek_weighted());
				}
			}
			
			mcr.setList_pd_vasicek(vasicek_pd);
			
			
			//LIST ASET ECL
			DateFormat df_ecl = new SimpleDateFormat("dd/MM/yyyy");
			Date date_ecl = df_ecl.parse(body.getPeriode_ecl());
			
			List<RequestAsetECL> req_aset = body.getList_aset_ecl();
			double lgd = Double.parseDouble(body.getLgd()) / 100 ;
			
			List<GetVasicekAset> aset_list = new ArrayList<GetVasicekAset>();
			List<GetVasicekSummary> sumary_list = new ArrayList<GetVasicekSummary>();
			
			double total_saldo = 0.0;
			double total_ecl_normal = 0.0;
			double total_ecl_weight = 0.0;
			List<Integer> fil = new ArrayList<Integer>();
			
			int asx = 0;
			for (RequestAsetECL req_as : req_aset) {
				
				GetVasicekAset ase = new GetVasicekAset();
				ase.setId_aset(Integer.parseInt(req_as.getId_aset()));
				ase.setAset(this.serviceAsset.findById(Integer.parseInt(req_as.getId_aset())));
				ase.setNo_rekening(req_as.getNo_rekening());
				ase.setMata_uang(req_as.getMata_uang());
				ase.setId_bank(Integer.parseInt(req_as.getId_bank()));
				
				VasicekBank bank = this.serviceBank.findById(Integer.parseInt(req_as.getId_bank()));
				VasicekBankJoin bank_join = new VasicekBankJoin();
				bank_join.setId_vas_bank(bank.getId_vas_bank());
				bank_join.setNama_bank(bank.getNama_bank());
				bank_join.setAkronim(bank.getAkronim());
				bank_join.setId_rating(bank.getId_rating());
				bank_join.setTahun(bank.getTahun());
				bank_join.setNilai(bank.getNilai());
				bank_join.setRating(this.serviceRating.findById(bank.getId_rating()));
				bank_join.setReferensi(bank.getReferensi());
				ase.setBank(bank_join);
				
				double bunga = Double.parseDouble(req_as.getSuku_bunga()) / 100;
				ase.setSuku_bunga(bunga);
				ase.setTgl_jatuh_tempo(req_as.getTgl_jatuh_tempo());
				
				double saldo = Double.parseDouble(req_as.getSaldo());
				total_saldo += saldo;
				ase.setSaldo(saldo);
				ase.setLgd(lgd);
				
				double scan_normal = 0.0;
				double scan_weighted = 0.0;
				
				for (GetVasicekPD vasi : vasicek_pd) {
					if (vasi.getId_rating() == bank.getId_rating()) {
						scan_normal = vasi.getVasicek_adjusted_normal();
						scan_weighted = vasi.getVasicek_adjusted_weighted();
						break;
					}
				}
				
				ase.setPd_normal_scan(scan_normal);
				ase.setPd_weighted_scan(scan_weighted);
				
				DateFormat dfx = new SimpleDateFormat("dd/MM/yyyy");
				int count_day = calculateNumberOfDaysBetween(date_ecl, dfx.parse(req_as.getTgl_jatuh_tempo()));
				count_day -= 1;
				
				System.out.println("COUNT DAY: "+count_day);
				//(K7*(1-((1-L7)^((J7-$C$4)/360)))*N7)
				double norx = Math.pow((1.0 - scan_normal), (count_day / 360.0));
				System.out.println("Norx: "+norx);
				double nor = saldo * (1.0 - norx) * lgd;
				System.out.println("Nor: "+nor);
				ase.setEcl_normal_scan(nor);
				total_ecl_normal += nor;
				
				double norx_w = Math.pow((1.0 - scan_weighted), (count_day / 360.0));
				System.out.println("Norx_w: "+norx_w);
				double nor_w = saldo * (1.0 - norx_w) * lgd;
				System.out.println("Nor_w: "+nor_w);
				ase.setEcl_weighted_scan(nor_w);
				total_ecl_weight += nor_w;
				aset_list.add(ase);
				
				//FILTER ASET YG ADA.
				if (asx == 0) {
					fil.add(Integer.parseInt(req_as.getId_aset()));
				} else {
					boolean check = true;
					for (Integer integer : fil) {
						if (integer == Integer.parseInt(req_as.getId_aset())) {
							check = false;
							break;
						}
					}
					if (check) {
						fil.add(Integer.parseInt(req_as.getId_aset()));
					}
				}
				asx++;
			}
			
			//LIST SUMMARY
			for (int j = 0; j < fil.size(); j++) {		
				GetVasicekSummary sum = new GetVasicekSummary();
				sum.setId_aset(fil.get(j));
				sum.setAset(this.serviceAsset.findById(fil.get(j)));
				
				double sum_saldo = 0.0;
				double sum_normal = 0.0;
				double sum_weighted = 0.0;
				
				for (GetVasicekAset asl : aset_list) {
					if (fil.get(j) == asl.getId_aset()) {
						sum_saldo += asl.getSaldo();
						sum_normal += asl.getEcl_normal_scan();
						sum_weighted += asl.getEcl_weighted_scan();
					}
				}
				sum.setSaldo(sum_saldo);
				sum.setEcl_normal_scan(sum_normal);
				sum.setEcl_weighted_scan(sum_weighted);
				sumary_list.add(sum);
			}
			
			
			//TAMBAHKAN DATA TERAKHIR SEBAGAI SUM NYA
			GetVasicekAset ast = new GetVasicekAset();
			ast.setSaldo(total_saldo);
			ast.setEcl_normal_scan(total_ecl_normal);
			ast.setEcl_weighted_scan(total_ecl_weight);
			aset_list.add(ast);
			
			GetVasicekSummary sum = new GetVasicekSummary();
			sum.setSaldo(total_saldo);
			sum.setEcl_normal_scan(total_ecl_normal);
			sum.setEcl_weighted_scan(total_ecl_weight);
			sumary_list.add(sum);
			
			mcr.setPeriode_ecl(body.getPeriode_ecl());
			mcr.setList_aset(aset_list);
			mcr.setList_summary(sumary_list);
			logger.info("success Simulate");
			return new ResponseEntity(new ResponObject(1, "List ",mcr),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//SIMULATE MacroEconomic Agustmen
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/vasicek/simulate-aset")
	public ResponseEntity<?> vasicekSimulateAset(@RequestHeader("Authorization") String Authorization,
			@RequestBody RequestVasicek body) throws Exception {
		
		String method = "POST";
		String url = "/vasicek/simulate-aset";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.getId_vasicek_draft() ==null || body.getId_vasicek_draft().toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "getId_vasicek_draft null!"),HttpStatus.BAD_REQUEST);
		
		if (body.getInterval()==null || body.getInterval().toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "interval null!"),HttpStatus.BAD_REQUEST);
		if (body.getStart_date()==null ) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.getEnd_date()==null ) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);		
		
		if (body.getPersen_normal()==null ) return new ResponseEntity(new ResponObject(2, "persen_normal null"),HttpStatus.BAD_REQUEST);
		if (body.getPersen_upturn()==null ) return new ResponseEntity(new ResponObject(2, "persen_upturn null"),HttpStatus.BAD_REQUEST);		
		if (body.getPersen_downturn()==null ) return new ResponseEntity(new ResponObject(2, "persen_downturn null"),HttpStatus.BAD_REQUEST);
		
		if (body.getLgd()==null ) return new ResponseEntity(new ResponObject(2, "LGD null"),HttpStatus.BAD_REQUEST);		
		if (body.getPeriode_ecl()==null ) return new ResponseEntity(new ResponObject(2, "Periode ECL null"),HttpStatus.BAD_REQUEST);
		
		try {
			logger.info( "hit api get get simulate vasicek aset");

			int interval = Integer.parseInt(body.getInterval());
			if (interval != 3 && interval != 6 && interval != 12) {
				logger.info("interval hanya di quartal , semester , tahun."); //3 6 9 12.
				return new ResponseEntity(new ResponList(2, "interval hanya tersedia 3 , 6 ,12"),HttpStatus.BAD_REQUEST);
			}

			logger.info( "id_draft_vasicek : "+body.getId_vasicek_draft());
			logger.info( "interval : "+interval);
			logger.info( "start_date : "+body.getStart_date());
			logger.info( "end_date : "+body.getEnd_date());
			logger.info( "save : "+body.getSave());
			
			List<VasicekGDP> list = new ArrayList<VasicekGDP>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date date1 = new Date();
			Date date2 = new Date();
			
			double per_normal = 1;
			double per_upturn = 0.0;
			double per_downturn = 0.0;
			try {
				if (!body.getStart_date().isEmpty() && !body.getEnd_date().isEmpty()) {
					
					date1 = df.parse(body.getStart_date());
					date2 = df.parse(body.getEnd_date());
					
					logger.info( "start : "+date1);
					logger.info( "end : "+date2);	
				}
				
				if (!body.getPersen_normal().isEmpty()) {
					
					per_normal = Double.parseDouble(body.getPersen_normal()) / 100;
					per_upturn = Double.parseDouble(body.getPersen_upturn()) / 100;
					per_downturn = Double.parseDouble(body.getPersen_downturn()) /100;
					
					if ((per_normal + per_upturn + per_downturn) != 1) {
						return new ResponseEntity(new ResponList(2, "hasil persentase ketika di jumlahkan harus 100"),HttpStatus.BAD_REQUEST);
					}
				}
				
			} catch (Exception e) {
				// TODO: handle exception
//					e.printStackTrace();
			}
			
			if (body.getStart_date() == "" || body.getEnd_date() == "") {
				list = serviceGDP.findAll();
			} else {
				list = serviceGDP.findStartEnd(date1 , date2);
			}
			
			GetVasicek mcr = new GetVasicek();
			mcr.setTitle_bucket("MacroEconomic Adjustment");
			mcr.setPeriode_mulai(body.getStart_date());
			mcr.setPeriode_selesai(body.getEnd_date());
			mcr.setInterval_bucket(interval);
			mcr.setPersen_normal(per_normal);
			mcr.setPersen_uptum(per_upturn);
			mcr.setPersen_downtum(per_downturn);
			
			List<String> header = new ArrayList<String>();
			
			//sengaja karena data tidak ada per bulan, interval dibagi 3.
			interval = interval / 3;
			
			double count_dt = list.size() / (double) interval;
			
			System.out.println("COUNT DT before: "+count_dt);
			
			if (count_dt > (list.size() / interval)) {
				count_dt += 1;
			}
			
			double [] nilai = new double[(int) count_dt];
			int [] flg_projection = new int[(int) count_dt];
			double t_nilai = 0.0;
			
			System.out.println("SIZE SAMPLE : "+list.size());
			System.out.println("INTERVAL : "+interval);
			System.out.println("STEP : "+(list.size() / interval)+1);
			
			List<GetVasicekMacroList> list_macro = new ArrayList<GetVasicekMacroList>();
			
			int index_nilai = 0;
			int i = 0;
			int count_projection = 0;
			for (VasicekGDP vasicekGDP : list) {
				if ((i % interval) == 0) {
					System.out.println("BULAN TAHUN : "+vasicekGDP.getBulan()+" "+vasicekGDP.getTahun());
					header.add(	serviceInput.getPeriode(vasicekGDP.getBulan(), vasicekGDP.getTahun()));
					nilai[index_nilai] = vasicekGDP.getValue_gdp();
					flg_projection[index_nilai] = vasicekGDP.getFlg_projection();
					if (vasicekGDP.getFlg_projection() == 1) {
						count_projection += 1;
					}
					t_nilai += vasicekGDP.getValue_gdp();
					index_nilai++;
				} 
				i++;
			}
			
			//hitung model hanya yg bukan projeksion.
			double [] list_noprojection = new double[(nilai.length-count_projection)];
			double t_noprojection = 0.0;
			int ix = 0;
			for (int j = 0; j < nilai.length; j++) {
				if (flg_projection[j] == 0) {
					list_noprojection[ix] = nilai[j];
					t_noprojection += nilai[j];
					ix++;
				}
			}
			
			
			double mean = (t_noprojection / list_noprojection.length);
			double stdev = serviceMevFL.calculateSD(list_noprojection);
			double confidence_level = 0.99;
			double t_distribution = 2.57582939565744; //TINV(1-99%, 9999999999) statis / rumus.
			
			double [] list_upturn = new double [nilai.length];
			double [] list_downturn = new double [nilai.length];
			double [] list_weighted = new double [nilai.length];
			double [] list_smev_normal = new double [nilai.length];
			double [] list_smev_weight = new double [nilai.length];
			
			for (int j = 0; j < nilai.length; j++) {
				
				double standard = (nilai[j] - mean) / stdev;
				
				if (standard > t_distribution) {
					standard = t_distribution;
				} else if (standard < (-1 * t_distribution)) {
					standard = t_distribution;
				}
				
				list_smev_normal[j] = standard;
				
				if (flg_projection[j] == 1) {
					
					list_upturn[j] = nilai[j] + (1 * stdev);
					list_downturn[j] = nilai[j] - (1.5 * stdev);
					double weigted = (per_normal * nilai[j]) + (per_upturn * (nilai[j] + (1 * stdev))) + (per_downturn * (nilai[j] - (1.5 * stdev)));
					list_weighted[j] = weigted;
					double standard_weight = (weigted - mean) / stdev;
					
					if (standard_weight > t_distribution) {
						standard_weight = t_distribution;
					} else if (standard_weight < (-1 * t_distribution)) {
						standard_weight = t_distribution;
					}
					list_smev_weight[j] = standard_weight;			
				} 
			}
			
			GetVasicekMacroList macro_normal = new GetVasicekMacroList();
			macro_normal.setTitle("GDP_IND (Normal)");
			macro_normal.setNilai(nilai);
			list_macro.add(macro_normal);
			
			GetVasicekMacroList macro_upturn = new GetVasicekMacroList();
			macro_upturn.setTitle("GDP_IND (Upturn)");
			macro_upturn.setNilai(list_upturn);
			list_macro.add(macro_upturn);
			
			GetVasicekMacroList macro_downturn = new GetVasicekMacroList();
			macro_downturn.setTitle("GDP_IND (Downturn)");
			macro_downturn.setNilai(list_downturn);
			list_macro.add(macro_downturn);
			
			GetVasicekMacroList macro_weighted = new GetVasicekMacroList();
			macro_weighted.setTitle("GDP_IND (Weighted)");
			macro_weighted.setNilai(list_weighted);
			list_macro.add(macro_weighted);
			
			GetVasicekMacroList macro_smev_normal = new GetVasicekMacroList();
			macro_smev_normal.setTitle("SMEV (Normal)");
			macro_smev_normal.setNilai(list_smev_normal);
			list_macro.add(macro_smev_normal);
			
			GetVasicekMacroList macro_smev_weigted = new GetVasicekMacroList();
			macro_smev_weigted.setTitle("SMEV (Weighted)");
			macro_smev_weigted.setNilai(list_smev_weight);
			list_macro.add(macro_smev_weigted);
			
			mcr.setHeader_macroeconomic(header);
			mcr.setList_flg_projection(flg_projection);
			mcr.setList_macroeconomic(list_macro);
			mcr.setHistorical_mean(mean);
			mcr.setHistorical_stdev(stdev);
			mcr.setConfidence_level(confidence_level);
			mcr.setT_distribution(t_distribution);
		
			//DISINI SET PD VASICEK
			String periode_projection = header.get(header.size()-1);
			List<VasicekRating> activeRatings = this.serviceRating.findByStatus(1);
			List<GetVasicekPD> vasicek_pd = new ArrayList<GetVasicekPD>();
			
			//disini masih kacau langsung get akhir index, harusnya liat projection nya.
			double macro_adjustment_normal = list_smev_normal[list_smev_normal.length-1];
			double macro_adjustment_weighted = list_smev_weight[list_smev_weight.length-1]; 
			
			double check_min_normal = 1.0; //hanya sebagai tanda awal.
			double check_min_weighted = 1.0; //hanya sebagai tanda awal.
			
			for (VasicekRating activeRating : activeRatings) {
				
				GetVasicekPD rat = new GetVasicekPD();
				rat.setId_rating(activeRating.getId_rating());
				rat.setRating(activeRating);
				rat.setValue_rating(activeRating.getValue());
				rat.setPeriode_projection(periode_projection);
				
				double base_formula = (0.12 * (1 - Math.exp(-50 * activeRating.getValue())) / (1 - Math.exp(-50))) + 
						(0.24 * (1 - (1 - Math.exp(-50 * activeRating.getValue()))) / (1 - Math.exp(-50)));
				
				rat.setValue_base_formula(base_formula);
				
				//VASICEK NORMAL.
				double vasicekNormal = 0.0;
				if (activeRating.getValue() <= 0 || activeRating.getValue() >= 1.0) {
					vasicekNormal = activeRating.getValue();
				} else {
					
					//NORMSDIST( (NORMSINV(valueRating) - SQRT(BASE FORMULA) * MacroAdjustment) / SQRT(1 - BASE FORMULA));
					double v_normsinv = normsInv(activeRating.getValue()) - (Math.sqrt(base_formula) * macro_adjustment_normal);
					
					vasicekNormal = normsDist(v_normsinv / Math.sqrt(1 - base_formula));
				}
				rat.setVasicek_normal(vasicekNormal);
				
				if (vasicekNormal <= 0 || vasicekNormal >= 1) {
					//no condition.
				} else {
					if (vasicekNormal < check_min_normal) {
						check_min_normal = vasicekNormal;
					}
				}
				
				//VASICEK Weighted.
				double vasicekWeigted = 0.0;
				if (activeRating.getValue() <= 0 || activeRating.getValue() >= 1.0) {
					vasicekWeigted = activeRating.getValue();
				} else {
					
					//NORMSDIST( (NORMSINV(valueRating) - SQRT(BASE FORMULA) * MacroAdjustment) / SQRT(1 - BASE FORMULA));
					double v_normsinv = normsInv(activeRating.getValue()) - (Math.sqrt(base_formula) * macro_adjustment_weighted);
					
					vasicekWeigted = normsDist(v_normsinv / Math.sqrt(1 - base_formula));
				}
				rat.setVasicek_weighted(vasicekWeigted);
				
				if (vasicekWeigted <= 0 || vasicekWeigted >= 1) {
					//no condition.
				} else {
					if (vasicekWeigted < check_min_weighted) {
						check_min_weighted = vasicekWeigted;
					}
				}
				
				vasicek_pd.add(rat);
			}
			
			
			//KARENA DIATAS BARU KETAUAN NILAI MINIMAL NYA , FOREACH KEMBALI UNTUK ISI ADJUSTED.
			for (GetVasicekPD getVasicekPD : vasicek_pd) {
				if (getVasicekPD.getVasicek_normal() == 0) {
//					getVasicekPD.setVasicek_adjusted_normal(check_min_normal); update biarkan jadi 0 ngga usah di adjusted ke minimal
					getVasicekPD.setVasicek_adjusted_normal(getVasicekPD.getVasicek_normal());
				} else if (getVasicekPD.getVasicek_normal() == 1.0) {
					getVasicekPD.setVasicek_adjusted_normal(1.0);
				} else {
					getVasicekPD.setVasicek_adjusted_normal(getVasicekPD.getVasicek_normal());
				}
				
				if (getVasicekPD.getVasicek_weighted() == 0) {
//					getVasicekPD.setVasicek_adjusted_weighted(check_min_weighted); update biarkan jadi 0 ngga usah di adjusted ke minimal
					getVasicekPD.setVasicek_adjusted_weighted(getVasicekPD.getVasicek_weighted());
				} else if (getVasicekPD.getVasicek_weighted() == 1.0) {
					getVasicekPD.setVasicek_adjusted_weighted(1.0);
				} else {
					getVasicekPD.setVasicek_adjusted_weighted(getVasicekPD.getVasicek_weighted());
				}
			}
			
			mcr.setList_pd_vasicek(vasicek_pd);
			
			//LIST ASET ECL
			DateFormat df_ecl = new SimpleDateFormat("yyyy-MM-dd");
			Date date_ecl = df_ecl.parse(body.getPeriode_ecl());
			
//			List<RequestAsetECL> req_aset = body.getList_aset_ecl();
			List<VasicekAsetList> req_aset = this.serviceListAsset.findByIdDraft(Integer.parseInt(body.getId_vasicek_draft()));
			if (req_aset.size() <= 0) {
				return new ResponseEntity(new ResponObject(2, "List Aset Empty"),HttpStatus.BAD_REQUEST);
			}
			
			double lgd = Double.parseDouble(body.getLgd()) / 100 ;
			
			List<GetVasicekAset> aset_list = new ArrayList<GetVasicekAset>();
			List<GetVasicekSummary> sumary_list = new ArrayList<GetVasicekSummary>();
			
			double total_saldo = 0.0;
			double total_ecl_normal = 0.0;
			double total_ecl_weight = 0.0;
			List<Integer> fil = new ArrayList<Integer>();
			
			int asx = 0;
			for (VasicekAsetList req_as : req_aset) {
				
				GetVasicekAset ase = new GetVasicekAset();
				ase.setId_aset(req_as.getId_jenis());
				ase.setAset(this.serviceAsset.findById(req_as.getId_jenis()));
				ase.setNo_rekening(req_as.getNo_rekening());
				ase.setMata_uang(req_as.getMata_uang());
				ase.setId_bank(req_as.getId_bank());
				
				VasicekBank bank = this.serviceBank.findById(req_as.getId_bank());
				VasicekBankJoin bank_join = new VasicekBankJoin();
				bank_join.setId_vas_bank(bank.getId_vas_bank());
				bank_join.setNama_bank(bank.getNama_bank());
				bank_join.setAkronim(bank.getAkronim());
				bank_join.setId_rating(bank.getId_rating());
				bank_join.setTahun(bank.getTahun());
				bank_join.setNilai(bank.getNilai());
				bank_join.setRating(this.serviceRating.findById(bank.getId_rating()));
				ase.setBank(bank_join);
				
				double bunga = req_as.getSuku_bunga();
				ase.setSuku_bunga(bunga);
				ase.setTgl_jatuh_tempo(this.configGetDate.format((req_as.getTgl_jatuh_tempo())));
				
				double saldo = req_as.getSaldo();
				total_saldo += saldo;
				ase.setSaldo(saldo);
				ase.setLgd(lgd);
				
				double scan_normal = 0.0;
				double scan_weighted = 0.0;
				
				for (GetVasicekPD vasi : vasicek_pd) {
					if (vasi.getId_rating() == bank.getId_rating()) {
						scan_normal = vasi.getVasicek_adjusted_normal();
						scan_weighted = vasi.getVasicek_adjusted_weighted();
						break;
					}
				}
				
				ase.setPd_normal_scan(scan_normal);
				ase.setPd_weighted_scan(scan_weighted);
				
				DateFormat dfx = new SimpleDateFormat("dd/MM/yyyy");
				int count_day = calculateNumberOfDaysBetween(date_ecl, req_as.getTgl_jatuh_tempo());
				count_day -= 1;
				
				System.out.println("COUNT DAY: "+count_day);
				//(K7*(1-((1-L7)^((J7-$C$4)/360)))*N7)
				double norx = Math.pow((1.0 - scan_normal), (count_day / 360.0));
				System.out.println("Norx: "+norx);
				double nor = saldo * (1.0 - norx) * lgd;
				System.out.println("Nor: "+nor);
				ase.setEcl_normal_scan(nor);
				total_ecl_normal += nor;
				
				double norx_w = Math.pow((1.0 - scan_weighted), (count_day / 360.0));
				System.out.println("Norx_w: "+norx_w);
				double nor_w = saldo * (1.0 - norx_w) * lgd;
				System.out.println("Nor_w: "+nor_w);
				ase.setEcl_weighted_scan(nor_w);
				total_ecl_weight += nor_w;
				aset_list.add(ase);
				
				//FILTER ASET YG ADA.
				if (asx == 0) {
					fil.add(req_as.getId_jenis());
				} else {
					boolean check = true;
					for (Integer integer : fil) {
						if (integer == req_as.getId_jenis()) {
							check = false;
							break;
						}
					}
					if (check) {
						fil.add(req_as.getId_jenis());
					}
				}
				asx++;
			}
			
			//LIST SUMMARY
			for (int j = 0; j < fil.size(); j++) {		
				GetVasicekSummary sum = new GetVasicekSummary();
				sum.setId_aset(fil.get(j));
				sum.setAset(this.serviceAsset.findById(fil.get(j)));
				
				double sum_saldo = 0.0;
				double sum_normal = 0.0;
				double sum_weighted = 0.0;
				
				for (GetVasicekAset asl : aset_list) {
					if (fil.get(j) == asl.getId_aset()) {
						sum_saldo += asl.getSaldo();
						sum_normal += asl.getEcl_normal_scan();
						sum_weighted += asl.getEcl_weighted_scan();
					}
				}
				sum.setSaldo(sum_saldo);
				sum.setEcl_normal_scan(sum_normal);
				sum.setEcl_weighted_scan(sum_weighted);
				sumary_list.add(sum);
			}
			
			
			//TAMBAHKAN DATA TERAKHIR SEBAGAI SUM NYA
			GetVasicekAset ast = new GetVasicekAset();
			ast.setSaldo(total_saldo);
			ast.setEcl_normal_scan(total_ecl_normal);
			ast.setEcl_weighted_scan(total_ecl_weight);
			aset_list.add(ast);
			
			GetVasicekSummary sum = new GetVasicekSummary();
			sum.setSaldo(total_saldo);
			sum.setEcl_normal_scan(total_ecl_normal);
			sum.setEcl_weighted_scan(total_ecl_weight);
			sumary_list.add(sum);
			
			mcr.setPeriode_ecl(body.getPeriode_ecl());
			mcr.setList_aset(aset_list);
			mcr.setList_summary(sumary_list);
			
			if (body.getSave().contentEquals("1")) {
				this.saveVasicek(mcr,Integer.parseInt(body.getId_vasicek_draft()), 
						Integer.parseInt(body.getCreated_by()),
						total_ecl_normal, total_ecl_weight);	
			}
			
			logger.info("success Simulate");
			return new ResponseEntity(new ResponObject(1, "List ",mcr),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private void saveVasicek(GetVasicek mcr, int id_draft, int created_by, double saldo_normal, double saldo_weighted) {
		
		VasicekResultDraft res_draft = new VasicekResultDraft();
		res_draft.setId_draft_vasicek(id_draft);
		res_draft.setInterval_bucket(mcr.getInterval_bucket());
		res_draft.setPeriode_mulai(mcr.getPeriode_mulai());
		res_draft.setPeriode_selesai(mcr.getPeriode_selesai());
		res_draft.setHeader_macroeconomic(mcr.getHeader_macroeconomic().toString());
		res_draft.setList_flg_projection(Arrays.toString(mcr.getList_flg_projection()));
		res_draft.setPersen_normal(mcr.getPersen_normal());
		res_draft.setPersen_uptum(mcr.getPersen_uptum());
		res_draft.setPersen_downtum(mcr.getPersen_downtum());
		res_draft.setHistorical_mean(mcr.getHistorical_mean());
		res_draft.setHistorical_stdev(mcr.getHistorical_stdev());
		res_draft.setConfidence_level(mcr.getConfidence_level());
		res_draft.setT_distribution(mcr.getT_distribution());
		res_draft.setPeriode_ecl(mcr.getPeriode_ecl());
		res_draft.setSaldo_normal_scan(saldo_normal);
		res_draft.setSaldo_weighted_scan(saldo_weighted);
		res_draft.setCreated_by(created_by);
		res_draft.setCreated_date(new Date());
		res_draft.setResult_flag_model(0);
		this.serviceVasicek.saveDraft(res_draft);
		
		int i = 0;
		for (GetVasicekAset getvas : mcr.getList_aset()) {
			System.out.println("HANIF CHECK ASET SAVE i= "+i+" size -1= "+(mcr.getList_aset().size()-1));
			if (i == mcr.getList_aset().size()-1) {
				break;
			}
		
			VasicekResultAset res_aset = new VasicekResultAset();
			res_aset.setId_vasicek_result_draft(res_draft.getId_vasicek_result_draft());
			res_aset.setId_aset(getvas.getId_aset());
			res_aset.setId_bank(getvas.getId_bank());
			res_aset.setNama_aset(getvas.getAset().getNama_aset());
			res_aset.setNama_bank(getvas.getBank().getNama_bank());
			res_aset.setAkronim(getvas.getBank().getAkronim());
			res_aset.setTahun_bank(getvas.getBank().getTahun());
			res_aset.setKode_rating(getvas.getBank().getRating().getKode_rating());
			res_aset.setValue_rating(getvas.getBank().getNilai());
			res_aset.setNo_rekening(getvas.getNo_rekening());
			res_aset.setMata_uang(getvas.getMata_uang());
			res_aset.setSuku_bunga(getvas.getSuku_bunga());
			res_aset.setTgl_jatuh_tempo(getvas.getTgl_jatuh_tempo());
			res_aset.setSaldo(getvas.getSaldo());
			res_aset.setLgd(getvas.getLgd());
			res_aset.setPd_normal_scan(getvas.getPd_normal_scan());
			res_aset.setEcl_normal_scan(getvas.getEcl_normal_scan());
			res_aset.setPd_weighted_scan(getvas.getPd_weighted_scan());
			res_aset.setEcl_weighted_scan(getvas.getEcl_weighted_scan());
			this.serviceVasicek.saveAset(res_aset);
			i++;
		}
		
		for (GetVasicekMacroList getmac : mcr.getList_macroeconomic()) {
			
			VasicekResultMacro res_mac = new VasicekResultMacro();
			res_mac.setId_vasicek_result_draft(res_draft.getId_vasicek_result_draft());
			res_mac.setTitle(getmac.getTitle());
			res_mac.setNilai(Arrays.toString(getmac.getNilai()));
			this.serviceVasicek.saveMacro(res_mac);
			
		}
		
		for (GetVasicekPD getpd : mcr.getList_pd_vasicek()) {
			VasicekResultPD res_pd = new VasicekResultPD();
			res_pd.setId_vasicek_result_draft(res_draft.getId_vasicek_result_draft());
			res_pd.setId_rating(getpd.getId_rating());
			res_pd.setKode_rating(getpd.getRating().getKode_rating());
			res_pd.setValue_rating(getpd.getValue_rating());
			res_pd.setPeriode_projection(getpd.getPeriode_projection());
			res_pd.setValue_base_formula(getpd.getValue_base_formula());
			res_pd.setVasicek_normal(getpd.getVasicek_normal());
			res_pd.setVasicek_adjusted_normal(getpd.getVasicek_adjusted_normal());
			res_pd.setVasicek_weighted(getpd.getVasicek_weighted());
			res_pd.setVasicek_adjusted_weighted(getpd.getVasicek_adjusted_weighted());
			this.serviceVasicek.savePd(res_pd);
			
		}
	}
	
	public int calculateNumberOfDaysBetween(Date startDate, Date endDate) {
	    if (startDate.after(endDate)) {
	        return 1;
	    }

	    long startDateTime = startDate.getTime();
	    long endDateTime = endDate.getTime();
	    long milPerDay = 1000*60*60*24; 

	    int numOfDays = (int) ((endDateTime - startDateTime) / milPerDay); // calculate vacation duration in days

	    return ( numOfDays + 1); // add one day to include start date in interval
	}
	
	static double normsDist(double x) {
        double b1 = 0.319381530;
        double b2 = -0.356563782;
        double b3 = 1.781477937;
        double b4 = -1.821255978;
        double b5 = 1.330274429;
        double p = 0.2316419;
        double c = 0.39894228;

        if (x >= 0.0) {
            double t = 1.0 / (1.0 + p * x);
            return (1.0 - c * Math.exp(-x * x / 2.0) * t *
                    (t * (t * (t * (t * b5 + b4) + b3) + b2) + b1));
        } else {
            double t = 1.0 / (1.0 - p * x);
            return (c * Math.exp(-x * x / 2.0) * t * (t *
                    (t * (t * (t * b5 + b4) + b3) + b2) + b1));
        }
    }
	
	static double normsInv(double p) {
	    
		double mu = 0.0;
		double sigma = 1.0;
		
		if (p < 0 || p > 1)
	    {
	        System.out.println("The probality p must be bigger than 0 and smaller than 1");
	    }
	    if (sigma < 0)
	    {
	        System.out.println("The standard deviation sigma must be positive");
	    }

	    if (p == 0)
	    {
	        return -1;
	    }
	    if (p == 1)
	    {
	        return 1;
	    }
	    if (sigma == 0)
	    {
	        return mu;
	    }

	    double q, r, val;

	    q = p - 0.5;

	    /*-- use AS 241 --- */
	    /* double ppnd16_(double *p, long *ifault)*/
	    /*      ALGORITHM AS241  APPL. STATIST. (1988) VOL. 37, NO. 3
	            Produces the normal deviate Z corresponding to a given lower
	            tail area of P; Z is accurate to about 1 part in 10**16.
	    */
	    if (Math.abs(q) <= .425)
	    {/* 0.075 <= p <= 0.925 */
	        r = .180625 - q * q;
	        val =
	               q * (((((((r * 2509.0809287301226727 +
	                          33430.575583588128105) * r + 67265.770927008700853) * r +
	                        45921.953931549871457) * r + 13731.693765509461125) * r +
	                      1971.5909503065514427) * r + 133.14166789178437745) * r +
	                    3.387132872796366608)
	               / (((((((r * 5226.495278852854561 +
	                        28729.085735721942674) * r + 39307.89580009271061) * r +
	                      21213.794301586595867) * r + 5394.1960214247511077) * r +
	                    687.1870074920579083) * r + 42.313330701600911252) * r + 1);
	    }
	    else
	    { /* closer than 0.075 from {0,1} boundary */

	        /* r = min(p, 1-p) < 0.075 */
	        if (q > 0)
	            r = 1 - p;
	        else
	            r = p;

	        r = Math.sqrt(-Math.log(r));
	        /* r = sqrt(-log(r))  <==>  min(p, 1-p) = exp( - r^2 ) */

	        if (r <= 5)
	        { /* <==> min(p,1-p) >= exp(-25) ~= 1.3888e-11 */
	            r += -1.6;
	            val = (((((((r * 7.7454501427834140764e-4 +
	                       .0227238449892691845833) * r + .24178072517745061177) *
	                     r + 1.27045825245236838258) * r +
	                    3.64784832476320460504) * r + 5.7694972214606914055) *
	                  r + 4.6303378461565452959) * r +
	                 1.42343711074968357734)
	                / (((((((r *
	                         1.05075007164441684324e-9 + 5.475938084995344946e-4) *
	                        r + .0151986665636164571966) * r +
	                       .14810397642748007459) * r + .68976733498510000455) *
	                     r + 1.6763848301838038494) * r +
	                    2.05319162663775882187) * r + 1);
	        }
	        else
	        { /* very close to  0 or 1 */
	            r += -5;
	            val = (((((((r * 2.01033439929228813265e-7 +
	                       2.71155556874348757815e-5) * r +
	                      .0012426609473880784386) * r + .026532189526576123093) *
	                    r + .29656057182850489123) * r +
	                   1.7848265399172913358) * r + 5.4637849111641143699) *
	                 r + 6.6579046435011037772)
	                / (((((((r *
	                         2.04426310338993978564e-15 + 1.4215117583164458887e-7) *
	                        r + 1.8463183175100546818e-5) * r +
	                       7.868691311456132591e-4) * r + .0148753612908506148525)
	                     * r + .13692988092273580531) * r +
	                    .59983220655588793769) * r + 1);
	        }

	        if (q < 0.0)
	        {
	            val = -val;
	        }
	    }

	    return mu + sigma * val;
	}
}
