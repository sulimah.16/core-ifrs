package com.hanifalbaaits.java.controller;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.config.JwtTokenUtil;
import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelRespon.UserJoin;
import com.hanifalbaaits.java.modelRespon.UserLogin;
import com.hanifalbaaits.java.service.UserRoleService;
import com.hanifalbaaits.java.service.UserService;
import com.hanifalbaaits.java.utils.Encryption;

import io.jsonwebtoken.ExpiredJwtException;

@RestController
public class AuthenticateUser {
	
	private Logger logger = LoggerFactory.getLogger(AuthenticateUser.class);
	
	@Autowired
	UserService serviceUser;
	
	@Autowired
	UserRoleService serviceRole;
	
	@Autowired
	Encryption encrypt;
	
	@Autowired
	JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/")
	public ResponseEntity<?> mainRoute(HttpServletRequest request) {
		logger.info("This is main path a service IFRS");
		return new ResponseEntity(new ResponObject(1, "This is main path a service IFRS"),HttpStatus.OK);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="core-ifrs/auth")
	public ResponseEntity<?> login(@RequestBody Map<String,String> body, 
			HttpServletRequest request) {
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("username")==null || body.get("username").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "username null!"),HttpStatus.BAD_REQUEST);
		if (body.get("password")==null || body.get("password").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "password null!"),HttpStatus.BAD_REQUEST);
		if (body.get("device")==null || body.get("device").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "device null"),HttpStatus.BAD_REQUEST);
		
		String method = "POST";
		String url = "/auth";
		
		try {
			logger.info("hit api authenticate login");
			
			User check = serviceUser.findByUsername(body.get("username"));
			if(check==null) {
				return new ResponseEntity(new ResponObject(2, "Username not registered!"),HttpStatus.BAD_REQUEST);
			}
			if(check.getStatus() == 0) {
				return new ResponseEntity(new ResponObject(2, "Your access denied. Contact administrator!"),HttpStatus.BAD_REQUEST);
			}
			if(!check.getPassword().contentEquals(encrypt.getSHA256EncryptedValue(body.get("password")))) {
				return new ResponseEntity(new ResponObject(2, "Password wrong!"),HttpStatus.BAD_REQUEST);
			}
			String token = jwtTokenUtil.generateToken(check);
			logger.info("generate token: "+token);
			
//			Calendar cal = Calendar.getInstance();
//			cal.add(Calendar.DAY_OF_MONTH, 7); //set expired 7 days;
						
			UserJoin user_join = new UserJoin();
			user_join.setId_user(check.getId_user());
			user_join.setUsername(check.getUsername());
			user_join.setPassword(check.getPassword());
			user_join.setId_role(check.getId_role());
			user_join.setRole(this.serviceRole.findById(check.getId_role()));
			user_join.setStatus(check.getStatus());
			
			if (check.getStatus() == 1) {
				user_join.setNm_status("ACTIVE");
			} else {
				user_join.setNm_status("NON ACTIVE");
			}
			
			user_join.setEmail(check.getEmail());
			user_join.setNama(check.getNama());
			user_join.setNo_telp(check.getNo_telp());
			
			user_join.setAlamat(check.getAlamat());
			user_join.setJ_k(check.getJ_k());
			if (check.getJ_k() == 0) {
				user_join.setNm_jk("Wanita");
			} else {
				user_join.setNm_jk("Pria");
			}
			
			user_join.setDivisi(check.getDivisi());
			user_join.setJabatan(check.getJabatan());
			
			user_join.setPath_profile(check.getPath_profile());
			user_join.setCreated_by(check.getCreated_by());
			user_join.setCreated_date(configGetDate.format(check.getCreated_date()));
			
			if(check.getUpdated_date()!=null) {
				user_join.setUpdated_by(check.getUpdated_by());
				user_join.setUpdated_date(configGetDate.format(check.getUpdated_date()));
			}
		
			logger.info("success login - save data");
			int respact = this.serviceAktivitas.createAktivitas("LOGIN", check, body.get("device"), method, url);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			return new ResponseEntity(new ResponObject(1, "Login Success!", new UserLogin(user_join, token)),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public ResponMiddleware AuthenticateMiddleware(String authorization) {
	
		try {
			logger.info("AuthenticateMiddleware");
			logger.info("token: "+authorization);
			
			if(authorization == null || authorization.contentEquals("")) {
				return new ResponMiddleware(2, "Need header authorizarion!");
			}
			if(authorization.contentEquals("1q2w3e4r5t-imissyou")) {
				User admin = new User();
				admin.setId_user(0);
				return new ResponMiddleware(1, "Success!",admin); //pertama kali untuk admin.
			}
			if(!authorization.startsWith("Bearer")) {
				return new ResponMiddleware(2, "Authorization start with 'Bearer '!");
			}
			String jwtToken = authorization.substring(7);
			String username = "";
			try {
				username = jwtTokenUtil.getUsernameFromToken(jwtToken);
			} catch (IllegalArgumentException e) {
				
				logger.info("Unable to get JWT Token");
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);
				return new ResponMiddleware(2, "Unable to get JWT Token!");
			} catch (ExpiredJwtException e) {
				logger.info("JWT Token has expired");
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);
				return new ResponMiddleware(2, "JWT Token has expired!");
			}
			
			User user = serviceUser.findByUsername(username);
			if(user == null) {
				return new ResponMiddleware(2, "Unable to get JWT Token!");
			}
			if(user.getStatus() != 1) {
				return new ResponMiddleware(2,"Your access denied. Contact administrator!");
			}
			
			boolean check = jwtTokenUtil.validateToken(jwtToken, user);
			if(!check) { return new ResponMiddleware(2, "No Authorized!");}
			
			return new ResponMiddleware(1, "Success!",user);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponMiddleware(3,e.getMessage());
		}
	}
}
