package com.hanifalbaaits.java.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.VasicekAset;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.service.VasicekAsetService;

@RestController
@RequestMapping(value="core-ifrs")
public class VasicekAsetController {
	
	private Logger logger = LoggerFactory.getLogger(VasicekAsetController.class);

	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	VasicekAsetService serviceAset;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/vas-aset")
	public ResponseEntity<?> getAset(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/vas-aset";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get vasicek aset");
			
			List<VasicekAset> data = serviceAset.findAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data Aset");
				return new ResponseEntity(new ResponList(1, "empty Aset",data),HttpStatus.OK);
			}
			
			logger.info(method+"|"+url+"|"+"data aset");
			return new ResponseEntity(new ResponList(1, "List Aset",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/vas-aset")
	public ResponseEntity<?> AddAset(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) {
		
		
		String method = "POST";
		String url = "/vas-aset";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api add aset");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "nama :"+body.get("nama"));
		
			VasicekAset nmember = new VasicekAset();
			nmember.setNama_aset(body.get("nama"));
			this.serviceAset.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success save aset");
			int respact = this.serviceAktivitas.createAktivitas("ASET",resp.getData(),method,url,nmember.getId_vas_aset());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes add aset",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/vas-aset/{id}")
	public ResponseEntity<?> updateAset(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) {
		
		String method = "PUT";
		String url = "/vas-aset/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api put user");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "nama :"+body.get("nama"));
		
			VasicekAset nmember = this.serviceAset.findById(id);
			if (nmember == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada data aset"),HttpStatus.BAD_REQUEST);
			}
			
			nmember.setNama_aset(body.get("nama"));
			this.serviceAset.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success update aset");
			int respact = this.serviceAktivitas.createAktivitas("ASET",resp.getData(),method,url,nmember.getId_vas_aset());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update aset",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@DeleteMapping(value="/vas-aset/{id}")
	public ResponseEntity<?> deleteAset(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) {
		
		String method = "PUT";
		String url = "/vas-aset/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api delete aset");
		
			VasicekAset nmember = this.serviceAset.findById(id);
			if (nmember == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada data aset"),HttpStatus.BAD_REQUEST);
			}
			this.serviceAset.delete(id);
			
			logger.info(method+"|"+url+"|"+"success delete aset");
			int respact = this.serviceAktivitas.createAktivitas("ASET",resp.getData(),method,url,nmember.getId_vas_aset());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes delete aset",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
