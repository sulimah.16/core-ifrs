package com.hanifalbaaits.java.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelRespon.UserJoin;
import com.hanifalbaaits.java.service.UserRoleService;
import com.hanifalbaaits.java.service.UserService;
import com.hanifalbaaits.java.utils.Encryption;

@RestController
@RequestMapping(value="core-ifrs")
public class MasterUser {
	
	private Logger logger = LoggerFactory.getLogger(MasterUser.class);

	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	UserService serviceUser;
	
	@Autowired
	UserRoleService serviceRole;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	Encryption encrypt;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/user")
	public ResponseEntity<?> getUser(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/user";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api get master user");
			
			List<User> data = serviceUser.findAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data User");
				return new ResponseEntity(new ResponList(2, "empty User",data),HttpStatus.OK);
			}
			
			List<UserJoin> dataJoin = joinUser(data);
			logger.info(method+"|"+url+"|"+"data user");
			return new ResponseEntity(new ResponList(1, "List User",dataJoin),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/user/{id}")
	public ResponseEntity<?> getUserID(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id
			) {
		
		String method = "GET";
		String url = "/user/"+id;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api get master user");
			
			User check = this.serviceUser.findById(id);
		
			if (check == null) {
				logger.info(method+"|"+url+"|"+"tidak ada data User");
				return new ResponseEntity(new ResponList(1, "empty User"),HttpStatus.OK);
			}
			
			UserJoin user_join = new UserJoin();
			user_join.setId_user(check.getId_user());
			user_join.setUsername(check.getUsername());
			user_join.setPassword(check.getPassword());
			user_join.setId_role(check.getId_role());
			user_join.setRole(this.serviceRole.findById(check.getId_role()));
			user_join.setStatus(check.getStatus());
			
			if (check.getStatus() == 1) {
				user_join.setNm_status("ACTIVE");
			} else {
				user_join.setNm_status("NON ACTIVE");
			}
			
			user_join.setEmail(check.getEmail());
			user_join.setNama(check.getNama());
			user_join.setNo_telp(check.getNo_telp());
			
			user_join.setAlamat(check.getAlamat());
			user_join.setJ_k(check.getJ_k());
			if (check.getJ_k() == 0) {
				user_join.setNm_jk("Wanita");
			} else {
				user_join.setNm_jk("Pria");
			}
			
			user_join.setPath_profile(check.getPath_profile());
			user_join.setCreated_by(check.getCreated_by());
			user_join.setCreated_date(configGetDate.format(check.getCreated_date()));
			
			if(check.getUpdated_date()!=null) {
				user_join.setUpdated_by(check.getUpdated_by());
				user_join.setUpdated_date(configGetDate.format(check.getUpdated_date()));
			}
		
			logger.info(method+"|"+url+"|"+"data member");
			return new ResponseEntity(new ResponObject(1, "List User",user_join),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/change_password")
	public ResponseEntity<?> ChangePassword(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) {
		
		
		String method = "POST";
		String url = "/change_password";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api change password");
			
			if (body.get("password_old").isEmpty() || body.get("password_old") == null) {
				return new ResponseEntity(new ResponObject(2, "password_old null!"),HttpStatus.BAD_REQUEST);
			}
			if (body.get("password_new").isEmpty() || body.get("password_new") == null) {
				return new ResponseEntity(new ResponObject(2, "password_new null!"),HttpStatus.BAD_REQUEST);
			}
			if (body.get("password_confirm").isEmpty() || body.get("password_confirm") == null) {
				return new ResponseEntity(new ResponObject(2, "password_confirm null!"),HttpStatus.BAD_REQUEST);
			}
			
			if (!body.get("password_new").contentEquals(body.get("password_confirm"))) {
				return new ResponseEntity(new ResponObject(2, "not match!"),HttpStatus.BAD_REQUEST);
			}
			
			User data = resp.getData();
			if(!data.getPassword().contentEquals(encrypt.getSHA256EncryptedValue(body.get("password_old")))) {
				return new ResponseEntity(new ResponObject(2, "not match with old password!"),HttpStatus.BAD_REQUEST);
			}
			
			User upd = data;
			upd.setPassword(encrypt.getSHA256EncryptedValue(body.get("password_new")));
			upd.setUpdated_by(data.getId_user());
			upd.setUpdated_date(new Date());
			this.serviceUser.save(upd);
			
			logger.info(method+"|"+url+"|"+"success change password");
			int respact = this.serviceAktivitas.createAktivitas("CHANGEPASSWORD",data,"",method,url);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes change password!"),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/user")
	public ResponseEntity<?> RegisterUser(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) {
		
		
		String method = "POST";
		String url = "/user";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api add user");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("username")==null || body.get("username").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "username null!"),HttpStatus.BAD_REQUEST);
			if (body.get("password")==null || body.get("password").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "password null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_role")==null || body.get("id_role").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_role null"),HttpStatus.BAD_REQUEST);
			if (body.get("status")==null || body.get("status").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "status null!"),HttpStatus.BAD_REQUEST);
			if (body.get("email")==null || body.get("email").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "email null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null"),HttpStatus.BAD_REQUEST);
			if (body.get("no_telp")==null || body.get("no_telp").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "no_telp null!"),HttpStatus.BAD_REQUEST);
			if (body.get("alamat")==null || body.get("alamat").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "alamat null!"),HttpStatus.BAD_REQUEST);
			if (body.get("jk")==null || body.get("jk").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jk null"),HttpStatus.BAD_REQUEST);
//			if (body.get("path_profile")==null || body.get("path_profile").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "path_profile null!"),HttpStatus.BAD_REQUEST);
			if (body.get("divisi")==null || body.get("divisi").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "divisi null!"),HttpStatus.BAD_REQUEST);
			if (body.get("jabatan")==null || body.get("jabatan").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jabatan null"),HttpStatus.BAD_REQUEST);

			logger.info(method+"|"+url+"|"+"username :"+body.get("username"));
			logger.info(method+"|"+url+"|"+"password :"+body.get("password"));
			logger.info(method+"|"+url+"|"+"id_role :"+body.get("id_role"));
			logger.info(method+"|"+url+"|"+"status :"+body.get("status"));
			logger.info(method+"|"+url+"|"+"email :"+body.get("email"));
			logger.info(method+"|"+url+"|"+"nama :"+body.get("nama"));
			logger.info(method+"|"+url+"|"+"no_telp :"+body.get("no_telp"));
			logger.info(method+"|"+url+"|"+"alamat :"+body.get("alamat"));
			logger.info(method+"|"+url+"|"+"jk :"+body.get("jk"));
			logger.info(method+"|"+url+"|"+"path_profile :"+body.get("path_profile"));
			logger.info(method+"|"+url+"|"+"divisi :"+body.get("divisi"));
			logger.info(method+"|"+url+"|"+"jabatan :"+body.get("jabatan"));
			
			User check = serviceUser.findByUsername(body.get("username"));
			if(check != null) { return new ResponseEntity(new ResponObject(2, "username telah terdaftar!"),HttpStatus.BAD_REQUEST); }
				
			Date today = new Date();
			User nmember = new User();
			User created = resp.getData();
			nmember.setCreated_by(created.getId_user());
			nmember.setCreated_date(today);
			
			nmember.setUsername(body.get("username"));
			nmember.setPassword(encrypt.getSHA256EncryptedValue(body.get("password"))); //default nomor 12345
			nmember.setId_role(Integer.parseInt(body.get("id_role")));
			nmember.setStatus(Integer.parseInt(body.get("status")));
			nmember.setEmail(body.get("email"));
			nmember.setNama(body.get("nama"));
			nmember.setNo_telp(body.get("no_telp"));
			nmember.setAlamat(body.get("alamat"));
			nmember.setJ_k(Integer.parseInt(body.get("jk")));
			nmember.setPath_profile(body.get("path_profile"));
			nmember.setJabatan(body.get("jabatan"));
			nmember.setDivisi(body.get("divisi"));
			this.serviceUser.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success save user");
			int respact = this.serviceAktivitas.createAktivitas("USER",created,method,url,nmember.getId_user());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes add user",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/user/{id}")
	public ResponseEntity<?> updateUser(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) {
		
		
		String method = "PUT";
		String url = "/user/"+id;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api put user");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
//			if (body.get("username")==null || body.get("username").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "username null!"),HttpStatus.BAD_REQUEST);
//			if (body.get("password")==null || body.get("password").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "password null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_role")==null || body.get("id_role").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_role null"),HttpStatus.BAD_REQUEST);
			if (body.get("status")==null || body.get("status").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "status null!"),HttpStatus.BAD_REQUEST);
			if (body.get("email")==null || body.get("email").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "email null!"),HttpStatus.BAD_REQUEST);
			if (body.get("nama")==null || body.get("nama").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "nama null"),HttpStatus.BAD_REQUEST);
			if (body.get("no_telp")==null || body.get("no_telp").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "no_telp null!"),HttpStatus.BAD_REQUEST);
			if (body.get("alamat")==null || body.get("alamat").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "alamat null!"),HttpStatus.BAD_REQUEST);
			if (body.get("jk")==null || body.get("jk").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jk null"),HttpStatus.BAD_REQUEST);
//			if (body.get("path_profile")==null || body.get("path_profile").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "path_profile null!"),HttpStatus.BAD_REQUEST);
			if (body.get("divisi")==null || body.get("divisi").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "divisi null!"),HttpStatus.BAD_REQUEST);
			if (body.get("jabatan")==null || body.get("jabatan").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "jabatan null"),HttpStatus.BAD_REQUEST);
			
//			logger.info(method+"|"+url+"|"+"username :"+body.get("username"));
//			logger.info(method+"|"+url+"|"+"password :"+body.get("password"));
			logger.info(method+"|"+url+"|"+"id_role :"+body.get("id_role"));
			logger.info(method+"|"+url+"|"+"status :"+body.get("status"));
			logger.info(method+"|"+url+"|"+"email :"+body.get("email"));
			logger.info(method+"|"+url+"|"+"nama :"+body.get("nama"));
			logger.info(method+"|"+url+"|"+"no_telp :"+body.get("no_telp"));
			logger.info(method+"|"+url+"|"+"alamat :"+body.get("alamat"));
			logger.info(method+"|"+url+"|"+"jk :"+body.get("jk"));
			logger.info(method+"|"+url+"|"+"path_profile :"+body.get("path_profile"));
			
			User nmember = serviceUser.findById(id);
			if(nmember == null ) { return new ResponseEntity(new ResponObject(2, "there is no user"),HttpStatus.BAD_REQUEST); }
			
			Date today = new Date();
			User created = resp.getData();
			nmember.setUpdated_by(created.getId_user());
			nmember.setUpdated_date(today);
			
			//nmember.setUsername(body.get("username"));
			//nmember.setPassword(encrypt.getSHA256EncryptedValue(body.get("password"))); //default nomor 12345
			nmember.setId_role(Integer.parseInt(body.get("id_role")));
			nmember.setStatus(Integer.parseInt(body.get("status")));
			nmember.setEmail(body.get("email"));
			nmember.setNama(body.get("nama"));
			nmember.setNo_telp(body.get("no_telp"));
			nmember.setAlamat(body.get("alamat"));
			nmember.setJ_k(Integer.parseInt(body.get("jk")));
			nmember.setPath_profile(body.get("path_profile"));
			nmember.setJabatan(body.get("jabatan"));
			nmember.setDivisi(body.get("divisi"));
			this.serviceUser.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success update member");
			int respact = this.serviceAktivitas.createAktivitas("USER",created,method,url,nmember.getId_user());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update user",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/change_password/{id_user}")
	public ResponseEntity<?> ChangePasswordByAdmin(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id_user,
			@RequestBody Map<String,String> body) {
		
		
		String method = "PUT";
		String url = "/change_password/"+id_user;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api change password by admin");
			
			if (body.get("password_new").isEmpty() || body.get("password_new") == null) {
				return new ResponseEntity(new ResponObject(2, "password_new null!"),HttpStatus.BAD_REQUEST);
			}
			if (body.get("password_confirm").isEmpty() || body.get("password_confirm") == null) {
				return new ResponseEntity(new ResponObject(2, "password_confirm null!"),HttpStatus.BAD_REQUEST);
			}
			
			if (!body.get("password_new").contentEquals(body.get("password_confirm"))) {
				return new ResponseEntity(new ResponObject(2, "not match!"),HttpStatus.BAD_REQUEST);
			}
			
			User upd = this.serviceUser.findById(id_user);
			upd.setPassword(encrypt.getSHA256EncryptedValue(body.get("password_new")));
			upd.setUpdated_by(resp.getData().getId_user());
			upd.setUpdated_date(new Date());
			this.serviceUser.save(upd);
			
			logger.info(method+"|"+url+"|"+"success change password");
			int respact = this.serviceAktivitas.createAktivitas("CHANGEPASSWORD",upd,"",method,url);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes change password!"),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/user_status/{id}/{status}")
	public ResponseEntity<?> updateUserStatus(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@PathVariable int status) {
		
		String method = "PUT";
		String url = "/user_status/"+id+"/"+status;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api put user status");
			
			User nmember = serviceUser.findById(id);
			
			Date today = new Date();
			User created = resp.getData();
			
			nmember.setUpdated_by(created.getId_user());
			nmember.setUpdated_date(today);
			nmember.setStatus(status);	
			this.serviceUser.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success update member");
			int respact = this.serviceAktivitas.createAktivitas("USER",created,method,url,nmember.getId_user());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update user",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public List<UserJoin> joinUser(List<User> data){
		
		List<UserJoin> resp = new ArrayList<UserJoin>();
//		List<User> dumy_mem = new ArrayList<User>();
		
		for (User check : data) {
			
			UserJoin user_join = new UserJoin();
//			User mem = new User();
//			if(dumy_mem.size()>0) {
//				int loop = 0;
//				for(User a : dumy_mem) {
//					if(a.getId_user() == act.getId_user()) {
//						mem = a;
//						break;
//					} else if (loop == dumy_mem.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
//						mem = this.serviceUser.findById(act.getId_user());
//						dumy_mem.add(mem);
//						break;
//					}	
//					loop++;
//				}
//			} else {
//				mem = this.serviceUser.findById(act.getId_user());
//				dumy_mem.add(mem);
//			}
			
			user_join.setId_user(check.getId_user());
			user_join.setUsername(check.getUsername());
			user_join.setPassword(check.getPassword());
			user_join.setId_role(check.getId_role());
			user_join.setRole(this.serviceRole.findById(check.getId_role()));
			user_join.setStatus(check.getStatus());
			
			if (check.getStatus() == 1) {
				user_join.setNm_status("ACTIVE");
			} else {
				user_join.setNm_status("NON ACTIVE");
			}
			
			user_join.setEmail(check.getEmail());
			user_join.setNama(check.getNama());
			user_join.setNo_telp(check.getNo_telp());
			user_join.setJabatan(check.getJabatan());
			user_join.setDivisi(check.getDivisi());
			user_join.setAlamat(check.getAlamat());
			user_join.setJ_k(check.getJ_k());
			if (check.getJ_k() == 0) {
				user_join.setNm_jk("Wanita");
			} else {
				user_join.setNm_jk("Pria");
			}
			
			user_join.setPath_profile(check.getPath_profile());
			user_join.setCreated_by(check.getCreated_by());
			user_join.setCreated_date(configGetDate.format(check.getCreated_date()));
			
			if(check.getUpdated_date()!=null) {
				user_join.setUpdated_by(check.getUpdated_by());
				user_join.setUpdated_date(configGetDate.format(check.getUpdated_date()));
			}
			
			resp.add(user_join);
		}
		return resp;
	}
	
}
