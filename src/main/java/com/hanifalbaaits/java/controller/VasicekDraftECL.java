package com.hanifalbaaits.java.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.VasicekResultDraft;
import com.hanifalbaaits.java.modelDB.ECL;
import com.hanifalbaaits.java.modelDB.ECLLIST;
import com.hanifalbaaits.java.modelDB.ECLSKENARIO;
import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.DraftBucket;
import com.hanifalbaaits.java.modelDB.InputModelFL;
import com.hanifalbaaits.java.modelDB.Komentar;
import com.hanifalbaaits.java.modelDB.MAPE;
import com.hanifalbaaits.java.modelDB.MAPELIST;
import com.hanifalbaaits.java.modelDB.NilaiBucket;
import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelDB.VasicekAset;
import com.hanifalbaaits.java.modelDB.VasicekRating;
import com.hanifalbaaits.java.modelDB.VasicekResultAset;
import com.hanifalbaaits.java.modelDB.VasicekResultDraft;
import com.hanifalbaaits.java.modelDB.VasicekResultMacro;
import com.hanifalbaaits.java.modelDB.VasicekResultPD;
import com.hanifalbaaits.java.modelRespon.DraftBucketJoin;
import com.hanifalbaaits.java.modelRespon.DraftEclJoin;
import com.hanifalbaaits.java.modelRespon.Ecl;
import com.hanifalbaaits.java.modelRespon.EclBucket;
import com.hanifalbaaits.java.modelRespon.EclSkenario;
import com.hanifalbaaits.java.modelRespon.GetVasicek;
import com.hanifalbaaits.java.modelRespon.GetVasicekAset;
import com.hanifalbaaits.java.modelRespon.GetVasicekMacroList;
import com.hanifalbaaits.java.modelRespon.GetVasicekPD;
import com.hanifalbaaits.java.modelRespon.GetVasicekSummary;
import com.hanifalbaaits.java.modelRespon.KomentarJoin;
import com.hanifalbaaits.java.modelRespon.Mape;
import com.hanifalbaaits.java.modelRespon.MapeDraft;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelRespon.VasicekBankJoin;
import com.hanifalbaaits.java.modelRespon.VasicekResultDraftJoin;
import com.hanifalbaaits.java.service.DraftEclService;
import com.hanifalbaaits.java.service.DraftService;
import com.hanifalbaaits.java.service.DraftBucketService;
import com.hanifalbaaits.java.service.InputMevFL;
import com.hanifalbaaits.java.service.KomentarService;
import com.hanifalbaaits.java.service.SaveECLService;
import com.hanifalbaaits.java.service.SaveVasicekResultService;
import com.hanifalbaaits.java.service.SegmentService;
import com.hanifalbaaits.java.service.UserService;
import com.hanifalbaaits.java.service.VasicekAsetDraftService;
import com.hanifalbaaits.java.service.VasicekAsetService;
import com.hanifalbaaits.java.service.VasicekBankService;
import com.hanifalbaaits.java.service.VasicekRatingService;

@RestController
@RequestMapping(value="core-ifrs")
public class VasicekDraftECL {

	private Logger logger = LoggerFactory.getLogger(VasicekDraftECL.class);
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	UserService serviceUser;
	
	@Autowired
	KomentarService serviceKomentar;
	
	@Autowired
	SaveVasicekResultService serviceVasicek;
	
	@Autowired
	VasicekRatingService serviceRating;
	
	@Autowired
	VasicekAsetService serviceAset;
	
	@Autowired
	VasicekAsetDraftService serviceAsetDraft;
	
	@Autowired
	VasicekBankService serviceBank;
	
	int id_save_ecl = 0;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/vasicek/draft-ecl")
	public ResponseEntity<?> getInputData(@RequestHeader("Authorization") String Authorization) throws Exception {
		
		String method = "GET";
		String url = "/vasicek/draft-ecl";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			List<VasicekResultDraft> data = serviceVasicek.findResultDraftAll();
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponList(1, "empty ECL",data),HttpStatus.OK);
			}
		
			List<User> dumyUser = new ArrayList<User>();
			List<VasicekResultDraftJoin> dataJoin = new ArrayList<VasicekResultDraftJoin>();
			
			for (VasicekResultDraft vas_draf : data) {
				
				VasicekResultDraftJoin dr = new VasicekResultDraftJoin();

				dr.setId_vasicek_result_draft(vas_draf.getId_vasicek_result_draft());
				dr.setId_draft_vasicek(vas_draf.getId_draft_vasicek());
				dr.setAset_draft(this.serviceAsetDraft.findById(vas_draf.getId_draft_vasicek()));
				dr.setInterval_bucket(vas_draf.getInterval_bucket());
				dr.setPeriode_mulai(vas_draf.getPeriode_mulai());
				dr.setPeriode_selesai(vas_draf.getPeriode_selesai());
				dr.setHeader_macroeconomic(vas_draf.getHeader_macroeconomic());
				dr.setList_flg_projection(vas_draf.getList_flg_projection());
				dr.setPersen_normal(vas_draf.getPersen_normal());
				dr.setPersen_uptum(vas_draf.getPersen_uptum());
				dr.setPersen_downtum(vas_draf.getPersen_downtum());
				dr.setHistorical_mean(vas_draf.getHistorical_mean());
				dr.setHistorical_stdev(vas_draf.getHistorical_stdev());
				dr.setConfidence_level(vas_draf.getConfidence_level());
				dr.setT_distribution(vas_draf.getT_distribution());
				dr.setPeriode_ecl(vas_draf.getPeriode_ecl());
				
				dr.setSaldo_normal_scan(vas_draf.getSaldo_normal_scan());
				dr.setSaldo_weighted_scan(vas_draf.getSaldo_weighted_scan());
				dr.setResult_saldo_exiting(vas_draf.getResult_saldo_exiting());
				dr.setResult_saldo_selisih(vas_draf.getResult_saldo_selisih());
				dr.setResult_flag_model(vas_draf.getResult_flag_model());
				
				dr.setCreated_by(vas_draf.getCreated_by());
				dr.setCreated_date(vas_draf.getCreated_date());
				dr.setUpdated_by(vas_draf.getUpdated_by());
				dr.setUpdated_date(vas_draf.getUpdated_date());
				
				User user = new User();
				if(dumyUser.size()>0) {
					int loop = 0;
					for(User a : dumyUser) {
						if(a.getId_user() == vas_draf.getCreated_by()) {
							user = a;
							break;
						} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							user = this.serviceUser.findById(vas_draf.getCreated_by());
							dumyUser.add(user);
							break;
						}	
						loop++;
					}
				} else {
					user = this.serviceUser.findById(vas_draf.getCreated_by());
					dumyUser.add(user);
				}
				dr.setName_created(user);
				
				if (vas_draf.getUpdated_by() != 0) {
					User useru = new User();
					if(dumyUser.size()>0) {
						int loop = 0;
						for(User a : dumyUser) {
							if(a.getId_user() == vas_draf.getUpdated_by()) {
								useru = a;
								break;
							} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
								useru = this.serviceUser.findById(vas_draf.getUpdated_by());
								dumyUser.add(useru);
								break;
							}	
							loop++;
						}
					} else {
						useru = this.serviceUser.findById(vas_draf.getUpdated_by());
						dumyUser.add(useru);
					}
					dr.setName_updated(useru);
				}
				
				
				List<KomentarJoin> komjo = new ArrayList<KomentarJoin>();
				List<Komentar> komens = this.serviceKomentar.findByTypeIdTypeAll(3, vas_draf.getId_vasicek_result_draft());
				
				for (Komentar komen : komens) {
					KomentarJoin kojo = new KomentarJoin();
					kojo.setId_komentar_bucket(komen.getId_komentar_bucket());
					kojo.setId_type(komen.getId_type());
					kojo.setFlag(komen.getFlag());
					kojo.setId_bucket(komen.getId_bucket());
					kojo.setKomentar(komen.getKomentar());
					kojo.setCreated_by(komen.getCreated_by());
					kojo.setCreated_date(komen.getCreated_date());
					kojo.setUpdated_date(komen.getUpdated_date());
					
					if(dumyUser.size()>0) {
						int loopX = 0;
						for(User a : dumyUser) {
							if(a.getId_user() == komen.getCreated_by()) {
								user = a;
								break;
							} else if (loopX == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
								user = this.serviceUser.findById(komen.getCreated_by());
								dumyUser.add(user);
								break;
							}	
							loopX++;
						}
					} else {
						user = this.serviceUser.findById(komen.getCreated_by());
						dumyUser.add(user);
					}
					kojo.setUser(user);
					komjo.add(kojo);
				}
				dr.setList_komentar(komjo);
				
				
				dataJoin.add(dr);
			}
			
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponList(1, "List ECL",dataJoin),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/vasicek/draft-ecl/{id}")
	public ResponseEntity<?> updateSaldoSelisih(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) {
		
		String method = "PUT";
		String url = "/vasicek/draft-ecl/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api put draft-ecl");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("result_saldo_exiting")==null || body.get("result_saldo_exiting").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "result_saldo_exiting null!"),HttpStatus.BAD_REQUEST);
			if (body.get("result_flag_model")==null || body.get("result_flag_model").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "result_flag_model null"),HttpStatus.BAD_REQUEST);
//			if (body.get("created_by")==null || body.get("created_by").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "created_by null!"),HttpStatus.BAD_REQUEST);
			
			
			logger.info(method+"|"+url+"|"+ "result_saldo_exiting :"+body.get("result_saldo_exiting"));
//			logger.info(method+"|"+url+"|"+ "result_saldo_selisih :"+body.get("result_saldo_selisih"));
			
			VasicekResultDraft nmember = this.serviceVasicek.findResultDraft(id);
			if(nmember == null ) { return new ResponseEntity(new ResponObject(2, "there is no Draft ECL"),HttpStatus.BAD_REQUEST); }
		
			double saldo_exiting = Double.parseDouble(body.get("result_saldo_exiting"));
			
			nmember.setResult_saldo_exiting(saldo_exiting);
			
			//FLAG MODEL 0 -> belum update
			//FLAG MODEL 1 -> saldo dgn normal
			//FLAG MODEL 2 -> saldo dgn weighted
			nmember.setResult_flag_model(Integer.parseInt(body.get("result_flag_model")));
			
			if (body.get("result_flag_model").contentEquals("1")) {
				
				nmember.setResult_saldo_selisih(nmember.getSaldo_normal_scan() - saldo_exiting);
				
			} else if (body.get("result_flag_model").contentEquals("2")) {
				
				nmember.setResult_saldo_selisih(nmember.getSaldo_weighted_scan() - saldo_exiting);
				
			}
			nmember.setUpdated_date(new Date());
			nmember.setUpdated_by(resp.getData().getId_user());
			this.serviceVasicek.saveDraft(nmember);
			logger.info(method+"|"+url+"|"+"respact: ");
			
			return new ResponseEntity(new ResponObject(1, "Succes update Draft ECL",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
		
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@DeleteMapping(value="/vasicek/draft-ecl/{id}")
	public ResponseEntity<?> deleteInput(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) throws Exception {
		
		String method = "DELETE";
		String url = "/vasicek/draft-ecl/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api delete data list asset");
			logger.info(method+"|"+url+"|"+"input data list ecl id: "+id);
			this.serviceVasicek.deleteResultVasicek(id);
			this.serviceKomentar.deleteByIdDraft(3, id);
			
			int respact = this.serviceAktivitas.createAktivitas("LIST VASICEK ECL",resp.getData(),method,url,id);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			logger.info(method+"|"+url+"|"+"delete data List Ecl");	
			return new ResponseEntity(new ResponObject(1, "delete List Ecl."),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/vasicek/draft-ecl/{id}")
	public ResponseEntity<?> getDraftID(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id
			) {
		
		String method = "GET";
		String url = "/vasicek/draft-ecl/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get draft-ecl");
			
			VasicekResultDraft result_draft = this.serviceVasicek.findResultDraft(id);
			
			if (result_draft == null) {
				logger.info(method+"|"+url+"|"+"tidak ada data draft-ecl");
				return new ResponseEntity(new ResponList(2, "empty draft-ecl"),HttpStatus.OK);
			}
			
			List<User> dumyUser = new ArrayList<User>();
			
			List<VasicekResultAset> result_aset = this.serviceVasicek.findResultAset(id);
			List<VasicekResultMacro> result_macro = this.serviceVasicek.findResultMacro(id);
			List<VasicekResultPD> result_pd = this.serviceVasicek.findResultPd(id);
			
			GetVasicek rst = new GetVasicek();
			
			VasicekResultDraftJoin dr = new VasicekResultDraftJoin();
			dr.setId_vasicek_result_draft(result_draft.getId_vasicek_result_draft());
			dr.setId_draft_vasicek(result_draft.getId_draft_vasicek());
			dr.setAset_draft(this.serviceAsetDraft.findById(result_draft.getId_draft_vasicek()));
			dr.setInterval_bucket(result_draft.getInterval_bucket());
			dr.setPeriode_mulai(result_draft.getPeriode_mulai());
			dr.setPeriode_selesai(result_draft.getPeriode_selesai());
			dr.setHeader_macroeconomic(result_draft.getHeader_macroeconomic());
			dr.setList_flg_projection(result_draft.getList_flg_projection());
			dr.setPersen_normal(result_draft.getPersen_normal());
			dr.setPersen_uptum(result_draft.getPersen_uptum());
			dr.setPersen_downtum(result_draft.getPersen_downtum());
			dr.setHistorical_mean(result_draft.getHistorical_mean());
			dr.setHistorical_stdev(result_draft.getHistorical_stdev());
			dr.setConfidence_level(result_draft.getConfidence_level());
			dr.setT_distribution(result_draft.getT_distribution());
			dr.setPeriode_ecl(result_draft.getPeriode_ecl());
			
			dr.setSaldo_normal_scan(result_draft.getSaldo_normal_scan());
			dr.setSaldo_weighted_scan(result_draft.getSaldo_weighted_scan());
			dr.setResult_saldo_exiting(result_draft.getResult_saldo_exiting());
			dr.setResult_saldo_selisih(result_draft.getResult_saldo_selisih());
			dr.setResult_flag_model(result_draft.getResult_flag_model());
			
			dr.setCreated_by(result_draft.getCreated_by());
			dr.setCreated_date(result_draft.getCreated_date());
			dr.setUpdated_by(result_draft.getUpdated_by());
			dr.setUpdated_date(result_draft.getUpdated_date());
			
			User user = new User();
			if(dumyUser.size()>0) {
				int loop = 0;
				for(User a : dumyUser) {
					if(a.getId_user() == result_draft.getCreated_by()) {
						user = a;
						break;
					} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						user = this.serviceUser.findById(result_draft.getCreated_by());
						dumyUser.add(user);
						break;
					}	
					loop++;
				}
			} else {
				user = this.serviceUser.findById(result_draft.getCreated_by());
				dumyUser.add(user);
			}
			dr.setName_created(user);
			
			if (result_draft.getUpdated_by() != 0) {
				User useru = new User();
				if(dumyUser.size()>0) {
					int loop = 0;
					for(User a : dumyUser) {
						if(a.getId_user() == result_draft.getUpdated_by()) {
							useru = a;
							break;
						} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							useru = this.serviceUser.findById(result_draft.getUpdated_by());
							dumyUser.add(useru);
							break;
						}	
						loop++;
					}
				} else {
					useru = this.serviceUser.findById(result_draft.getUpdated_by());
					dumyUser.add(useru);
				}
				dr.setName_updated(useru);
			}
			
			rst.setResult_draft(dr);
			rst.setResult_aset(result_aset);
			rst.setResult_macro(result_macro);
			rst.setResult_pd(result_pd);
			
			rst.setCreated_by(result_draft.getCreated_by());
			rst.setCreated_date(configGetDate.format(result_draft.getCreated_date()));
			rst.setCreated_user(user);
				
			List<KomentarJoin> komjo = new ArrayList<KomentarJoin>();
			List<Komentar> komens = this.serviceKomentar.findByTypeIdTypeAll(3, result_draft.getId_vasicek_result_draft());
			
			for (Komentar komen : komens) {
				KomentarJoin kojo = new KomentarJoin();
				kojo.setId_komentar_bucket(komen.getId_komentar_bucket());
				kojo.setId_type(komen.getId_type());
				kojo.setFlag(komen.getFlag());
				kojo.setId_bucket(komen.getId_bucket());
				kojo.setKomentar(komen.getKomentar());
				kojo.setCreated_by(komen.getCreated_by());
				kojo.setCreated_date(komen.getCreated_date());
				kojo.setUpdated_date(komen.getUpdated_date());
				
				if(dumyUser.size()>0) {
					int loopX = 0;
					for(User a : dumyUser) {
						if(a.getId_user() == komen.getCreated_by()) {
							user = a;
							break;
						} else if (loopX == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							user = this.serviceUser.findById(komen.getCreated_by());
							dumyUser.add(user);
							break;
						}	
						loopX++;
					}
				} else {
					user = this.serviceUser.findById(komen.getCreated_by());
					dumyUser.add(user);
				}
				kojo.setUser(user);
				komjo.add(kojo);
			}
			rst.setList_komentar(komjo);
				
			/////////////////////////////////
			
			rst.setTitle_bucket("MacroEconomic Adjustment");
			rst.setInterval_bucket(result_draft.getInterval_bucket());
			rst.setPeriode_mulai(result_draft.getPeriode_mulai());
			rst.setPeriode_selesai(result_draft.getPeriode_selesai());
			
			List<String> header = new ArrayList<String>();
			JSONArray list_header = new JSONArray(result_draft.getHeader_macroeconomic());
			for (int i = 0; i < list_header.length(); i++) {
				header.add(list_header.getString(i)); 
			}
			rst.setHeader_macroeconomic(header);
			
			JSONArray list_flag = new JSONArray(result_draft.getList_flg_projection());
			int [] list_f = new int [list_flag.length()]; 
			for (int i = 0; i < list_flag.length(); i++) {
				list_f[i] = list_flag.getInt(i);
			}
			rst.setList_flg_projection(list_f);
			
			List<GetVasicekMacroList> list_macro = new ArrayList<GetVasicekMacroList>();
			for (VasicekResultMacro mac : result_macro) {
				GetVasicekMacroList mc = new GetVasicekMacroList();
				mc.setTitle(mac.getTitle());
				
				JSONArray list_nilai = new JSONArray(mac.getNilai());
				double [] list_n = new double [list_nilai.length()]; 
				for (int i = 0; i < list_nilai.length(); i++) {
					list_n[i] = list_nilai.getDouble(i);
				}
				mc.setNilai(list_n);
				list_macro.add(mc);
			}
			rst.setList_macroeconomic(list_macro);
			
			rst.setPersen_normal(result_draft.getPersen_normal());
			rst.setPersen_uptum(result_draft.getPersen_uptum());
			rst.setPersen_downtum(result_draft.getPersen_downtum());
			rst.setHistorical_mean(result_draft.getHistorical_mean());
			rst.setHistorical_stdev(result_draft.getHistorical_stdev());
			rst.setConfidence_level(result_draft.getConfidence_level());
			rst.setT_distribution(result_draft.getT_distribution());
			
			List<GetVasicekPD> vasicek_pd = new ArrayList<GetVasicekPD>();
			for (VasicekResultPD vas_pd : result_pd) {
				
				GetVasicekPD vas_pdd = new GetVasicekPD();
				vas_pdd.setId_rating(vas_pd.getId_rating());
				vas_pdd.setPeriode_projection(vas_pd.getPeriode_projection());
				vas_pdd.setRating(this.serviceRating.findById(vas_pd.getId_rating()));
				vas_pdd.setValue_base_formula(vas_pd.getValue_base_formula());
				vas_pdd.setValue_rating(vas_pd.getValue_rating());
				vas_pdd.setVasicek_adjusted_normal(vas_pd.getVasicek_adjusted_normal());
				vas_pdd.setVasicek_adjusted_weighted(vas_pd.getVasicek_adjusted_weighted());
				vas_pdd.setVasicek_normal(vas_pd.getVasicek_normal());
				vas_pdd.setVasicek_weighted(vas_pd.getVasicek_weighted());
				vasicek_pd.add(vas_pdd);
			}
			rst.setList_pd_vasicek(vasicek_pd);
			
			rst.setPeriode_ecl(result_draft.getPeriode_ecl());
			
			double total_saldo = 0.0;
			double total_ecl_normal = 0.0;
			double total_ecl_weight = 0.0;
			List<GetVasicekAset> aset_list = new ArrayList<GetVasicekAset>();
			List<GetVasicekSummary> sumary_list = new ArrayList<GetVasicekSummary>();
			
			List<Integer> fil = new ArrayList<Integer>();
			int asx = 0;
			for (VasicekResultAset getVas : result_aset) {
				GetVasicekAset vas = new GetVasicekAset();
				
				VasicekAset asetv = new VasicekAset();
				asetv.setId_vas_aset(getVas.getId_aset());
				asetv.setNama_aset(getVas.getNama_aset());
				vas.setAset(asetv);
				
				VasicekBankJoin bankJ = new VasicekBankJoin();
				bankJ.setAkronim(getVas.getAkronim());
//				bankJ.setId_rating(getVas.getI);
				bankJ.setId_vas_bank(getVas.getId_bank());
				bankJ.setNama_bank(getVas.getNama_bank());
				bankJ.setTahun(getVas.getTahun_bank());
				bankJ.setNilai(getVas.getValue_rating());
				
				VasicekRating rat = new VasicekRating();
				rat.setKode_rating(getVas.getKode_rating());
				rat.setValue(getVas.getValue_rating());
				bankJ.setRating(rat);
//				vas.setBank(this.serviceBank.findById(getVas.getId_bank()));
				vas.setBank(bankJ);
				
				vas.setEcl_normal_scan(getVas.getEcl_normal_scan());
				vas.setEcl_weighted_scan(getVas.getEcl_weighted_scan());
				vas.setId_aset(getVas.getId_aset());
				vas.setId_bank(getVas.getId_bank());
				vas.setLgd(getVas.getLgd());
				vas.setMata_uang(getVas.getMata_uang());
				vas.setNo_rekening(getVas.getNo_rekening());
				vas.setPd_normal_scan(getVas.getPd_normal_scan());
				vas.setPd_weighted_scan(getVas.getPd_weighted_scan());
				vas.setSaldo(getVas.getSaldo());
				vas.setSuku_bunga(getVas.getSuku_bunga());
				vas.setTgl_jatuh_tempo(getVas.getTgl_jatuh_tempo());
				aset_list.add(vas);
				
				total_saldo += getVas.getSaldo();
				total_ecl_normal += getVas.getEcl_normal_scan();
				total_ecl_weight += getVas.getEcl_weighted_scan();
				
				//FILTER ASET YG ADA.
				if (asx == 0) {
					fil.add(getVas.getId_aset());
				} else {
					boolean check = true;
					for (Integer integer : fil) {
						if (integer == getVas.getId_aset()) {
							check = false;
							break;
						}
					}
					if (check) {
						fil.add(getVas.getId_aset());
					}
				}
				asx++;
			}
			
			//TAMBAHKAN DATA TERAKHIR SEBAGAI SUM NYA
			GetVasicekAset ast = new GetVasicekAset();
			ast.setSaldo(total_saldo);
			ast.setEcl_normal_scan(total_ecl_normal);
			ast.setEcl_weighted_scan(total_ecl_weight);
			aset_list.add(ast);
			rst.setList_aset(aset_list);
			
			//LIST SUMMARY
			for (int j = 0; j < fil.size(); j++) {		
				GetVasicekSummary sum = new GetVasicekSummary();
				sum.setId_aset(fil.get(j));
				sum.setAset(this.serviceAset.findById(fil.get(j)));
				
				double sum_saldo = 0.0;
				double sum_normal = 0.0;
				double sum_weighted = 0.0;
				
				for (GetVasicekAset asl : aset_list) {
					if (fil.get(j) == asl.getId_aset()) {
						sum_saldo += asl.getSaldo();
						sum_normal += asl.getEcl_normal_scan();
						sum_weighted += asl.getEcl_weighted_scan();
					}
				}
				sum.setSaldo(sum_saldo);
				sum.setEcl_normal_scan(sum_normal);
				sum.setEcl_weighted_scan(sum_weighted);
				sumary_list.add(sum);
			}
			
			GetVasicekSummary sum = new GetVasicekSummary();
			sum.setSaldo(total_saldo);
			sum.setEcl_normal_scan(total_ecl_normal);
			sum.setEcl_weighted_scan(total_ecl_weight);
			sumary_list.add(sum);
			rst.setList_summary(sumary_list);
			
			logger.info(method+"|"+url+"|"+"data Draft ECL");
			return new ResponseEntity(new ResponObject(1, "Draft ECL",rst),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
