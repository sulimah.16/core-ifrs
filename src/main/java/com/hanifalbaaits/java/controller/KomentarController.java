package com.hanifalbaaits.java.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.Komentar;
import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelRespon.KomentarJoin;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.service.KomentarService;
import com.hanifalbaaits.java.service.UserService;

@RestController
@RequestMapping(value="core-ifrs")
public class KomentarController {
	
	private Logger logger = LoggerFactory.getLogger(KomentarController.class);
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	KomentarService serviceKomentar;
	
	@Autowired
	UserService serviceUser;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/komentar")
	public ResponseEntity<?> getLastAdmin(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/komentar";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info("hit api master komentar");
			List<Komentar> dt = serviceKomentar.findAll();
			
			if (dt.size() == 0) {
				logger.info("tidak ada data Komentar");
				return new ResponseEntity(new ResponList(1, "empty Komentar",dt),HttpStatus.OK);
			}
			logger.info("success List Komentar");
			return new ResponseEntity(new ResponList(1, "List Komentar",dt),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/komentar/all/{type}/{flag}/{id}")
	public ResponseEntity<?> getKomentarMemberAll(@RequestHeader("Authorization") String Authorization,
			@PathVariable int type,
			@PathVariable int flag,
			@PathVariable int id
			) {
	
		String method = "GET";
		String url = "/komentar/user/"+id;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info("hit api get master komentar");
			List<Komentar> dt = serviceKomentar.findByTypeIdTypeAll(type, flag, id);
			
			if (dt.size() == 0) {
				logger.info("tidak ada data Komentar");
				return new ResponseEntity(new ResponList(1, "empty Komentar",dt),HttpStatus.OK);
			}
			
			List<KomentarJoin> dtj = getKomentarJoin(dt);
			logger.info("success List Komentar");
			return new ResponseEntity(new ResponList(1, "List Komentar",dtj),HttpStatus.OK);
			
			
		} catch (Exception e) {
			// TODO: handle exception
			
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/komentar/{type}/{flag}/{id}")
	public ResponseEntity<?> getKomentarMember(@RequestHeader("Authorization") String Authorization,
			@PathVariable int type,
			@PathVariable int flag,
			@PathVariable int id
			) {
	
		String method = "GET";
		String url = "/komentar/user/"+id;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info("hit api get master komentar");
			List<Komentar> dt = serviceKomentar.findByTypeIdType(type,flag,id);
			
			if (dt.size() == 0) {
				logger.info("tidak ada data Komentar");
				return new ResponseEntity(new ResponList(1, "empty Komentar",dt),HttpStatus.OK);
			}
			List<KomentarJoin> dtj = getKomentarJoin(dt);
			logger.info("success List Komentar");
			return new ResponseEntity(new ResponList(1, "List Komentar",dtj),HttpStatus.OK);
			
			
		} catch (Exception e) {
			// TODO: handle exception
			
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/komentar")
	public ResponseEntity<?> createActivity(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		
		String method = "POST";
		String url = "/komentar";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("type")==null || body.get("type").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "type null!"),HttpStatus.BAD_REQUEST);
		if (body.get("flag")==null || body.get("flag").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "flag null!"),HttpStatus.BAD_REQUEST);
		if (body.get("bucket")==null || body.get("bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "bucket null!"),HttpStatus.BAD_REQUEST);
		if (body.get("komentar")==null || body.get("komentar").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "komentar null!"),HttpStatus.BAD_REQUEST);
		if (body.get("created_by")==null || body.get("created_by").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "created_by null"),HttpStatus.BAD_REQUEST);
		
		try {
			logger.info( "hit api get Komentar search");

			String type = body.get("type"); //admin //customer //member
			String flag = body.get("flag");
			String bucket = body.get("bucket");
			String komentar = body.get("komentar");
			String created_by = body.get("created_by").trim();
			
			//TYPE 1 -> draft bucket simulate
			//FLAG 0 -> utama
			//FLAG 1 -> araging ...
			
			//TYPE 2 -> draft ecl
			//FLAG 0 -> utama
			//FLAG 1 -> mape ...
			//FLAG 2 -> list ecl no model / proporsional / solver
			
			//TYPE 3 -> draft vasicek
			
			logger.info( "type : "+type);
			logger.info( "flag : "+flag);
			logger.info( "bucket : "+bucket);
			logger.info( "komentar : "+komentar);
			logger.info( "created_by : "+created_by);
		
			Komentar komen = new Komentar();
			komen.setId_type(Integer.parseInt(type));
			komen.setId_bucket(Integer.parseInt(bucket));
			komen.setFlag(Integer.parseInt(flag));
			komen.setKomentar(komentar);
			komen.setCreated_by(Integer.parseInt(created_by));
			komen.setCreated_date(new Date());
			komen.setUpdated_date(new Date());
			this.serviceKomentar.save(komen);
			return new ResponseEntity(new ResponList(1, "Berhasil"),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/komentar/{id}")
	public ResponseEntity<?> updateActivity(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) throws Exception {
		
		
		String method = "POST";
		String url = "/komentar/"+id;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("type")==null || body.get("type").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "type null!"),HttpStatus.BAD_REQUEST);
		if (body.get("flag")==null || body.get("flag").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "flag null!"),HttpStatus.BAD_REQUEST);
		if (body.get("bucket")==null || body.get("bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "bucket null!"),HttpStatus.BAD_REQUEST);
		if (body.get("komentar")==null || body.get("komentar").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "komentar null!"),HttpStatus.BAD_REQUEST);
		if (body.get("created_by")==null || body.get("created_by").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "created_by null"),HttpStatus.BAD_REQUEST);
		
		try {
			logger.info( "hit api get Komentar search");

			String type = body.get("type"); //admin //customer //member
			String flag = body.get("flag");
			String bucket = body.get("bucket");
			String komentar = body.get("komentar");
			String created_by = body.get("created_by").trim();
			
			logger.info( "type : "+type);
			logger.info( "flag : "+flag);
			logger.info( "bucket : "+bucket);
			logger.info( "komentar : "+komentar);
			logger.info( "created_by : "+created_by);
		
			Komentar komen = this.serviceKomentar.findById(id);
			komen.setId_type(Integer.parseInt(type));
			komen.setId_bucket(Integer.parseInt(bucket));
			komen.setFlag(Integer.parseInt(flag));
			komen.setKomentar(komentar);
			komen.setCreated_by(Integer.parseInt(created_by));
			komen.setCreated_date(new Date());
			komen.setUpdated_date(new Date());
			this.serviceKomentar.save(komen);
			return new ResponseEntity(new ResponList(1, "Berhasil Update"),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@DeleteMapping(value="/komentar/{id}")
	public ResponseEntity<?>delActivity(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) throws Exception {
		
		
		String method = "POST";
		String url = "/komentar/"+id;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
		try {
			logger.info( "hit api get Komentar search");
			this.serviceKomentar.delete(id);
			return new ResponseEntity(new ResponList(1, "Berhasil Hapus"),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public List<KomentarJoin> getKomentarJoin(List<Komentar> komens){
		
		List<KomentarJoin> komjo = new ArrayList<KomentarJoin>();
		List<User> dumyUser = new ArrayList<User>();
		
		for (Komentar komen : komens) {
			
			KomentarJoin kojo = new KomentarJoin();
			kojo.setId_komentar_bucket(komen.getId_komentar_bucket());
			kojo.setId_type(komen.getId_type());
			kojo.setFlag(komen.getFlag());
			kojo.setId_bucket(komen.getId_bucket());
			kojo.setKomentar(komen.getKomentar());
			kojo.setCreated_by(komen.getCreated_by());
			kojo.setCreated_date(komen.getCreated_date());
			kojo.setUpdated_date(komen.getUpdated_date());
			
			User user = new User();
			if(dumyUser.size()>0) {
				int loop = 0;
				for(User a : dumyUser) {
					if(a.getId_user() == komen.getCreated_by()) {
						user = a;
						break;
					} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						user = this.serviceUser.findById(komen.getCreated_by());
						dumyUser.add(user);
						break;
					}	
					loop++;
				}
			} else {
				user = this.serviceUser.findById(komen.getCreated_by());
				dumyUser.add(user);
			}
			kojo.setUser(user);
			komjo.add(kojo);
		}
		return komjo;
	}
}
