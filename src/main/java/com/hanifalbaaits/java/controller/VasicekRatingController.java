package com.hanifalbaaits.java.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.VasicekRating;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.service.VasicekRatingService;

@RestController
@RequestMapping(value="core-ifrs")
public class VasicekRatingController {
	
	private Logger logger = LoggerFactory.getLogger(VasicekRatingController.class);

	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	VasicekRatingService serviceRating;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/vas-rating")
	public ResponseEntity<?> getRating(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/vas-rating";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get vasicek rating");
			
			List<VasicekRating> data = serviceRating.findAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data Rating");
				return new ResponseEntity(new ResponList(1, "empty Rating",data),HttpStatus.OK);
			}
			
			logger.info(method+"|"+url+"|"+"data user");
			return new ResponseEntity(new ResponList(1, "List Rating",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/vas-rating")
	public ResponseEntity<?> AddRating(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) {
		
		
		String method = "POST";
		String url = "/vas-rating";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api add rating");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("kode")==null || body.get("kode").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "kode null!"),HttpStatus.BAD_REQUEST);
			if (body.get("value")==null || body.get("value").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "value null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "kode :"+body.get("kode"));
			logger.info(method+"|"+url+"|"+ "value :"+body.get("value"));
			
			VasicekRating check = serviceRating.findByKode(body.get("kode"));
			if(check != null) { return new ResponseEntity(new ResponObject(2, "kode telah terdaftar!"),HttpStatus.BAD_REQUEST); }
	
			VasicekRating nmember = new VasicekRating();
			
			nmember.setKode_rating(body.get("kode"));
			nmember.setValue(Double.parseDouble(body.get("value")));
			nmember.setStatus(1);
			this.serviceRating.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success save rating");
			int respact = this.serviceAktivitas.createAktivitas("RATING",resp.getData(),method,url,nmember.getId_rating());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes add rating",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/vas-rating/{id}")
	public ResponseEntity<?> updateRating(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) {
		
		String method = "PUT";
		String url = "/vas-rating/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api put rating");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("kode")==null || body.get("kode").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "kode null"),HttpStatus.BAD_REQUEST);
			if (body.get("value")==null || body.get("value").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "value null!"),HttpStatus.BAD_REQUEST);
			if (body.get("status")==null || body.get("status").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "status null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "kode :"+body.get("kode"));
			logger.info(method+"|"+url+"|"+ "value :"+body.get("value"));
			logger.info(method+"|"+url+"|"+ "status :"+body.get("status"));
			
			VasicekRating nmember = serviceRating.findById(id);
			if(nmember == null ) { return new ResponseEntity(new ResponObject(2, "tidak adak rating"),HttpStatus.BAD_REQUEST); }
			
			VasicekRating chk = serviceRating.findByKode(body.get("kode"));
			if (chk != null && !body.get("kode").equals(chk.getKode_rating())) {
				return new ResponseEntity(new ResponObject(2, "kode rating telah terpakai"),HttpStatus.BAD_REQUEST);
			}
			
			nmember.setKode_rating(body.get("kode"));
			nmember.setValue(Double.parseDouble(body.get("value")));	
			nmember.setStatus(Integer.parseInt(body.get("status")));
			this.serviceRating.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success update rating");
			int respact = this.serviceAktivitas.createAktivitas("RATING",resp.getData(),method,url,nmember.getId_rating());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update rating",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
