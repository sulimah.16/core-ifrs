package com.hanifalbaaits.java.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.DraftECL;
import com.hanifalbaaits.java.modelDB.ECL;
import com.hanifalbaaits.java.modelDB.ECLLIST;
import com.hanifalbaaits.java.modelDB.ECLSKENARIO;
import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.DraftBucket;
import com.hanifalbaaits.java.modelDB.InputModelFL;
import com.hanifalbaaits.java.modelDB.Komentar;
import com.hanifalbaaits.java.modelDB.MAPE;
import com.hanifalbaaits.java.modelDB.MAPELIST;
import com.hanifalbaaits.java.modelDB.NilaiBucket;
import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelRespon.DraftBucketJoin;
import com.hanifalbaaits.java.modelRespon.DraftEclJoin;
import com.hanifalbaaits.java.modelRespon.Ecl;
import com.hanifalbaaits.java.modelRespon.EclBucket;
import com.hanifalbaaits.java.modelRespon.EclSkenario;
import com.hanifalbaaits.java.modelRespon.KomentarJoin;
import com.hanifalbaaits.java.modelRespon.Mape;
import com.hanifalbaaits.java.modelRespon.MapeDraft;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.service.DraftEclService;
import com.hanifalbaaits.java.service.DraftService;
import com.hanifalbaaits.java.service.DraftBucketService;
import com.hanifalbaaits.java.service.InputMevFL;
import com.hanifalbaaits.java.service.KomentarService;
import com.hanifalbaaits.java.service.SaveECLService;
import com.hanifalbaaits.java.service.SegmentService;
import com.hanifalbaaits.java.service.UserService;

@RestController
@RequestMapping(value="core-ifrs")
public class MasterDraftECL {

	private Logger logger = LoggerFactory.getLogger(MasterDraftECL.class);
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	UserService serviceUser;
	
	@Autowired
	DraftEclService serviceECL;
	
	@Autowired
	SegmentService serviceSegment;
	
	@Autowired
	KomentarService serviceKomentar;
	
	@Autowired
	DraftBucketService serviceDraftBucket;
	
	@Autowired
	InputMevFL serviceModel;
	
	@Autowired
	SaveECLService serviceSaveECL;
	
	@Autowired
	InputMevFL serviceInputFL;
	
	@Autowired
	DraftService serviceDraft;
	
	@Autowired
	DraftBucketService serviceBucketService;
		
	@Autowired
	MasterReport serviceReport;
	
	@Autowired
	MasterDraftBucket serviceControllerBS;
	
	@Autowired
	MasterMapeECL serviceMapeController;
	
	int id_save_ecl = 0;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/draft-ecl")
	public ResponseEntity<?> getInputData(@RequestHeader("Authorization") String Authorization) throws Exception {
		
		String method = "GET";
		String url = "/draft-ecl";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			List<DraftECL> data = serviceECL.findAll();
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponList(1, "empty ECL",data),HttpStatus.OK);
			}
			
			List<DraftEclJoin> dataJoin = getDraftEclJoin(data);
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponList(1, "List ECL",dataJoin),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(value="/draft-ecl")
	public ResponseEntity<?> createInputData(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/draft-ecl";
		this.id_save_ecl = 0;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api add draft-ecl");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_draft_bucket")==null || body.get("id_draft_bucket").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_draft_bucket null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_model")==null || body.get("id_model").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_model null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_regresi")==null || body.get("id_regresi").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_regresi null!"),HttpStatus.BAD_REQUEST);
			if (body.get("month_forecast")==null || body.get("month_forecast").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "month_forecast null!"),HttpStatus.BAD_REQUEST);
			if (body.get("periode")==null || body.get("periode").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "periode null!"),HttpStatus.BAD_REQUEST);
			if (body.get("lgd")==null || body.get("lgd").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "lgd null!"),HttpStatus.BAD_REQUEST);
			if (body.get("normal")==null || body.get("normal").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "normal null!"),HttpStatus.BAD_REQUEST);
			if (body.get("optimis")==null || body.get("optimis").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "optimis null!"),HttpStatus.BAD_REQUEST);
			if (body.get("pesimis")==null || body.get("pesimis").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "pesimis null!"),HttpStatus.BAD_REQUEST);
			if (body.get("average_mape")==null || body.get("average_mape").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "average_mape null!"),HttpStatus.BAD_REQUEST);
			if (body.get("ecl_no_model")==null || body.get("ecl_no_model").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "ecl_no_model null!"),HttpStatus.BAD_REQUEST);
			if (body.get("ecl_proporsional")==null || body.get("ecl_proporsional").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "ecl_proporsional null!"),HttpStatus.BAD_REQUEST);
			if (body.get("ecl_solver")==null || body.get("ecl_solver").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "ecl_solver null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "id_draft_bucket :"+body.get("id_draft_bucket"));
			logger.info(method+"|"+url+"|"+ "id_model :"+body.get("id_model"));
			logger.info(method+"|"+url+"|"+ "id_regresi :"+body.get("id_regresi"));
			logger.info(method+"|"+url+"|"+ "month_forecast :"+body.get("month_forecast"));
			logger.info(method+"|"+url+"|"+ "periode :"+body.get("periode"));
			logger.info(method+"|"+url+"|"+ "lgd :"+body.get("lgd"));
			logger.info(method+"|"+url+"|"+ "normal :"+body.get("normal"));
			logger.info(method+"|"+url+"|"+ "optimis :"+body.get("optimis"));
			logger.info(method+"|"+url+"|"+ "pesimis :"+body.get("pesimis"));
			logger.info(method+"|"+url+"|"+ "average_mape :"+body.get("average_mape"));
			logger.info(method+"|"+url+"|"+ "ecl_no_model :"+body.get("ecl_no_model"));
			logger.info(method+"|"+url+"|"+ "ecl_proporsional :"+body.get("ecl_proporsional"));
			logger.info(method+"|"+url+"|"+ "ecl_solver :"+body.get("ecl_solver"));
			
			Date periode = null;
			int id_draft_bucket = 0;
			int id_model = 0;
			int id_regresi = 0;
			int periode_forecast = 0;
			double lgd = 0.0;
			double normal = 0.0;
			double optimis = 0.0;
			double pesimis = 0.0;
			double average_mape = 0.0;
			double ecl_no_model = 0.0;
			double ecl_proporsional = 0.0;
			double ecl_solver = 0.0;
			
			try {
				
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				periode = df.parse(body.get("periode"));
				
				logger.info(method+"|"+url+"|"+ "periode :"+periode.toString());
				id_draft_bucket = Integer.parseInt(body.get("id_draft_bucket")); 
				id_model = Integer.parseInt(body.get("id_model"));
				id_regresi = Integer.parseInt(body.get("id_regresi"));
				periode_forecast = Integer.parseInt(body.get("month_forecast"));
				lgd = Double.parseDouble(body.get("lgd"))/100;
				normal = Double.parseDouble(body.get("normal"))/100;
				optimis = Double.parseDouble(body.get("optimis"))/100;
				pesimis = Double.parseDouble(body.get("pesimis"))/100;
				average_mape = Double.parseDouble(body.get("average_mape"));
				ecl_no_model = Double.parseDouble(body.get("ecl_no_model"));
				ecl_proporsional = Double.parseDouble(body.get("ecl_proporsional"));
				ecl_solver = Double.parseDouble(body.get("ecl_solver"));
				
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);
				return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			Date today = new Date();
			DraftECL input = new DraftECL();
			
			User created = resp.getData();
			input.setCreated_by(created.getId_user());
			input.setCreated_date(today);
			input.setUpdated_by(0);
					
			input.setId_draft_bucket(id_draft_bucket);
			input.setId_model(id_model);
			input.setId_regresi(id_regresi);
			input.setMonth_forecast(periode_forecast);
			input.setPeriode(periode);
			input.setLgd(lgd);
			input.setNormal(normal);
			input.setOptimis(optimis);
			input.setPesimis(pesimis);
			input.setAverage_mape(average_mape);
			input.setEcl_no_model(ecl_no_model);
			input.setEcl_proporsional(ecl_proporsional);
			input.setEcl_solver(ecl_solver);
			
			
			//FLAG MODEL 0 -> belum update
			//FLAG MODEL 1 -> saldo dgn no model
			//FLAG MODEL 2 -> saldo dgn proporsional
			//FLAG MODEL 3 -> saldo dgn solver
			input.setResult_flag_model(0);
			input.setResult_saldo_exiting(0);
			input.setResult_saldo_selisih(0);
			
			this.serviceECL.save(input);
			this.id_save_ecl = input.getId_draft_ecl();
			
			
			DateFormat formatYear = new SimpleDateFormat("yyyy");  
			DateFormat formatMonth = new SimpleDateFormat("MM");
			

			DraftBucket drabu = this.serviceBucketService.findById(id_draft_bucket);
			Draft dr = this.serviceDraft.findById(drabu.getId_draft());
			List<NilaiBucket> nb = this.serviceBucketService.findNilaiBucketByIdDraftBucket(id_draft_bucket);
			DraftBucketJoin respon = serviceControllerBS.joinDraftBucket(dr, drabu, nb);
			
			if (id_model != 0) {
				
				//SAVE MAPE DAN LIST NYA
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = df.parse(body.get("start_date_mape").trim()+" 00:00");
					date2 = df.parse(body.get("end_date_mape").trim()+" 23:59");
					
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				int bulan_start = Integer.parseInt(formatMonth.format(date1));
				int tahun_start = Integer.parseInt(formatYear.format(date1));
				int bulan_end = Integer.parseInt(formatMonth.format(date2));
				int tahun_end = Integer.parseInt(formatYear.format(date2));
				
				//CEK TANGGAL nya harus ada sesuai dengan draft bucket
				Calendar cals = Calendar.getInstance();
				cals.setTime(date1);
				cals.set(Calendar.DATE, 1);
				
				if (cals.getTimeInMillis() < drabu.getStart_date().getTime() || cals.getTimeInMillis() > drabu.getEnd_date().getTime()) {
					return new ResponseEntity(new ResponObject(2, "Periode Start Date tidak memungkinkan dari draft bucket. Mohon periksa kembali"),HttpStatus.BAD_REQUEST);
				}
				
				Calendar cale = Calendar.getInstance();
				cale.setTime(date2);
				cale.set(Calendar.DATE, 1);
				
				if (cale.getTimeInMillis() > drabu.getEnd_date().getTime() || cale.getTimeInMillis() < cals.getTimeInMillis()) {
					return new ResponseEntity(new ResponObject(2, "Periode End Date tidak memungkinkan dari draft bucket. Mohon periksa kembali"),HttpStatus.BAD_REQUEST);	
				}
				
				//UPDATE PARAMETER MODEL JUGA ID SEGMENT
				InputModelFL model_fl = this.serviceInputFL.findFLByIdModelSegment(id_model, drabu.getId_segment());
				
				int jml_bulan = serviceReport.getBulanfromDate(date1, date2) + 1; //sengaja di tambahin angka 1.
				
				logger.info( "id_draft_bucket : "+id_draft_bucket);
				logger.info( "id_model : "+id_model);
				logger.info( "id_regresi : "+id_regresi);
				logger.info( "periode_forecast : "+periode_forecast);
				logger.info( "start_date : "+body.get("start_date_mape"));
				logger.info( "end_date : "+body.get("end_date_mape"));
				logger.info(method+"|"+url+"|"+ "bulan start :"+bulan_start);
				logger.info(method+"|"+url+"|"+ "tahun start :"+tahun_start);
				logger.info(method+"|"+url+"|"+ "bulan end :"+bulan_end);
				logger.info(method+"|"+url+"|"+ "tahun end :"+tahun_end);
				logger.info(method+"|"+url+"|"+ "jumlah_bulan :"+jml_bulan);
				
				MapeDraft mape_dr = serviceMapeController.getMAPE(nb, respon, id_regresi, model_fl, periode_forecast, date1, date2, jml_bulan, tahun_start, bulan_start);
				
				MAPE save_mape = new MAPE();
				save_mape.setId_draft_ecl(input.getId_draft_ecl());
				save_mape.setId_draft_bucket(id_draft_bucket);
				save_mape.setInput_regresi(id_regresi);
				save_mape.setId_model(id_model);
				save_mape.setPeriode_forecast(mape_dr.getPeriode_forecast());
				save_mape.setList_header(mape_dr.getHeader().toString());
				save_mape.setAverage_mape(mape_dr.getAverage_mape());
				save_mape.setThreshold_mape(mape_dr.getThreshold_mape());
				save_mape.setHasil_mape(mape_dr.getHasil_mape());
				save_mape.setPeriode_start(date1);
				save_mape.setPeriode_end(date2);
				this.serviceSaveECL.saveMape(save_mape);
				
				for (Mape map : mape_dr.getList_mape()) {
					MAPELIST save_lismap = new MAPELIST();
					save_lismap.setId_draft_ecl(input.getId_draft_ecl());
					save_lismap.setPeriode(map.getPeriode());
					save_lismap.setPeriode_bulan(map.getPeriode_bulan());
					save_lismap.setPeriode_tahun(map.getPeriode_tahun());
					save_lismap.setNilai_var1(map.getNilai_var1());
					save_lismap.setNilai_var2(map.getNilai_var2());
					save_lismap.setLogit_expected(map.getLogit_expected());
					save_lismap.setExpected(map.getExpected());
					save_lismap.setActual(map.getActual());
					save_lismap.setMape(map.getMape());
					this.serviceSaveECL.saveMapeList(save_lismap);
				}
			}
			
			//DISINI SAVE ECL NYA
			DateFormat df_ecl = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date periode_ecl = new Date();
			try {
				periode_ecl = df_ecl.parse(body.get("periode").trim()+" 00:00");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			int bulan_periode = Integer.parseInt(formatMonth.format(periode_ecl));
			int tahun_periode = Integer.parseInt(formatYear.format(periode_ecl));
			logger.info(method+"|"+url+"|"+ "periode :"+body.get("periode"));
			logger.info(method+"|"+url+"|"+ "tahun_periode :"+tahun_periode);
			logger.info(method+"|"+url+"|"+ "bulan_periode :"+bulan_periode);
			
			double scaling = 0.0;
			if (body.get("scaling") != null && !body.get("scaling").isEmpty()) {
				scaling = Double.parseDouble(body.get("scaling"));
				System.out.println("SCALING: "+scaling);
			}
			
			List<EclBucket> list_bucket = new ArrayList<EclBucket>();
			
			for (int i = 0; i < 2; i++) {
				if (i == 0) {
					
					EclBucket buc = serviceMapeController.getBucketECL(id_regresi, bulan_periode, tahun_periode, periode_forecast, 
							nb, 0, respon.getJumlah_bucket(), lgd , normal , optimis, pesimis, scaling,respon.getAraging().getHeader_bucket(), drabu.getId_segment());
					if (buc == null) {
						continue;
					}
					list_bucket.add(buc);
					if (id_model == 0) {
						break;	
					}
					
				} else {
					
					EclBucket buc = serviceMapeController.getBucketECL(id_regresi, bulan_periode, tahun_periode, periode_forecast, 
							nb, id_model, respon.getJumlah_bucket(), lgd , normal , optimis, pesimis, scaling,respon.getAraging().getHeader_bucket(), drabu.getId_segment());
					if (buc == null) {
						continue;
					}
					list_bucket.add(buc);
					
				}
			}
			
			for (EclBucket ecb : list_bucket) {
				ECL eclb = new ECL();
				eclb.setId_draft_ecl(input.getId_draft_ecl());
				eclb.setId_model(ecb.getId_model());
				eclb.setPeriode_forecast(ecb.getPeriode_forecast());
				eclb.setPeriode(ecb.getPeriode());
				eclb.setNo_model_sum_ead(ecb.getNo_model_sum_ead());
				eclb.setNo_model_sum_ecl(ecb.getNo_model_sum_ecl());
				eclb.setLogit_expected_psak(ecb.getLogit_expected_psak());
				
				eclb.setExpected_psak(ecb.getExpected_psak());
				eclb.setExpected(ecb.getExpected());
				eclb.setLast(ecb.getLast());
				eclb.setMev(ecb.getMev());
				eclb.setProporsional_sum_ead(ecb.getProporsional_sum_ead());
				eclb.setProporsional_sum_ecl(ecb.getProporsional_sum_ecl());
				eclb.setProporsional_sum_ecl_forward_looking(ecb.getProporsional_sum_ecl_forward_looking());
				eclb.setPd_forward_looking(ecb.getPd_forward_looking());
				eclb.setSolver_average_pd_base(ecb.getSolver_average_pd_base());
				eclb.setSolver_average_pd_forward_looking(ecb.getSolver_average_pd_forward_looking());
				eclb.setSolver_sum_ead(ecb.getSolver_sum_ead());
				eclb.setSolver_sum_ecl(ecb.getSolver_sum_ecl());
				eclb.setSolver_sum_ecl_forward_looking(ecb.getSolver_sum_ecl_forward_looking());
				eclb.setScaling(ecb.getScaling());
				eclb.setTarget(ecb.getTarget());
				this.serviceSaveECL.saveEcl(eclb);
				
				if (ecb.getId_model() != 0) {
					
					ECLSKENARIO skecel = new ECLSKENARIO();
					skecel.setId_draft_ecl(input.getId_draft_ecl());
					skecel.setStdev_var1(ecb.getSkenario().getStdev_var1());
					skecel.setStdev_var2(ecb.getSkenario().getStdev_var2());
					skecel.setNormal_var1(ecb.getSkenario().getNormal_var1());
					skecel.setNormal_var2(ecb.getSkenario().getNormal_var2());
					skecel.setNormal_probability(ecb.getSkenario().getNormal_probability());
					skecel.setOptimis_var1(ecb.getSkenario().getOptimis_var1());
					skecel.setOptimis_var2(ecb.getSkenario().getOptimis_var2());
					skecel.setOptimis_probability(ecb.getSkenario().getOptimis_probability());
					skecel.setPesimis_var1(ecb.getSkenario().getPesimis_var1());
					skecel.setPesimis_var2(ecb.getSkenario().getPesimis_var2());
					skecel.setPesimis_probability(ecb.getSkenario().getPesimis_probability());
					skecel.setWeigthed_var1(ecb.getSkenario().getWeigthed_var1());
					skecel.setWeigthed_var2(ecb.getSkenario().getWeigthed_var2());
					this.serviceSaveECL.saveEclSkenario(skecel);
				}
				
				if (ecb.getId_model() == 0) {
					
					for (Ecl eclno : ecb.getNo_model_list()) {
						
						ECLLIST save_eclist = new ECLLIST();
						save_eclist.setId_draft_ecl(input.getId_draft_ecl());
						save_eclist.setId_type(0);
						save_eclist.setBucket(eclno.getBucket());
						save_eclist.setPd_base(eclno.getPd_base());
						save_eclist.setMev_effect(eclno.getMev_effect());
						save_eclist.setLogit_pd(eclno.getLogit_pd());
						save_eclist.setPd_forward_looking(eclno.getPd_forward_looking());
						save_eclist.setLgd(eclno.getLgd());
						save_eclist.setEad(eclno.getEad());
						save_eclist.setEcl_base(eclno.getEcl_base());
						save_eclist.setEcl_forward_looking(eclno.getEcl_forward_looking());
						this.serviceSaveECL.saveEclList(save_eclist);
						
					}
					
				} else {
					
					for (Ecl eclno : ecb.getProporsional_list()) {
						
						ECLLIST save_eclist = new ECLLIST();
						save_eclist.setId_draft_ecl(input.getId_draft_ecl());
						save_eclist.setId_type(1);
						save_eclist.setBucket(eclno.getBucket());
						save_eclist.setPd_base(eclno.getPd_base());
						save_eclist.setMev_effect(eclno.getMev_effect());
						save_eclist.setLogit_pd(eclno.getLogit_pd());
						save_eclist.setPd_forward_looking(eclno.getPd_forward_looking());
						save_eclist.setLgd(eclno.getLgd());
						save_eclist.setEad(eclno.getEad());
						save_eclist.setEcl_base(eclno.getEcl_base());
						save_eclist.setEcl_forward_looking(eclno.getEcl_forward_looking());
						this.serviceSaveECL.saveEclList(save_eclist);
						
					}

					for (Ecl eclno : ecb.getSolver_list()) {
						
						ECLLIST save_eclist = new ECLLIST();
						save_eclist.setId_draft_ecl(input.getId_draft_ecl());
						save_eclist.setId_type(2);
						save_eclist.setBucket(eclno.getBucket());
						save_eclist.setPd_base(eclno.getPd_base());
						save_eclist.setMev_effect(eclno.getMev_effect());
						save_eclist.setLogit_pd(eclno.getLogit_pd());
						save_eclist.setPd_forward_looking(eclno.getPd_forward_looking());
						save_eclist.setLgd(eclno.getLgd());
						save_eclist.setEad(eclno.getEad());
						save_eclist.setEcl_base(eclno.getEcl_base());
						save_eclist.setEcl_forward_looking(eclno.getEcl_forward_looking());
						this.serviceSaveECL.saveEclList(save_eclist);
						
					}
					
				}
			}
			
			logger.info(method+"|"+url+"|"+"success save list ecl");
			int respact = this.serviceAktivitas.createAktivitas("LIST ECL",created,method,url,input.getId_draft_ecl());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes add List Ecl",input),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			this.serviceECL.delete(this.id_save_ecl);
			this.serviceSaveECL.deleteECL(this.id_save_ecl);
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(value="/draft-ecl/{id}")
	public ResponseEntity<?> updateSaldoSelisih(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) {
		
		String method = "PUT";
		String url = "/draft-ecl/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api put draft-ecl");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("result_flag_model")==null || body.get("result_flag_model").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "result_flag_model null"),HttpStatus.BAD_REQUEST);
			if (body.get("result_saldo_exiting")==null || body.get("result_saldo_exiting").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "result_saldo_exiting null!"),HttpStatus.BAD_REQUEST);
//			if (body.get("result_flag_model")==null || body.get("result_flag_model").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "result_flag_model null"),HttpStatus.BAD_REQUEST);
//			if (body.get("created_by")==null || body.get("created_by").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "created_by null!"),HttpStatus.BAD_REQUEST);
			
			
			logger.info(method+"|"+url+"|"+ "result_flag_model :"+body.get("result_flag_model"));
			logger.info(method+"|"+url+"|"+ "result_saldo_exiting :"+body.get("result_saldo_exiting"));
//			logger.info(method+"|"+url+"|"+ "result_saldo_selisih :"+body.get("result_saldo_selisih"));
			
			DraftECL nmember = serviceECL.findById(id);
			if(nmember == null ) { return new ResponseEntity(new ResponObject(2, "there is no Draft ECL"),HttpStatus.BAD_REQUEST); }
		
			double saldo_exiting = Double.parseDouble(body.get("result_saldo_exiting"));
			
			nmember.setResult_saldo_exiting(saldo_exiting);
			
			//FLAG MODEL 0 -> belum update
			//FLAG MODEL 1 -> saldo dgn no model
			//FLAG MODEL 2 -> saldo dgn proporsional
			//FLAG MODEL 3 -> saldo dgn solver
			nmember.setResult_flag_model(Integer.parseInt(body.get("result_flag_model")));
			
			if (body.get("result_flag_model").contentEquals("1")) {
				
				nmember.setResult_saldo_selisih(nmember.getEcl_no_model() - saldo_exiting);
				
			} else if (body.get("result_flag_model").contentEquals("2")) {
				
				nmember.setResult_saldo_selisih(nmember.getEcl_proporsional() - saldo_exiting);
				
			} else if (body.get("result_flag_model").contentEquals("3")) {
				
				nmember.setResult_saldo_selisih(nmember.getEcl_solver() - saldo_exiting);
				
			}
			nmember.setUpdated_date(new Date());
			nmember.setUpdated_by(resp.getData().getId_user());
			this.serviceECL.save(nmember);
			logger.info(method+"|"+url+"|"+"respact: ");
			
			return new ResponseEntity(new ResponObject(1, "Succes update Draft ECL",nmember),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@DeleteMapping(value="/draft-ecl/{id}")
	public ResponseEntity<?> deleteInput(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) throws Exception {
		
		String method = "DELETE";
		String url = "/draft-ecl/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api delete data list asset");
			logger.info(method+"|"+url+"|"+"input data list ecl id: "+id);
			this.serviceECL.delete(id);
			this.serviceSaveECL.deleteECL(id);
			this.serviceKomentar.deleteByIdDraft(2, id);
			
			int respact = this.serviceAktivitas.createAktivitas("LIST ECL",resp.getData(),method,url,id);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			logger.info(method+"|"+url+"|"+"delete data List Ecl");	
			return new ResponseEntity(new ResponObject(1, "delete List Ecl."),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/draft-ecl/{id}")
	public ResponseEntity<?> getDraftID(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id
			) {
		
		String method = "GET";
		String url = "/draft-ecl/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api get draft-ecl");
			
			DraftECL dt = serviceECL.findById(id);
			
			if (dt == null) {
				logger.info(method+"|"+url+"|"+"tidak ada data draft-ecl");
				return new ResponseEntity(new ResponList(2, "empty draft-ecl"),HttpStatus.OK);
			}
			
			List<User> dumyUser = new ArrayList<User>();
			List<DraftBucket> dumyDraftBucket = new ArrayList<DraftBucket>();
			List<InputModelFL> dumyInputModelFL = new ArrayList<InputModelFL>();
			List<Segment> dumySegment = new ArrayList<Segment>();
			
		
			DraftEclJoin rst = new DraftEclJoin();
				
			rst.setId_draft_ecl(dt.getId_draft_ecl());
			rst.setId_draft_bucket(dt.getId_draft_bucket());
			rst.setId_model(dt.getId_model());
				
			DraftBucket bucket = new DraftBucket();
			if(dumyDraftBucket.size()>0) {
				int loop = 0;
				for(DraftBucket a : dumyDraftBucket) {
					if(a.getId_draft_bucket() == dt.getId_draft_bucket()) {
						bucket = a;
						break;
					} else if (loop == dumyDraftBucket.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						bucket = this.serviceDraftBucket.findById(dt.getId_draft_bucket());
						dumyDraftBucket.add(bucket);
						break;
					}	
					loop++;
				}
			} else {
				bucket = this.serviceDraftBucket.findById(dt.getId_draft_bucket());
				dumyDraftBucket.add(bucket);
			}
			rst.setDraft_bucket(bucket);
				
			//SEGMEN
			Segment seg = new Segment();
			if (dt.getId_draft_bucket() != 0) {
				if(dumySegment.size()>0) {
					int loop = 0;
					for(Segment a : dumySegment) {
						if(a.getId_segment() == bucket.getId_segment()) {
							seg = a;
							break;
						} else if (loop == dumySegment.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							seg = this.serviceSegment.findById(bucket.getId_segment());
							dumySegment.add(seg);
							break;
						}	
						loop++;
					}
				} else {
					seg = this.serviceSegment.findById(bucket.getId_segment());
					dumySegment.add(seg);
				}
				rst.setSegment(seg);
			}
				
			InputModelFL model = new InputModelFL();
			if(dumyInputModelFL.size()>0) {
				int loop = 0;
				for(InputModelFL a : dumyInputModelFL) {
					if(a.getId_model() == dt.getId_model()) {
						model = a;
						break;
					} else if (loop == dumyInputModelFL.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						//UPDATE PARAMETER MODEL JUGA ID SEGMENT 
						model = this.serviceModel.findFLByIdModelSegment(dt.getId_model(), seg.getId_segment());
						dumyInputModelFL.add(model);
						break;
					}	
					loop++;
				}
			} else {
				//UPDATE PARAMETER MODEL JUGA ID SEGMENT
				model = this.serviceModel.findFLByIdModelSegment(dt.getId_model(), seg.getId_segment());
				dumyInputModelFL.add(model);
			}
			rst.setModel(model);
			rst.setId_regresi(dt.getId_regresi());
			rst.setMonth_forecast(dt.getMonth_forecast());
			rst.setPeriode(configGetDate.format(dt.getPeriode()));
			
			rst.setLgd(dt.getLgd());
			rst.setNormal(dt.getNormal());
			rst.setOptimis(dt.getOptimis());
			rst.setPesimis(dt.getPesimis());
			rst.setAverage_mape(dt.getAverage_mape());
			rst.setEcl_no_model(dt.getEcl_no_model());
			rst.setEcl_proporsional(dt.getEcl_proporsional());
			rst.setEcl_solver(dt.getEcl_solver());
			rst.setResult_flag_model(dt.getResult_flag_model());
			rst.setResult_saldo_exiting(dt.getResult_saldo_exiting());
			rst.setResult_saldo_selisih(dt.getResult_saldo_selisih());
			
			rst.setCreated_by(dt.getCreated_by());
			rst.setCreated_date(configGetDate.format(dt.getCreated_date()));
			
			User user = new User();
			if(dumyUser.size()>0) {
				int loop = 0;
				for(User a : dumyUser) {
					if(a.getId_user() == dt.getCreated_by()) {
						user = a;
						break;
					} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						user = this.serviceUser.findById(dt.getCreated_by());
						dumyUser.add(user);
						break;
					}	
					loop++;
				}
			} else {
				user = this.serviceUser.findById(dt.getCreated_by());
				dumyUser.add(user);
			}
			rst.setCreated_user(user);
			
			if (dt.getUpdated_by() != 0) {
				User useru = new User();
				if(dumyUser.size()>0) {
					int loop = 0;
					for(User a : dumyUser) {
						if(a.getId_user() == dt.getUpdated_by()) {
							useru = a;
							break;
						} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							useru = this.serviceUser.findById(dt.getUpdated_by());
							dumyUser.add(useru);
							break;
						}	
						loop++;
					}
				} else {
					useru = this.serviceUser.findById(dt.getUpdated_by());
					dumyUser.add(useru);
				}
				rst.setUpdated_by(dt.getUpdated_by());
				rst.setUpdated_user(useru);
				rst.setUpdated_date(configGetDate.format(dt.getUpdated_date()));
			}
				
			List<KomentarJoin> komjo = new ArrayList<KomentarJoin>();
			List<Komentar> komens = this.serviceKomentar.findByTypeIdTypeAll(2, dt.getId_draft_ecl());
			
			for (Komentar komen : komens) {
				KomentarJoin kojo = new KomentarJoin();
				kojo.setId_komentar_bucket(komen.getId_komentar_bucket());
				kojo.setId_type(komen.getId_type());
				kojo.setFlag(komen.getFlag());
				kojo.setId_bucket(komen.getId_bucket());
				kojo.setKomentar(komen.getKomentar());
				kojo.setCreated_by(komen.getCreated_by());
				kojo.setCreated_date(komen.getCreated_date());
				kojo.setUpdated_date(komen.getUpdated_date());
				
				if(dumyUser.size()>0) {
					int loopX = 0;
					for(User a : dumyUser) {
						if(a.getId_user() == komen.getCreated_by()) {
							user = a;
							break;
						} else if (loopX == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							user = this.serviceUser.findById(komen.getCreated_by());
							dumyUser.add(user);
							break;
						}	
						loopX++;
					}
				} else {
					user = this.serviceUser.findById(komen.getCreated_by());
					dumyUser.add(user);
				}
				kojo.setUser(user);
				komjo.add(kojo);
			}
			rst.setList_komentar(komjo);
				
			//UPDATE TAMBAHKAN DATA MAPE DAN ECL LIST.
			//SENGAJA DI KOMEN KARENA INI GET ALL
				
			MapeDraft mape_draft = new MapeDraft();
			MAPE saveMape = this.serviceSaveECL.findMape(dt.getId_draft_ecl());
			if (saveMape != null) {
				List<MAPELIST> saveMapeList = this.serviceSaveECL.findMapeList(dt.getId_draft_ecl());
				mape_draft.setDraft_bucket(null);
				mape_draft.setId_regresi(saveMape.getInput_regresi());
				mape_draft.setId_model(saveMape.getId_model());
				
				if (saveMape.getInput_regresi() == 1) {
					mape_draft.setInput_regresi("ODR");
				} else if (saveMape.getInput_regresi() == 2) {
					mape_draft.setInput_regresi("AVERAGE");
				} else if (saveMape.getInput_regresi() == 3) {
					mape_draft.setInput_regresi("SUM LOSS RATE");
				} else if (saveMape.getInput_regresi() == 4) {
					mape_draft.setInput_regresi("SUM DELTA LOSS RATE");
				}
				
				mape_draft.setModel(model);
				mape_draft.setPeriode_forecast(saveMape.getPeriode_forecast());
				
				List<String> list_head = new ArrayList<String>();
				JSONArray list_heads = new JSONArray(saveMape.getList_header());
//				logger.info(method+"|"+url+"|"+ "list header mape: "+saveMape.getList_header());
				for (int i = 0; i < list_heads.length(); i++) {
					list_head.add(list_heads.getString(i));
				}
				
				mape_draft.setHeader(list_head);
				mape_draft.setAverage_mape(saveMape.getAverage_mape());
				mape_draft.setThreshold_mape(saveMape.getThreshold_mape());
				mape_draft.setHasil_mape(saveMape.getHasil_mape());
				List<Mape> smls = new ArrayList<Mape>();
				for (MAPELIST sml : saveMapeList) {
					logger.info(method+"|"+url+"|"+ "list mape: "+sml.toString());
					Mape sm = new Mape();
					sm.setActual(sml.getActual());
					sm.setExpected(sml.getExpected());
					sm.setLogit_expected(sml.getLogit_expected());
					sm.setMape(sml.getMape());
					sm.setNilai_var1(sml.getNilai_var1());
					sm.setNilai_var2(sml.getNilai_var2());
					sm.setPeriode(sml.getPeriode());
					sm.setPeriode_bulan(sml.getPeriode_bulan());
					sm.setPeriode_tahun(sml.getPeriode_tahun());
					smls.add(sm);
				}
				mape_draft.setList_mape(smls);
				mape_draft.setPeriode_start(saveMape.getPeriode_start());
				mape_draft.setPeriode_end(saveMape.getPeriode_end());
				rst.setMape_draft(mape_draft);
			}
			
			List<EclBucket> ecl_list = new ArrayList<EclBucket>();
			List<ECL> ecls = this.serviceSaveECL.findEcl(dt.getId_draft_ecl());
			ECLSKENARIO skena = this.serviceSaveECL.findEclSkenario(dt.getId_draft_ecl());
			List<ECLLIST> eclst = this.serviceSaveECL.findEclList(dt.getId_draft_ecl());
			
			for (ECL ecl : ecls) {
				
				EclBucket ecl_bucket = new EclBucket();
				ecl_bucket.setId_model(ecl.getId_model());
				ecl_bucket.setPeriode_forecast(ecl.getPeriode_forecast());
				ecl_bucket.setPeriode(ecl.getPeriode());
				
				if (ecl.getId_model() != 0) {
					ecl_bucket.setModel(model);
					EclSkenario ecskena = new EclSkenario();
					ecskena.setStdev_var1(skena.getStdev_var1());
					ecskena.setStdev_var2(skena.getStdev_var2());
					ecskena.setNormal_var1(skena.getNormal_var1());
					ecskena.setNormal_var2(skena.getNormal_var2());
					ecskena.setNormal_probability(skena.getNormal_probability());
					ecskena.setOptimis_var1(skena.getOptimis_var1());
					ecskena.setOptimis_var2(skena.getOptimis_var2());
					ecskena.setOptimis_probability(skena.getOptimis_probability());
					ecskena.setPesimis_var1(skena.getPesimis_var1());
					ecskena.setPesimis_var2(skena.getPesimis_var2());
					ecskena.setPesimis_probability(skena.getPesimis_probability());
					ecskena.setWeigthed_var1(skena.getWeigthed_var1());
					ecskena.setWeigthed_var2(skena.getWeigthed_var2());
					ecl_bucket.setSkenario(ecskena);
				}
				
			
				ecl_bucket.setNo_model_sum_ead(ecl.getNo_model_sum_ead());
				ecl_bucket.setNo_model_sum_ecl(ecl.getNo_model_sum_ecl());
				ecl_bucket.setLogit_expected_psak(ecl.getLogit_expected_psak());
				ecl_bucket.setExpected_psak(ecl.getExpected_psak());
				ecl_bucket.setExpected(ecl.getExpected());
				ecl_bucket.setLast(ecl.getLast());
				ecl_bucket.setMev(ecl.getMev());
				
				ecl_bucket.setProporsional_sum_ead(ecl.getProporsional_sum_ead());
				ecl_bucket.setProporsional_sum_ecl(ecl.getProporsional_sum_ecl());
				ecl_bucket.setProporsional_sum_ecl_forward_looking(ecl.getProporsional_sum_ecl_forward_looking());
				
				ecl_bucket.setPd_forward_looking(ecl.getPd_forward_looking());
			
				ecl_bucket.setSolver_average_pd_base(ecl.getSolver_average_pd_base());
				ecl_bucket.setSolver_average_pd_forward_looking(ecl.getSolver_average_pd_forward_looking());
				ecl_bucket.setSolver_sum_ead(ecl.getSolver_sum_ead());
				ecl_bucket.setSolver_sum_ecl(ecl.getSolver_sum_ecl());
				ecl_bucket.setSolver_sum_ecl_forward_looking(ecl.getSolver_sum_ecl_forward_looking());
				
				ecl_bucket.setScaling(ecl.getScaling());
				ecl_bucket.setTarget(ecl.getTarget());
				
				if (ecl.getId_model() == 0) {
					
					List<Ecl> no_model_list = new ArrayList<Ecl>();
					for (ECLLIST ecld : eclst) {
						if (ecld.getId_type() != 0) {
							continue;
						}
						Ecl pecl = new Ecl();
						pecl.setBucket(ecld.getBucket());
						pecl.setPd_base(ecld.getPd_base());
						pecl.setMev_effect(ecld.getMev_effect());
						pecl.setLogit_pd(ecld.getLogit_pd());
						pecl.setPd_forward_looking(ecld.getPd_forward_looking());
						pecl.setLgd(ecld.getLgd());
						pecl.setEad(ecld.getEad());
						pecl.setEcl_base(ecld.getEcl_base());
						pecl.setEcl_forward_looking(ecld.getEcl_forward_looking());
						no_model_list.add(pecl);
					}
					ecl_bucket.setNo_model_list(no_model_list);
					
				} else {
					
					List<Ecl> proporsional_list = new ArrayList<Ecl>();
					List<Ecl> solver_list = new ArrayList<Ecl>();
					for (ECLLIST ecle : eclst) {
						if (ecle.getId_type() == 0) {
							continue;
						}
						
						Ecl pecl = new Ecl();
						pecl.setBucket(ecle.getBucket());
						pecl.setPd_base(ecle.getPd_base());
						pecl.setMev_effect(ecle.getMev_effect());
						pecl.setLogit_pd(ecle.getLogit_pd());
						pecl.setPd_forward_looking(ecle.getPd_forward_looking());
						pecl.setLgd(ecle.getLgd());
						pecl.setEad(ecle.getEad());
						pecl.setEcl_base(ecle.getEcl_base());
						pecl.setEcl_forward_looking(ecle.getEcl_forward_looking());
						if (ecle.getId_type() == 1) {
							proporsional_list.add(pecl);
						}
						if (ecle.getId_type() == 2) {
							solver_list.add(pecl);
						}
					}
					ecl_bucket.setProporsional_list(proporsional_list);
					ecl_bucket.setSolver_list(solver_list);
				}
				ecl_list.add(ecl_bucket);
			}
			rst.setEcl_list(ecl_list);
				
			logger.info(method+"|"+url+"|"+"data Draft ECL");
			return new ResponseEntity(new ResponObject(1, "Draft ECL",rst),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public List<DraftEclJoin> getDraftEclJoin(List<DraftECL> data){
		
		List<DraftEclJoin> result = new ArrayList<DraftEclJoin>();
		List<User> dumyUser = new ArrayList<User>();
		List<DraftBucket> dumyDraftBucket = new ArrayList<DraftBucket>();
		List<InputModelFL> dumyInputModelFL = new ArrayList<InputModelFL>();
		List<Segment> dumySegment = new ArrayList<Segment>();
		
		for (DraftECL dt : data) {
			DraftEclJoin rst = new DraftEclJoin();
			
			rst.setId_draft_ecl(dt.getId_draft_ecl());
			rst.setId_draft_bucket(dt.getId_draft_bucket());
			rst.setId_model(dt.getId_model());
			
			DraftBucket bucket = new DraftBucket();
			if(dumyDraftBucket.size()>0) {
				int loop = 0;
				for(DraftBucket a : dumyDraftBucket) {
					if(a.getId_draft_bucket() == dt.getId_draft_bucket()) {
						bucket = a;
						break;
					} else if (loop == dumyDraftBucket.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						bucket = this.serviceDraftBucket.findById(dt.getId_draft_bucket());
						dumyDraftBucket.add(bucket);
						break;
					}	
					loop++;
				}
			} else {
				bucket = this.serviceDraftBucket.findById(dt.getId_draft_bucket());
				dumyDraftBucket.add(bucket);
			}
			rst.setDraft_bucket(bucket);
			
			//SEGMEN
			Segment seg = new Segment();
			if (dt.getId_draft_bucket() != 0) {
				if(dumySegment.size()>0) {
					int loop = 0;
					for(Segment a : dumySegment) {
						if(a.getId_segment() == bucket.getId_segment()) {
							seg = a;
							break;
						} else if (loop == dumySegment.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							seg = this.serviceSegment.findById(bucket.getId_segment());
							dumySegment.add(seg);
							break;
						}	
						loop++;
					}
				} else {
					seg = this.serviceSegment.findById(bucket.getId_segment());
					dumySegment.add(seg);
				}
				rst.setSegment(seg);
			}
			
			InputModelFL model = new InputModelFL();
			if (dt.getId_model() != 0) {
				if(dumyInputModelFL.size()>0) {
					int loop = 0;
					for(InputModelFL a : dumyInputModelFL) {
						if(a.getId_model() == dt.getId_model()) {
							model = a;
							break;
						} else if (loop == dumyInputModelFL.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							//UPDATE PARAMETER MODEL JUGA ID SEGMENT 
							model = this.serviceModel.findFLByIdModelSegment(dt.getId_model(), seg.getId_segment());
							dumyInputModelFL.add(model);
							break;
						}	
						loop++;
					}
				} else {
					//UPDATE PARAMETER MODEL JUGA ID SEGMENT
					model = this.serviceModel.findFLByIdModelSegment(dt.getId_model(), seg.getId_segment());
					dumyInputModelFL.add(model);
				}
			}
			
			rst.setModel(model);
			rst.setId_regresi(dt.getId_regresi());
			rst.setMonth_forecast(dt.getMonth_forecast());
			rst.setPeriode(configGetDate.format(dt.getPeriode()));
			
			rst.setLgd(dt.getLgd());
			rst.setNormal(dt.getNormal());
			rst.setOptimis(dt.getOptimis());
			rst.setPesimis(dt.getPesimis());
			rst.setAverage_mape(dt.getAverage_mape());
			rst.setEcl_no_model(dt.getEcl_no_model());
			rst.setEcl_proporsional(dt.getEcl_proporsional());
			rst.setEcl_solver(dt.getEcl_solver());
			rst.setResult_flag_model(dt.getResult_flag_model());
			rst.setResult_saldo_exiting(dt.getResult_saldo_exiting());
			rst.setResult_saldo_selisih(dt.getResult_saldo_selisih());
			
			rst.setCreated_by(dt.getCreated_by());
			rst.setCreated_date(configGetDate.format(dt.getCreated_date()));
			
			User user = new User();
			if(dumyUser.size()>0) {
				int loop = 0;
				for(User a : dumyUser) {
					if(a.getId_user() == dt.getCreated_by()) {
						user = a;
						break;
					} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						user = this.serviceUser.findById(dt.getCreated_by());
						dumyUser.add(user);
						break;
					}	
					loop++;
				}
			} else {
				user = this.serviceUser.findById(dt.getCreated_by());
				dumyUser.add(user);
			}
			rst.setCreated_user(user);
			
			if (dt.getUpdated_by() != 0) {
				User useru = new User();
				if(dumyUser.size()>0) {
					int loop = 0;
					for(User a : dumyUser) {
						if(a.getId_user() == dt.getUpdated_by()) {
							useru = a;
							break;
						} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							useru = this.serviceUser.findById(dt.getUpdated_by());
							dumyUser.add(useru);
							break;
						}	
						loop++;
					}
				} else {
					useru = this.serviceUser.findById(dt.getUpdated_by());
					dumyUser.add(useru);
				}
				rst.setUpdated_by(dt.getUpdated_by());
				rst.setUpdated_user(useru);
				rst.setUpdated_date(configGetDate.format(dt.getUpdated_date()));
			}
			
			List<KomentarJoin> komjo = new ArrayList<KomentarJoin>();
			List<Komentar> komens = this.serviceKomentar.findByTypeIdTypeAll(2, dt.getId_draft_ecl());
			
			for (Komentar komen : komens) {
				KomentarJoin kojo = new KomentarJoin();
				kojo.setId_komentar_bucket(komen.getId_komentar_bucket());
				kojo.setId_type(komen.getId_type());
				kojo.setFlag(komen.getFlag());
				kojo.setId_bucket(komen.getId_bucket());
				kojo.setKomentar(komen.getKomentar());
				kojo.setCreated_by(komen.getCreated_by());
				kojo.setCreated_date(komen.getCreated_date());
				kojo.setUpdated_date(komen.getUpdated_date());
				
				if(dumyUser.size()>0) {
					int loopX = 0;
					for(User a : dumyUser) {
						if(a.getId_user() == komen.getCreated_by()) {
							user = a;
							break;
						} else if (loopX == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							user = this.serviceUser.findById(komen.getCreated_by());
							dumyUser.add(user);
							break;
						}	
						loopX++;
					}
				} else {
					user = this.serviceUser.findById(komen.getCreated_by());
					dumyUser.add(user);
				}
				kojo.setUser(user);
				komjo.add(kojo);
			}
			rst.setList_komentar(komjo);
			
			//UPDATE TAMBAHKAN DATA MAPE DAN ECL LIST.
			//SENGAJA DI KOMEN KARENA INI GET ALL
			
//			MapeDraft mape_draft = new MapeDraft();
//			MAPE saveMape = this.serviceSaveECL.findMape(dt.getId_draft_ecl());
//			List<MAPELIST> saveMapeList = this.serviceSaveECL.findMapeList(dt.getId_draft_ecl());
//			mape_draft.setDraft_bucket(null);
//			mape_draft.setId_regresi(saveMape.getInput_regresi());
//			mape_draft.setId_model(saveMape.getId_model());
//			
//			if (saveMape.getInput_regresi() == 1) {
//				mape_draft.setInput_regresi("ODR");
//			} else if (saveMape.getInput_regresi() == 2) {
//				mape_draft.setInput_regresi("AVERAGE");
//			} else if (saveMape.getInput_regresi() == 3) {
//				mape_draft.setInput_regresi("SUM LOSS RATE");
//			} else if (saveMape.getInput_regresi() == 4) {
//				mape_draft.setInput_regresi("SUM DELTA LOSS RATE");
//			}
//			
//			InputModelFL modelx = this.serviceInputFL.findFLById(saveMape.getId_model());
//			mape_draft.setModel(modelx);
//			mape_draft.setPeriode_forecast(saveMape.getPeriode_forecast());
//			
//			List<String> list_head = new ArrayList<String>();
//			JSONArray list_heads = new JSONArray(saveMape.getList_header());
//			
//			for (int i = 0; i < list_heads.size(); i++) {
//				list_head.add(list_heads.getString(i));
//			}
//			
//			mape_draft.setHeader(list_head);
//			mape_draft.setAverage_mape(saveMape.getAverage_mape());
//			mape_draft.setThreshold_mape(saveMape.getThreshold_mape());
//			mape_draft.setHasil_mape(saveMape.getHasil_mape());
//			List<Mape> smls = new ArrayList<Mape>();
//			for (MAPELIST sml : saveMapeList) {
//				Mape sm = new Mape();
//				sm.setActual(sml.getActual());
//				sm.setExpected(sml.getExpected());
//				sm.setLogit_expected(sml.getLogit_expected());
//				sm.setMape(sml.getMape());
//				sm.setNilai_var1(sml.getNilai_var1());
//				sm.setNilai_var2(sml.getNilai_var2());
//				sm.setPeriode(sml.getPeriode());
//				sm.setPeriode_bulan(sml.getPeriode_bulan());
//				sm.setPeriode_tahun(sml.getPeriode_tahun());
//				smls.add(sm);
//			}
//			mape_draft.setList_mape(smls);
//			mape_draft.setPeriode_start(saveMape.getPeriode_start());
//			mape_draft.setPeriode_end(saveMape.getPeriode_end());
//			rst.setMape_draft(mape_draft);
//			
//			List<EclBucket> ecl_list = new ArrayList<EclBucket>();
//			
//			List<ECL> ecls = this.serviceSaveECL.findEcl(dt.getId_draft_ecl());
//			ECLSKENARIO skena = this.serviceSaveECL.findEclSkenario(dt.getId_draft_ecl());
//			List<ECLLIST> eclst = this.serviceSaveECL.findEclList(dt.getId_draft_ecl());
//			
//			for (ECL ecl : ecls) {
//				
//				EclBucket ecl_bucket = new EclBucket();
//				ecl_bucket.setId_model(ecl.getId_model());
//				ecl_bucket.setPeriode_forecast(ecl.getPeriode_forecast());
//				if (ecl.getId_model() != 0) {	
//					ecl_bucket.setModel(modelx);
//				}
//				ecl_bucket.setModel(modelx);
//				ecl_bucket.setPeriode(ecl.getPeriode());
//				
//				if (ecl.getId_model() != 0) {
//					EclSkenario ecskena = new EclSkenario();
//					ecskena.setStdev_var1(skena.getStdev_var1());
//					ecskena.setStdev_var2(skena.getStdev_var2());
//					ecskena.setNormal_var1(skena.getNormal_var1());
//					ecskena.setNormal_var2(skena.getNormal_var2());
//					ecskena.setNormal_probability(skena.getNormal_probability());
//					ecskena.setOptimis_var1(skena.getOptimis_var1());
//					ecskena.setOptimis_var2(skena.getOptimis_var2());
//					ecskena.setOptimis_probability(skena.getOptimis_probability());
//					ecskena.setPesimis_var1(skena.getPesimis_var1());
//					ecskena.setPesimis_var2(skena.getPesimis_var2());
//					ecskena.setPesimis_probability(skena.getPesimis_probability());
//					ecskena.setWeigthed_var1(skena.getWeigthed_var1());
//					ecskena.setWeigthed_var2(skena.getWeigthed_var2());
//					ecl_bucket.setSkenario(ecskena);
//				}
//				
//			
//				ecl_bucket.setNo_model_sum_ead(ecl.getNo_model_sum_ead());
//				ecl_bucket.setNo_model_sum_ecl(ecl.getNo_model_sum_ecl());
//				ecl_bucket.setLogit_expected_psak(ecl.getLogit_expected_psak());
//				ecl_bucket.setExpected_psak(ecl.getExpected_psak());
//				ecl_bucket.setExpected(ecl.getExpected());
//				ecl_bucket.setLast(ecl.getLast());
//				ecl_bucket.setMev(ecl.getMev());
//				
//				ecl_bucket.setProporsional_sum_ead(ecl.getProporsional_sum_ead());
//				ecl_bucket.setProporsional_sum_ecl_forward_looking(ecl.getProporsional_sum_ecl_forward_looking());
//				
//				ecl_bucket.setPd_forward_looking(ecl.getPd_forward_looking());
//			
//				ecl_bucket.setSolver_average_pd_base(ecl.getSolver_average_pd_base());
//				ecl_bucket.setSolver_average_pd_forward_looking(ecl.getSolver_average_pd_forward_looking());
//				ecl_bucket.setSolver_sum_ead(ecl.getSolver_sum_ead());
//				ecl_bucket.setSolver_sum_ecl(ecl.getSolver_sum_ecl());
//				ecl_bucket.setSolver_sum_ecl_forward_looking(ecl.getSolver_sum_ecl_forward_looking());
//				
//				if (ecl.getId_model() == 0) {
//					
//					List<Ecl> no_model_list = new ArrayList<Ecl>();
//					for (ECLLIST ecld : eclst) {
//						if (ecld.getId_type() != 0) {
//							continue;
//						}
//						Ecl pecl = new Ecl();
//						pecl.setBucket(ecld.getBucket());
//						pecl.setPd_base(ecld.getPd_base());
//						pecl.setMev_effect(ecld.getMev_effect());
//						pecl.setLogit_pd(ecld.getLogit_pd());
//						pecl.setPd_forward_looking(ecld.getPd_forward_looking());
//						pecl.setLgd(ecld.getLgd());
//						pecl.setEad(ecld.getEad());
//						pecl.setEcl_base(ecld.getEcl_base());
//						pecl.setEcl_forward_looking(ecld.getEcl_forward_looking());
//						no_model_list.add(pecl);
//					}
//					ecl_bucket.setNo_model_list(no_model_list);
//					
//				} else {
//					
//					List<Ecl> proporsional_list = new ArrayList<Ecl>();
//					List<Ecl> solver_list = new ArrayList<Ecl>();
//					for (ECLLIST ecle : eclst) {
//						if (ecle.getId_type() == 0) {
//							continue;
//						}
//						
//						Ecl pecl = new Ecl();
//						pecl.setBucket(ecle.getBucket());
//						pecl.setPd_base(ecle.getPd_base());
//						pecl.setMev_effect(ecle.getMev_effect());
//						pecl.setLogit_pd(ecle.getLogit_pd());
//						pecl.setPd_forward_looking(ecle.getPd_forward_looking());
//						pecl.setLgd(ecle.getLgd());
//						pecl.setEad(ecle.getEad());
//						pecl.setEcl_base(ecle.getEcl_base());
//						pecl.setEcl_forward_looking(ecle.getEcl_forward_looking());
//						if (ecle.getId_type() == 1) {
//							proporsional_list.add(pecl);
//						}
//						if (ecle.getId_type() == 2) {
//							solver_list.add(pecl);
//						}
//					}
//					ecl_bucket.setProporsional_list(proporsional_list);
//					ecl_bucket.setSolver_list(solver_list);
//				}
//				ecl_list.add(ecl_bucket);
//			}
//			rst.setEcl_list(ecl_list);
			
			result.add(rst);
		}
		return result;
	}	

}
