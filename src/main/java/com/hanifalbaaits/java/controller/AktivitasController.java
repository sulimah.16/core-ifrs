package com.hanifalbaaits.java.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hanifalbaaits.java.modelDB.Aktivitas;
import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelRespon.AktivitasJoin;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.service.AktivitasService;
import com.hanifalbaaits.java.service.UserService;

@RestController
@RequestMapping(value="core-ifrs")
public class AktivitasController {
	
	private Logger logger = LoggerFactory.getLogger(AktivitasController.class);
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasService serviceAktivitas;
	
	@Autowired
	UserService serviceUser;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/activity/admin")
	public ResponseEntity<?> getLastAdmin(@RequestHeader("Authorization") String Authorization
			) {
		
		String method = "GET";
		String url = "/activity/admin";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info("hit api master activity");
			List<Aktivitas> dt = serviceAktivitas.findLastAdmin();
			
			if (dt.size() == 0) {
				logger.info("tidak ada data Aktivitas");
				return new ResponseEntity(new ResponList(1, "empty Aktivitas",dt),HttpStatus.OK);
			}
			List<AktivitasJoin> data = joinActivity(dt);
			logger.info("success List Aktivitas");
			return new ResponseEntity(new ResponList(1, "List Aktivitas",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/activity/user/{id}")
	public ResponseEntity<?> getAktivitasMember(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id
			) {
	
		String method = "GET";
		String url = "/activity/user/"+id;
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info("hit api get master activity");
			List<Aktivitas> dt = serviceAktivitas.findLastByIduser(id);
			
			if (dt.size() == 0) {
				logger.info("tidak ada data Aktivitas");
				return new ResponseEntity(new ResponList(1, "empty Aktivitas",dt),HttpStatus.OK);
			}
			List<AktivitasJoin> data = joinActivity(dt);
			logger.info("success List Aktivitas");
			return new ResponseEntity(new ResponList(1, "List Aktivitas",data),HttpStatus.OK);
			
			
		} catch (Exception e) {
			// TODO: handle exception
			
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(value="/activity/search")
	public ResponseEntity<?> searchActivity(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		
		String method = "POST";
		String url = "/activity/search";
		
		logger.info(method+"|"+url+"|"+"Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
		if (body.get("type_role")==null || body.get("type_role").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "type_role null!"),HttpStatus.BAD_REQUEST);
		if (body.get("id")==null || body.get("id").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id null!"),HttpStatus.BAD_REQUEST);
		if (body.get("start_date")==null || body.get("start_date").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "start_date null"),HttpStatus.BAD_REQUEST);
		if (body.get("end_date")==null || body.get("end_date").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "end_date null"),HttpStatus.BAD_REQUEST);
		
		
		try {
			logger.info( "hit api get Aktivitas search");

			String type_role = body.get("type_role"); //admin //customer //member
			String id = body.get("id");
			String start_date = body.get("start_date").trim();
			String end_date = body.get("end_date").trim();
			
			logger.info( "type role : "+type_role);
			logger.info( "id : "+id);
			logger.info( "start_date : "+start_date);
			logger.info( "end_date : "+end_date);
		
			List<Aktivitas> list = new ArrayList<Aktivitas>();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date1 = df.parse(start_date+" 00:00");
			Date date2 = df.parse(end_date+" 23:59");
			
			
			if (type_role.contentEquals("admin")) {
				list = serviceAktivitas.findByAdmin(date1, date2);
			}  else {
				list = serviceAktivitas.findByIdUser(Integer.parseInt(id),date1, date2);
			}
			
			if (list.size() == 0) {
				logger.info("tidak ada data Aktivitas");
				return new ResponseEntity(new ResponList(1, "empty Aktivitas",list),HttpStatus.OK);
			}
			List<AktivitasJoin> data = joinActivity(list);
			logger.info("success List Aktivitas");
			return new ResponseEntity(new ResponList(1, "List Aktivitas",data),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public List<AktivitasJoin> joinActivity(List<Aktivitas> data){
		
		List<AktivitasJoin> resp = new ArrayList<AktivitasJoin>();
		List<User> dumy_mem = new ArrayList<User>();
		for (Aktivitas act : data) {
			
			AktivitasJoin asj = new AktivitasJoin();
			User mem = new User();
			
			if(dumy_mem.size()>0) {
				int loop = 0;
				for(User a : dumy_mem) {
					if(a.getId_user() == act.getId_user()) {
						mem = a;
						break;
					} else if (loop == dumy_mem.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						mem = this.serviceUser.findById(act.getId_user());
						dumy_mem.add(mem);
						break;
					}	
					loop++;
				}
			} else {
				mem = this.serviceUser.findById(act.getId_user());
				dumy_mem.add(mem);
			}
			
			asj.setUser(mem);
			asj.setId_aktivitas(act.getId_aktivitas());
			asj.setId_user(act.getId_user());
			asj.setCaption(act.getCaption());
			asj.setDevice(act.getDevice());
			asj.setIp(act.getIp());
			asj.setKeterangan(act.getKeterangan());
			asj.setMethod_path(act.getMethod_path());
//			Date msk = asj.getTanggal_activity();
//			msk.setHours(msk.getHours()+7);
			asj.setTanggal_aktivitas(configGetDate.format(act.getTanggal_aktivitas()));
			resp.add(asj);
		}
		return resp;
	}
	
	public int createAktivitas(String caption,User mem, String method, String path, int relation) {
		int resact = 1;
		
		Date today = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy HH:mm:ss");  
        String strDate = dateFormat.format(today);
        
		String desc = mem.getNama();
		if (method.contains("POST")) {
			desc += " telah menambahkan data "+caption.toLowerCase()+" pada tanggal "+strDate+". ID:"+relation;
		} else if (method.contains("PUT")) {
			desc += " telah mengubah data "+caption.toLowerCase()+" pada tanggal "+strDate+". ID:"+relation;
		} else if (method.contains("DELETE")) {
			desc += " telah menghapus data "+caption.toLowerCase()+" pada tanggal "+strDate+". ID:"+relation;
		} else {
			desc += " telah melakukan sesuatu pada "+caption.toLowerCase()+" pada tanggal "+strDate+". ID:"+relation;
		}
		
		Aktivitas last = this.serviceAktivitas.findLastLoginByIdUser(mem.getId_user());
		String device = "";
		String ip = ""; 		
		if (last != null) {
			device = last.getDevice();
			ip = last.getIp();
		}
		
		Aktivitas audit = new Aktivitas();
		audit.setCaption(caption);
		audit.setId_user(mem.getId_user());
		audit.setKeterangan(desc);
		audit.setDevice(device);
		audit.setIp(ip);
		audit.setMethod_path(method+" "+path);
		audit.setTanggal_aktivitas(new Date());
		this.serviceAktivitas.save(audit);
		return resact;
	}
	
	public int createAktivitas(String caption,User mem, String ket, String method, String path) {
		int resact = 1;
		Date today = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy HH:mm:ss");
        String strDate = dateFormat.format(today);
        
		String desc = mem.getNama();
		if (caption.contains("LOGIN")) {
			desc += " telah login pada tanggal "+strDate;
		} else if (caption.contains("CHANGEPASSWORD")) {
			desc += " telah mengubah password pada tanggal "+strDate;
		} else if (caption.contains("CHANGEEMAIL")) {
			desc += " telah mengubah email pada tanggal "+strDate;
		} else if (caption.contains("REGISTER")) {
			desc += " telah register baru pada tanggal "+strDate;
		} else {
			desc += " telah melakukan sesuatu pada "+caption.toLowerCase()+" pada tanggal "+strDate;
		}
		
		String device = "";
		String ip = "";
		if (caption.contains("LOGIN")) {
			device = ket.split("\\|")[0];
			ip = ket.split("\\|")[1];
		} else if (caption.contains("REGISTER")) {
			device = "";
			ip = "";
		} else  {
			Aktivitas last = this.serviceAktivitas.findLastLoginByIdUser(mem.getId_user());
			device = last.getDevice();
			ip = last.getIp();
		}
		
		Aktivitas audit = new Aktivitas();
		audit.setCaption(caption);
		audit.setId_user(mem.getId_user());
		audit.setKeterangan(desc);
		audit.setDevice(device);
		audit.setIp(ip);
		audit.setMethod_path(method+" "+path);
		audit.setTanggal_aktivitas(new Date());
		this.serviceAktivitas.save(audit);
		return resact;
	}
}
