package com.hanifalbaaits.java.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.hanifalbaaits.java.modelDB.VasicekAsetList;
import com.hanifalbaaits.java.modelDB.VasicekAset;
import com.hanifalbaaits.java.modelDB.VasicekAsetDraft;
import com.hanifalbaaits.java.modelDB.VasicekBank;
import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.UploadFile;
import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelRespon.AsetListJoin;
import com.hanifalbaaits.java.modelRespon.ResponList;
import com.hanifalbaaits.java.modelRespon.ResponMiddleware;
import com.hanifalbaaits.java.modelRespon.ResponObject;
import com.hanifalbaaits.java.modelRespon.VasicekBankJoin;
import com.hanifalbaaits.java.modelUtils.KoreksiImport;
import com.hanifalbaaits.java.modelUtils.KoreksiPesan;
import com.hanifalbaaits.java.service.VasicekAsetListService;
import com.hanifalbaaits.java.service.VasicekAsetService;
import com.hanifalbaaits.java.service.VasicekBankService;
import com.hanifalbaaits.java.service.VasicekRatingService;
import com.hanifalbaaits.java.service.UploadFileService;
import com.hanifalbaaits.java.service.UploadService;
import com.hanifalbaaits.java.service.UserService;
import com.hanifalbaaits.java.service.VasicekAsetDraftService;

@RestController
@RequestMapping(value="core-ifrs")
public class VasicekAsetListCon {

	private Logger logger = LoggerFactory.getLogger(VasicekAsetListCon.class);
	
	@Value("${file.path_input}")
	String path_file;
	
	@Autowired
	AuthenticateUser auth;
	
	@Autowired
	AktivitasController serviceAktivitas;
	
	@Autowired
	UserService serviceUser;
	
	@Autowired
	VasicekAsetListService serviceAset;
	
	@Autowired
    UploadService uploadService;
	
	@Autowired
	UploadFileService serviceUpload;
	
	@Autowired
	VasicekAsetService serviceTypeAset;
	
	@Autowired
	VasicekBankService serviceBank;
	
	@Autowired
	VasicekRatingService serviceRating;
	
	@Autowired
	VasicekAsetDraftService serviceDraft;
	
	SimpleDateFormat configInputDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	SimpleDateFormat configGetDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/aset-list")
	public ResponseEntity<?> getInputData(@RequestHeader("Authorization") String Authorization) throws Exception {
		
		String method = "GET";
		String url = "/aset-list";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			List<VasicekAsetList> data = serviceAset.findAll();
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponList(1, "empty input",data),HttpStatus.OK);
			}
			
			List<AsetListJoin> dataJoin = getAsetListJoin(data);
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponList(1, "List Input",dataJoin),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/aset-list/draft/{id}")
	public ResponseEntity<?> getInputDataByDraft(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) throws Exception {
		
		String method = "GET";
		String url = "/aset-list/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			List<VasicekAsetList> data = serviceAset.findByIdDraft(id);
			
			if (data.size() == 0) {
				logger.info(method+"|"+url+"|"+"tidak ada data input");
				return new ResponseEntity(new ResponList(1, "empty input",data),HttpStatus.OK);
			}
			
			List<AsetListJoin> dataJoin = getAsetListJoin(data);
			logger.info(method+"|"+url+"|"+"data input");
			return new ResponseEntity(new ResponList(1, "List Input",dataJoin),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(value="/aset-list")
	public ResponseEntity<?> createInputData(@RequestHeader("Authorization") String Authorization,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "POST";
		String url = "/aset-list";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api add aset-list");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_jenis")==null || body.get("id_jenis").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_jenis null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_bank")==null || body.get("id_bank").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_bank null!"),HttpStatus.BAD_REQUEST);
			if (body.get("no_rekening")==null || body.get("no_rekening").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "no_rekening null!"),HttpStatus.BAD_REQUEST);
			if (body.get("mata_uang")==null || body.get("mata_uang").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "mata_uang null!"),HttpStatus.BAD_REQUEST);
			if (body.get("suku_bunga")==null || body.get("suku_bunga").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "suku_bunga null!"),HttpStatus.BAD_REQUEST);
			if (body.get("tgl_jatuh_tempo")==null || body.get("tgl_jatuh_tempo").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "tgl_jatuh_tempo null!"),HttpStatus.BAD_REQUEST);
			if (body.get("saldo")==null || body.get("saldo").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "saldo null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "id_jenis :"+body.get("id_jenis"));
			logger.info(method+"|"+url+"|"+ "id_bank :"+body.get("id_bank"));
			logger.info(method+"|"+url+"|"+ "no_rekening :"+body.get("no_rekening"));
			logger.info(method+"|"+url+"|"+ "mata_uang :"+body.get("mata_uang"));
			logger.info(method+"|"+url+"|"+ "suku_bunga :"+body.get("suku_bunga"));
			logger.info(method+"|"+url+"|"+ "tgl_jatuh_tempo :"+body.get("tgl_jatuh_tempo"));
			logger.info(method+"|"+url+"|"+ "saldo :"+body.get("saldo"));
			logger.info(method+"|"+url+"|"+ "draft :"+body.get("id_draft"));
			
			VasicekAset ch_aset = this.serviceTypeAset.findById(Integer.parseInt(body.get("id_jenis")));
			if (ch_aset == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada ID Jenis Aset tersebut"),HttpStatus.BAD_REQUEST);
			}
			VasicekBank ch_bank = this.serviceBank.findById(Integer.parseInt(body.get("id_bank")));
			if (ch_bank == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada ID Bank tersebut"),HttpStatus.BAD_REQUEST);
			}
			
			Date tgl_jatuh_tempo = null;
			double saldo = 0.0;
			double bunga = 0.0;
			
			try {
				
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				tgl_jatuh_tempo = df.parse(body.get("tgl_jatuh_tempo"));
				
				logger.info(method+"|"+url+"|"+ "tgl_jatuh_tempo :"+tgl_jatuh_tempo.toString());
				saldo = Double.parseDouble(body.get("saldo"));
				bunga = Double.parseDouble(body.get("suku_bunga")) / 100;
				
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);
				return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			Date today = new Date();
			VasicekAsetDraft nmember = new VasicekAsetDraft();
			
			if (body.get("id_draft") == null || body.get("id_draft").isEmpty()) {
				
				VasicekAsetDraft check = serviceDraft.findLast();
				String kode = "DRA0001";
				if(check != null) {
					logger.info(method+"|"+url+"|"+ "draft terakhir : "+check.getKode_draft());
					String kd = check.getKode_draft().substring(3);
					int codenow = Integer.parseInt(kd) + 1;	
					int lcdn = String.valueOf(codenow).length();
					if(lcdn == 1) {kode = "DRA000"+String.valueOf(codenow);} 
					else if (lcdn ==2) { kode = "DRA00"+String.valueOf(codenow);}
					else if (lcdn ==3) { kode = "DRA0"+String.valueOf(codenow);}
					else { kode = "DRA"+String.valueOf(codenow);}
				}
				
				String nm_draft = body.get("nama_draft");
				if (body.get("nama_draft") == null || body.get("nama_draft").isEmpty()) {
					nm_draft = kode;
				}
				
				User created = resp.getData();
				nmember.setCreated_by(created.getId_user());
				nmember.setCreated_date(today);
				nmember.setKode_draft(kode);
				nmember.setNama_draft(nm_draft);
				this.serviceDraft.save(nmember);
				
				logger.info(method+"|"+url+"|"+"success save draft");
				int respact = this.serviceAktivitas.createAktivitas("DRAFT ASET",created,method,url,nmember.getId_draft_vasicek());
				logger.info(method+"|"+url+"|"+"respact: "+respact);
				
			} else {
				nmember = this.serviceDraft.findById(Integer.parseInt(body.get("id_draft")));
			}
			
			if (nmember == null || nmember.getKode_draft() == null || nmember.getKode_draft().isEmpty()) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada ID Draft tersebut"),HttpStatus.BAD_REQUEST);
			}
					
			VasicekAsetList input = new VasicekAsetList();
			
			User created = resp.getData();
			input.setCreated_by(created.getId_user());
			input.setUpdated_by(created.getId_user());
			input.setCreated_date(today);
			input.setUpdated_date(today);
					
			input.setId_jenis(ch_aset.getId_vas_aset());
			input.setId_draft_vasicek(nmember.getId_draft_vasicek());
			input.setId_bank(ch_bank.getId_vas_bank());
			input.setTgl_jatuh_tempo(tgl_jatuh_tempo);
			input.setNo_rekening(body.get("no_rekening"));
			input.setMata_uang(body.get("mata_uang"));
			input.setSuku_bunga(bunga);
			input.setSaldo(saldo);
			this.serviceAset.save(input);
			
			logger.info(method+"|"+url+"|"+"success save list aset");
			int respact = this.serviceAktivitas.createAktivitas("LIST ASET",created,method,url,input.getId_list_aset());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes add List Aset",input),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PutMapping(value="/aset-list/{id}")
	public ResponseEntity<?> updateInputData(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id,
			@RequestBody Map<String,String> body) throws Exception {
		
		String method = "PUT";
		String url = "/aset-list/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+ "hit api put aset-list");
			
			if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_jenis")==null || body.get("id_jenis").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_jenis null!"),HttpStatus.BAD_REQUEST);
			if (body.get("id_bank")==null || body.get("id_bank").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "id_bank null!"),HttpStatus.BAD_REQUEST);
			if (body.get("no_rekening")==null || body.get("no_rekening").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "no_rekening null!"),HttpStatus.BAD_REQUEST);
			if (body.get("mata_uang")==null || body.get("mata_uang").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "mata_uang null!"),HttpStatus.BAD_REQUEST);
			if (body.get("suku_bunga")==null || body.get("suku_bunga").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "suku_bunga null!"),HttpStatus.BAD_REQUEST);
			if (body.get("tgl_jatuh_tempo")==null || body.get("tgl_jatuh_tempo").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "tgl_jatuh_tempo null!"),HttpStatus.BAD_REQUEST);
			if (body.get("saldo")==null || body.get("saldo").toString().isEmpty()) return new ResponseEntity(new ResponObject(2, "saldo null!"),HttpStatus.BAD_REQUEST);
			
			logger.info(method+"|"+url+"|"+ "id_jenis :"+body.get("id_jenis"));
			logger.info(method+"|"+url+"|"+ "id_bank :"+body.get("id_bank"));
			logger.info(method+"|"+url+"|"+ "no_rekening :"+body.get("no_rekening"));
			logger.info(method+"|"+url+"|"+ "mata_uang :"+body.get("mata_uang"));
			logger.info(method+"|"+url+"|"+ "suku_bunga :"+body.get("suku_bunga"));
			logger.info(method+"|"+url+"|"+ "tgl_jatuh_tempo :"+body.get("tgl_jatuh_tempo"));
			logger.info(method+"|"+url+"|"+ "saldo :"+body.get("saldo"));
			
			VasicekAset ch_aset = this.serviceTypeAset.findById(Integer.parseInt(body.get("id_jenis")));
			if (ch_aset == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada ID Jenis Aset tersebut"),HttpStatus.BAD_REQUEST);
			}
			VasicekBank ch_bank = this.serviceBank.findById(Integer.parseInt(body.get("id_bank")));
			if (ch_bank == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada ID Bank tersebut"),HttpStatus.BAD_REQUEST);
			}
			
			Date tgl_jatuh_tempo = null;
			double saldo = 0.0;
			double bunga = 0.0;
			
			try {
				
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				tgl_jatuh_tempo = df.parse(body.get("tgl_jatuh_tempo"));
				
				logger.info(method+"|"+url+"|"+ "tgl_jatuh_tempo :"+tgl_jatuh_tempo.toString());
				saldo = Double.parseDouble(body.get("saldo"));
				bunga = Double.parseDouble(body.get("suku_bunga")) / 100;
				
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);
				return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			Date today = new Date();
			
			VasicekAsetList input = this.serviceAset.findById(id);
			if (input == null) {
				return new ResponseEntity(new ResponObject(2, "Tidak ada List Aset tersebut"),HttpStatus.BAD_REQUEST);
			}
			
			User created = resp.getData();
			input.setUpdated_by(created.getId_user());
			input.setUpdated_date(today);
					
			input.setId_jenis(ch_aset.getId_vas_aset());
			input.setId_bank(ch_bank.getId_vas_bank());
			input.setTgl_jatuh_tempo(tgl_jatuh_tempo);
			input.setNo_rekening(body.get("no_rekening"));
			input.setMata_uang(body.get("mata_uang"));
			input.setSuku_bunga(bunga);
			input.setSaldo(saldo);
			this.serviceAset.save(input);
			
			logger.info(method+"|"+url+"|"+"success save draft");
			int respact = this.serviceAktivitas.createAktivitas("LIST ASET",created,method,url,input.getId_list_aset());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			return new ResponseEntity(new ResponObject(1, "Succes update data List Aset",input),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3,e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@DeleteMapping(value="/aset-list/{id}")
	public ResponseEntity<?> deleteInput(@RequestHeader("Authorization") String Authorization,
			@PathVariable int id) throws Exception {
		
		String method = "DELETE";
		String url = "/aset-list/"+id;
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			logger.info(method+"|"+url+"|"+"hit api delete data list asset");
			logger.info(method+"|"+url+"|"+"input data list aset id: "+id);
			this.serviceAset.delete(id);
			
			int respact = this.serviceAktivitas.createAktivitas("LIST ASET",resp.getData(),method,url,id);
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
			logger.info(method+"|"+url+"|"+"delete data List Aset");	
			return new ResponseEntity(new ResponObject(1, "delete List Aset."),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked"})
	@PostMapping(value="/aset-list/upload")
	public ResponseEntity<?> uploadHarian(@RequestHeader("Authorization") String Authorization,
		@RequestParam Map<String, String> body,
		@RequestParam("file") MultipartFile file
		) throws Exception, ParseException {
	
		String method = "POST";
		String url = "/aset-list/upload";
		
		logger.info(method+"|"+url+"|"+ "Authorization: "+Authorization);
		ResponMiddleware resp = auth.AuthenticateMiddleware(Authorization);
		if(resp.getStatus() ==  2) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(2, resp.getMessage()),HttpStatus.BAD_REQUEST);
		}
		if(resp.getStatus() == 3) {
			logger.info(resp.getMessage());
			return new ResponseEntity(new ResponObject(3, resp.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info("CREATE INBOUND FILE: "+body.toString());
		logger.info("FILE original: "+file.getOriginalFilename()+" TYPE: "+file.getContentType());
		logger.info("FILE name: "+file.getName());
		
		//try untuk baca extensi file harus CSV
		String extension = "";
		try {
			String split [] = file.getOriginalFilename().split("\\.");
			extension = split[split.length-1];
			if(!extension.toLowerCase().contentEquals("csv")) {
				logger.info("validasi => File type must CSV");
					return new ResponseEntity(new ResponObject(2, "File type must CSV"),HttpStatus.BAD_REQUEST);	
				}
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		logger.info("Lolos validasi file csv");
		
		if (body.toString()==null) return new ResponseEntity(new ResponObject(2, "body null"),HttpStatus.BAD_REQUEST);	
		if (body.get("id_jenis") == null || body.get("id_jenis").isEmpty()) return new ResponseEntity(new ResponObject(2, "id_jenis null"),HttpStatus.BAD_REQUEST);	
		if (body.get("id_bank") == null || body.get("id_bank").isEmpty()) return new ResponseEntity(new ResponObject(2, "id_bank null"),HttpStatus.BAD_REQUEST);	
		
		Date today = new Date();
		
		VasicekAsetDraft nmember = new VasicekAsetDraft();
		
		if (body.get("id_draft") == null || body.get("id_draft").isEmpty()) {
			
			VasicekAsetDraft check = serviceDraft.findLast();
			String kode = "DRA0001";
			if(check != null) {
				logger.info(method+"|"+url+"|"+ "draft terakhir : "+check.getKode_draft());
				String kd = check.getKode_draft().substring(3);
				int codenow = Integer.parseInt(kd) + 1;	
				int lcdn = String.valueOf(codenow).length();
				if(lcdn == 1) {kode = "DRA000"+String.valueOf(codenow);} 
				else if (lcdn ==2) { kode = "DRA00"+String.valueOf(codenow);}
				else if (lcdn ==3) { kode = "DRA0"+String.valueOf(codenow);}
				else { kode = "DRA"+String.valueOf(codenow);}
			}
			
			String nm_draft = body.get("nama_draft");
			if (body.get("nama_draft") == null || body.get("nama_draft").isEmpty()) {
				nm_draft = kode;
			}
			
			User created = resp.getData();
			nmember.setCreated_by(created.getId_user());
			nmember.setCreated_date(today);
			nmember.setKode_draft(kode);
			nmember.setNama_draft(nm_draft);
			this.serviceDraft.save(nmember);
			
			logger.info(method+"|"+url+"|"+"success save draft");
			int respact = this.serviceAktivitas.createAktivitas("DRAFT ASET",created,method,url,nmember.getId_draft_vasicek());
			logger.info(method+"|"+url+"|"+"respact: "+respact);
			
		} else {
			nmember = this.serviceDraft.findById(Integer.parseInt(body.get("id_draft")));
		}
		
		if (nmember == null || nmember.getKode_draft() == null || nmember.getKode_draft().isEmpty()) {
			return new ResponseEntity(new ResponObject(2, "Tidak ada ID Draft tersebut"),HttpStatus.BAD_REQUEST);
		}
		
		VasicekAset ch_aset = this.serviceTypeAset.findById(Integer.parseInt(body.get("id_jenis")));
		if (ch_aset == null) {
			return new ResponseEntity(new ResponObject(2, "Tidak ada ID Jenis Aset tersebut"),HttpStatus.BAD_REQUEST);
		}
		VasicekBank ch_bank = this.serviceBank.findById(Integer.parseInt(body.get("id_bank")));
		if (ch_bank == null) {
			return new ResponseEntity(new ResponObject(2, "Tidak ada ID Bank tersebut"),HttpStatus.BAD_REQUEST);
		}
		
		try {
			logger.info("UPLOAD FILE untuk INPUT");
			logger.info("UPLOAD-FILE id_jenis: "+ch_aset.getId_vas_aset());
			logger.info("UPLOAD-FILE id_bank: "+ch_bank.getId_vas_bank());
			logger.info("NAMA aset: "+ch_aset.getNama_aset());
			logger.info("NAMA bank: "+ch_bank.getNama_bank());
			
			int lst = 1;
			UploadFile dmy = this.serviceUpload.findLast();
			if (dmy != null) {
				lst = dmy.getId_upload() + 1;
			}
			
			UploadFile nw = new UploadFile();
			nw.setTrue_name(file.getOriginalFilename());
			nw.setType_file(file.getContentType());
			nw.setSize_file((int) file.getSize());
			nw.setType_folder("input");
			nw.setTanggal_upload(today);
			nw.setId_user(resp.getData().getId_user());
				
			String filename = String.valueOf(lst)+today.getTime()+"_LSA"+"."+extension;
			logger.info(method+"|"+url+"|"+"filename: "+filename);
			
			nw.setPath_name("input/"+filename);
			
			String uripath = uploadService.storeFile(file,filename,"input");		 
			String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
			.path("core-ifrs/view_file/input/")
			.path(uripath)
			.toUriString();
			
			this.serviceUpload.save(nw);
			logger.info(method+"|"+url+"|"+"success upload");
			
			logger.info("UPLOAD-FILE uripath: "+uripath);
			logger.info("UPLOAD-FILE fileDownloadUri: "+fileDownloadUri);
			
			KoreksiImport impor = new KoreksiImport();
			impor.setId_jenis(ch_aset.getId_vas_aset());
			impor.setJenis(ch_aset);
			impor.setId_bank(ch_bank.getId_vas_bank());
			impor.setBank(ch_bank);
			impor.setCount_import(0);
			impor.setCount_success(0);
			impor.setCount_failed(0);
			impor.setTgl_import(today.toString());
			impor.setTitle("Import File Type Harian");
			impor.setFile_upload(nw);
			
			List<KoreksiPesan> pesanKoreksi = new ArrayList<KoreksiPesan>();
			
			try (BufferedReader br = new BufferedReader(new FileReader(path_file+"/"+uripath))) {
			    String line;
			  
			    //TEMPLATE: tanggal;idpiutang;kodecustomer;namacustomer;kodeproyek;namaproyek;pemberikerja,dll
			    int i = -1;
				
			    while ((line = br.readLine()) != null) {
			    	i++;
			    	String clean = line.replaceAll("\\P{Print}", ""); //ini perintah untuk remove unicode
			        String[] values = clean.split("\\;");
			        
			        if (i == 0) {
						continue;
					}
			       
			        logger.info("PERULANGAN KE- : "+i);
			        String nomor = String.valueOf(i);
			       
			    try {
			    	
			    	logger.info("Values length : "+values.length);
			        if (values.length != 5) {
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Jumlah field harus 6")); 
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					} 		
			        if (values[0] == null || values[0].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "field no rekening tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[1]== null || values[1].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "field mata uang tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[2]== null || values[2].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "field suku bunga tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[3] == null || values[3].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "field tgl jatuh tempo tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        if (values[4]== null || values[4].isEmpty()) { 
			        	pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "field saldo tidak ada")); 
			        	impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
			        	continue; }
			        
			        String no_rekening = values[0];
			        String mata_uang = values[1];
			        String suku_bunga = values[2];
			        String tgl_jatuh = values[3];
			        String saldo = values[4];
			        
			        logger.info("Nomor input : "+nomor);
					logger.info("no_rekening : "+no_rekening);
					logger.info("mata_uang : "+mata_uang);
					logger.info("suku_bunga : "+suku_bunga);
					logger.info("tgl_jatuh : "+tgl_jatuh);
					logger.info("saldo : "+saldo);
					
					Date tgljatuh = null;
					double dsaldo = 0.0;
					double dbunga = 0.0;
					
					try {
						
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						tgljatuh = df.parse(tgl_jatuh);
						
						logger.info(method+"|"+url+"|"+ "tgljatuh :"+tgljatuh.toString());
//						dsaldo = Double.parseDouble(saldo);
						dsaldo = Double.parseDouble(saldo.replaceAll("\\,", "."));
						dbunga = Double.parseDouble(suku_bunga) / 100;
						
					} catch (Exception e) {
						// TODO: handle exception
						logger.info(e.toString());
						logger.info(this.getClass().toGenericString(), e);
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Pastikan field sesuai format")); 
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
					
					VasicekAsetList input = new VasicekAsetList();
					User created = resp.getData();
					input.setCreated_by(created.getId_user());
					input.setCreated_date(today);
					input.setUpdated_by(created.getId_user());
					input.setUpdated_date(today);
					
					input.setId_draft_vasicek(nmember.getId_draft_vasicek());
					input.setId_jenis(ch_aset.getId_vas_aset());
					input.setId_bank(ch_bank.getId_vas_bank());
					input.setNo_rekening(no_rekening);
					input.setMata_uang(mata_uang);
					input.setSuku_bunga(dbunga);
					input.setTgl_jatuh_tempo(tgljatuh);
					input.setSaldo(dsaldo);
					this.serviceAset.save(input);
					
					logger.info(method+"|"+url+"|"+"success save input");
					int respact = this.serviceAktivitas.createAktivitas("LIST ASET",created,method,url,input.getId_list_aset());
					logger.info(method+"|"+url+"|"+"respact: "+respact);
					
					impor.setCount_import(impor.getCount_import()+1);
					impor.setCount_success(impor.getCount_success()+1);
					
			    	} catch (Exception e) {
						// TODO: handle exception
			    		logger.info(e.toString());
						logger.info(this.getClass().toGenericString(), e);	
						pesanKoreksi.add(new KoreksiPesan(nomor, "Gagal", "Pastikan sesuai format"));
						impor.setCount_import(impor.getCount_import()+1);
						impor.setCount_failed(impor.getCount_failed()+1);
						continue;
					}
			    }
			    
			} catch (Exception e) {
				// TODO: handle exception
				logger.info(e.toString());
				logger.info(this.getClass().toGenericString(), e);	
				impor.setList_pesan(pesanKoreksi);
				return new ResponseEntity(new ResponObject(3, e.getMessage(),impor),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			if (pesanKoreksi.size() > 0) {
				this.serviceDraft.deleteByIdDraft(nmember.getId_draft_vasicek());
				this.serviceAset.deleteDataInputByDraft(nmember.getId_draft_vasicek());
			}
			
			impor.setList_pesan(pesanKoreksi);
			return new ResponseEntity(new ResponObject(1, "Data Impor File. Silahkan Unggah kembali jika data gagal.",impor),HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.toString());
			logger.info(this.getClass().toGenericString(), e);	
			return new ResponseEntity(new ResponObject(3, e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public List<AsetListJoin> getAsetListJoin(List<VasicekAsetList> data){
		
		List<AsetListJoin> result = new ArrayList<AsetListJoin>();
		List<User> dumyUser = new ArrayList<User>();
		List<VasicekAset> dumyVasicekAset = new ArrayList<VasicekAset>();
		List<VasicekBank> dumyVasicekBank = new ArrayList<VasicekBank>();
		List<VasicekAsetDraft> dumyVasicekAsetDraft = new ArrayList<VasicekAsetDraft>();
		
		for (VasicekAsetList dt : data) {
			AsetListJoin rst = new AsetListJoin();
			
			rst.setId_list_aset(dt.getId_list_aset());
			rst.setId_jenis(dt.getId_jenis());
			rst.setId_bank(dt.getId_bank());
			rst.setId_draft(dt.getId_draft_vasicek());
			
			
			VasicekAsetDraft draft = new VasicekAsetDraft();
			if(dumyVasicekAsetDraft.size()>0) {
				int loop = 0;
				for(VasicekAsetDraft a : dumyVasicekAsetDraft) {
					if(a.getId_draft_vasicek() == dt.getId_draft_vasicek()) {
						draft = a;
						break;
					} else if (loop == dumyVasicekAsetDraft.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						draft = this.serviceDraft.findById(dt.getId_draft_vasicek());
						dumyVasicekAsetDraft.add(draft);
						break;
					}	
					loop++;
				}
			} else {
				draft = this.serviceDraft.findById(dt.getId_jenis());
				dumyVasicekAsetDraft.add(draft);
			}
			rst.setDraft(draft);
			
			VasicekAset jenis = new VasicekAset();
			if(dumyVasicekAset.size()>0) {
				int loop = 0;
				for(VasicekAset a : dumyVasicekAset) {
					if(a.getId_vas_aset() == dt.getId_jenis()) {
						jenis = a;
						break;
					} else if (loop == dumyVasicekAset.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						jenis = this.serviceTypeAset.findById(dt.getId_jenis());
						dumyVasicekAset.add(jenis);
						break;
					}	
					loop++;
				}
			} else {
				jenis = this.serviceTypeAset.findById(dt.getId_jenis());
				dumyVasicekAset.add(jenis);
			}
			rst.setAset(jenis);
			
			VasicekBank bank = new VasicekBank();
			if(dumyVasicekBank.size()>0) {
				int loop = 0;
				for(VasicekBank a : dumyVasicekBank) {
					if(a.getId_vas_bank() == dt.getId_bank()) {
						bank = a;
						break;
					} else if (loop == dumyVasicekBank.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						bank = this.serviceBank.findById(dt.getId_bank());
						dumyVasicekBank.add(bank);
						break;
					}	
					loop++;
				}
			} else {
				bank = this.serviceBank.findById(dt.getId_bank());
				dumyVasicekBank.add(bank);
			}
			
			VasicekBankJoin bank_join = new VasicekBankJoin();
			bank_join.setId_vas_bank(bank.getId_vas_bank());
			bank_join.setNama_bank(bank.getNama_bank());
			bank_join.setAkronim(bank.getAkronim());
			bank_join.setTahun(bank.getTahun());
			bank_join.setId_rating(bank.getId_rating());
			bank_join.setNilai(bank.getNilai());
			bank_join.setRating(this.serviceRating.findById(bank.getId_rating()));
			bank_join.setReferensi(bank.getReferensi());
			rst.setBank(bank_join);
			
			rst.setTgl_jatuh_tempo(configGetDate.format(dt.getTgl_jatuh_tempo()));
			
			rst.setNo_rekening(dt.getNo_rekening());
			rst.setMata_uang(dt.getMata_uang());
			rst.setSuku_bunga(dt.getSuku_bunga());
			rst.setSaldo(dt.getSaldo());
				
			rst.setCreated_by(dt.getCreated_by());
			rst.setCreated_date(configGetDate.format(dt.getCreated_date()));
			if(dt.getUpdated_date()!=null) {
				rst.setUpdated_by(dt.getUpdated_by());
				rst.setUpdated_date(configGetDate.format(dt.getUpdated_date()));
			}
			
			User user = new User();
			if(dumyUser.size()>0) {
				int loop = 0;
				for(User a : dumyUser) {
					if(a.getId_user() == dt.getCreated_by()) {
						user = a;
						break;
					} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
						user = this.serviceUser.findById(dt.getCreated_by());
						dumyUser.add(user);
						break;
					}	
					loop++;
				}
			} else {
				user = this.serviceUser.findById(dt.getCreated_by());
				dumyUser.add(user);
			}
			rst.setCreated_user(user);
			
			if (dt.getUpdated_by() != 0) {
				User user_upd = new User();
				if(dumyUser.size()>0) {
					int loop = 0;
					for(User ab : dumyUser) {
						System.out.println("user: "+ab.getNama());
						if(ab.getId_user() == dt.getUpdated_by()) {
							user_upd = ab;
							break;
						} else if (loop == dumyUser.size()-1) { //jika samper perulangan terakhir ngga ketemu hit API
							user_upd = this.serviceUser.findById(dt.getUpdated_by());
							dumyUser.add(user_upd);
							break;
						}	
						loop++;
					}
				} else {
					user_upd = this.serviceUser.findById(dt.getUpdated_by());
					dumyUser.add(user_upd);
				}
				rst.setUpdated_user(user_upd);
			}
			result.add(rst);
		}
		return result;
	}	

}
