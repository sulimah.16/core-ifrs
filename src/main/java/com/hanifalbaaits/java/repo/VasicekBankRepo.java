package com.hanifalbaaits.java.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.VasicekBank;

@Repository
public interface VasicekBankRepo extends JpaRepository<VasicekBank,Integer> {
	
	@Query("select u from VasicekBank u ")
	List<VasicekBank> findAll();

	@Query("select u from VasicekBank u where id_vas_bank = ?1")
	VasicekBank findById(int id);
	
	@Query("select u from VasicekBank u where nama_bank = ?1 and akronim = ?2 and tahun = ?3")
	VasicekBank findBySame(String nama, String akronim, int tahun);
}
