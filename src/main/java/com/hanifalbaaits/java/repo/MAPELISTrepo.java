package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.MAPELIST;

@Repository
public interface MAPELISTrepo extends JpaRepository<MAPELIST,Integer> {
	
	@Query("select u from MAPELIST u where id_draft_ecl = ?1")
	List<MAPELIST> findByIdDraft(int id_draft_ecl);
	
	@Transactional
	@Modifying
	@Query(
			  value = "delete from t_mape_list where id_draft_ecl = ?1", 
			  nativeQuery = true)
	void deleteByIdDraft(int id_draft_ecl);
}
