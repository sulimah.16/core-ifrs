package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.InputMEV;

@Repository
public interface InputMEVrepo extends JpaRepository<InputMEV,Integer> {
	
	@Query("select u from InputMEV u ")
	List<InputMEV> findAll();

	@Query("select u from InputMEV u where id_mev = ?1")
	InputMEV findById(int id);
	
	@Query(
			  value = "select * from t_mev_input order by id_mev desc limit 1", 
			  nativeQuery = true)
	InputMEV findSTDEV();
	
	@Query("select u from InputMEV u where bulan = ?1 and tahun = ?2")
	InputMEV findByBulanTahun(int bulan, int tahun);
	
	@Transactional
	@Modifying
	@Query(
			  value = "truncate table t_mev_input", 
			  nativeQuery = true)
	void truncate();
}
