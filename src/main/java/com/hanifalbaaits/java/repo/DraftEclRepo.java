package com.hanifalbaaits.java.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.DraftECL;

@Repository
public interface DraftEclRepo extends JpaRepository<DraftECL,Integer> {
	
	@Query("select u from DraftECL u ")
	List<DraftECL> findAll();
	
	@Query(
			  value = "select * from t_draft_ecl group by id_model", 
			  nativeQuery = true)
	List<DraftECL> findGrupIdModel();

	@Query("select u from DraftECL u where id_draft_ecl = ?1")
	DraftECL findById(int id);
	
	@Query("select u from DraftECL u where id_draft_bucket = ?1")
	List<DraftECL> findByIdDraftBucket(int id);
	
}
