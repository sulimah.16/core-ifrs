package com.hanifalbaaits.java.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.User;

@Repository
public interface UserRepo extends JpaRepository<User,Integer> {

	@Query("select u from User u ")
	List<User> findAll();

	@Query("select u from User u where id_user= ?1")
	User findById(int id);
	
	@Query("select u from User u where username= ?1")
	User findByUsername(String username);	
}
