package com.hanifalbaaits.java.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.VasicekRating;

@Repository
public interface VasicekRatingRepo extends JpaRepository<VasicekRating,Integer> {
	
	@Query("select u from VasicekRating u ")
	List<VasicekRating> findAll();

	@Query("select u from VasicekRating u where id_rating= ?1")
	VasicekRating findById(int id);
	
	@Query("select u from VasicekRating u where kode_rating= ?1")
	VasicekRating findByKode(String id);
	
	@Query("select u from VasicekRating u where status= ?1")
	List<VasicekRating> findByStatus(int status);
}
