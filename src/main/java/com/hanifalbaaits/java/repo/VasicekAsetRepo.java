package com.hanifalbaaits.java.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.VasicekAset;

@Repository
public interface VasicekAsetRepo extends JpaRepository<VasicekAset,Integer> {
	
	@Query("select u from VasicekAset u ")
	List<VasicekAset> findAll();

	@Query("select u from VasicekAset u where id_vas_aset = ?1")
	VasicekAset findById(int id);
}
