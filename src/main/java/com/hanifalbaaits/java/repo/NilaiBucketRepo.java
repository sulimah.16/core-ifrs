package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.NilaiBucket;

@Repository
public interface NilaiBucketRepo extends JpaRepository<NilaiBucket,Integer> {
	
	@Query("select u from NilaiBucket u ")
	List<NilaiBucket> findAll();

	@Query("select u from NilaiBucket u where id_nilai_bucket = ?1")
	NilaiBucket findById(int id);
	
	@Query("select u from NilaiBucket u where id_draft_bucket = ?1")
	List<NilaiBucket> findNilaiBucketByIdDraftBucket(int id);
	
	@Query("select u from NilaiBucket u where id_draft_bucket = ?1 and id_step = ?2")
	List<NilaiBucket> findNilaiBucketByIdDraftBucketSTEP(int id, int id_step);
	
	@Query("select u from NilaiBucket u where id_draft_bucket = ?1 and id_step = ?2 and periode_bulan = ?3 and periode_tahun = ?4")
	NilaiBucket findNilaiBucketByIdDraftBucketSTEPdate(int id, int id_step, int bulan , int tahun);
	
	@Modifying
	@Transactional
	@Query("delete from NilaiBucket b where id_draft_bucket = ?1")
	void deleteByIdDraftBucket(int id);
	
	@Transactional
	@Modifying
	@Query(
			  value = "truncate table t_draft_bucket", 
			  nativeQuery = true)
	void truncate();
}
