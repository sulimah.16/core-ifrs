package com.hanifalbaaits.java.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.UserRole;

@Repository
public interface UserRoleRepo extends JpaRepository<UserRole,Integer> {
	
	@Query("select u from UserRole u ")
	List<UserRole> findAll();

	@Query("select u from UserRole u where id_role= ?1")
	UserRole findById(int id);
}
