package com.hanifalbaaits.java.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.Segment;

@Repository
public interface SegmentRepo extends JpaRepository<Segment,Integer> {
	
	@Query("select u from Segment u ")
	List<Segment> findAll();

	@Query("select u from Segment u where id_segment= ?1")
	Segment findById(int id);
	
	@Query("select u from Segment u where kode_segment= ?1")
	Segment findByKode(String id);
}
