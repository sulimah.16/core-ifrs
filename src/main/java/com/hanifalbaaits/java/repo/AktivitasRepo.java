package com.hanifalbaaits.java.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.Aktivitas;

@Repository
public interface AktivitasRepo extends JpaRepository<Aktivitas,Integer> {
	
	@Query(
			  value = "select * from t_aktivitas where tanggal_aktivitas >= ?1 and tanggal_aktivitas <= ?2 order by id_aktivitas desc limit 20", 
			  nativeQuery = true)
	List<Aktivitas> findByAdmin(Date start, Date end);
	
	@Query(
			  value = "select * from t_aktivitas where tanggal_aktivitas >= ?1 and tanggal_aktivitas <= ?2", 
			  nativeQuery = true)
	List<Aktivitas> findByAdminDashboard(Date start, Date end);
	
	@Query(
			  value = "select * from t_aktivitas where id_user = ?1 and tanggal_aktivitas >= ?2 and tanggal_aktivitas <= ?3 order by id_aktivitas desc limit 20", 
			  nativeQuery = true)
	List<Aktivitas> findByIdUser(int id_user, Date start, Date end);
	
	@Query(
			  value = "select * from t_aktivitas order by id_aktivitas desc limit 20", 
			  nativeQuery = true)
	List<Aktivitas> findLastAdmin();
	
	@Query(
			  value = "select * from t_aktivitas where id_user = ?1 order by id_aktivitas desc limit 20", 
			  nativeQuery = true)
	List<Aktivitas> findLastByIduser(int id_user);
	
	@Query(
			  value = "select * from t_aktivitas where id_user = ?1 and caption='LOGIN' order by id_aktivitas desc limit 1", 
			  nativeQuery = true)
	Aktivitas findLastLoginByIdUser(int id_user);
	
}
