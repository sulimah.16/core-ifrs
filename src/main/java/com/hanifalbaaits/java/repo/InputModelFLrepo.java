package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.InputModelFL;

@Repository
public interface InputModelFLrepo extends JpaRepository<InputModelFL,Integer> {
	
	@Query("select u from InputModelFL u ")
	List<InputModelFL> findAll();

	@Query("select u from InputModelFL u where id_model = ?1")
	InputModelFL findById(int id);
	
	@Query("select u from InputModelFL u where nomor = ?1")
	List<InputModelFL> findByNomor(String nomor);
	
	@Query("select u from InputModelFL u where id_segment = ?1 and status = ?2")
	List<InputModelFL> findByIdSegmen(int id_segment, int status);
	
	@Query("select u from InputModelFL u where id_model = ?1 and id_segment = ?2")
	InputModelFL findByIdModelSegment(int id , int id_segment);
	
	@Query("select u from InputModelFL u where nomor = ?1 and id_segment = ?2")
	InputModelFL findByNomorSegment(String nomor, int id_segment);

//	@Transactional
//	@Modifying
//	@Query(
//			  value = "truncate table t_model_fl", 
//			  nativeQuery = true)
//	void truncate();
	
	@Transactional
	@Modifying
	@Query(
			  value = "delete from t_model_fl where id_segment = ?1", 
			  nativeQuery = true)
	void deleteBySegment(int id_segment);
}
