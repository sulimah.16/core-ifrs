package com.hanifalbaaits.java.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.UploadFile;

@Repository
public interface UploadFileRepo extends JpaRepository<UploadFile,Integer> {

	@Query(
			  value = "select * from t_upload_file order by id_upload desc", 
			  nativeQuery = true)
	List<UploadFile> findAll();
	
	@Query("select u from UploadFile u where id_upload = ?1")
	UploadFile findById(int id_asset);

	@Query(
			  value = "select * from t_upload_file order by id_upload desc limit 1", 
			  nativeQuery = true)
	UploadFile findLast();
}
