package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.VasicekAsetList;

@Repository
public interface VasicekAsetListRepo extends JpaRepository<VasicekAsetList,Integer> {
	
	@Query("select u from VasicekAsetList u ")
	List<VasicekAsetList> findAll();

	@Query("select u from VasicekAsetList u where id_list_aset = ?1")
	VasicekAsetList findById(int id);
	
	@Query("select u from VasicekAsetList u where id_draft_vasicek = ?1")
	List<VasicekAsetList> findByIdDraft(int id_draft_vasicek);
	
	@Modifying
	@Transactional
	@Query("delete from VasicekAsetList u where id_draft_vasicek = ?1")
	void deleteDataInputByDraft(int id);
}
