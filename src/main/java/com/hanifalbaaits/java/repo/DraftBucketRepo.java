package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.DraftBucket;

@Repository
public interface DraftBucketRepo extends JpaRepository<DraftBucket,Integer> {
	
	@Query("select u from DraftBucket u ")
	List<DraftBucket> findAll();

	@Query("select u from DraftBucket u where id_draft_bucket = ?1")
	DraftBucket findById(int id);
	
	@Query("select u from DraftBucket u where kode_draft_bucket = ?1")
	DraftBucket findDraftBucketByKode(String kode);
	
	@Query(
			  value = "select * from t_draft_bucket order by id_draft_bucket desc limit 1", 
			  nativeQuery = true)
	DraftBucket findLast();
	
	@Transactional
	@Modifying
	@Query(
			  value = "truncate table t_draft_bucket", 
			  nativeQuery = true)
	void truncate();
}
