package com.hanifalbaaits.java.repo;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.DataInput;

@Repository
public interface DataInputRepo extends JpaRepository<DataInput,Integer> {
		
	@Query("select u from DataInput u where id_input = ?1")
	DataInput findById(int id_input);

	@Query("select u from DataInput u where id_draft = ?1")
	List<DataInput> findByDraftX(int id_draft);
	
	@Query(
			  value = "select * from t_data_input where id_draft = ?1 order by tanggal_piutang", 
			  nativeQuery = true)
	List<DataInput> findByDraft(int id_draft);
	
	@Query("select u from DataInput u where id_draft = ?1 and tanggal_piutang >= ?2 and tanggal_piutang <= ?3")
	List<DataInput> findByDraftPeriodeX(int id_draft, Date start, Date end);
	
	@Query(
			  value = "select * from t_data_input where id_draft = ?1 and tanggal_piutang >= ?2 and tanggal_piutang <= ?3 order by tanggal_piutang", 
			  nativeQuery = true)
	List<DataInput> findByDraftPeriode(int id_draft, Date start, Date end);
	
	@Modifying
	@Transactional
	@Query("delete from DataInput b where id_draft = ?1")
	void deleteDataInputByDraft(int id);
	
	@Modifying
	@Transactional
	@Query("delete from DataInput b where path_document = ?1")
	void deleteDataInputByPathFile(String path_document);
	
	@Transactional
	@Modifying
	@Query(
			  value = "truncate table t_data_input", 
			  nativeQuery = true)
	void truncate();

}
