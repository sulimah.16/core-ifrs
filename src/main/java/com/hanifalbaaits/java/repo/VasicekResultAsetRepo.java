package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.VasicekResultAset;

@Repository
public interface VasicekResultAsetRepo extends JpaRepository<VasicekResultAset,Integer> {
	
	@Query("select u from VasicekResultAset u where id_vasicek_result_draft = ?1")
	List<VasicekResultAset> findByIdDraft(int id_vasicek_result_draft);
	
	@Transactional
	@Modifying
	@Query(
			  value = "delete from t_vasicek_result_aset where id_vasicek_result_draft = ?1", 
			  nativeQuery = true)
	void deleteByIdDraft(int id_vasicek_result_draft);
	
	@Transactional
	@Modifying
	@Query(
			  value = "delete from t_vasicek_result_aset where id_draft_vasicek = ?1", 
			  nativeQuery = true)
	void deleteByIdDraftAset(int id_vasicek_result_draft);
}
