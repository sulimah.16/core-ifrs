package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.VasicekAsetDraft;

@Repository
public interface VasicekAsetDraftRepo extends JpaRepository<VasicekAsetDraft,Integer> {
	
	@Query("select u from VasicekAsetDraft u ")
	List<VasicekAsetDraft> findAll();

	@Query("select u from VasicekAsetDraft u where id_draft_vasicek = ?1")
	VasicekAsetDraft findById(int id);
	
	@Query(
			  value = "select * from t_vasicek_aset_draft order by id_draft_vasicek desc limit 1", 
			  nativeQuery = true)
	VasicekAsetDraft findLast();
	
	@Transactional
	@Modifying
	@Query(
			  value = "delete from t_vasicek_aset_draft where id_draft_vasicek = ?1", 
			  nativeQuery = true)
	void deleteByIdDraft(int id_vasicek_result_draft);
}
