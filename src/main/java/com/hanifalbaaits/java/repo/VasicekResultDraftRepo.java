package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.VasicekResultDraft;

@Repository
public interface VasicekResultDraftRepo extends JpaRepository<VasicekResultDraft,Integer> {
	
	@Query("select u from VasicekResultDraft u")
	List<VasicekResultDraft> findByAll();
	
	@Query("select u from VasicekResultDraft u where id_vasicek_result_draft = ?1")
	VasicekResultDraft findByIdDraft(int id_vasicek_result_draft);
	
	@Transactional
	@Modifying
	@Query(
			  value = "delete from t_vasicek_result_draft where id_vasicek_result_draft = ?1", 
			  nativeQuery = true)
	void deleteByIdDraft(int id_vasicek_result_draft);
}
