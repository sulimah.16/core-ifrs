package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.ECL;

@Repository
public interface ECLrepo extends JpaRepository<ECL,Integer> {
	
	@Query("select u from ECL u where id_draft_ecl = ?1")
	List<ECL> findByIdDraft(int id_draft_ecl);
	
	@Transactional
	@Modifying
	@Query(
			  value = "delete from t_ecl where id_draft_ecl = ?1", 
			  nativeQuery = true)
	void deleteByIdDraft(int id_draft_ecl);
}
