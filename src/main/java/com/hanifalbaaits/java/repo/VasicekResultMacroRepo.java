package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.VasicekResultMacro;

@Repository
public interface VasicekResultMacroRepo extends JpaRepository<VasicekResultMacro,Integer> {
	
	@Query("select u from VasicekResultMacro u where id_vasicek_result_draft = ?1")
	List<VasicekResultMacro> findByIdDraft(int id_vasicek_result_draft);
	
	@Transactional
	@Modifying
	@Query(
			  value = "delete from t_vasicek_result_macro where id_vasicek_result_draft = ?1", 
			  nativeQuery = true)
	void deleteByIdDraft(int id_vasicek_result_draft);
}
