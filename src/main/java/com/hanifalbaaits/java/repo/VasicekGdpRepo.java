package com.hanifalbaaits.java.repo;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.VasicekGDP;

@Repository
public interface VasicekGdpRepo extends JpaRepository<VasicekGDP,Integer> {
	
	@Query(
			  value = "select * from t_vasicek_gdp order by tahun , bulan", 
			  nativeQuery = true)
	List<VasicekGDP> findAll();

	@Query("select u from VasicekGDP u where id_vas_gdp = ?1")
	VasicekGDP findById(int id);
	
	@Query(
			  value = "select * from t_vasicek_gdp where periode >= ?1 and periode <= ?2 order by tahun, bulan", 
			  nativeQuery = true)
	List<VasicekGDP> findStartEnd(Date start, Date end);
	
	@Query("select u from VasicekGDP u where bulan= ?1 and tahun = ?2")
	VasicekGDP findByBulanTahun(int bulan, int tahun);
	
	@Transactional
	@Modifying
	@Query(
			  value = "truncate table t_vasicek_gdp", 
			  nativeQuery = true)
	void truncate();
}
