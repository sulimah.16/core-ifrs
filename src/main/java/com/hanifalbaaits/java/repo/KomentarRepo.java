package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.Komentar;

@Repository
public interface KomentarRepo extends JpaRepository<Komentar,Integer> {
	
	@Query("select l from Komentar l where id_komentar_bucket = ?1 ")
	Komentar findById(int id);
	
//	@Query("select l from Komentar l where id_type = ?1 and id_bucket = ?2")
//	List<Komentar> findByTypeIdType(int type, int id);
	
	@Query(
			  value = "select * from t_komentar_bucket where id_type = ?1 and flag = ?2 and id_bucket = ?3 order by id_komentar_bucket desc",
			  nativeQuery = true)
	List<Komentar> findByTypeIdTypeAll(int type, int flag, int id);
	
	@Query(
			  value = "select * from t_komentar_bucket where id_type = ?1 and id_bucket = ?2 order by id_komentar_bucket desc",
			  nativeQuery = true)
	List<Komentar> findByTypeIdTypeAll(int type,int id);
	
	@Query(
			  value = "select * from t_komentar_bucket where id_type = ?1 and flag = ?2 and id_bucket = ?3 order by id_komentar_bucket desc limit 15", 
			  nativeQuery = true)
	List<Komentar> findByTypeIdType(int type, int flag, int id);
	
	@Transactional
	@Modifying
	@Query(
			  value = "delete from t_komentar_bucket where id_type = ?1 and id_bucket =?2", 
			  nativeQuery = true)
	void deleteByIdDraft(int id_type, int id_bucket);
}
