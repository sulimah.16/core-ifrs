package com.hanifalbaaits.java.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hanifalbaaits.java.modelDB.Draft;

@Repository
public interface DraftRepo extends JpaRepository<Draft,Integer> {
	
	@Query("select u from Draft u ")
	List<Draft> findAll();

	@Query("select u from Draft u where id_draft = ?1")
	Draft findById(int id);
	
	@Query("select u from Draft u where flag_draft = ?1")
	List<Draft> findDraftByIdFlag(int id);
	
	@Query(
			  value = "select * from t_draft order by id_draft desc limit 1", 
			  nativeQuery = true)
	Draft findLast();
	
	@Transactional
	@Modifying
	@Query(
			  value = "truncate table t_draft", 
			  nativeQuery = true)
	void truncate();
}
