package com.hanifalbaaits.java.modelRespon;

import java.util.Date;

public class VasicekDraftECLJoin {
	
	private int id_draft_vasicek;
	private String kode_draft;
	private String nama_draft;
	private int created_by;
	private Date created_date;
	private int updated_by;
	private Date updated_date;
	private String name_created;
	private String name_updated;
	public int getId_draft_vasicek() {
		return id_draft_vasicek;
	}
	public void setId_draft_vasicek(int id_draft_vasicek) {
		this.id_draft_vasicek = id_draft_vasicek;
	}
	public String getKode_draft() {
		return kode_draft;
	}
	public void setKode_draft(String kode_draft) {
		this.kode_draft = kode_draft;
	}
	public String getNama_draft() {
		return nama_draft;
	}
	public void setNama_draft(String nama_draft) {
		this.nama_draft = nama_draft;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public String getName_created() {
		return name_created;
	}
	public void setName_created(String name_created) {
		this.name_created = name_created;
	}
	public String getName_updated() {
		return name_updated;
	}
	public void setName_updated(String name_updated) {
		this.name_updated = name_updated;
	}
}
