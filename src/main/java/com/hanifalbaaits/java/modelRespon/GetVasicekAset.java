package com.hanifalbaaits.java.modelRespon;

import com.hanifalbaaits.java.modelDB.VasicekAset;

public class GetVasicekAset {
	
	private int id_aset;
	private int id_bank;
	private VasicekAset aset;
	private VasicekBankJoin bank;
	private String no_rekening;
	private String mata_uang;
	private double suku_bunga;
	private String tgl_jatuh_tempo;
	private double saldo;
	private double lgd;
	private double pd_normal_scan;
	private double pd_weighted_scan;
	private double ecl_normal_scan;
	private double ecl_weighted_scan;
	public int getId_aset() {
		return id_aset;
	}
	public void setId_aset(int id_aset) {
		this.id_aset = id_aset;
	}
	public int getId_bank() {
		return id_bank;
	}
	public void setId_bank(int id_bank) {
		this.id_bank = id_bank;
	}
	public VasicekAset getAset() {
		return aset;
	}
	public void setAset(VasicekAset aset) {
		this.aset = aset;
	}
	public VasicekBankJoin getBank() {
		return bank;
	}
	public void setBank(VasicekBankJoin bank) {
		this.bank = bank;
	}
	public String getNo_rekening() {
		return no_rekening;
	}
	public void setNo_rekening(String no_rekening) {
		this.no_rekening = no_rekening;
	}
	public String getMata_uang() {
		return mata_uang;
	}
	public void setMata_uang(String mata_uang) {
		this.mata_uang = mata_uang;
	}
	public double getSuku_bunga() {
		return suku_bunga;
	}
	public void setSuku_bunga(double suku_bunga) {
		this.suku_bunga = suku_bunga;
	}
	public String getTgl_jatuh_tempo() {
		return tgl_jatuh_tempo;
	}
	public void setTgl_jatuh_tempo(String tgl_jatuh_tempo) {
		this.tgl_jatuh_tempo = tgl_jatuh_tempo;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getLgd() {
		return lgd;
	}
	public void setLgd(double lgd) {
		this.lgd = lgd;
	}
	public double getPd_normal_scan() {
		return pd_normal_scan;
	}
	public void setPd_normal_scan(double pd_normal_scan) {
		this.pd_normal_scan = pd_normal_scan;
	}
	public double getPd_weighted_scan() {
		return pd_weighted_scan;
	}
	public void setPd_weighted_scan(double pd_weighted_scan) {
		this.pd_weighted_scan = pd_weighted_scan;
	}
	public double getEcl_normal_scan() {
		return ecl_normal_scan;
	}
	public void setEcl_normal_scan(double ecl_normal_scan) {
		this.ecl_normal_scan = ecl_normal_scan;
	}
	public double getEcl_weighted_scan() {
		return ecl_weighted_scan;
	}
	public void setEcl_weighted_scan(double ecl_weighted_scan) {
		this.ecl_weighted_scan = ecl_weighted_scan;
	}
	
}
