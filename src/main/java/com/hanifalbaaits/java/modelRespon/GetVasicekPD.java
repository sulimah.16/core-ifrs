package com.hanifalbaaits.java.modelRespon;

import com.hanifalbaaits.java.modelDB.VasicekRating;

public class GetVasicekPD {
	
	private int id_rating;
	private VasicekRating rating;
	private String periode_projection;
	private double value_rating;
	private double value_base_formula;
	private double vasicek_normal;
	private double vasicek_adjusted_normal;
	private double vasicek_weighted;
	private double vasicek_adjusted_weighted;
	public int getId_rating() {
		return id_rating;
	}
	public void setId_rating(int id_rating) {
		this.id_rating = id_rating;
	}
	public VasicekRating getRating() {
		return rating;
	}
	public void setRating(VasicekRating rating) {
		this.rating = rating;
	}
	public double getValue_rating() {
		return value_rating;
	}
	public void setValue_rating(double value_rating) {
		this.value_rating = value_rating;
	}
	public double getValue_base_formula() {
		return value_base_formula;
	}
	public void setValue_base_formula(double value_base_formula) {
		this.value_base_formula = value_base_formula;
	}
	public double getVasicek_normal() {
		return vasicek_normal;
	}
	public void setVasicek_normal(double vasicek_normal) {
		this.vasicek_normal = vasicek_normal;
	}
	public double getVasicek_adjusted_normal() {
		return vasicek_adjusted_normal;
	}
	public void setVasicek_adjusted_normal(double vasicek_adjusted_normal) {
		this.vasicek_adjusted_normal = vasicek_adjusted_normal;
	}
	public double getVasicek_weighted() {
		return vasicek_weighted;
	}
	public void setVasicek_weighted(double vasicek_weighted) {
		this.vasicek_weighted = vasicek_weighted;
	}
	public double getVasicek_adjusted_weighted() {
		return vasicek_adjusted_weighted;
	}
	public void setVasicek_adjusted_weighted(double vasicek_adjusted_weighted) {
		this.vasicek_adjusted_weighted = vasicek_adjusted_weighted;
	}
	public String getPeriode_projection() {
		return periode_projection;
	}
	public void setPeriode_projection(String periode_projection) {
		this.periode_projection = periode_projection;
	}	
}
