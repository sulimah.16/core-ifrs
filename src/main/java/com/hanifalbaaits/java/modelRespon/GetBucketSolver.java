package com.hanifalbaaits.java.modelRespon;

import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.Segment;

public class GetBucketSolver {

	private Draft draft;
	private Segment segment;
	private int hari_first_bucket;
	private int interval_bucket;
	private int jumlah_bucket;
	private String start_date;
	private String end_date;
	
	private Double intersep;
	private Double gdp;
	private Double bi7;
	
	private Double normal_gdp;
	private Double normal_bi7;
	private Double normal_probability;

	private Double optimis_gdp;
	private Double optimis_bi7;
	private Double optimis_probability;
	
	private Double pesimis_gdp;
	private Double pesimis_bi7;
	private Double pesimis_probability;
	
	private Double weighted_mev_gdp;
	private Double weighted_mev_bi7;
	
	private Double default_lgd;
	
	private GetBucketSolverHeader solver;

	public Draft getDraft() {
		return draft;
	}

	public void setDraft(Draft draft) {
		this.draft = draft;
	}

	public Segment getSegment() {
		return segment;
	}

	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	public int getInterval_bucket() {
		return interval_bucket;
	}

	public void setInterval_bucket(int interval_bucket) {
		this.interval_bucket = interval_bucket;
	}

	public int getJumlah_bucket() {
		return jumlah_bucket;
	}

	public void setJumlah_bucket(int jumlah_bucket) {
		this.jumlah_bucket = jumlah_bucket;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public Double getIntersep() {
		return intersep;
	}

	public void setIntersep(Double intersep) {
		this.intersep = intersep;
	}

	public Double getGdp() {
		return gdp;
	}

	public void setGdp(Double gdp) {
		this.gdp = gdp;
	}

	public Double getBi7() {
		return bi7;
	}

	public void setBi7(Double bi7) {
		this.bi7 = bi7;
	}

	public Double getNormal_gdp() {
		return normal_gdp;
	}

	public void setNormal_gdp(Double normal_gdp) {
		this.normal_gdp = normal_gdp;
	}

	public Double getNormal_bi7() {
		return normal_bi7;
	}

	public void setNormal_bi7(Double normal_bi7) {
		this.normal_bi7 = normal_bi7;
	}

	public Double getNormal_probability() {
		return normal_probability;
	}

	public void setNormal_probability(Double normal_probability) {
		this.normal_probability = normal_probability;
	}

	public Double getOptimis_gdp() {
		return optimis_gdp;
	}

	public void setOptimis_gdp(Double optimis_gdp) {
		this.optimis_gdp = optimis_gdp;
	}

	public Double getOptimis_bi7() {
		return optimis_bi7;
	}

	public void setOptimis_bi7(Double optimis_bi7) {
		this.optimis_bi7 = optimis_bi7;
	}

	public Double getOptimis_probability() {
		return optimis_probability;
	}

	public void setOptimis_probability(Double optimis_probability) {
		this.optimis_probability = optimis_probability;
	}

	public Double getPesimis_gdp() {
		return pesimis_gdp;
	}

	public void setPesimis_gdp(Double pesimis_gdp) {
		this.pesimis_gdp = pesimis_gdp;
	}

	public Double getPesimis_bi7() {
		return pesimis_bi7;
	}

	public void setPesimis_bi7(Double pesimis_bi7) {
		this.pesimis_bi7 = pesimis_bi7;
	}

	public Double getPesimis_probability() {
		return pesimis_probability;
	}

	public void setPesimis_probability(Double pesimis_probability) {
		this.pesimis_probability = pesimis_probability;
	}

	public Double getWeighted_mev_gdp() {
		return weighted_mev_gdp;
	}

	public void setWeighted_mev_gdp(Double weighted_mev_gdp) {
		this.weighted_mev_gdp = weighted_mev_gdp;
	}

	public Double getWeighted_mev_bi7() {
		return weighted_mev_bi7;
	}

	public void setWeighted_mev_bi7(Double weighted_mev_bi7) {
		this.weighted_mev_bi7 = weighted_mev_bi7;
	}

	public GetBucketSolverHeader getSolver() {
		return solver;
	}

	public void setSolver(GetBucketSolverHeader solver) {
		this.solver = solver;
	}

	public Double getDefault_lgd() {
		return default_lgd;
	}

	public void setDefault_lgd(Double default_lgd) {
		this.default_lgd = default_lgd;
	}

	public int getHari_first_bucket() {
		return hari_first_bucket;
	}

	public void setHari_first_bucket(int hari_first_bucket) {
		this.hari_first_bucket = hari_first_bucket;
	}
}
