package com.hanifalbaaits.java.modelRespon;

import java.util.Date;
import java.util.List;

import com.hanifalbaaits.java.modelDB.InputModelFL;

public class MapeDraft {
	
	private DraftBucketJoin draft_bucket;
	private String input_regresi;
	private int id_regresi;
	private int id_model;
	private InputModelFL model;
	private int periode_forecast;
	private List<String> header;
	private double average_mape;
	private double threshold_mape;
	private String hasil_mape;
	private List<Mape> list_mape;
	private Date periode_start;
	private Date periode_end;
	public String getInput_regresi() {
		return input_regresi;
	}
	public void setInput_regresi(String input_regresi) {
		this.input_regresi = input_regresi;
	}
	public int getId_model() {
		return id_model;
	}
	public void setId_model(int id_model) {
		this.id_model = id_model;
	}
	public InputModelFL getModel() {
		return model;
	}
	public void setModel(InputModelFL model) {
		this.model = model;
	}
	public int getPeriode_forecast() {
		return periode_forecast;
	}
	public void setPeriode_forecast(int periode_forecast) {
		this.periode_forecast = periode_forecast;
	}
	public List<String> getHeader() {
		return header;
	}
	public void setHeader(List<String> header) {
		this.header = header;
	}
	public double getAverage_mape() {
		return average_mape;
	}
	public void setAverage_mape(double average_mape) {
		this.average_mape = average_mape;
	}
	public double getThreshold_mape() {
		return threshold_mape;
	}
	public void setThreshold_mape(double threshold_mape) {
		this.threshold_mape = threshold_mape;
	}
	public String getHasil_mape() {
		return hasil_mape;
	}
	public void setHasil_mape(String hasil_mape) {
		this.hasil_mape = hasil_mape;
	}
	public List<Mape> getList_mape() {
		return list_mape;
	}
	public void setList_mape(List<Mape> list_mape) {
		this.list_mape = list_mape;
	}
	public Date getPeriode_start() {
		return periode_start;
	}
	public void setPeriode_start(Date periode_start) {
		this.periode_start = periode_start;
	}
	public Date getPeriode_end() {
		return periode_end;
	}
	public void setPeriode_end(Date periode_end) {
		this.periode_end = periode_end;
	}
	public int getId_regresi() {
		return id_regresi;
	}
	public void setId_regresi(int id_regresi) {
		this.id_regresi = id_regresi;
	}
	public DraftBucketJoin getDraft_bucket() {
		return draft_bucket;
	}
	public void setDraft_bucket(DraftBucketJoin draft_bucket) {
		this.draft_bucket = draft_bucket;
	}
}
