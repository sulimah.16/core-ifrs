package com.hanifalbaaits.java.modelRespon;

import java.util.Date;

import com.hanifalbaaits.java.modelDB.User;

public class KomentarJoin {

	private int id_komentar_bucket;
	private int id_type;
	private int id_bucket;
	private int flag;
	private String komentar;
	private int created_by;
	private Date created_date;
	private Date updated_date;
	private User user;
	public int getId_komentar_bucket() {
		return id_komentar_bucket;
	}
	public void setId_komentar_bucket(int id_komentar_bucket) {
		this.id_komentar_bucket = id_komentar_bucket;
	}
	public int getId_type() {
		return id_type;
	}
	public void setId_type(int id_type) {
		this.id_type = id_type;
	}
	public int getId_bucket() {
		return id_bucket;
	}
	public void setId_bucket(int id_bucket) {
		this.id_bucket = id_bucket;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public String getKomentar() {
		return komentar;
	}
	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
