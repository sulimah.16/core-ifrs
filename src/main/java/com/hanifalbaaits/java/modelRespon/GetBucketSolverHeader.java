package com.hanifalbaaits.java.modelRespon;

import java.util.List;

public class GetBucketSolverHeader {

	private String title_solver;
	
	private Double psak71;
	private Double last_odr;
	private Double mev_effect;
	private Double forward_looking;
	private Double agregat_pd_base;
	private Double pd_forward_looking;
	private Double target;
	private Double scalling;
	
	private String [] header;
	private List<GetBucketSolverList> list_solver;
	public String getTitle_solver() {
		return title_solver;
	}
	public void setTitle_solver(String title_solver) {
		this.title_solver = title_solver;
	}
	public Double getPsak71() {
		return psak71;
	}
	public void setPsak71(Double psak71) {
		this.psak71 = psak71;
	}
	public Double getLast_odr() {
		return last_odr;
	}
	public void setLast_odr(Double last_odr) {
		this.last_odr = last_odr;
	}
	public Double getMev_effect() {
		return mev_effect;
	}
	public void setMev_effect(Double mev_effect) {
		this.mev_effect = mev_effect;
	}
	public Double getForward_looking() {
		return forward_looking;
	}
	public void setForward_looking(Double forward_looking) {
		this.forward_looking = forward_looking;
	}
	public Double getAgregat_pd_base() {
		return agregat_pd_base;
	}
	public void setAgregat_pd_base(Double agregat_pd_base) {
		this.agregat_pd_base = agregat_pd_base;
	}
	public Double getPd_forward_looking() {
		return pd_forward_looking;
	}
	public void setPd_forward_looking(Double pd_forward_looking) {
		this.pd_forward_looking = pd_forward_looking;
	}
	public Double getTarget() {
		return target;
	}
	public void setTarget(Double target) {
		this.target = target;
	}
	public Double getScalling() {
		return scalling;
	}
	public void setScalling(Double scalling) {
		this.scalling = scalling;
	}
	public String[] getHeader() {
		return header;
	}
	public void setHeader(String[] header) {
		this.header = header;
	}
	public List<GetBucketSolverList> getList_solver() {
		return list_solver;
	}
	public void setList_solver(List<GetBucketSolverList> list_solver) {
		this.list_solver = list_solver;
	}
}
