package com.hanifalbaaits.java.modelRespon;

public class EclSkenario {
	
	private double stdev_var1;
	private double stdev_var2;
	private double normal_var1;
	private double normal_var2;
	private double normal_probability;

	private double optimis_var1;
	private double optimis_var2;
	private double optimis_probability;
	
	private double pesimis_var1;
	private double pesimis_var2;
	private double pesimis_probability;
	
	private double weigthed_var1;
	private double weigthed_var2;
	public double getStdev_var1() {
		return stdev_var1;
	}
	public void setStdev_var1(double stdev_var1) {
		this.stdev_var1 = stdev_var1;
	}
	public double getStdev_var2() {
		return stdev_var2;
	}
	public void setStdev_var2(double stdev_var2) {
		this.stdev_var2 = stdev_var2;
	}
	public double getNormal_var1() {
		return normal_var1;
	}
	public void setNormal_var1(double normal_var1) {
		this.normal_var1 = normal_var1;
	}
	public double getNormal_var2() {
		return normal_var2;
	}
	public void setNormal_var2(double normal_var2) {
		this.normal_var2 = normal_var2;
	}
	public double getNormal_probability() {
		return normal_probability;
	}
	public void setNormal_probability(double normal_probability) {
		this.normal_probability = normal_probability;
	}
	public double getOptimis_var1() {
		return optimis_var1;
	}
	public void setOptimis_var1(double optimis_var1) {
		this.optimis_var1 = optimis_var1;
	}
	public double getOptimis_var2() {
		return optimis_var2;
	}
	public void setOptimis_var2(double optimis_var2) {
		this.optimis_var2 = optimis_var2;
	}
	public double getOptimis_probability() {
		return optimis_probability;
	}
	public void setOptimis_probability(double optimis_probability) {
		this.optimis_probability = optimis_probability;
	}
	public double getPesimis_var1() {
		return pesimis_var1;
	}
	public void setPesimis_var1(double pesimis_var1) {
		this.pesimis_var1 = pesimis_var1;
	}
	public double getPesimis_var2() {
		return pesimis_var2;
	}
	public void setPesimis_var2(double pesimis_var2) {
		this.pesimis_var2 = pesimis_var2;
	}
	public double getPesimis_probability() {
		return pesimis_probability;
	}
	public void setPesimis_probability(double pesimis_probability) {
		this.pesimis_probability = pesimis_probability;
	}
	public double getWeigthed_var1() {
		return weigthed_var1;
	}
	public void setWeigthed_var1(double weigthed_var1) {
		this.weigthed_var1 = weigthed_var1;
	}
	public double getWeigthed_var2() {
		return weigthed_var2;
	}
	public void setWeigthed_var2(double weigthed_var2) {
		this.weigthed_var2 = weigthed_var2;
	}

}
