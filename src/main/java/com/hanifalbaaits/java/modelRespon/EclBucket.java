package com.hanifalbaaits.java.modelRespon;

import java.util.List;

import com.hanifalbaaits.java.modelDB.InputModelFL;

public class EclBucket {
	
	private int id_model;
	private int periode_forecast;
	
	private InputModelFL model;
	private String periode;
	private EclSkenario skenario;
	
	//UNTUK NO MODEL
	
	private List<Ecl> no_model_list;
	private double no_model_sum_ead;
	private double no_model_sum_ecl;
	
	//UNTUK PROPORSIONAL

	private double logit_expected_psak;
	private double expected_psak;
	
	private double expected;
	private double last;
	private double mev;
	
	private List<Ecl> proporsional_list;
	private double proporsional_sum_ead;
	private double proporsional_sum_ecl;
	private double proporsional_sum_ecl_forward_looking;
	
	//UNTUK SOLVER
	private double pd_forward_looking;
	
	private List<Ecl> solver_list;
	private double solver_average_pd_base;
	private double solver_average_pd_forward_looking;
	private double solver_sum_ead;
	private double solver_sum_ecl;
	private double solver_sum_ecl_forward_looking;
	
	private double target;
	private double scaling;
	public int getId_model() {
		return id_model;
	}
	public void setId_model(int id_model) {
		this.id_model = id_model;
	}
	public int getPeriode_forecast() {
		return periode_forecast;
	}
	public void setPeriode_forecast(int periode_forecast) {
		this.periode_forecast = periode_forecast;
	}
	public InputModelFL getModel() {
		return model;
	}
	public void setModel(InputModelFL model) {
		this.model = model;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public EclSkenario getSkenario() {
		return skenario;
	}
	public void setSkenario(EclSkenario skenario) {
		this.skenario = skenario;
	}
	public List<Ecl> getNo_model_list() {
		return no_model_list;
	}
	public void setNo_model_list(List<Ecl> no_model_list) {
		this.no_model_list = no_model_list;
	}
	public double getNo_model_sum_ead() {
		return no_model_sum_ead;
	}
	public void setNo_model_sum_ead(double no_model_sum_ead) {
		this.no_model_sum_ead = no_model_sum_ead;
	}
	public double getNo_model_sum_ecl() {
		return no_model_sum_ecl;
	}
	public void setNo_model_sum_ecl(double no_model_sum_ecl) {
		this.no_model_sum_ecl = no_model_sum_ecl;
	}
	public double getLogit_expected_psak() {
		return logit_expected_psak;
	}
	public void setLogit_expected_psak(double logit_expected_psak) {
		this.logit_expected_psak = logit_expected_psak;
	}
	public double getExpected_psak() {
		return expected_psak;
	}
	public void setExpected_psak(double expected_psak) {
		this.expected_psak = expected_psak;
	}
	public double getExpected() {
		return expected;
	}
	public void setExpected(double expected) {
		this.expected = expected;
	}
	public double getLast() {
		return last;
	}
	public void setLast(double last) {
		this.last = last;
	}
	public double getMev() {
		return mev;
	}
	public void setMev(double mev) {
		this.mev = mev;
	}
	public List<Ecl> getProporsional_list() {
		return proporsional_list;
	}
	public void setProporsional_list(List<Ecl> proporsional_list) {
		this.proporsional_list = proporsional_list;
	}
	public double getProporsional_sum_ead() {
		return proporsional_sum_ead;
	}
	public void setProporsional_sum_ead(double proporsional_sum_ead) {
		this.proporsional_sum_ead = proporsional_sum_ead;
	}
	public double getProporsional_sum_ecl() {
		return proporsional_sum_ecl;
	}
	public void setProporsional_sum_ecl(double proporsional_sum_ecl) {
		this.proporsional_sum_ecl = proporsional_sum_ecl;
	}
	public double getProporsional_sum_ecl_forward_looking() {
		return proporsional_sum_ecl_forward_looking;
	}
	public void setProporsional_sum_ecl_forward_looking(double proporsional_sum_ecl_forward_looking) {
		this.proporsional_sum_ecl_forward_looking = proporsional_sum_ecl_forward_looking;
	}
	public List<Ecl> getSolver_list() {
		return solver_list;
	}
	public void setSolver_list(List<Ecl> solver_list) {
		this.solver_list = solver_list;
	}
	public double getSolver_average_pd_base() {
		return solver_average_pd_base;
	}
	public void setSolver_average_pd_base(double solver_average_pd_base) {
		this.solver_average_pd_base = solver_average_pd_base;
	}
	public double getSolver_average_pd_forward_looking() {
		return solver_average_pd_forward_looking;
	}
	public void setSolver_average_pd_forward_looking(double solver_average_pd_forward_looking) {
		this.solver_average_pd_forward_looking = solver_average_pd_forward_looking;
	}
	public double getSolver_sum_ead() {
		return solver_sum_ead;
	}
	public void setSolver_sum_ead(double solver_sum_ead) {
		this.solver_sum_ead = solver_sum_ead;
	}
	public double getSolver_sum_ecl() {
		return solver_sum_ecl;
	}
	public void setSolver_sum_ecl(double solver_sum_ecl) {
		this.solver_sum_ecl = solver_sum_ecl;
	}
	public double getSolver_sum_ecl_forward_looking() {
		return solver_sum_ecl_forward_looking;
	}
	public void setSolver_sum_ecl_forward_looking(double solver_sum_ecl_forward_looking) {
		this.solver_sum_ecl_forward_looking = solver_sum_ecl_forward_looking;
	}
	public double getTarget() {
		return target;
	}
	public void setTarget(double target) {
		this.target = target;
	}
	public double getScaling() {
		return scaling;
	}
	public void setScaling(double scaling) {
		this.scaling = scaling;
	}
	public double getPd_forward_looking() {
		return pd_forward_looking;
	}
	public void setPd_forward_looking(double pd_forward_looking) {
		this.pd_forward_looking = pd_forward_looking;
	}
	
}
