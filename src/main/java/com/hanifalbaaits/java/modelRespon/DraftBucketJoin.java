package com.hanifalbaaits.java.modelRespon;

import java.util.Date;
import java.util.List;

import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.User;

public class DraftBucketJoin {
	
	private int id_draft_bucket;
	private String kode_draft_bucket;
	private String nama_draft_bucket;
	private Segment segment;
	private int id_draft;
	private int id_segment;
	private int hari_first;
	private int interval_bucket;
	private int jumlah_bucket;
	private String flag_normalisasi;
	private int moving_titik;
	private String start_date;
	private String end_date;
	private int created_by;
	private User created_user;
	private Date created_date;
	
	private GetBucket araging;
	private GetBucket rollrate_belum_normal;
	private GetBucket rollrate_normal;
	private GetBucket average_rollrate_lossrate;
	private GetBucket rollrate_normalisasi_average;
	private GetBucket lossrate;
	private GetBucket moving_average;
	private GetBucket sum_delta_lossrate;
	private GetBucket odr;
	private List<KomentarJoin> list_komentar;
	public int getId_draft_bucket() {
		return id_draft_bucket;
	}
	public void setId_draft_bucket(int id_draft_bucket) {
		this.id_draft_bucket = id_draft_bucket;
	}
	public String getKode_draft_bucket() {
		return kode_draft_bucket;
	}
	public void setKode_draft_bucket(String kode_draft_bucket) {
		this.kode_draft_bucket = kode_draft_bucket;
	}
	public String getNama_draft_bucket() {
		return nama_draft_bucket;
	}
	public void setNama_draft_bucket(String nama_draft_bucket) {
		this.nama_draft_bucket = nama_draft_bucket;
	}
	public int getId_draft() {
		return id_draft;
	}
	public void setId_draft(int id_draft) {
		this.id_draft = id_draft;
	}
	public int getId_segment() {
		return id_segment;
	}
	public void setId_segment(int id_segment) {
		this.id_segment = id_segment;
	}
	public int getHari_first() {
		return hari_first;
	}
	public void setHari_first(int hari_first) {
		this.hari_first = hari_first;
	}
	public int getInterval_bucket() {
		return interval_bucket;
	}
	public void setInterval_bucket(int interval_bucket) {
		this.interval_bucket = interval_bucket;
	}
	public int getJumlah_bucket() {
		return jumlah_bucket;
	}
	public void setJumlah_bucket(int jumlah_bucket) {
		this.jumlah_bucket = jumlah_bucket;
	}
	public String getFlag_normalisasi() {
		return flag_normalisasi;
	}
	public void setFlag_normalisasi(String flag_normalisasi) {
		this.flag_normalisasi = flag_normalisasi;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public GetBucket getAraging() {
		return araging;
	}
	public void setAraging(GetBucket araging) {
		this.araging = araging;
	}
	public GetBucket getAverage_rollrate_lossrate() {
		return average_rollrate_lossrate;
	}
	public void setAverage_rollrate_lossrate(GetBucket average_rollrate_lossrate) {
		this.average_rollrate_lossrate = average_rollrate_lossrate;
	}
	public GetBucket getLossrate() {
		return lossrate;
	}
	public void setLossrate(GetBucket lossrate) {
		this.lossrate = lossrate;
	}
	public GetBucket getSum_delta_lossrate() {
		return sum_delta_lossrate;
	}
	public void setSum_delta_lossrate(GetBucket sum_delta_lossrate) {
		this.sum_delta_lossrate = sum_delta_lossrate;
	}
	public GetBucket getOdr() {
		return odr;
	}
	public void setOdr(GetBucket odr) {
		this.odr = odr;
	}
	public User getCreated_user() {
		return created_user;
	}
	public void setCreated_user(User created_user) {
		this.created_user = created_user;
	}
	public int getMoving_titik() {
		return moving_titik;
	}
	public void setMoving_titik(int moving_titik) {
		this.moving_titik = moving_titik;
	}
	public GetBucket getRollrate_belum_normal() {
		return rollrate_belum_normal;
	}
	public void setRollrate_belum_normal(GetBucket rollrate_belum_normal) {
		this.rollrate_belum_normal = rollrate_belum_normal;
	}
	public GetBucket getRollrate_normal() {
		return rollrate_normal;
	}
	public void setRollrate_normal(GetBucket rollrate_normal) {
		this.rollrate_normal = rollrate_normal;
	}
	public GetBucket getRollrate_normalisasi_average() {
		return rollrate_normalisasi_average;
	}
	public void setRollrate_normalisasi_average(GetBucket rollrate_normalisasi_average) {
		this.rollrate_normalisasi_average = rollrate_normalisasi_average;
	}
	public GetBucket getMoving_average() {
		return moving_average;
	}
	public void setMoving_average(GetBucket moving_average) {
		this.moving_average = moving_average;
	}
	public Segment getSegment() {
		return segment;
	}
	public void setSegment(Segment segment) {
		this.segment = segment;
	}
	public List<KomentarJoin> getList_komentar() {
		return list_komentar;
	}
	public void setList_komentar(List<KomentarJoin> list_komentar) {
		this.list_komentar = list_komentar;
	}
	
}
