package com.hanifalbaaits.java.modelRespon;

public class Mape {
	
	private String periode;
	private int periode_bulan;
	private int periode_tahun;
	private double nilai_var1;
	private double nilai_var2;
	private double logit_expected;
	private double expected;
	private double actual;
	private double mape;
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public int getPeriode_bulan() {
		return periode_bulan;
	}
	public void setPeriode_bulan(int periode_bulan) {
		this.periode_bulan = periode_bulan;
	}
	public int getPeriode_tahun() {
		return periode_tahun;
	}
	public void setPeriode_tahun(int periode_tahun) {
		this.periode_tahun = periode_tahun;
	}
	public double getNilai_var1() {
		return nilai_var1;
	}
	public void setNilai_var1(double nilai_var1) {
		this.nilai_var1 = nilai_var1;
	}
	public double getNilai_var2() {
		return nilai_var2;
	}
	public void setNilai_var2(double nilai_var2) {
		this.nilai_var2 = nilai_var2;
	}
	public double getLogit_expected() {
		return logit_expected;
	}
	public void setLogit_expected(double logit_expected) {
		this.logit_expected = logit_expected;
	}
	public double getExpected() {
		return expected;
	}
	public void setExpected(double expected) {
		this.expected = expected;
	}
	public double getActual() {
		return actual;
	}
	public void setActual(double actual) {
		this.actual = actual;
	}
	public double getMape() {
		return mape;
	}
	public void setMape(double mape) {
		this.mape = mape;
	}
}
