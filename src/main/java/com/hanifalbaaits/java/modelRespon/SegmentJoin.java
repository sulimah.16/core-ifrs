package com.hanifalbaaits.java.modelRespon;

public class SegmentJoin {

	private int id_segment;
	private String kode_segment;
	private String nama_segment;
	private int created_by;
	private String created_date;
	private int updated_by;
	private String updated_date;
	public int getId_segment() {
		return id_segment;
	}
	public void setId_segment(int id_segment) {
		this.id_segment = id_segment;
	}
	public String getKode_segment() {
		return kode_segment;
	}
	public void setKode_segment(String kode_segment) {
		this.kode_segment = kode_segment;
	}
	public String getNama_segment() {
		return nama_segment;
	}
	public void setNama_segment(String nama_segment) {
		this.nama_segment = nama_segment;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public String getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}
}
