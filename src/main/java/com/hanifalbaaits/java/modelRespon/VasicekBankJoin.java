package com.hanifalbaaits.java.modelRespon;

import com.hanifalbaaits.java.modelDB.VasicekRating;

public class VasicekBankJoin {

	private int id_vas_bank;
	private String nama_bank;
	private String akronim;
	private int id_rating;
	private String referensi;
	private int tahun;
	private double nilai;
	private VasicekRating rating;
	public int getId_vas_bank() {
		return id_vas_bank;
	}
	public void setId_vas_bank(int id_vas_bank) {
		this.id_vas_bank = id_vas_bank;
	}
	public String getNama_bank() {
		return nama_bank;
	}
	public void setNama_bank(String nama_bank) {
		this.nama_bank = nama_bank;
	}
	public String getAkronim() {
		return akronim;
	}
	public void setAkronim(String akronim) {
		this.akronim = akronim;
	}
	public int getId_rating() {
		return id_rating;
	}
	public void setId_rating(int id_rating) {
		this.id_rating = id_rating;
	}
	public VasicekRating getRating() {
		return rating;
	}
	public void setRating(VasicekRating rating) {
		this.rating = rating;
	}
	public int getTahun() {
		return tahun;
	}
	public void setTahun(int tahun) {
		this.tahun = tahun;
	}
	public double getNilai() {
		return nilai;
	}
	public void setNilai(double nilai) {
		this.nilai = nilai;
	}
	public String getReferensi() {
		return referensi;
	}
	public void setReferensi(String referensi) {
		this.referensi = referensi;
	}
	
}
