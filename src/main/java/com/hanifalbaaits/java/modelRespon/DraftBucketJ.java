package com.hanifalbaaits.java.modelRespon;

import java.util.Date;
import java.util.List;

import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.User;

public class DraftBucketJ {

	private int id_draft_bucket;
	private String kode_draft_bucket;
	private String nama_draft_bucket;
	private Segment segment;
	private int id_draft;
	private int id_segment;
	private int hari_first;
	private int interval_bucket;
	private int jumlah_bucket;
	private String flag_normalisasi;
	private int moving_average;
	private Date start_date;
	private Date end_date;
	private int created_by;
	private Date created_date;
	private User dibuat_oleh;
	private String dibuat_tgl;
	private List<KomentarJoin> list_komentar;
	
	public int getId_draft_bucket() {
		return id_draft_bucket;
	}
	public void setId_draft_bucket(int id_draft_bucket) {
		this.id_draft_bucket = id_draft_bucket;
	}
	public String getKode_draft_bucket() {
		return kode_draft_bucket;
	}
	public void setKode_draft_bucket(String kode_draft_bucket) {
		this.kode_draft_bucket = kode_draft_bucket;
	}
	public String getNama_draft_bucket() {
		return nama_draft_bucket;
	}
	public void setNama_draft_bucket(String nama_draft_bucket) {
		this.nama_draft_bucket = nama_draft_bucket;
	}
	public int getId_draft() {
		return id_draft;
	}
	public void setId_draft(int id_draft) {
		this.id_draft = id_draft;
	}
	public int getId_segment() {
		return id_segment;
	}
	public void setId_segment(int id_segment) {
		this.id_segment = id_segment;
	}
	public int getHari_first() {
		return hari_first;
	}
	public void setHari_first(int hari_first) {
		this.hari_first = hari_first;
	}
	public int getInterval_bucket() {
		return interval_bucket;
	}
	public void setInterval_bucket(int interval_bucket) {
		this.interval_bucket = interval_bucket;
	}
	public int getJumlah_bucket() {
		return jumlah_bucket;
	}
	public void setJumlah_bucket(int jumlah_bucket) {
		this.jumlah_bucket = jumlah_bucket;
	}
	public String getFlag_normalisasi() {
		return flag_normalisasi;
	}
	public void setFlag_normalisasi(String flag_normalisasi) {
		this.flag_normalisasi = flag_normalisasi;
	}
	public int getMoving_average() {
		return moving_average;
	}
	public void setMoving_average(int moving_average) {
		this.moving_average = moving_average;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public User getDibuat_oleh() {
		return dibuat_oleh;
	}
	public void setDibuat_oleh(User dibuat_oleh) {
		this.dibuat_oleh = dibuat_oleh;
	}
	public String getDibuat_tgl() {
		return dibuat_tgl;
	}
	public void setDibuat_tgl(String dibuat_tgl) {
		this.dibuat_tgl = dibuat_tgl;
	}
	public Segment getSegment() {
		return segment;
	}
	public void setSegment(Segment segment) {
		this.segment = segment;
	}
	public List<KomentarJoin> getList_komentar() {
		return list_komentar;
	}
	public void setList_komentar(List<KomentarJoin> list_komentar) {
		this.list_komentar = list_komentar;
	}
	
}
