package com.hanifalbaaits.java.modelRespon;

import java.util.List;

import com.hanifalbaaits.java.modelDB.DraftBucket;
import com.hanifalbaaits.java.modelDB.InputModelFL;
import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.User;

public class DraftEclJoin {
	
	private int id_draft_ecl;
	private int id_draft_bucket;
	private int id_model;
	private DraftBucket draft_bucket;
	private Segment segment;
	private InputModelFL model;
	private int id_regresi;
	private int month_forecast;
	private String periode;
	private double lgd;
	private double normal;
	private double optimis;
	private double pesimis;
	private double average_mape;
	private double ecl_no_model;
	private double ecl_proporsional;
	private double ecl_solver;
	private int result_flag_model;
	private double result_saldo_exiting;
	private double result_saldo_selisih;
	private int created_by;
	private User created_user;
	private int updated_by;
	private User updated_user;
	private String created_date;
	private String updated_date;
	private List<KomentarJoin> list_komentar;
	private MapeDraft mape_draft;
	private List<EclBucket> ecl_list;
	
	public int getId_draft_ecl() {
		return id_draft_ecl;
	}
	public void setId_draft_ecl(int id_draft_ecl) {
		this.id_draft_ecl = id_draft_ecl;
	}
	public int getId_draft_bucket() {
		return id_draft_bucket;
	}
	public void setId_draft_bucket(int id_draft_bucket) {
		this.id_draft_bucket = id_draft_bucket;
	}
	public int getId_model() {
		return id_model;
	}
	public void setId_model(int id_model) {
		this.id_model = id_model;
	}
	public DraftBucket getDraft_bucket() {
		return draft_bucket;
	}
	public void setDraft_bucket(DraftBucket draft_bucket) {
		this.draft_bucket = draft_bucket;
	}
	public InputModelFL getModel() {
		return model;
	}
	public void setModel(InputModelFL model) {
		this.model = model;
	}
	public int getId_regresi() {
		return id_regresi;
	}
	public void setId_regresi(int id_regresi) {
		this.id_regresi = id_regresi;
	}
	public int getMonth_forecast() {
		return month_forecast;
	}
	public void setMonth_forecast(int month_forecast) {
		this.month_forecast = month_forecast;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public double getLgd() {
		return lgd;
	}
	public void setLgd(double lgd) {
		this.lgd = lgd;
	}
	public double getNormal() {
		return normal;
	}
	public void setNormal(double normal) {
		this.normal = normal;
	}
	public double getOptimis() {
		return optimis;
	}
	public void setOptimis(double optimis) {
		this.optimis = optimis;
	}
	public double getPesimis() {
		return pesimis;
	}
	public void setPesimis(double pesimis) {
		this.pesimis = pesimis;
	}
	public double getAverage_mape() {
		return average_mape;
	}
	public void setAverage_mape(double average_mape) {
		this.average_mape = average_mape;
	}
	public double getEcl_no_model() {
		return ecl_no_model;
	}
	public void setEcl_no_model(double ecl_no_model) {
		this.ecl_no_model = ecl_no_model;
	}
	public double getEcl_proporsional() {
		return ecl_proporsional;
	}
	public void setEcl_proporsional(double ecl_proporsional) {
		this.ecl_proporsional = ecl_proporsional;
	}
	public double getEcl_solver() {
		return ecl_solver;
	}
	public void setEcl_solver(double ecl_solver) {
		this.ecl_solver = ecl_solver;
	}
	public User getCreated_user() {
		return created_user;
	}
	public void setCreated_user(User created_user) {
		this.created_user = created_user;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public Segment getSegment() {
		return segment;
	}
	public void setSegment(Segment segment) {
		this.segment = segment;
	}
	public List<KomentarJoin> getList_komentar() {
		return list_komentar;
	}
	public void setList_komentar(List<KomentarJoin> list_komentar) {
		this.list_komentar = list_komentar;
	}
	public int getResult_flag_model() {
		return result_flag_model;
	}
	public void setResult_flag_model(int result_flag_model) {
		this.result_flag_model = result_flag_model;
	}
	public double getResult_saldo_exiting() {
		return result_saldo_exiting;
	}
	public void setResult_saldo_exiting(double result_saldo_exiting) {
		this.result_saldo_exiting = result_saldo_exiting;
	}
	public double getResult_saldo_selisih() {
		return result_saldo_selisih;
	}
	public void setResult_saldo_selisih(double result_saldo_selisih) {
		this.result_saldo_selisih = result_saldo_selisih;
	}
	public MapeDraft getMape_draft() {
		return mape_draft;
	}
	public void setMape_draft(MapeDraft mape_draft) {
		this.mape_draft = mape_draft;
	}
	public List<EclBucket> getEcl_list() {
		return ecl_list;
	}
	public void setEcl_list(List<EclBucket> ecl_list) {
		this.ecl_list = ecl_list;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public User getUpdated_user() {
		return updated_user;
	}
	public void setUpdated_user(User updated_user) {
		this.updated_user = updated_user;
	}
	public String getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}
}
