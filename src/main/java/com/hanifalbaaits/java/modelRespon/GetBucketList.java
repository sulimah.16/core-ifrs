package com.hanifalbaaits.java.modelRespon;

public class GetBucketList {
	
	private String periode;
	private int periode_bulan;
	private int periode_tahun;
	private Double [] nilai = {};
	private Double jumlah_outstanding;
	private Double average;
	private Double sum;
	private Double persentase_odr;
	
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public Double[] getNilai() {
		return nilai;
	}
	public void setNilai(Double[] nilai) {
		this.nilai = nilai;
	}
	public Double getJumlah_outstanding() {
		return jumlah_outstanding;
	}
	public void setJumlah_outstanding(Double jumlah_outstanding) {
		this.jumlah_outstanding = jumlah_outstanding;
	}
	public int getPeriode_bulan() {
		return periode_bulan;
	}
	public void setPeriode_bulan(int periode_bulan) {
		this.periode_bulan = periode_bulan;
	}
	public int getPeriode_tahun() {
		return periode_tahun;
	}
	public void setPeriode_tahun(int periode_tahun) {
		this.periode_tahun = periode_tahun;
	}
	public Double getAverage() {
		return average;
	}
	public void setAverage(Double average) {
		this.average = average;
	}
	public Double getSum() {
		return sum;
	}
	public void setSum(Double sum) {
		this.sum = sum;
	}
	public Double getPersentase_odr() {
		return persentase_odr;
	}
	public void setPersentase_odr(Double persentase_odr) {
		this.persentase_odr = persentase_odr;
	}
}
