package com.hanifalbaaits.java.modelRespon;

import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.User;

public class DataInputJoin {
	private int id_input;
	private int id_draft;
	private Draft draft;
	private int id_segment;
	private Segment segment;
	private String tanggal_piutang;
	private int periode_bulan;
	private int periode_tahun;
	private String periode;
	private String id_piutang;
	private String kode_customer;
	private String nama_customer;
	private String kode_proyek;
	private String nama_proyek;
	private String pemberi_kerja;
	private String doc_number;
	private String doc_type;
	private String jenis_piutang;
	private int umur_piutang_hari;
	private double saldo_piutang;
	private String path_document;
	private int created_by;
	private User created_user;
	private String created_date;
	private int updated_by;
	private String updated_date;
	public int getId_input() {
		return id_input;
	}
	public void setId_input(int id_input) {
		this.id_input = id_input;
	}
	public int getId_draft() {
		return id_draft;
	}
	public void setId_draft(int id_draft) {
		this.id_draft = id_draft;
	}
	public Draft getDraft() {
		return draft;
	}
	public void setDraft(Draft draft) {
		this.draft = draft;
	}
	public int getId_segment() {
		return id_segment;
	}
	public void setId_segment(int id_segment) {
		this.id_segment = id_segment;
	}
	public Segment getSegment() {
		return segment;
	}
	public void setSegment(Segment segment) {
		this.segment = segment;
	}
	public int getPeriode_bulan() {
		return periode_bulan;
	}
	public void setPeriode_bulan(int periode_bulan) {
		this.periode_bulan = periode_bulan;
	}
	public int getPeriode_tahun() {
		return periode_tahun;
	}
	public void setPeriode_tahun(int periode_tahun) {
		this.periode_tahun = periode_tahun;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public String getId_piutang() {
		return id_piutang;
	}
	public void setId_piutang(String id_piutang) {
		this.id_piutang = id_piutang;
	}
	public String getKode_customer() {
		return kode_customer;
	}
	public void setKode_customer(String kode_customer) {
		this.kode_customer = kode_customer;
	}
	public String getNama_customer() {
		return nama_customer;
	}
	public void setNama_customer(String nama_customer) {
		this.nama_customer = nama_customer;
	}
	public String getKode_proyek() {
		return kode_proyek;
	}
	public void setKode_proyek(String kode_proyek) {
		this.kode_proyek = kode_proyek;
	}
	public String getNama_proyek() {
		return nama_proyek;
	}
	public void setNama_proyek(String nama_proyek) {
		this.nama_proyek = nama_proyek;
	}
	public String getPemberi_kerja() {
		return pemberi_kerja;
	}
	public void setPemberi_kerja(String pemberi_kerja) {
		this.pemberi_kerja = pemberi_kerja;
	}
	public String getDoc_number() {
		return doc_number;
	}
	public void setDoc_number(String doc_number) {
		this.doc_number = doc_number;
	}
	public String getDoc_type() {
		return doc_type;
	}
	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}
	public String getJenis_piutang() {
		return jenis_piutang;
	}
	public void setJenis_piutang(String jenis_piutang) {
		this.jenis_piutang = jenis_piutang;
	}
	public double getSaldo_piutang() {
		return saldo_piutang;
	}
	public void setSaldo_piutang(double saldo_piutang) {
		this.saldo_piutang = saldo_piutang;
	}
	public String getPath_document() {
		return path_document;
	}
	public void setPath_document(String path_document) {
		this.path_document = path_document;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public User getCreated_user() {
		return created_user;
	}
	public void setCreated_user(User created_user) {
		this.created_user = created_user;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public String getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}
	public String getTanggal_piutang() {
		return tanggal_piutang;
	}
	public void setTanggal_piutang(String tanggal_piutang) {
		this.tanggal_piutang = tanggal_piutang;
	}
	public int getUmur_piutang_hari() {
		return umur_piutang_hari;
	}
	public void setUmur_piutang_hari(int umur_piutang_hari) {
		this.umur_piutang_hari = umur_piutang_hari;
	}
}
