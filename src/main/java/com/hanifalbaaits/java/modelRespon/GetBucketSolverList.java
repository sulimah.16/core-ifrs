package com.hanifalbaaits.java.modelRespon;

public class GetBucketSolverList {

	private String nama_bucket;
	private Double [] nilai;
	public String getNama_bucket() {
		return nama_bucket;
	}
	public void setNama_bucket(String nama_bucket) {
		this.nama_bucket = nama_bucket;
	}
	public Double[] getNilai() {
		return nilai;
	}
	public void setNilai(Double[] nilai) {
		this.nilai = nilai;
	}
}
