package com.hanifalbaaits.java.modelRespon;

import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelDB.VasicekAset;
import com.hanifalbaaits.java.modelDB.VasicekAsetDraft;

public class AsetListJoin {
	
	private int id_list_aset;
	private int id_jenis;
	private int id_bank;
	private int id_draft;
	private VasicekAsetDraft draft;
	private VasicekAset aset;
	private VasicekBankJoin bank;
	private String no_rekening;
	private String mata_uang;
	private double suku_bunga;
	private String tgl_jatuh_tempo;
	private double saldo;
	private int created_by;
	private User created_user;
	private String created_date;
	private int updated_by;
	private User updated_user;
	private String updated_date;
	public int getId_list_aset() {
		return id_list_aset;
	}
	public void setId_list_aset(int id_list_aset) {
		this.id_list_aset = id_list_aset;
	}
	public int getId_jenis() {
		return id_jenis;
	}
	public void setId_jenis(int id_jenis) {
		this.id_jenis = id_jenis;
	}
	public int getId_bank() {
		return id_bank;
	}
	public void setId_bank(int id_bank) {
		this.id_bank = id_bank;
	}
	public VasicekAset getAset() {
		return aset;
	}
	public void setAset(VasicekAset aset) {
		this.aset = aset;
	}
	public VasicekBankJoin getBank() {
		return bank;
	}
	public void setBank(VasicekBankJoin bank) {
		this.bank = bank;
	}
	public String getNo_rekening() {
		return no_rekening;
	}
	public void setNo_rekening(String no_rekening) {
		this.no_rekening = no_rekening;
	}
	public String getMata_uang() {
		return mata_uang;
	}
	public void setMata_uang(String mata_uang) {
		this.mata_uang = mata_uang;
	}
	public double getSuku_bunga() {
		return suku_bunga;
	}
	public void setSuku_bunga(double suku_bunga) {
		this.suku_bunga = suku_bunga;
	}
	public String getTgl_jatuh_tempo() {
		return tgl_jatuh_tempo;
	}
	public void setTgl_jatuh_tempo(String tgl_jatuh_tempo) {
		this.tgl_jatuh_tempo = tgl_jatuh_tempo;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public User getCreated_user() {
		return created_user;
	}
	public void setCreated_user(User created_user) {
		this.created_user = created_user;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public User getUpdated_user() {
		return updated_user;
	}
	public void setUpdated_user(User updated_user) {
		this.updated_user = updated_user;
	}
	public String getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public int getId_draft() {
		return id_draft;
	}
	public void setId_draft(int id_draft) {
		this.id_draft = id_draft;
	}
	public VasicekAsetDraft getDraft() {
		return draft;
	}
	public void setDraft(VasicekAsetDraft draft) {
		this.draft = draft;
	}
}
