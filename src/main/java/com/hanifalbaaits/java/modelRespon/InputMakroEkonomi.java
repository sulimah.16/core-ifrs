package com.hanifalbaaits.java.modelRespon;

public class InputMakroEkonomi {

	private String skenario;
	private Double gdp;
	private Double bi7;
	private Double probabilty;
	public String getSkenario() {
		return skenario;
	}
	public void setSkenario(String skenario) {
		this.skenario = skenario;
	}
	public Double getGdp() {
		return gdp;
	}
	public void setGdp(Double gdp) {
		this.gdp = gdp;
	}
	public Double getBi7() {
		return bi7;
	}
	public void setBi7(Double bi7) {
		this.bi7 = bi7;
	}
	public Double getProbabilty() {
		return probabilty;
	}
	public void setProbabilty(Double probabilty) {
		this.probabilty = probabilty;
	}
	
}
