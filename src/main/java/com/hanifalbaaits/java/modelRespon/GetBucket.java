package com.hanifalbaaits.java.modelRespon;

import java.util.List;

import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.Segment;

public class GetBucket {

	private String title_bucket;
	private Draft draft;
	private Segment segment;
	private int hari_first_bucket;
	private int interval_bucket;
	private int jumlah_bucket;
	private String normalisasi;
	private String start_date;
	private String end_date;
	private List<String> header_bucket;
	private List<?> list_bucket;
	public String getTitle_bucket() {
		return title_bucket;
	}
	public void setTitle_bucket(String title_bucket) {
		this.title_bucket = title_bucket;
	}
	public Draft getDraft() {
		return draft;
	}
	public void setDraft(Draft draft) {
		this.draft = draft;
	}
	public Segment getSegment() {
		return segment;
	}
	public void setSegment(Segment segment) {
		this.segment = segment;
	}
	public int getInterval_bucket() {
		return interval_bucket;
	}
	public void setInterval_bucket(int interval_bucket) {
		this.interval_bucket = interval_bucket;
	}
	public int getJumlah_bucket() {
		return jumlah_bucket;
	}
	public void setJumlah_bucket(int jumlah_bucket) {
		this.jumlah_bucket = jumlah_bucket;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public List<String> getHeader_bucket() {
		return header_bucket;
	}
	public void setHeader_bucket(List<String> header_bucket) {
		this.header_bucket = header_bucket;
	}
	public List<?> getList_bucket() {
		return list_bucket;
	}
	public void setList_bucket(List<?> list_bucket) {
		this.list_bucket = list_bucket;
	}
	public int getHari_first_bucket() {
		return hari_first_bucket;
	}
	public void setHari_first_bucket(int hari_first_bucket) {
		this.hari_first_bucket = hari_first_bucket;
	}
	public String getNormalisasi() {
		return normalisasi;
	}
	public void setNormalisasi(String normalisasi) {
		this.normalisasi = normalisasi;
	}
}
