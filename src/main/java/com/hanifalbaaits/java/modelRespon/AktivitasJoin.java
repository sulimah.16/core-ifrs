package com.hanifalbaaits.java.modelRespon;

import com.hanifalbaaits.java.modelDB.User;

public class AktivitasJoin {

	private int id_aktivitas;
	private int id_user;
	private User user;
	private String method_path;
	private String caption;
	private String keterangan;
	private String device;
	private String ip;
	private String tanggal_aktivitas;
	public int getId_aktivitas() {
		return id_aktivitas;
	}
	public void setId_aktivitas(int id_aktivitas) {
		this.id_aktivitas = id_aktivitas;
	}
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public String getMethod_path() {
		return method_path;
	}
	public void setMethod_path(String method_path) {
		this.method_path = method_path;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getTanggal_aktivitas() {
		return tanggal_aktivitas;
	}
	public void setTanggal_aktivitas(String tanggal_aktivitas) {
		this.tanggal_aktivitas = tanggal_aktivitas;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
