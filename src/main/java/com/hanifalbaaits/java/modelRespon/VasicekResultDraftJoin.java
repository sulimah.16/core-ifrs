package com.hanifalbaaits.java.modelRespon;

import java.util.Date;
import java.util.List;

import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelDB.VasicekAsetDraft;

public class VasicekResultDraftJoin {
	
	private int id_vasicek_result_draft;
	private int id_draft_vasicek;
	private VasicekAsetDraft aset_draft;
	private int interval_bucket;
	private String periode_mulai;
	private String periode_selesai;
	private String header_macroeconomic;
	private String list_flg_projection;
	private double persen_normal;
	private double persen_uptum;
	private double persen_downtum;
	private double historical_mean;
	private double historical_stdev;
	private double confidence_level;
	private double t_distribution;
	private double saldo_normal_scan;
	private double saldo_weighted_scan;
	private double result_saldo_exiting;
	private double result_saldo_selisih;
	private double result_flag_model;
	private String periode_ecl;
	private int created_by;
	private Date created_date;
	private int updated_by;
	private Date updated_date;
	private User name_created;
	private User name_updated;
	private List<KomentarJoin> list_komentar;
	public int getId_vasicek_result_draft() {
		return id_vasicek_result_draft;
	}
	public void setId_vasicek_result_draft(int id_vasicek_result_draft) {
		this.id_vasicek_result_draft = id_vasicek_result_draft;
	}
	public int getId_draft_vasicek() {
		return id_draft_vasicek;
	}
	public void setId_draft_vasicek(int id_draft_vasicek) {
		this.id_draft_vasicek = id_draft_vasicek;
	}
	public int getInterval_bucket() {
		return interval_bucket;
	}
	public void setInterval_bucket(int interval_bucket) {
		this.interval_bucket = interval_bucket;
	}
	public String getPeriode_mulai() {
		return periode_mulai;
	}
	public void setPeriode_mulai(String periode_mulai) {
		this.periode_mulai = periode_mulai;
	}
	public String getPeriode_selesai() {
		return periode_selesai;
	}
	public void setPeriode_selesai(String periode_selesai) {
		this.periode_selesai = periode_selesai;
	}
	public String getHeader_macroeconomic() {
		return header_macroeconomic;
	}
	public void setHeader_macroeconomic(String header_macroeconomic) {
		this.header_macroeconomic = header_macroeconomic;
	}
	public String getList_flg_projection() {
		return list_flg_projection;
	}
	public void setList_flg_projection(String list_flg_projection) {
		this.list_flg_projection = list_flg_projection;
	}
	public double getPersen_normal() {
		return persen_normal;
	}
	public void setPersen_normal(double persen_normal) {
		this.persen_normal = persen_normal;
	}
	public double getPersen_uptum() {
		return persen_uptum;
	}
	public void setPersen_uptum(double persen_uptum) {
		this.persen_uptum = persen_uptum;
	}
	public double getPersen_downtum() {
		return persen_downtum;
	}
	public void setPersen_downtum(double persen_downtum) {
		this.persen_downtum = persen_downtum;
	}
	public double getHistorical_mean() {
		return historical_mean;
	}
	public void setHistorical_mean(double historical_mean) {
		this.historical_mean = historical_mean;
	}
	public double getHistorical_stdev() {
		return historical_stdev;
	}
	public void setHistorical_stdev(double historical_stdev) {
		this.historical_stdev = historical_stdev;
	}
	public double getConfidence_level() {
		return confidence_level;
	}
	public void setConfidence_level(double confidence_level) {
		this.confidence_level = confidence_level;
	}
	public double getT_distribution() {
		return t_distribution;
	}
	public void setT_distribution(double t_distribution) {
		this.t_distribution = t_distribution;
	}
	public String getPeriode_ecl() {
		return periode_ecl;
	}
	public void setPeriode_ecl(String periode_ecl) {
		this.periode_ecl = periode_ecl;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public User getName_created() {
		return name_created;
	}
	public void setName_created(User name_created) {
		this.name_created = name_created;
	}
	public User getName_updated() {
		return name_updated;
	}
	public void setName_updated(User name_updated) {
		this.name_updated = name_updated;
	}
	public VasicekAsetDraft getAset_draft() {
		return aset_draft;
	}
	public void setAset_draft(VasicekAsetDraft aset_draft) {
		this.aset_draft = aset_draft;
	}
	public List<KomentarJoin> getList_komentar() {
		return list_komentar;
	}
	public void setList_komentar(List<KomentarJoin> list_komentar) {
		this.list_komentar = list_komentar;
	}
	public double getSaldo_normal_scan() {
		return saldo_normal_scan;
	}
	public void setSaldo_normal_scan(double saldo_normal_scan) {
		this.saldo_normal_scan = saldo_normal_scan;
	}
	public double getSaldo_weighted_scan() {
		return saldo_weighted_scan;
	}
	public void setSaldo_weighted_scan(double saldo_weighted_scan) {
		this.saldo_weighted_scan = saldo_weighted_scan;
	}
	public double getResult_saldo_selisih() {
		return result_saldo_selisih;
	}
	public void setResult_saldo_selisih(double result_saldo_selisih) {
		this.result_saldo_selisih = result_saldo_selisih;
	}
	public double getResult_saldo_exiting() {
		return result_saldo_exiting;
	}
	public void setResult_saldo_exiting(double result_saldo_exiting) {
		this.result_saldo_exiting = result_saldo_exiting;
	}
	public double getResult_flag_model() {
		return result_flag_model;
	}
	public void setResult_flag_model(double result_flag_model) {
		this.result_flag_model = result_flag_model;
	}
	
}
