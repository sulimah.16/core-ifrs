package com.hanifalbaaits.java.modelRespon;

import com.hanifalbaaits.java.modelDB.Segment;

public class InputModelFLJoin {
	
	private int id_model;
	private String nomor;
	private String var1;
	private String var2;
	private int lag1;
	private int lag2;
	private double c_coef;
	private double c_coef_pvalue;
	private double koef_var1;
	private double koef_var1_pvalue;
	private double koef_var2;
	private double koef_var2_pvalue;
	private double adjr_square;
	private double f_prob;
	private int tanda;
	private int signifikan;
	private int id_segment;
	private int status;
	private Segment segment;
	
	public int getId_model() {
		return id_model;
	}
	public void setId_model(int id_model) {
		this.id_model = id_model;
	}
	public String getNomor() {
		return nomor;
	}
	public void setNomor(String nomor) {
		this.nomor = nomor;
	}
	public String getVar1() {
		return var1;
	}
	public void setVar1(String var1) {
		this.var1 = var1;
	}
	public String getVar2() {
		return var2;
	}
	public void setVar2(String var2) {
		this.var2 = var2;
	}
	public int getLag1() {
		return lag1;
	}
	public void setLag1(int lag1) {
		this.lag1 = lag1;
	}
	public int getLag2() {
		return lag2;
	}
	public void setLag2(int lag2) {
		this.lag2 = lag2;
	}
	public double getC_coef() {
		return c_coef;
	}
	public void setC_coef(double c_coef) {
		this.c_coef = c_coef;
	}
	public double getC_coef_pvalue() {
		return c_coef_pvalue;
	}
	public void setC_coef_pvalue(double c_coef_pvalue) {
		this.c_coef_pvalue = c_coef_pvalue;
	}
	public double getKoef_var1() {
		return koef_var1;
	}
	public void setKoef_var1(double koef_var1) {
		this.koef_var1 = koef_var1;
	}
	public double getKoef_var1_pvalue() {
		return koef_var1_pvalue;
	}
	public void setKoef_var1_pvalue(double koef_var1_pvalue) {
		this.koef_var1_pvalue = koef_var1_pvalue;
	}
	public double getKoef_var2() {
		return koef_var2;
	}
	public void setKoef_var2(double koef_var2) {
		this.koef_var2 = koef_var2;
	}
	public double getKoef_var2_pvalue() {
		return koef_var2_pvalue;
	}
	public void setKoef_var2_pvalue(double koef_var2_pvalue) {
		this.koef_var2_pvalue = koef_var2_pvalue;
	}
	public double getAdjr_square() {
		return adjr_square;
	}
	public void setAdjr_square(double adjr_square) {
		this.adjr_square = adjr_square;
	}
	public double getF_prob() {
		return f_prob;
	}
	public void setF_prob(double f_prob) {
		this.f_prob = f_prob;
	}
	public int getTanda() {
		return tanda;
	}
	public void setTanda(int tanda) {
		this.tanda = tanda;
	}
	public int getSignifikan() {
		return signifikan;
	}
	public void setSignifikan(int signifikan) {
		this.signifikan = signifikan;
	}
	public int getId_segment() {
		return id_segment;
	}
	public void setId_segment(int id_segment) {
		this.id_segment = id_segment;
	}
	public Segment getSegment() {
		return segment;
	}
	public void setSegment(Segment segment) {
		this.segment = segment;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
