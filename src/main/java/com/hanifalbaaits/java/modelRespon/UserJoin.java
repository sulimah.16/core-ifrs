package com.hanifalbaaits.java.modelRespon;

import com.hanifalbaaits.java.modelDB.UserRole;

public class UserJoin {

	private int id_user;
	private String username;
	private String password;
	private int id_role;
	private UserRole role;
	private int status;
	private String nm_status;
	private String email;
	private String nama;
	private String no_telp;
	private String alamat;
	private String divisi;
	private String jabatan;
	private int j_k;
	private String nm_jk;
	private String path_profile;
	private int created_by;
	private String created_date;
	private int updated_by;
	private String updated_date;
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getId_role() {
		return id_role;
	}
	public void setId_role(int id_role) {
		this.id_role = id_role;
	}
	public UserRole getRole() {
		return role;
	}
	public void setRole(UserRole role) {
		this.role = role;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getNm_status() {
		return nm_status;
	}
	public void setNm_status(String nm_status) {
		this.nm_status = nm_status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNo_telp() {
		return no_telp;
	}
	public void setNo_telp(String no_telp) {
		this.no_telp = no_telp;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public int getJ_k() {
		return j_k;
	}
	public void setJ_k(int j_k) {
		this.j_k = j_k;
	}
	public String getNm_jk() {
		return nm_jk;
	}
	public void setNm_jk(String nm_jk) {
		this.nm_jk = nm_jk;
	}
	public String getPath_profile() {
		return path_profile;
	}
	public void setPath_profile(String path_profile) {
		this.path_profile = path_profile;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public String getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}
	public String getDivisi() {
		return divisi;
	}
	public void setDivisi(String divisi) {
		this.divisi = divisi;
	}
	public String getJabatan() {
		return jabatan;
	}
	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}
}
