package com.hanifalbaaits.java.modelRespon;

public class UserLogin {
	private UserJoin user;
	private String token;
	public UserLogin(UserJoin user,String token) {
		this.user = user; this.token=token;
	}
	public UserJoin getUser() {
		return user;
	}
	public void setUser(UserJoin user) {
		this.user = user;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
