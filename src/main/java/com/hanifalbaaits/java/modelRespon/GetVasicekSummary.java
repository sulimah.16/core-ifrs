package com.hanifalbaaits.java.modelRespon;

import com.hanifalbaaits.java.modelDB.VasicekAset;

public class GetVasicekSummary {
	
	private int id_aset;
	private VasicekAset aset;
	private double saldo;
	private double ecl_normal_scan;
	private double ecl_weighted_scan;
	public int getId_aset() {
		return id_aset;
	}
	public void setId_aset(int id_aset) {
		this.id_aset = id_aset;
	}
	public VasicekAset getAset() {
		return aset;
	}
	public void setAset(VasicekAset aset) {
		this.aset = aset;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getEcl_normal_scan() {
		return ecl_normal_scan;
	}
	public void setEcl_normal_scan(double ecl_normal_scan) {
		this.ecl_normal_scan = ecl_normal_scan;
	}
	public double getEcl_weighted_scan() {
		return ecl_weighted_scan;
	}
	public void setEcl_weighted_scan(double ecl_weighted_scan) {
		this.ecl_weighted_scan = ecl_weighted_scan;
	}
}
