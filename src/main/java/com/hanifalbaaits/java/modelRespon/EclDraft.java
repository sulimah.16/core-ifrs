package com.hanifalbaaits.java.modelRespon;

import java.util.List;

public class EclDraft {
	
	private int input_regresi; // 1 2 3
	private String regresi; // ODR / average / delta loss
	private DraftBucketJoin draft_bucket;	
	private List<EclBucket> ecl_list;
	public int getInput_regresi() {
		return input_regresi;
	}
	public void setInput_regresi(int input_regresi) {
		this.input_regresi = input_regresi;
	}
	public String getRegresi() {
		return regresi;
	}
	public void setRegresi(String regresi) {
		this.regresi = regresi;
	}
	public DraftBucketJoin getDraft_bucket() {
		return draft_bucket;
	}
	public void setDraft_bucket(DraftBucketJoin draft_bucket) {
		this.draft_bucket = draft_bucket;
	}
	public List<EclBucket> getEcl_list() {
		return ecl_list;
	}
	public void setEcl_list(List<EclBucket> ecl_list) {
		this.ecl_list = ecl_list;
	}

}
