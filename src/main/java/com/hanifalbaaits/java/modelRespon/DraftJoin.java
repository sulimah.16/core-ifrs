package com.hanifalbaaits.java.modelRespon;

public class DraftJoin {

	private int id_draft;
	private String kode_draft;
	private String nama_draft;
	private int flag_draft;
	private int flag_insert;
	private String header_bucket;
	private int jumlah_bucket;
	private int created_by;
	private String created_date;
	private int updated_by;
	private String updated_date;
	public int getId_draft() {
		return id_draft;
	}
	public void setId_draft(int id_draft) {
		this.id_draft = id_draft;
	}
	public String getKode_draft() {
		return kode_draft;
	}
	public void setKode_draft(String kode_draft) {
		this.kode_draft = kode_draft;
	}
	public String getNama_draft() {
		return nama_draft;
	}
	public void setNama_draft(String nama_draft) {
		this.nama_draft = nama_draft;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public String getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}
	public int getFlag_draft() {
		return flag_draft;
	}
	public void setFlag_draft(int flag_draft) {
		this.flag_draft = flag_draft;
	}
	public int getFlag_insert() {
		return flag_insert;
	}
	public void setFlag_insert(int flag_insert) {
		this.flag_insert = flag_insert;
	}
	public String getHeader_bucket() {
		return header_bucket;
	}
	public void setHeader_bucket(String header_bucket) {
		this.header_bucket = header_bucket;
	}
	public int getJumlah_bucket() {
		return jumlah_bucket;
	}
	public void setJumlah_bucket(int jumlah_bucket) {
		this.jumlah_bucket = jumlah_bucket;
	}
}
