package com.hanifalbaaits.java.modelRespon;

import java.util.List;

import com.hanifalbaaits.java.modelDB.User;
import com.hanifalbaaits.java.modelDB.VasicekResultAset;
import com.hanifalbaaits.java.modelDB.VasicekResultDraft;
import com.hanifalbaaits.java.modelDB.VasicekResultMacro;
import com.hanifalbaaits.java.modelDB.VasicekResultPD;

public class GetVasicek {

	////DATA MASTER
	private VasicekResultDraftJoin result_draft;
	private List<VasicekResultAset> result_aset;
	private List<VasicekResultMacro> result_macro;
	private List<VasicekResultPD> result_pd;
	
	/////////////
	
	private String title_bucket;
	private int interval_bucket;
	private String periode_mulai;
	private String periode_selesai;	
	private List<String> header_macroeconomic;
	private int [] list_flg_projection;
	private List<GetVasicekMacroList> list_macroeconomic;
	private double persen_normal;
	private double persen_uptum;
	private double persen_downtum;
	private double historical_mean;
	private double historical_stdev;
	private double confidence_level;
	private double t_distribution;
	private List<GetVasicekPD> list_pd_vasicek;
	private String periode_ecl;
	private List<GetVasicekAset> list_aset;
	private List<GetVasicekSummary> list_summary;
	private List<KomentarJoin> list_komentar;
	private int created_by;
	private User created_user;
	private int updated_by;
	private User updated_user;
	private String created_date;
	private String updated_date;
	
	public String getTitle_bucket() {
		return title_bucket;
	}
	public void setTitle_bucket(String title_bucket) {
		this.title_bucket = title_bucket;
	}
	public int getInterval_bucket() {
		return interval_bucket;
	}
	public void setInterval_bucket(int interval_bucket) {
		this.interval_bucket = interval_bucket;
	}
	public List<String> getHeader_macroeconomic() {
		return header_macroeconomic;
	}
	public void setHeader_macroeconomic(List<String> header_macroeconomic) {
		this.header_macroeconomic = header_macroeconomic;
	}
	public List<GetVasicekMacroList> getList_macroeconomic() {
		return list_macroeconomic;
	}
	public void setList_macroeconomic(List<GetVasicekMacroList> list_macroeconomic) {
		this.list_macroeconomic = list_macroeconomic;
	}
	public double getPersen_normal() {
		return persen_normal;
	}
	public void setPersen_normal(double persen_normal) {
		this.persen_normal = persen_normal;
	}
	public double getPersen_uptum() {
		return persen_uptum;
	}
	public void setPersen_uptum(double persen_uptum) {
		this.persen_uptum = persen_uptum;
	}
	public double getPersen_downtum() {
		return persen_downtum;
	}
	public void setPersen_downtum(double persen_downtum) {
		this.persen_downtum = persen_downtum;
	}
	public double getHistorical_mean() {
		return historical_mean;
	}
	public void setHistorical_mean(double historical_mean) {
		this.historical_mean = historical_mean;
	}
	public double getHistorical_stdev() {
		return historical_stdev;
	}
	public void setHistorical_stdev(double historical_stdev) {
		this.historical_stdev = historical_stdev;
	}
	public double getConfidence_level() {
		return confidence_level;
	}
	public void setConfidence_level(double confidence_level) {
		this.confidence_level = confidence_level;
	}
	public double getT_distribution() {
		return t_distribution;
	}
	public void setT_distribution(double t_distribution) {
		this.t_distribution = t_distribution;
	}
	public int[] getList_flg_projection() {
		return list_flg_projection;
	}
	public void setList_flg_projection(int[] list_flg_projection) {
		this.list_flg_projection = list_flg_projection;
	}
	public String getPeriode_mulai() {
		return periode_mulai;
	}
	public void setPeriode_mulai(String periode_mulai) {
		this.periode_mulai = periode_mulai;
	}
	public String getPeriode_selesai() {
		return periode_selesai;
	}
	public void setPeriode_selesai(String periode_selesai) {
		this.periode_selesai = periode_selesai;
	}
	public List<GetVasicekPD> getList_pd_vasicek() {
		return list_pd_vasicek;
	}
	public void setList_pd_vasicek(List<GetVasicekPD> list_pd_vasicek) {
		this.list_pd_vasicek = list_pd_vasicek;
	}
	public List<GetVasicekAset> getList_aset() {
		return list_aset;
	}
	public void setList_aset(List<GetVasicekAset> list_aset) {
		this.list_aset = list_aset;
	}
	public List<?> getList_summary() {
		return list_summary;
	}
	public void setList_summary(List<GetVasicekSummary> list_summary) {
		this.list_summary = list_summary;
	}
	public String getPeriode_ecl() {
		return periode_ecl;
	}
	public void setPeriode_ecl(String periode_ecl) {
		this.periode_ecl = periode_ecl;
	}
	public VasicekResultDraftJoin getResult_draft() {
		return result_draft;
	}
	public void setResult_draft(VasicekResultDraftJoin result_draft) {
		this.result_draft = result_draft;
	}
	public List<VasicekResultAset> getResult_aset() {
		return result_aset;
	}
	public void setResult_aset(List<VasicekResultAset> result_aset) {
		this.result_aset = result_aset;
	}
	public List<VasicekResultMacro> getResult_macro() {
		return result_macro;
	}
	public void setResult_macro(List<VasicekResultMacro> result_macro) {
		this.result_macro = result_macro;
	}
	public List<VasicekResultPD> getResult_pd() {
		return result_pd;
	}
	public void setResult_pd(List<VasicekResultPD> result_pd) {
		this.result_pd = result_pd;
	}
	public List<KomentarJoin> getList_komentar() {
		return list_komentar;
	}
	public void setList_komentar(List<KomentarJoin> list_komentar) {
		this.list_komentar = list_komentar;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public User getCreated_user() {
		return created_user;
	}
	public void setCreated_user(User created_user) {
		this.created_user = created_user;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public User getUpdated_user() {
		return updated_user;
	}
	public void setUpdated_user(User updated_user) {
		this.updated_user = updated_user;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}
}
