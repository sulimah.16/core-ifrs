package com.hanifalbaaits.java.modelRespon;

public class Ecl {
	
	private String bucket;
	private double pd_base;
	private double mev_effect;
	private double logit_pd;
	private double pd_forward_looking;
	private double lgd;
	private double ead;
	private double ecl_base;
	private double ecl_forward_looking;
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public double getPd_base() {
		return pd_base;
	}
	public void setPd_base(double pd_base) {
		this.pd_base = pd_base;
	}
	public double getMev_effect() {
		return mev_effect;
	}
	public void setMev_effect(double mev_effect) {
		this.mev_effect = mev_effect;
	}
	public double getLogit_pd() {
		return logit_pd;
	}
	public void setLogit_pd(double logit_pd) {
		this.logit_pd = logit_pd;
	}
	public double getPd_forward_looking() {
		return pd_forward_looking;
	}
	public void setPd_forward_looking(double pd_forward_looking) {
		this.pd_forward_looking = pd_forward_looking;
	}
	public double getLgd() {
		return lgd;
	}
	public void setLgd(double lgd) {
		this.lgd = lgd;
	}
	public double getEad() {
		return ead;
	}
	public void setEad(double ead) {
		this.ead = ead;
	}
	public double getEcl_base() {
		return ecl_base;
	}
	public void setEcl_base(double ecl_base) {
		this.ecl_base = ecl_base;
	}
	public double getEcl_forward_looking() {
		return ecl_forward_looking;
	}
	public void setEcl_forward_looking(double ecl_forward_looking) {
		this.ecl_forward_looking = ecl_forward_looking;
	}

}
