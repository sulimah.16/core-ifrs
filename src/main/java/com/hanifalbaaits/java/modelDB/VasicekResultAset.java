package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_vasicek_result_aset")
public class VasicekResultAset {
	
	@Id
	@Column(name="id_vasicek_result_aset")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_vasicek_result_aset;
	@Column(name="id_vasicek_result_draft")
	private int id_vasicek_result_draft;
	@Column(name="id_aset")
	private int id_aset;
	@Column(name="id_bank")
	private int id_bank;
	@Column(name="nama_aset")
	private String nama_aset;
	@Column(name="nama_bank")
	private String nama_bank;
	@Column(name="akronim")
	private String akronim;
	@Column(name="tahun_bank")
	private int tahun_bank;
	@Column(name="kode_rating")
	private String kode_rating;
	@Column(name="value_rating")
	private double value_rating;
	@Column(name="no_rekening")
	private String no_rekening;
	@Column(name="mata_uang")
	private String mata_uang;
	@Column(name="suku_bunga")
	private double suku_bunga;
	@Column(name="tgl_jatuh_tempo")
	private String tgl_jatuh_tempo;
	@Column(name="saldo")
	private double saldo;
	@Column(name="lgd")
	private double lgd;
	@Column(name="pd_normal_scan")
	private double pd_normal_scan;
	@Column(name="pd_weighted_scan")
	private double pd_weighted_scan;
	@Column(name="ecl_normal_scan")
	private double ecl_normal_scan;
	@Column(name="ecl_weighted_scan")
	private double ecl_weighted_scan;
	public int getId_vasicek_result_aset() {
		return id_vasicek_result_aset;
	}
	public void setId_vasicek_result_aset(int id_vasicek_result_aset) {
		this.id_vasicek_result_aset = id_vasicek_result_aset;
	}
	public int getId_vasicek_result_draft() {
		return id_vasicek_result_draft;
	}
	public void setId_vasicek_result_draft(int id_vasicek_result_draft) {
		this.id_vasicek_result_draft = id_vasicek_result_draft;
	}
	public int getId_aset() {
		return id_aset;
	}
	public void setId_aset(int id_aset) {
		this.id_aset = id_aset;
	}
	public int getId_bank() {
		return id_bank;
	}
	public void setId_bank(int id_bank) {
		this.id_bank = id_bank;
	}
	public String getNama_aset() {
		return nama_aset;
	}
	public void setNama_aset(String nama_aset) {
		this.nama_aset = nama_aset;
	}
	public String getNama_bank() {
		return nama_bank;
	}
	public void setNama_bank(String nama_bank) {
		this.nama_bank = nama_bank;
	}
	public String getAkronim() {
		return akronim;
	}
	public void setAkronim(String akronim) {
		this.akronim = akronim;
	}
	public String getKode_rating() {
		return kode_rating;
	}
	public void setKode_rating(String kode_rating) {
		this.kode_rating = kode_rating;
	}
	public double getValue_rating() {
		return value_rating;
	}
	public void setValue_rating(double value_rating) {
		this.value_rating = value_rating;
	}
	public String getNo_rekening() {
		return no_rekening;
	}
	public void setNo_rekening(String no_rekening) {
		this.no_rekening = no_rekening;
	}
	public String getMata_uang() {
		return mata_uang;
	}
	public void setMata_uang(String mata_uang) {
		this.mata_uang = mata_uang;
	}
	public double getSuku_bunga() {
		return suku_bunga;
	}
	public void setSuku_bunga(double suku_bunga) {
		this.suku_bunga = suku_bunga;
	}
	public String getTgl_jatuh_tempo() {
		return tgl_jatuh_tempo;
	}
	public void setTgl_jatuh_tempo(String tgl_jatuh_tempo) {
		this.tgl_jatuh_tempo = tgl_jatuh_tempo;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getLgd() {
		return lgd;
	}
	public void setLgd(double lgd) {
		this.lgd = lgd;
	}
	public double getPd_normal_scan() {
		return pd_normal_scan;
	}
	public void setPd_normal_scan(double pd_normal_scan) {
		this.pd_normal_scan = pd_normal_scan;
	}
	public double getPd_weighted_scan() {
		return pd_weighted_scan;
	}
	public void setPd_weighted_scan(double pd_weighted_scan) {
		this.pd_weighted_scan = pd_weighted_scan;
	}
	public double getEcl_normal_scan() {
		return ecl_normal_scan;
	}
	public void setEcl_normal_scan(double ecl_normal_scan) {
		this.ecl_normal_scan = ecl_normal_scan;
	}
	public double getEcl_weighted_scan() {
		return ecl_weighted_scan;
	}
	public void setEcl_weighted_scan(double ecl_weighted_scan) {
		this.ecl_weighted_scan = ecl_weighted_scan;
	}
	public int getTahun_bank() {
		return tahun_bank;
	}
	public void setTahun_bank(int tahun_bank) {
		this.tahun_bank = tahun_bank;
	}
}
