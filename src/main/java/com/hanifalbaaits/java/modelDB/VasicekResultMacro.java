package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_vasicek_result_macro")
public class VasicekResultMacro {
	
	@Id
	@Column(name="id_vasicek_macro")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_vasicek_macro;
	@Column(name="id_vasicek_result_draft")
	private int id_vasicek_result_draft;
	@Column(name="title")
	private String title;
	@Column(name="nilai")
	private String nilai;
	public int getId_vasicek_macro() {
		return id_vasicek_macro;
	}
	public void setId_vasicek_macro(int id_vasicek_macro) {
		this.id_vasicek_macro = id_vasicek_macro;
	}
	public int getId_vasicek_result_draft() {
		return id_vasicek_result_draft;
	}
	public void setId_vasicek_result_draft(int id_vasicek_result_draft) {
		this.id_vasicek_result_draft = id_vasicek_result_draft;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getNilai() {
		return nilai;
	}
	public void setNilai(String nilai) {
		this.nilai = nilai;
	}
}
