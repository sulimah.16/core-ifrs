package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_vasicek_gdp")
public class VasicekGDP {
	
	@Id
	@Column(name="id_vas_gdp")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_vas_gdp;
	@Column(name="periode")
	private Date periode;
	@Column(name="bulan")
	private int bulan;
	@Column(name="tahun")
	private int tahun;
	@Column(name="value_gdp")
	private double value_gdp;
	@Column(name="flg_projection")
	private int flg_projection;
	public int getId_vas_gdp() {
		return id_vas_gdp;
	}
	public void setId_vas_gdp(int id_vas_gdp) {
		this.id_vas_gdp = id_vas_gdp;
	}
	public int getBulan() {
		return bulan;
	}
	public void setBulan(int bulan) {
		this.bulan = bulan;
	}
	public int getTahun() {
		return tahun;
	}
	public void setTahun(int tahun) {
		this.tahun = tahun;
	}
	public int getFlg_projection() {
		return flg_projection;
	}
	public void setFlg_projection(int flg_projection) {
		this.flg_projection = flg_projection;
	}
	public double getValue_gdp() {
		return value_gdp;
	}
	public void setValue_gdp(double value_gdp) {
		this.value_gdp = value_gdp;
	}
	public Date getPeriode() {
		return periode;
	}
	public void setPeriode(Date periode) {
		this.periode = periode;
	}
}
