package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_data_input")
public class DataInput {
	@Id
	@Column(name="id_input")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_input;
	@Column(name="id_draft")
	private int id_draft;
	@Column(name="id_segment")
	private int id_segment;
	@Column(name="tanggal_piutang")
	private Date tanggal_piutang;
	@Column(name="periode_bulan")
	private int periode_bulan;
	@Column(name="periode_tahun")
	private int periode_tahun;
	@Column(name="id_piutang")
	private String id_piutang;
	@Column(name="kode_customer")
	private String kode_customer;
	@Column(name="nama_customer")
	private String nama_customer;
	@Column(name="kode_proyek")
	private String kode_proyek;
	@Column(name="nama_proyek")
	private String nama_proyek;
	@Column(name="pemberi_kerja")
	private String pemberi_kerja;
	@Column(name="doc_number")
	private String doc_number;
	@Column(name="doc_type")
	private String doc_type;
	@Column(name="jenis_piutang")
	private String jenis_piutang;
	@Column(name="umur_piutang_hari")
	private int umur_piutang_hari;
	@Column(name="saldo_piutang")
	private double saldo_piutang;
	@Column(name="path_document")
	private String path_document;
	@Column(name="created_by")
	private int created_by;
	@Column(name="created_date")
	private Date created_date;
	@Column(name="updated_by")
	private int updated_by;
	@Column(name="updated_date")
	private Date updated_date;
	public int getId_input() {
		return id_input;
	}
	public void setId_input(int id_input) {
		this.id_input = id_input;
	}
	public int getId_draft() {
		return id_draft;
	}
	public void setId_draft(int id_draft) {
		this.id_draft = id_draft;
	}
	public int getId_segment() {
		return id_segment;
	}
	public void setId_segment(int id_segment) {
		this.id_segment = id_segment;
	}
	public int getPeriode_bulan() {
		return periode_bulan;
	}
	public void setPeriode_bulan(int periode_bulan) {
		this.periode_bulan = periode_bulan;
	}
	public int getPeriode_tahun() {
		return periode_tahun;
	}
	public void setPeriode_tahun(int periode_tahun) {
		this.periode_tahun = periode_tahun;
	}
	public String getId_piutang() {
		return id_piutang;
	}
	public void setId_piutang(String id_piutang) {
		this.id_piutang = id_piutang;
	}
	public String getKode_customer() {
		return kode_customer;
	}
	public void setKode_customer(String kode_customer) {
		this.kode_customer = kode_customer;
	}
	public String getNama_customer() {
		return nama_customer;
	}
	public void setNama_customer(String nama_customer) {
		this.nama_customer = nama_customer;
	}
	public String getKode_proyek() {
		return kode_proyek;
	}
	public void setKode_proyek(String kode_proyek) {
		this.kode_proyek = kode_proyek;
	}
	public String getNama_proyek() {
		return nama_proyek;
	}
	public void setNama_proyek(String nama_proyek) {
		this.nama_proyek = nama_proyek;
	}
	public String getPemberi_kerja() {
		return pemberi_kerja;
	}
	public void setPemberi_kerja(String pemberi_kerja) {
		this.pemberi_kerja = pemberi_kerja;
	}
	public String getDoc_number() {
		return doc_number;
	}
	public void setDoc_number(String doc_number) {
		this.doc_number = doc_number;
	}
	public String getDoc_type() {
		return doc_type;
	}
	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}
	public String getJenis_piutang() {
		return jenis_piutang;
	}
	public void setJenis_piutang(String jenis_piutang) {
		this.jenis_piutang = jenis_piutang;
	}
	public double getSaldo_piutang() {
		return saldo_piutang;
	}
	public void setSaldo_piutang(double saldo_piutang) {
		this.saldo_piutang = saldo_piutang;
	}
	public String getPath_document() {
		return path_document;
	}
	public void setPath_document(String path_document) {
		this.path_document = path_document;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public Date getTanggal_piutang() {
		return tanggal_piutang;
	}
	public void setTanggal_piutang(Date tanggal_piutang) {
		this.tanggal_piutang = tanggal_piutang;
	}
	public int getUmur_piutang_hari() {
		return umur_piutang_hari;
	}
	public void setUmur_piutang_hari(int umur_piutang_hari) {
		this.umur_piutang_hari = umur_piutang_hari;
	}
}
