package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_ecl")
public class ECL {
	
	@Id
	@Column(name="id_ecl")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_ecl;
	@Column(name="id_draft_ecl")
	private int id_draft_ecl;
	@Column(name="id_model")
	private int id_model;
	@Column(name="periode_forecast")
	private int periode_forecast;
	@Column(name="periode")
	private String periode;
	@Column(name="no_model_sum_ead")
	private double no_model_sum_ead;
	@Column(name="no_model_sum_ecl")
	private double no_model_sum_ecl;
	@Column(name="logit_expected_psak")
	private double logit_expected_psak;
	@Column(name="expected_psak")
	private double expected_psak;
	@Column(name="expected")
	private double expected;
	@Column(name="last")
	private double last;
	@Column(name="mev")
	private double mev;
	@Column(name="proporsional_sum_ead")
	private double proporsional_sum_ead;
	@Column(name="proporsional_sum_ecl")
	private double proporsional_sum_ecl;
	@Column(name="proporsional_sum_ecl_forward_looking")
	private double proporsional_sum_ecl_forward_looking;
	@Column(name="pd_forward_looking")
	private double pd_forward_looking;
	@Column(name="solver_average_pd_base")
	private double solver_average_pd_base;
	@Column(name="solver_average_pd_forward_looking")
	private double solver_average_pd_forward_looking;
	@Column(name="solver_sum_ead")
	private double solver_sum_ead;
	@Column(name="solver_sum_ecl")
	private double solver_sum_ecl;
	@Column(name="solver_sum_ecl_forward_looking")
	private double solver_sum_ecl_forward_looking;
	@Column(name="target")
	private double target;
	@Column(name="scaling")
	private double scaling;
	public int getId_ecl() {
		return id_ecl;
	}
	public void setId_ecl(int id_ecl) {
		this.id_ecl = id_ecl;
	}
	public int getId_draft_ecl() {
		return id_draft_ecl;
	}
	public void setId_draft_ecl(int id_draft_ecl) {
		this.id_draft_ecl = id_draft_ecl;
	}
	public int getId_model() {
		return id_model;
	}
	public void setId_model(int id_model) {
		this.id_model = id_model;
	}
	public int getPeriode_forecast() {
		return periode_forecast;
	}
	public void setPeriode_forecast(int periode_forecast) {
		this.periode_forecast = periode_forecast;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public double getNo_model_sum_ead() {
		return no_model_sum_ead;
	}
	public void setNo_model_sum_ead(double no_model_sum_ead) {
		this.no_model_sum_ead = no_model_sum_ead;
	}
	public double getNo_model_sum_ecl() {
		return no_model_sum_ecl;
	}
	public void setNo_model_sum_ecl(double no_model_sum_ecl) {
		this.no_model_sum_ecl = no_model_sum_ecl;
	}
	public double getLogit_expected_psak() {
		return logit_expected_psak;
	}
	public void setLogit_expected_psak(double logit_expected_psak) {
		this.logit_expected_psak = logit_expected_psak;
	}
	public double getExpected_psak() {
		return expected_psak;
	}
	public void setExpected_psak(double expected_psak) {
		this.expected_psak = expected_psak;
	}
	public double getExpected() {
		return expected;
	}
	public void setExpected(double expected) {
		this.expected = expected;
	}
	public double getLast() {
		return last;
	}
	public void setLast(double last) {
		this.last = last;
	}
	public double getMev() {
		return mev;
	}
	public void setMev(double mev) {
		this.mev = mev;
	}
	public double getProporsional_sum_ead() {
		return proporsional_sum_ead;
	}
	public void setProporsional_sum_ead(double proporsional_sum_ead) {
		this.proporsional_sum_ead = proporsional_sum_ead;
	}
	public double getProporsional_sum_ecl() {
		return proporsional_sum_ecl;
	}
	public void setProporsional_sum_ecl(double proporsional_sum_ecl) {
		this.proporsional_sum_ecl = proporsional_sum_ecl;
	}
	public double getProporsional_sum_ecl_forward_looking() {
		return proporsional_sum_ecl_forward_looking;
	}
	public void setProporsional_sum_ecl_forward_looking(double proporsional_sum_ecl_forward_looking) {
		this.proporsional_sum_ecl_forward_looking = proporsional_sum_ecl_forward_looking;
	}
	public double getPd_forward_looking() {
		return pd_forward_looking;
	}
	public void setPd_forward_looking(double pd_forward_looking) {
		this.pd_forward_looking = pd_forward_looking;
	}
	public double getSolver_average_pd_base() {
		return solver_average_pd_base;
	}
	public void setSolver_average_pd_base(double solver_average_pd_base) {
		this.solver_average_pd_base = solver_average_pd_base;
	}
	public double getSolver_average_pd_forward_looking() {
		return solver_average_pd_forward_looking;
	}
	public void setSolver_average_pd_forward_looking(double solver_average_pd_forward_looking) {
		this.solver_average_pd_forward_looking = solver_average_pd_forward_looking;
	}
	public double getSolver_sum_ead() {
		return solver_sum_ead;
	}
	public void setSolver_sum_ead(double solver_sum_ead) {
		this.solver_sum_ead = solver_sum_ead;
	}
	public double getSolver_sum_ecl() {
		return solver_sum_ecl;
	}
	public void setSolver_sum_ecl(double solver_sum_ecl) {
		this.solver_sum_ecl = solver_sum_ecl;
	}
	public double getSolver_sum_ecl_forward_looking() {
		return solver_sum_ecl_forward_looking;
	}
	public void setSolver_sum_ecl_forward_looking(double solver_sum_ecl_forward_looking) {
		this.solver_sum_ecl_forward_looking = solver_sum_ecl_forward_looking;
	}
	public double getTarget() {
		return target;
	}
	public void setTarget(double target) {
		this.target = target;
	}
	public double getScaling() {
		return scaling;
	}
	public void setScaling(double scaling) {
		this.scaling = scaling;
	}
}
