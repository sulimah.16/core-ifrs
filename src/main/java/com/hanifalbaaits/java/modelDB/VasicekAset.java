package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_vasicek_aset")
public class VasicekAset {
	
	@Id
	@Column(name="id_vas_aset")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_vas_aset;
	@Column(name="nama_aset")
	private String nama_aset;
	public int getId_vas_aset() {
		return id_vas_aset;
	}
	public void setId_vas_aset(int id_vas_aset) {
		this.id_vas_aset = id_vas_aset;
	}
	public String getNama_aset() {
		return nama_aset;
	}
	public void setNama_aset(String nama_aset) {
		this.nama_aset = nama_aset;
	}
}
