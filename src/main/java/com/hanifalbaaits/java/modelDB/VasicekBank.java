package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_vasicek_bank")
public class VasicekBank {
	
	@Id
	@Column(name="id_vas_bank")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_vas_bank;
	@Column(name="nama_bank")
	private String nama_bank;
	@Column(name="akronim")
	private String akronim;
	@Column(name="id_rating")
	private int id_rating;
	@Column(name="referensi")
	private String referensi;
	@Column(name="tahun")
	private int tahun;
	@Column(name="nilai")
	private double nilai;
	public int getId_vas_bank() {
		return id_vas_bank;
	}
	public void setId_vas_bank(int id_vas_bank) {
		this.id_vas_bank = id_vas_bank;
	}
	public String getNama_bank() {
		return nama_bank;
	}
	public void setNama_bank(String nama_bank) {
		this.nama_bank = nama_bank;
	}
	public String getAkronim() {
		return akronim;
	}
	public void setAkronim(String akronim) {
		this.akronim = akronim;
	}
	public int getId_rating() {
		return id_rating;
	}
	public void setId_rating(int id_rating) {
		this.id_rating = id_rating;
	}
	public int getTahun() {
		return tahun;
	}
	public void setTahun(int tahun) {
		this.tahun = tahun;
	}
	public double getNilai() {
		return nilai;
	}
	public void setNilai(double nilai) {
		this.nilai = nilai;
	}
	public String getReferensi() {
		return referensi;
	}
	public void setReferensi(String referensi) {
		this.referensi = referensi;
	}
}
