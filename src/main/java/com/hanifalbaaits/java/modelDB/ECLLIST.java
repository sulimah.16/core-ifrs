package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_ecl_list")
public class ECLLIST {
	
	@Id
	@Column(name="id_ecl_list")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_ecl_list;
	@Column(name="id_draft_ecl")
	private int id_draft_ecl;
	@Column(name="id_type")
	private int id_type;
	@Column(name="bucket")
	private String bucket;
	@Column(name="pd_base")
	private double pd_base;
	@Column(name="mev_effect")
	private double mev_effect;
	@Column(name="logit_pd")
	private double logit_pd;
	@Column(name="pd_forward_looking")
	private double pd_forward_looking;
	@Column(name="lgd")
	private double lgd;
	@Column(name="ead")
	private double ead;
	@Column(name="ecl_base")
	private double ecl_base;
	@Column(name="ecl_forward_looking")
	private double ecl_forward_looking;
	public int getId_ecl_list() {
		return id_ecl_list;
	}
	public void setId_ecl_list(int id_ecl_list) {
		this.id_ecl_list = id_ecl_list;
	}
	public int getId_draft_ecl() {
		return id_draft_ecl;
	}
	public void setId_draft_ecl(int id_draft_ecl) {
		this.id_draft_ecl = id_draft_ecl;
	}
	public int getId_type() {
		return id_type;
	}
	public void setId_type(int id_type) {
		this.id_type = id_type;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public double getPd_base() {
		return pd_base;
	}
	public void setPd_base(double pd_base) {
		this.pd_base = pd_base;
	}
	public double getMev_effect() {
		return mev_effect;
	}
	public void setMev_effect(double mev_effect) {
		this.mev_effect = mev_effect;
	}
	public double getLogit_pd() {
		return logit_pd;
	}
	public void setLogit_pd(double logit_pd) {
		this.logit_pd = logit_pd;
	}
	public double getPd_forward_looking() {
		return pd_forward_looking;
	}
	public void setPd_forward_looking(double pd_forward_looking) {
		this.pd_forward_looking = pd_forward_looking;
	}
	public double getLgd() {
		return lgd;
	}
	public void setLgd(double lgd) {
		this.lgd = lgd;
	}
	public double getEad() {
		return ead;
	}
	public void setEad(double ead) {
		this.ead = ead;
	}
	public double getEcl_base() {
		return ecl_base;
	}
	public void setEcl_base(double ecl_base) {
		this.ecl_base = ecl_base;
	}
	public double getEcl_forward_looking() {
		return ecl_forward_looking;
	}
	public void setEcl_forward_looking(double ecl_forward_looking) {
		this.ecl_forward_looking = ecl_forward_looking;
	}
}
