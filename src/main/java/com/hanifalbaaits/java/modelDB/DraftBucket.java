package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_draft_bucket")
public class DraftBucket {
	
	@Id
	@Column(name="id_draft_bucket")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_draft_bucket;
	@Column(name="kode_draft_bucket")
	private String kode_draft_bucket;
	@Column(name="nama_draft_bucket")
	private String nama_draft_bucket;
	@Column(name="id_draft")
	private int id_draft;
	@Column(name="id_segment")
	private int id_segment;
	@Column(name="hari_first")
	private int hari_first;
	@Column(name="interval_bucket")
	private int interval_bucket;
	@Column(name="jumlah_bucket")
	private int jumlah_bucket;
	@Column(name="flag_normalisasi")
	private String flag_normalisasi;
	@Column(name="moving_average")
	private int moving_average;
	@Column(name="start_date")
	private Date start_date;
	@Column(name="end_date")
	private Date end_date;
	@Column(name="created_by")
	private int created_by;
	@Column(name="created_date")
	private Date created_date;
	public int getId_draft_bucket() {
		return id_draft_bucket;
	}
	public void setId_draft_bucket(int id_draft_bucket) {
		this.id_draft_bucket = id_draft_bucket;
	}
	public String getKode_draft_bucket() {
		return kode_draft_bucket;
	}
	public void setKode_draft_bucket(String kode_draft_bucket) {
		this.kode_draft_bucket = kode_draft_bucket;
	}
	public String getNama_draft_bucket() {
		return nama_draft_bucket;
	}
	public void setNama_draft_bucket(String nama_draft_bucket) {
		this.nama_draft_bucket = nama_draft_bucket;
	}
	public int getId_draft() {
		return id_draft;
	}
	public void setId_draft(int id_draft) {
		this.id_draft = id_draft;
	}
	public int getId_segment() {
		return id_segment;
	}
	public void setId_segment(int id_segment) {
		this.id_segment = id_segment;
	}
	public int getHari_first() {
		return hari_first;
	}
	public void setHari_first(int hari_first) {
		this.hari_first = hari_first;
	}
	public int getInterval_bucket() {
		return interval_bucket;
	}
	public void setInterval_bucket(int interval_bucket) {
		this.interval_bucket = interval_bucket;
	}
	public int getJumlah_bucket() {
		return jumlah_bucket;
	}
	public void setJumlah_bucket(int jumlah_bucket) {
		this.jumlah_bucket = jumlah_bucket;
	}
	public String getFlag_normalisasi() {
		return flag_normalisasi;
	}
	public void setFlag_normalisasi(String flag_normalisasi) {
		this.flag_normalisasi = flag_normalisasi;
	}
	public int getMoving_average() {
		return moving_average;
	}
	public void setMoving_average(int moving_average) {
		this.moving_average = moving_average;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	
}
