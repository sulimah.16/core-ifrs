package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_vasicek_rating")
public class VasicekRating {
	
	@Id
	@Column(name="id_rating")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_rating;
	@Column(name="kode_rating")
	private String kode_rating;
	@Column(name="value")
	private double value;
	@Column(name="status")
	private int status;
	public int getId_rating() {
		return id_rating;
	}
	public void setId_rating(int id_rating) {
		this.id_rating = id_rating;
	}
	public String getKode_rating() {
		return kode_rating;
	}
	public void setKode_rating(String kode_rating) {
		this.kode_rating = kode_rating;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
