package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_upload_file")
public class UploadFile {
	
	@Id
	@Column(name="id_upload")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_upload;
	@Column(name="id_user")
	private int id_user;
	@Column(name="type_folder")
	private String type_folder;
	@Column(name="true_name")
	private String true_name;
	@Column(name="path_name")
	private String path_name;
	@Column(name="type_file")
	private String type_file;
	@Column(name="size_file")
	private int size_file;
	@Column(name="tanggal_upload")
	private Date tanggal_upload;
	public int getId_upload() {
		return id_upload;
	}
	public void setId_upload(int id_upload) {
		this.id_upload = id_upload;
	}
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public String getType_folder() {
		return type_folder;
	}
	public void setType_folder(String type_folder) {
		this.type_folder = type_folder;
	}
	public String getTrue_name() {
		return true_name;
	}
	public void setTrue_name(String true_name) {
		this.true_name = true_name;
	}
	public String getPath_name() {
		return path_name;
	}
	public void setPath_name(String path_name) {
		this.path_name = path_name;
	}
	public String getType_file() {
		return type_file;
	}
	public void setType_file(String type_file) {
		this.type_file = type_file;
	}
	public Date getTanggal_upload() {
		return tanggal_upload;
	}
	public void setTanggal_upload(Date tanggal_upload) {
		this.tanggal_upload = tanggal_upload;
	}
	public int getSize_file() {
		return size_file;
	}
	public void setSize_file(int size_file) {
		this.size_file = size_file;
	}
}
