package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_draft_ecl")
public class DraftECL {
	
	@Id
	@Column(name="id_draft_ecl")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_draft_ecl;
	@Column(name="id_draft_bucket")
	private int id_draft_bucket;
	@Column(name="id_model")
	private int id_model;
	@Column(name="id_regresi")
	private int id_regresi;
	@Column(name="month_forecast")
	private int month_forecast;
	@Column(name="periode")
	private Date periode;
	@Column(name="lgd")
	private double lgd;
	@Column(name="normal")
	private double normal;
	@Column(name="optimis")
	private double optimis;
	@Column(name="pesimis")
	private double pesimis;
	@Column(name="average_mape")
	private double average_mape;
	@Column(name="ecl_no_model")
	private double ecl_no_model;
	@Column(name="ecl_proporsional")
	private double ecl_proporsional;
	@Column(name="ecl_solver")
	private double ecl_solver;
	@Column(name="result_flag_model")
	private int result_flag_model;
	@Column(name="result_saldo_exiting")
	private double result_saldo_exiting;
	@Column(name="result_saldo_selisih")
	private double result_saldo_selisih;
	@Column(name="created_by")
	private int created_by;
	@Column(name="created_date")
	private Date created_date;
	@Column(name="updated_by")
	private int updated_by;
	@Column(name="updated_date")
	private Date updated_date;
	public int getId_draft_ecl() {
		return id_draft_ecl;
	}
	public void setId_draft_ecl(int id_draft_ecl) {
		this.id_draft_ecl = id_draft_ecl;
	}
	public int getId_draft_bucket() {
		return id_draft_bucket;
	}
	public void setId_draft_bucket(int id_draft_bucket) {
		this.id_draft_bucket = id_draft_bucket;
	}
	public int getId_model() {
		return id_model;
	}
	public void setId_model(int id_model) {
		this.id_model = id_model;
	}
	public int getId_regresi() {
		return id_regresi;
	}
	public void setId_regresi(int id_regresi) {
		this.id_regresi = id_regresi;
	}
	public int getMonth_forecast() {
		return month_forecast;
	}
	public void setMonth_forecast(int month_forecast) {
		this.month_forecast = month_forecast;
	}
	public Date getPeriode() {
		return periode;
	}
	public void setPeriode(Date periode) {
		this.periode = periode;
	}
	public double getLgd() {
		return lgd;
	}
	public void setLgd(double lgd) {
		this.lgd = lgd;
	}
	public double getNormal() {
		return normal;
	}
	public void setNormal(double normal) {
		this.normal = normal;
	}
	public double getOptimis() {
		return optimis;
	}
	public void setOptimis(double optimis) {
		this.optimis = optimis;
	}
	public double getPesimis() {
		return pesimis;
	}
	public void setPesimis(double pesimis) {
		this.pesimis = pesimis;
	}
	public double getAverage_mape() {
		return average_mape;
	}
	public void setAverage_mape(double average_mape) {
		this.average_mape = average_mape;
	}
	public double getEcl_no_model() {
		return ecl_no_model;
	}
	public void setEcl_no_model(double ecl_no_model) {
		this.ecl_no_model = ecl_no_model;
	}
	public double getEcl_proporsional() {
		return ecl_proporsional;
	}
	public void setEcl_proporsional(double ecl_proporsional) {
		this.ecl_proporsional = ecl_proporsional;
	}
	public double getEcl_solver() {
		return ecl_solver;
	}
	public void setEcl_solver(double ecl_solver) {
		this.ecl_solver = ecl_solver;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public int getResult_flag_model() {
		return result_flag_model;
	}
	public void setResult_flag_model(int result_flag_model) {
		this.result_flag_model = result_flag_model;
	}
	public double getResult_saldo_exiting() {
		return result_saldo_exiting;
	}
	public void setResult_saldo_exiting(double result_saldo_exiting) {
		this.result_saldo_exiting = result_saldo_exiting;
	}
	public double getResult_saldo_selisih() {
		return result_saldo_selisih;
	}
	public void setResult_saldo_selisih(double result_saldo_selisih) {
		this.result_saldo_selisih = result_saldo_selisih;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
}
