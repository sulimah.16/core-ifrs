package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_komentar_bucket")
public class Komentar {

	@Id
	@Column(name="id_komentar_bucket")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_komentar_bucket;
	@Column(name="id_type")
	private int id_type;
	@Column(name="id_bucket")
	private int id_bucket;
	@Column(name="flag")
	private int flag;
	@Column(name="komentar")
	private String komentar;
	@Column(name="created_by")
	private int created_by;
	@Column(name="created_date")
	private Date created_date;
	@Column(name="updated_date")
	private Date updated_date;
	public int getId_komentar_bucket() {
		return id_komentar_bucket;
	}
	public void setId_komentar_bucket(int id_komentar_bucket) {
		this.id_komentar_bucket = id_komentar_bucket;
	}
	public int getId_type() {
		return id_type;
	}
	public void setId_type(int id_type) {
		this.id_type = id_type;
	}
	public int getId_bucket() {
		return id_bucket;
	}
	public void setId_bucket(int id_bucket) {
		this.id_bucket = id_bucket;
	}
	public String getKomentar() {
		return komentar;
	}
	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}

}
