package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_vasicek_result_draft")
public class VasicekResultDraft {
	
	@Id
	@Column(name="id_vasicek_result_draft")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_vasicek_result_draft;
	@Column(name="id_draft_vasicek")
	private int id_draft_vasicek;
	@Column(name="interval_bucket")
	private int interval_bucket;
	@Column(name="periode_mulai")
	private String periode_mulai;
	@Column(name="periode_selesai")
	private String periode_selesai;
	@Column(name="header_macroeconomic")
	private String header_macroeconomic;
	@Column(name="list_flg_projection")
	private String list_flg_projection;
	@Column(name="persen_normal")
	private double persen_normal;
	@Column(name="persen_uptum")
	private double persen_uptum;
	@Column(name="persen_downtum")
	private double persen_downtum;
	@Column(name="historical_mean")
	private double historical_mean;
	@Column(name="historical_stdev")
	private double historical_stdev;
	@Column(name="confidence_level")
	private double confidence_level;
	@Column(name="t_distribution")
	private double t_distribution;
	@Column(name="periode_ecl")
	private String periode_ecl;
	@Column(name="created_by")
	private int created_by;
	@Column(name="created_date")
	private Date created_date;
	
	@Column(name="saldo_normal_scan")
	private double saldo_normal_scan;
	@Column(name="saldo_weighted_scan")
	private double saldo_weighted_scan;
	@Column(name="result_saldo_exiting")
	private double result_saldo_exiting;
	@Column(name="result_saldo_selisih")
	private double result_saldo_selisih;
	
	@Column(name="result_flag_model")
	private double result_flag_model;
	
	@Column(name="updated_by")
	private int updated_by;
	@Column(name="updated_date")
	private Date updated_date;
	
	public int getId_vasicek_result_draft() {
		return id_vasicek_result_draft;
	}
	public void setId_vasicek_result_draft(int id_vasicek_result_draft) {
		this.id_vasicek_result_draft = id_vasicek_result_draft;
	}
	public int getId_draft_vasicek() {
		return id_draft_vasicek;
	}
	public void setId_draft_vasicek(int id_draft_vasicek) {
		this.id_draft_vasicek = id_draft_vasicek;
	}
	public int getInterval_bucket() {
		return interval_bucket;
	}
	public void setInterval_bucket(int interval_bucket) {
		this.interval_bucket = interval_bucket;
	}
	public String getPeriode_mulai() {
		return periode_mulai;
	}
	public void setPeriode_mulai(String periode_mulai) {
		this.periode_mulai = periode_mulai;
	}
	public String getPeriode_selesai() {
		return periode_selesai;
	}
	public void setPeriode_selesai(String periode_selesai) {
		this.periode_selesai = periode_selesai;
	}
	public String getHeader_macroeconomic() {
		return header_macroeconomic;
	}
	public void setHeader_macroeconomic(String header_macroeconomic) {
		this.header_macroeconomic = header_macroeconomic;
	}
	public String getList_flg_projection() {
		return list_flg_projection;
	}
	public void setList_flg_projection(String list_flg_projection) {
		this.list_flg_projection = list_flg_projection;
	}
	public double getPersen_normal() {
		return persen_normal;
	}
	public void setPersen_normal(double persen_normal) {
		this.persen_normal = persen_normal;
	}
	public double getPersen_uptum() {
		return persen_uptum;
	}
	public void setPersen_uptum(double persen_uptum) {
		this.persen_uptum = persen_uptum;
	}
	public double getPersen_downtum() {
		return persen_downtum;
	}
	public void setPersen_downtum(double persen_downtum) {
		this.persen_downtum = persen_downtum;
	}
	public double getHistorical_mean() {
		return historical_mean;
	}
	public void setHistorical_mean(double historical_mean) {
		this.historical_mean = historical_mean;
	}
	public double getHistorical_stdev() {
		return historical_stdev;
	}
	public void setHistorical_stdev(double historical_stdev) {
		this.historical_stdev = historical_stdev;
	}
	public double getConfidence_level() {
		return confidence_level;
	}
	public void setConfidence_level(double confidence_level) {
		this.confidence_level = confidence_level;
	}
	public double getT_distribution() {
		return t_distribution;
	}
	public void setT_distribution(double t_distribution) {
		this.t_distribution = t_distribution;
	}
	public String getPeriode_ecl() {
		return periode_ecl;
	}
	public void setPeriode_ecl(String periode_ecl) {
		this.periode_ecl = periode_ecl;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public double getSaldo_normal_scan() {
		return saldo_normal_scan;
	}
	public void setSaldo_normal_scan(double saldo_normal_scan) {
		this.saldo_normal_scan = saldo_normal_scan;
	}
	public double getSaldo_weighted_scan() {
		return saldo_weighted_scan;
	}
	public void setSaldo_weighted_scan(double saldo_weighted_scan) {
		this.saldo_weighted_scan = saldo_weighted_scan;
	}
	public double getResult_saldo_selisih() {
		return result_saldo_selisih;
	}
	public void setResult_saldo_selisih(double result_saldo_selisih) {
		this.result_saldo_selisih = result_saldo_selisih;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public double getResult_saldo_exiting() {
		return result_saldo_exiting;
	}
	public void setResult_saldo_exiting(double result_saldo_exiting) {
		this.result_saldo_exiting = result_saldo_exiting;
	}
	public double getResult_flag_model() {
		return result_flag_model;
	}
	public void setResult_flag_model(double result_flag_model) {
		this.result_flag_model = result_flag_model;
	}

}
