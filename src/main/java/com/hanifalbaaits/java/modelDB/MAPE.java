package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_mape")
public class MAPE {
	
	@Id
	@Column(name="id_mape")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_mape;
	@Column(name="id_draft_ecl")
	private int id_draft_ecl;
	@Column(name="id_draft_bucket")
	private int id_draft_bucket;
	@Column(name="input_regresi")
	private int input_regresi;
	@Column(name="id_model")
	private int id_model;
	@Column(name="periode_forecast")
	private int periode_forecast;
	@Column(name="list_header")
	private String list_header;
	@Column(name="average_mape")
	private double average_mape;
	@Column(name="threshold_mape")
	private double threshold_mape;
	@Column(name="hasil_mape")
	private String hasil_mape;
	@Column(name="periode_start")
	private Date periode_start;
	@Column(name="periode_end")
	private Date periode_end;
	public int getId_mape() {
		return id_mape;
	}
	public void setId_mape(int id_mape) {
		this.id_mape = id_mape;
	}
	public int getId_draft_ecl() {
		return id_draft_ecl;
	}
	public void setId_draft_ecl(int id_draft_ecl) {
		this.id_draft_ecl = id_draft_ecl;
	}
	public int getId_draft_bucket() {
		return id_draft_bucket;
	}
	public void setId_draft_bucket(int id_draft_bucket) {
		this.id_draft_bucket = id_draft_bucket;
	}
	public int getInput_regresi() {
		return input_regresi;
	}
	public void setInput_regresi(int input_regresi) {
		this.input_regresi = input_regresi;
	}
	public int getId_model() {
		return id_model;
	}
	public void setId_model(int id_model) {
		this.id_model = id_model;
	}
	public int getPeriode_forecast() {
		return periode_forecast;
	}
	public void setPeriode_forecast(int periode_forecast) {
		this.periode_forecast = periode_forecast;
	}
	public String getList_header() {
		return list_header;
	}
	public void setList_header(String list_header) {
		this.list_header = list_header;
	}
	public double getAverage_mape() {
		return average_mape;
	}
	public void setAverage_mape(double average_mape) {
		this.average_mape = average_mape;
	}
	public double getThreshold_mape() {
		return threshold_mape;
	}
	public void setThreshold_mape(double threshold_mape) {
		this.threshold_mape = threshold_mape;
	}
	public String getHasil_mape() {
		return hasil_mape;
	}
	public void setHasil_mape(String hasil_mape) {
		this.hasil_mape = hasil_mape;
	}
	public Date getPeriode_start() {
		return periode_start;
	}
	public void setPeriode_start(Date periode_start) {
		this.periode_start = periode_start;
	}
	public Date getPeriode_end() {
		return periode_end;
	}
	public void setPeriode_end(Date periode_end) {
		this.periode_end = periode_end;
	}
}
