package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_role")
public class UserRole {
	
	@Id
	@Column(name="id_role")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_role;
	@Column(name="nama")
	private String nama;
	@Column(name="keterangan")
	private String keterangan;
	public int getId_role() {
		return id_role;
	}
	public void setId_role(int id_role) {
		this.id_role = id_role;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
}
