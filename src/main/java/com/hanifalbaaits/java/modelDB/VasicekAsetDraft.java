package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_vasicek_aset_draft")
public class VasicekAsetDraft {
	
	@Id
	@Column(name="id_draft_vasicek")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_draft_vasicek;
	@Column(name="kode_draft")
	private String kode_draft;
	@Column(name="nama_draft")
	private String nama_draft;
	@Column(name="created_by")
	private int created_by;
	@Column(name="created_date")
	private Date created_date;
	@Column(name="updated_by")
	private int updated_by;
	@Column(name="updated_date")
	private Date updated_date;
	public int getId_draft_vasicek() {
		return id_draft_vasicek;
	}
	public void setId_draft_vasicek(int id_draft_vasicek) {
		this.id_draft_vasicek = id_draft_vasicek;
	}
	public String getKode_draft() {
		return kode_draft;
	}
	public void setKode_draft(String kode_draft) {
		this.kode_draft = kode_draft;
	}
	public String getNama_draft() {
		return nama_draft;
	}
	public void setNama_draft(String nama_draft) {
		this.nama_draft = nama_draft;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
}
