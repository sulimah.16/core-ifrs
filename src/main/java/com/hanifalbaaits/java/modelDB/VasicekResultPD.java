package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_vasicek_result_pd")
public class VasicekResultPD {
	
	@Id
	@Column(name="id_vasicek_result_pd")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_vasicek_result_pd;
	@Column(name="id_vasicek_result_draft")
	private int id_vasicek_result_draft;
	@Column(name="id_rating")
	private int id_rating;
	@Column(name="kode_rating")
	private String kode_rating;
	@Column(name="value_rating")
	private double value_rating;
	@Column(name="periode_projection")
	private String periode_projection;
	@Column(name="value_base_formula")
	private double value_base_formula;
	@Column(name="vasicek_normal")
	private double vasicek_normal;
	@Column(name="vasicek_adjusted_normal")
	private double vasicek_adjusted_normal;
	@Column(name="vasicek_weighted")
	private double vasicek_weighted;
	@Column(name="vasicek_adjusted_weighted")
	private double vasicek_adjusted_weighted;
	public int getId_vasicek_result_pd() {
		return id_vasicek_result_pd;
	}
	public void setId_vasicek_result_pd(int id_vasicek_result_pd) {
		this.id_vasicek_result_pd = id_vasicek_result_pd;
	}
	public int getId_vasicek_result_draft() {
		return id_vasicek_result_draft;
	}
	public void setId_vasicek_result_draft(int id_vasicek_result_draft) {
		this.id_vasicek_result_draft = id_vasicek_result_draft;
	}
	public int getId_rating() {
		return id_rating;
	}
	public void setId_rating(int id_rating) {
		this.id_rating = id_rating;
	}
	public String getKode_rating() {
		return kode_rating;
	}
	public void setKode_rating(String kode_rating) {
		this.kode_rating = kode_rating;
	}
	public double getValue_rating() {
		return value_rating;
	}
	public void setValue_rating(double value_rating) {
		this.value_rating = value_rating;
	}
	public String getPeriode_projection() {
		return periode_projection;
	}
	public void setPeriode_projection(String periode_projection) {
		this.periode_projection = periode_projection;
	}
	public double getValue_base_formula() {
		return value_base_formula;
	}
	public void setValue_base_formula(double value_base_formula) {
		this.value_base_formula = value_base_formula;
	}
	public double getVasicek_normal() {
		return vasicek_normal;
	}
	public void setVasicek_normal(double vasicek_normal) {
		this.vasicek_normal = vasicek_normal;
	}
	public double getVasicek_adjusted_normal() {
		return vasicek_adjusted_normal;
	}
	public void setVasicek_adjusted_normal(double vasicek_adjusted_normal) {
		this.vasicek_adjusted_normal = vasicek_adjusted_normal;
	}
	public double getVasicek_weighted() {
		return vasicek_weighted;
	}
	public void setVasicek_weighted(double vasicek_weighted) {
		this.vasicek_weighted = vasicek_weighted;
	}
	public double getVasicek_adjusted_weighted() {
		return vasicek_adjusted_weighted;
	}
	public void setVasicek_adjusted_weighted(double vasicek_adjusted_weighted) {
		this.vasicek_adjusted_weighted = vasicek_adjusted_weighted;
	}
}
