package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_mape_list")
public class MAPELIST {
	
	@Id
	@Column(name="id_mape_list")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_mape_list;
	@Column(name="id_draft_ecl")
	private int id_draft_ecl;
	@Column(name="periode")
	private String periode;
	@Column(name="periode_bulan")
	private int periode_bulan;
	@Column(name="periode_tahun")
	private int periode_tahun;
	@Column(name="nilai_var1")
	private double nilai_var1;
	@Column(name="nilai_var2")
	private double nilai_var2;
	@Column(name="logit_expected")
	private double logit_expected;
	@Column(name="expected")
	private double expected;
	@Column(name="actual")
	private double actual;
	@Column(name="mape")
	private double mape;
	public int getId_mape_list() {
		return id_mape_list;
	}
	public void setId_mape_list(int id_mape_list) {
		this.id_mape_list = id_mape_list;
	}
	public int getId_draft_ecl() {
		return id_draft_ecl;
	}
	public void setId_draft_ecl(int id_draft_ecl) {
		this.id_draft_ecl = id_draft_ecl;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public int getPeriode_bulan() {
		return periode_bulan;
	}
	public void setPeriode_bulan(int periode_bulan) {
		this.periode_bulan = periode_bulan;
	}
	public int getPeriode_tahun() {
		return periode_tahun;
	}
	public void setPeriode_tahun(int periode_tahun) {
		this.periode_tahun = periode_tahun;
	}
	public double getNilai_var1() {
		return nilai_var1;
	}
	public void setNilai_var1(double nilai_var1) {
		this.nilai_var1 = nilai_var1;
	}
	public double getNilai_var2() {
		return nilai_var2;
	}
	public void setNilai_var2(double nilai_var2) {
		this.nilai_var2 = nilai_var2;
	}
	public double getLogit_expected() {
		return logit_expected;
	}
	public void setLogit_expected(double logit_expected) {
		this.logit_expected = logit_expected;
	}
	public double getExpected() {
		return expected;
	}
	public void setExpected(double expected) {
		this.expected = expected;
	}
	public double getActual() {
		return actual;
	}
	public void setActual(double actual) {
		this.actual = actual;
	}
	public double getMape() {
		return mape;
	}
	public void setMape(double mape) {
		this.mape = mape;
	}
}
