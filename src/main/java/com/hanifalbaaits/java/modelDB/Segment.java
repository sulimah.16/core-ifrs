package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_segment")
public class Segment {
	
	@Id
	@Column(name="id_segment")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_segment;
	@Column(name="kode_segment")
	private String kode_segment;
	@Column(name="nama_segment")
	private String nama_segment;
	@Column(name="created_by")
	private int created_by;
	@Column(name="created_date")
	private Date created_date;
	@Column(name="updated_by")
	private int updated_by;
	@Column(name="updated_date")
	private Date updated_date;
	public int getId_segment() {
		return id_segment;
	}
	public void setId_segment(int id_segment) {
		this.id_segment = id_segment;
	}
	public String getKode_segment() {
		return kode_segment;
	}
	public void setKode_segment(String kode_segment) {
		this.kode_segment = kode_segment;
	}
	public String getNama_segment() {
		return nama_segment;
	}
	public void setNama_segment(String nama_segment) {
		this.nama_segment = nama_segment;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
}
