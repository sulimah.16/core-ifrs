package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_mev_input")
public class InputMEV {
	@Id
	@Column(name="id_mev")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_mev;
	@Column(name="bulan")
	private int bulan;
	@Column(name="tahun")
	private int tahun;
	@Column(name="tanggal_mev")
	private Date tanggal_mev;
	@Column(name="gdp")
	private double gdp;
	@Column(name="lgdp")
	private double lgdp;
	@Column(name="dgdp_1")
	private double dgdp_1;
	@Column(name="dgdp_12")
	private double dgdp_12;
	@Column(name="lgdp_1")
	private double lgdp_1;
	@Column(name="lgdp_12")
	private double lgdp_12;
	@Column(name="gdpgr")
	private double gdpgr;
	@Column(name="kurs")
	private double kurs;
	@Column(name="lkurs")
	private double lkurs;
	@Column(name="dkurs")
	private double dkurs;
	@Column(name="dlkurs")
	private double dlkurs;
	@Column(name="i7rr")
	private double i7rr;
	@Column(name="di7rr")
	private double di7rr;
	@Column(name="inf")
	private double inf;
	@Column(name="dinf")
	private double dinf;
	@Column(name="govsp")
	private double govsp;
	@Column(name="lgovsp")
	private double lgovsp;
	@Column(name="dgovsp")
	private double dgovsp;
	@Column(name="dlgovsp")
	private double dlgovsp;
	@Column(name="govspgr")
	private double govspgr;
	@Column(name="housep")
	private double housep;
	@Column(name="lhousep")
	private double lhousep;
	@Column(name="dhousep")
	private double dhousep;
	@Column(name="dlhousep")
	private double dlhousep;
	
	@Column(name="dlgdp")
	private double dlgdp;
	@Column(name="dgdpgr")
	private double dgdpgr;
	@Column(name="oil")
	private double oil;
	@Column(name="loil")
	private double loil;
	@Column(name="doil")
	private double doil;
	@Column(name="dloil")
	private double dloil;
	
	public int getId_mev() {
		return id_mev;
	}
	public void setId_mev(int id_mev) {
		this.id_mev = id_mev;
	}
	public int getBulan() {
		return bulan;
	}
	public void setBulan(int bulan) {
		this.bulan = bulan;
	}
	public int getTahun() {
		return tahun;
	}
	public void setTahun(int tahun) {
		this.tahun = tahun;
	}
	public Date getTanggal_mev() {
		return tanggal_mev;
	}
	public void setTanggal_mev(Date tanggal_mev) {
		this.tanggal_mev = tanggal_mev;
	}
	public double getGdp() {
		return gdp;
	}
	public void setGdp(double gdp) {
		this.gdp = gdp;
	}
	public double getLgdp() {
		return lgdp;
	}
	public void setLgdp(double lgdp) {
		this.lgdp = lgdp;
	}
	public double getDgdp_1() {
		return dgdp_1;
	}
	public void setDgdp_1(double dgdp_1) {
		this.dgdp_1 = dgdp_1;
	}
	public double getDgdp_12() {
		return dgdp_12;
	}
	public void setDgdp_12(double dgdp_12) {
		this.dgdp_12 = dgdp_12;
	}
	public double getLgdp_1() {
		return lgdp_1;
	}
	public void setLgdp_1(double lgdp_1) {
		this.lgdp_1 = lgdp_1;
	}
	public double getLgdp_12() {
		return lgdp_12;
	}
	public void setLgdp_12(double lgdp_12) {
		this.lgdp_12 = lgdp_12;
	}
	public double getGdpgr() {
		return gdpgr;
	}
	public void setGdpgr(double gdpgr) {
		this.gdpgr = gdpgr;
	}
	public double getKurs() {
		return kurs;
	}
	public void setKurs(double kurs) {
		this.kurs = kurs;
	}
	public double getLkurs() {
		return lkurs;
	}
	public void setLkurs(double lkurs) {
		this.lkurs = lkurs;
	}
	public double getDkurs() {
		return dkurs;
	}
	public void setDkurs(double dkurs) {
		this.dkurs = dkurs;
	}
	public double getI7rr() {
		return i7rr;
	}
	public void setI7rr(double i7rr) {
		this.i7rr = i7rr;
	}
	public double getDi7rr() {
		return di7rr;
	}
	public void setDi7rr(double di7rr) {
		this.di7rr = di7rr;
	}
	public double getInf() {
		return inf;
	}
	public void setInf(double inf) {
		this.inf = inf;
	}
	public double getDinf() {
		return dinf;
	}
	public void setDinf(double dinf) {
		this.dinf = dinf;
	}
	public double getGovsp() {
		return govsp;
	}
	public void setGovsp(double govsp) {
		this.govsp = govsp;
	}
	public double getLgovsp() {
		return lgovsp;
	}
	public void setLgovsp(double lgovsp) {
		this.lgovsp = lgovsp;
	}
	public double getDgovsp() {
		return dgovsp;
	}
	public void setDgovsp(double dgovsp) {
		this.dgovsp = dgovsp;
	}
	public double getDlgovsp() {
		return dlgovsp;
	}
	public void setDlgovsp(double dlgovsp) {
		this.dlgovsp = dlgovsp;
	}
	public double getGovspgr() {
		return govspgr;
	}
	public void setGovspgr(double govspgr) {
		this.govspgr = govspgr;
	}
	public double getHousep() {
		return housep;
	}
	public void setHousep(double housep) {
		this.housep = housep;
	}
	public double getLhousep() {
		return lhousep;
	}
	public void setLhousep(double lhousep) {
		this.lhousep = lhousep;
	}
	public double getDhousep() {
		return dhousep;
	}
	public void setDhousep(double dhousep) {
		this.dhousep = dhousep;
	}
	public double getDlhousep() {
		return dlhousep;
	}
	public void setDlhousep(double dlhousep) {
		this.dlhousep = dlhousep;
	}
	public double getDlkurs() {
		return dlkurs;
	}
	public void setDlkurs(double dlkurs) {
		this.dlkurs = dlkurs;
	}
	public double getDlgdp() {
		return dlgdp;
	}
	public void setDlgdp(double dlgdp) {
		this.dlgdp = dlgdp;
	}
	public double getDgdpgr() {
		return dgdpgr;
	}
	public void setDgdpgr(double dgdpgr) {
		this.dgdpgr = dgdpgr;
	}
	public double getOil() {
		return oil;
	}
	public void setOil(double oil) {
		this.oil = oil;
	}
	public double getLoil() {
		return loil;
	}
	public void setLoil(double loil) {
		this.loil = loil;
	}
	public double getDoil() {
		return doil;
	}
	public void setDoil(double doil) {
		this.doil = doil;
	}
	public double getDloil() {
		return dloil;
	}
	public void setDloil(double dloil) {
		this.dloil = dloil;
	}
	
}
