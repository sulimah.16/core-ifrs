package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_aktivitas")
public class Aktivitas {
	
	@Id
	@Column(name="id_aktivitas")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_aktivitas;
	@Column(name="id_user")
	private int id_user;
	@Column(name="method_path")
	private String method_path;
	@Column(name="caption")
	private String caption;
	@Column(name="keterangan")
	private String keterangan;
	@Column(name="device")
	private String device;
	@Column(name="ip")
	private String ip;
	@Column(name="tanggal_aktivitas")
	private Date tanggal_aktivitas;
	public int getId_aktivitas() {
		return id_aktivitas;
	}
	public void setId_aktivitas(int id_aktivitas) {
		this.id_aktivitas = id_aktivitas;
	}
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public String getMethod_path() {
		return method_path;
	}
	public void setMethod_path(String method_path) {
		this.method_path = method_path;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Date getTanggal_aktivitas() {
		return tanggal_aktivitas;
	}
	public void setTanggal_aktivitas(Date tanggal_aktivitas) {
		this.tanggal_aktivitas = tanggal_aktivitas;
	}	
}
