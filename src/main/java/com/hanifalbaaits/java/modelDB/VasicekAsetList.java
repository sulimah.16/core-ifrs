package com.hanifalbaaits.java.modelDB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_vasicek_aset_list")
public class VasicekAsetList {
	
	@Id
	@Column(name="id_list_aset")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_list_aset;
	@Column(name="id_draft_vasicek")
	private int id_draft_vasicek;
	@Column(name="id_jenis")
	private int id_jenis;
	@Column(name="no_rekening")
	private String no_rekening;
	@Column(name="mata_uang")
	private String mata_uang;
	@Column(name="id_bank")
	private int id_bank;
	@Column(name="suku_bunga")
	private double suku_bunga;
	@Column(name="tgl_jatuh_tempo")
	private Date tgl_jatuh_tempo;
	@Column(name="saldo")
	private double saldo;
	@Column(name="created_by")
	private int created_by;
	@Column(name="created_date")
	private Date created_date;
	@Column(name="updated_by")
	private int updated_by;
	@Column(name="updated_date")
	private Date updated_date;
	public int getId_list_aset() {
		return id_list_aset;
	}
	public void setId_list_aset(int id_list_aset) {
		this.id_list_aset = id_list_aset;
	}
	public int getId_jenis() {
		return id_jenis;
	}
	public void setId_jenis(int id_jenis) {
		this.id_jenis = id_jenis;
	}
	public String getNo_rekening() {
		return no_rekening;
	}
	public void setNo_rekening(String no_rekening) {
		this.no_rekening = no_rekening;
	}
	public String getMata_uang() {
		return mata_uang;
	}
	public void setMata_uang(String mata_uang) {
		this.mata_uang = mata_uang;
	}
	public int getId_bank() {
		return id_bank;
	}
	public void setId_bank(int id_bank) {
		this.id_bank = id_bank;
	}
	public double getSuku_bunga() {
		return suku_bunga;
	}
	public void setSuku_bunga(double suku_bunga) {
		this.suku_bunga = suku_bunga;
	}
	public Date getTgl_jatuh_tempo() {
		return tgl_jatuh_tempo;
	}
	public void setTgl_jatuh_tempo(Date tgl_jatuh_tempo) {
		this.tgl_jatuh_tempo = tgl_jatuh_tempo;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public int getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(int updated_by) {
		this.updated_by = updated_by;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public int getId_draft_vasicek() {
		return id_draft_vasicek;
	}
	public void setId_draft_vasicek(int id_draft_vasicek) {
		this.id_draft_vasicek = id_draft_vasicek;
	}
}
