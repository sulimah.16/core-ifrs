package com.hanifalbaaits.java.modelDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_nilai_bucket")
public class NilaiBucket {
	
	@Id
	@Column(name="id_nilai_bucket")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_nilai_bucket;
	@Column(name="id_draft_bucket")
	private int id_draft_bucket;
	@Column(name="id_step")
	private int id_step;
	@Column(name="periode")
	private String periode;
	@Column(name="periode_bulan")
	private int periode_bulan;
	@Column(name="periode_tahun")
	private int periode_tahun;
	@Column(name="nilai")
	private String nilai;
	@Column(name="jumlah_outstanding")
	private double jumlah_outstanding;
	@Column(name="average")
	private double average;
	@Column(name="sum")
	private double sum;
	@Column(name="persentase_odr")
	private double persentase_odr;
	public int getId_nilai_bucket() {
		return id_nilai_bucket;
	}
	public void setId_nilai_bucket(int id_nilai_bucket) {
		this.id_nilai_bucket = id_nilai_bucket;
	}
	public int getId_draft_bucket() {
		return id_draft_bucket;
	}
	public void setId_draft_bucket(int id_draft_bucket) {
		this.id_draft_bucket = id_draft_bucket;
	}
	public int getId_step() {
		return id_step;
	}
	public void setId_step(int id_step) {
		this.id_step = id_step;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public int getPeriode_bulan() {
		return periode_bulan;
	}
	public void setPeriode_bulan(int periode_bulan) {
		this.periode_bulan = periode_bulan;
	}
	public int getPeriode_tahun() {
		return periode_tahun;
	}
	public void setPeriode_tahun(int periode_tahun) {
		this.periode_tahun = periode_tahun;
	}
	public String getNilai() {
		return nilai;
	}
	public void setNilai(String nilai) {
		this.nilai = nilai;
	}
	public double getJumlah_outstanding() {
		return jumlah_outstanding;
	}
	public void setJumlah_outstanding(double jumlah_outstanding) {
		this.jumlah_outstanding = jumlah_outstanding;
	}
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}
	public double getSum() {
		return sum;
	}
	public void setSum(double sum) {
		this.sum = sum;
	}
	public double getPersentase_odr() {
		return persentase_odr;
	}
	public void setPersentase_odr(double persentase_odr) {
		this.persentase_odr = persentase_odr;
	}
		
}
