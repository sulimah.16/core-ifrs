package com.hanifalbaaits.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.hanifalbaaits.java.properties.FileProperties;

@SpringBootApplication
@EnableConfigurationProperties({
	FileProperties.class
})

public class CoreIfrsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoreIfrsApplication.class, args);
	}

}
