package com.hanifalbaaits.java.modelUtils;

import java.util.List;

public class RequestVasicek {
	
	private String id_vasicek_draft;
	private String created_by;
	private String interval;
	private String start_date;
	private String end_date;
	private String persen_normal;
	private String persen_upturn;
	private String persen_downturn;
	private List<RequestAsetECL> list_aset_ecl;
	private String lgd;
	private String periode_ecl;
	private String save;
	public String getInterval() {
		return interval;
	}
	public void setInterval(String interval) {
		this.interval = interval;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getPersen_normal() {
		return persen_normal;
	}
	public void setPersen_normal(String persen_normal) {
		this.persen_normal = persen_normal;
	}
	public String getPersen_upturn() {
		return persen_upturn;
	}
	public void setPersen_upturn(String persen_upturn) {
		this.persen_upturn = persen_upturn;
	}
	public String getPersen_downturn() {
		return persen_downturn;
	}
	public void setPersen_downturn(String persen_downturn) {
		this.persen_downturn = persen_downturn;
	}
	public List<RequestAsetECL> getList_aset_ecl() {
		return list_aset_ecl;
	}
	public void setList_aset_ecl(List<RequestAsetECL> list_aset_ecl) {
		this.list_aset_ecl = list_aset_ecl;
	}
	public String getLgd() {
		return lgd;
	}
	public void setLgd(String lgd) {
		this.lgd = lgd;
	}
	public String getPeriode_ecl() {
		return periode_ecl;
	}
	public void setPeriode_ecl(String periode_ecl) {
		this.periode_ecl = periode_ecl;
	}
	public String getId_vasicek_draft() {
		return id_vasicek_draft;
	}
	public void setId_vasicek_draft(String id_vasicek_draft) {
		this.id_vasicek_draft = id_vasicek_draft;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getSave() {
		return save;
	}
	public void setSave(String save) {
		this.save = save;
	}
}
