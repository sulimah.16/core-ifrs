package com.hanifalbaaits.java.modelUtils;

import java.util.List;

import com.hanifalbaaits.java.modelDB.Draft;
import com.hanifalbaaits.java.modelDB.Segment;
import com.hanifalbaaits.java.modelDB.UploadFile;
import com.hanifalbaaits.java.modelDB.VasicekAset;
import com.hanifalbaaits.java.modelDB.VasicekBank;

public class KoreksiImport {
	private int id_draft;
	private int id_segment;
	private UploadFile file_upload;
	private int jumlah_bucket;
	private Draft draft;
	private Segment segment;
	private int id_jenis;
	private int id_bank;
	private VasicekAset jenis;
	private VasicekBank bank;
	private String title;
	private List<KoreksiPesan> list_pesan;
	private int count_import;
	private int count_success;
	private int count_failed;
	private String tgl_import;
	public int getCount_import() {
		return count_import;
	}
	public void setCount_import(int count_import) {
		this.count_import = count_import;
	}
	public int getCount_success() {
		return count_success;
	}
	public void setCount_success(int count_success) {
		this.count_success = count_success;
	}
	public int getCount_failed() {
		return count_failed;
	}
	public void setCount_failed(int count_failed) {
		this.count_failed = count_failed;
	}
	public String getTgl_import() {
		return tgl_import;
	}
	public void setTgl_import(String tgl_import) {
		this.tgl_import = tgl_import;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<KoreksiPesan> getList_pesan() {
		return list_pesan;
	}
	public void setList_pesan(List<KoreksiPesan> list_pesan) {
		this.list_pesan = list_pesan;
	}
	public int getId_draft() {
		return id_draft;
	}
	public void setId_draft(int id_draft) {
		this.id_draft = id_draft;
	}
	public int getId_segment() {
		return id_segment;
	}
	public void setId_segment(int id_segment) {
		this.id_segment = id_segment;
	}
	public Draft getDraft() {
		return draft;
	}
	public void setDraft(Draft draft) {
		this.draft = draft;
	}
	public Segment getSegment() {
		return segment;
	}
	public void setSegment(Segment segment) {
		this.segment = segment;
	}
	public int getJumlah_bucket() {
		return jumlah_bucket;
	}
	public void setJumlah_bucket(int jumlah_bucket) {
		this.jumlah_bucket = jumlah_bucket;
	}
	public UploadFile getFile_upload() {
		return file_upload;
	}
	public void setFile_upload(UploadFile file_upload) {
		this.file_upload = file_upload;
	}
	public int getId_jenis() {
		return id_jenis;
	}
	public void setId_jenis(int id_jenis) {
		this.id_jenis = id_jenis;
	}
	public int getId_bank() {
		return id_bank;
	}
	public void setId_bank(int id_bank) {
		this.id_bank = id_bank;
	}
	public VasicekAset getJenis() {
		return jenis;
	}
	public void setJenis(VasicekAset jenis) {
		this.jenis = jenis;
	}
	public VasicekBank getBank() {
		return bank;
	}
	public void setBank(VasicekBank bank) {
		this.bank = bank;
	}
}
