package com.hanifalbaaits.java.modelUtils;

public class RequestAsetECL {
	private String id_aset;
	private String id_bank;
	private String no_rekening;
	private String mata_uang;
	private String suku_bunga;
	private String tgl_jatuh_tempo;
	private String saldo;
	public String getId_aset() {
		return id_aset;
	}
	public void setId_aset(String id_aset) {
		this.id_aset = id_aset;
	}
	public String getId_bank() {
		return id_bank;
	}
	public void setId_bank(String id_bank) {
		this.id_bank = id_bank;
	}
	public String getNo_rekening() {
		return no_rekening;
	}
	public void setNo_rekening(String no_rekening) {
		this.no_rekening = no_rekening;
	}
	public String getMata_uang() {
		return mata_uang;
	}
	public void setMata_uang(String mata_uang) {
		this.mata_uang = mata_uang;
	}
	public String getSuku_bunga() {
		return suku_bunga;
	}
	public void setSuku_bunga(String suku_bunga) {
		this.suku_bunga = suku_bunga;
	}
	public String getTgl_jatuh_tempo() {
		return tgl_jatuh_tempo;
	}
	public void setTgl_jatuh_tempo(String tgl_jatuh_tempo) {
		this.tgl_jatuh_tempo = tgl_jatuh_tempo;
	}
	public String getSaldo() {
		return saldo;
	}
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
}
