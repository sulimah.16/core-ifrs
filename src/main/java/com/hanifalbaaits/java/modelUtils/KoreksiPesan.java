package com.hanifalbaaits.java.modelUtils;

public class KoreksiPesan {
	private String nomor_input;
	private String status;
	private String pesan;
	
	public KoreksiPesan (String nomor_input,String status, String pesan) {
		this.nomor_input = nomor_input;
		this.status = status;
		this.pesan = pesan;
	}
	
	public String getNomor_input() {
		return nomor_input;
	}
	public void setNomor_input(String nomor_input) {
		this.nomor_input = nomor_input;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPesan() {
		return pesan;
	}
	public void setPesan(String pesan) {
		this.pesan = pesan;
	}
}
