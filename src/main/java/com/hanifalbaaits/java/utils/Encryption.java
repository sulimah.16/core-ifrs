package com.hanifalbaaits.java.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

//import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class Encryption {
	
    public String decodeValue(String value) {
		try {
			return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException ex) {
			// TODO: handle exception
			throw new RuntimeException(ex.getCause());
		}
	}
	
	public String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }
	
	//INI SCRIPTNYA HANIF
	public String getMD5EncryptedValue(String password) {
		 final byte[] defaultBytes = password.getBytes();
		 try {
	            final MessageDigest md5MsgDigest = MessageDigest.getInstance("MD5");
	            md5MsgDigest.reset();
	            md5MsgDigest.update(defaultBytes);
	            final byte messageDigest[] = md5MsgDigest.digest();
	            final StringBuffer hexString = new StringBuffer();
	            for (final byte element : messageDigest) {
	                final String hex = Integer.toHexString(0xFF & element);
	                if (hex.length() == 1) {
	                    hexString.append('0');
	                }
	                hexString.append(hex);
	            }
	            password = hexString + "";
	        } catch (final NoSuchAlgorithmException nsae) {
	            nsae.printStackTrace();
	        }
	        return password;
	}
	
	public String getSHA256EncryptedValue(String password) {
		 try {
	            final MessageDigest md = MessageDigest.getInstance("SHA-256");
	            byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
	            
	            // bytes to hex
	            StringBuilder sb = new StringBuilder();
	            for (byte b : hashInBytes) {
	                sb.append(String.format("%02x", b));
	            }
	            password = sb.toString();
	        } catch (final NoSuchAlgorithmException nsae) {
	            nsae.printStackTrace();
	        }
		 	System.out.println("pass:"+password);
	        return password;
	}
	
	public String decodeBase64(String hash) {
		byte[] byteHash = Base64.getDecoder().decode(hash);
		return new String(byteHash);
	}
	
	public String encodeBase64(String text) {
		String encodedString = Base64.getEncoder().encodeToString(text.getBytes());
		return encodedString;
	}
	
//	@Bean
//	public PasswordEncoder passwordEncoder() {
//	    return new BCryptPasswordEncoder();
//	}
//	
//	public String hashBcrypt(String text) {
//		return passwordEncoder().encode(text);
//	}
}
