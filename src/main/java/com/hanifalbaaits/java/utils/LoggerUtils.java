package com.hanifalbaaits.java.utils;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class LoggerUtils {

	private Logger logger = LoggerFactory.getLogger(LoggerUtils.class);
	
	public void printLog(String uniq, String method,String url, String message) {
		logger.info("["+uniq+"] ["+method+"] ["+url+"] "+message);
	}
	
	public String printRequest(String uniq, String method, String url, HttpServletRequest request) {
		String ip_forward = request.getHeader("X-FORWARDED-FOR");
		String urlRequest = request.getHeader("HTTP-X-FORWARDED-HOST");
		String https = request.getHeader("HTTPS");
		if (https != null && "on".equals(https)) {
			https = "https://";
        } else {
        	https = "http://";
        }
		
		printLog(uniq,method,url,"Remote-address: "+request.getRemoteAddr());
		printLog(uniq,method,url,"Remote-host: "+request.getRemoteHost());
		printLog(uniq,method,url,"Request-url: "+request.getRequestURL());
		printLog(uniq,method,url,"Server-name: "+request.getServerName());
		printLog(uniq,method,url,"Server-port: "+request.getServerPort());
		printLog(uniq,method,url,"IP-forwarde: "+ip_forward);
		printLog(uniq,method,url,"Request-url: "+urlRequest);
		printLog(uniq,method,url,"http/https : "+https);
		return https+urlRequest;
	}
	
}
